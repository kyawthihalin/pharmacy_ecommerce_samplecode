<style>
    .sidebar .nav-dropdown-toggle,
    .nav-item .nav-link {
        display: flex;
        align-items: center;
    }
</style>
<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-title">System Section</li>
@if (backpack_user()->can('dashboard'))
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i>
            {{ trans('backpack::base.dashboard') }}</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('survey-notification') }}'><i
                class='nav-icon la la-laugh-beam'></i> Survey Dashboard</a></li>
@endif
{{-- <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
--}}
@if (backpack_user()->can('list city'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('city') }}'><i class='nav-icon las la-city'></i>
            Cities</a></li>
@endif
@if (backpack_user()->can('list township'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('township') }}'><i
                class='nav-icon las la-building'></i> Townships</a></li>
@endif
@if (backpack_user()->can('list banner'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('banner') }}'><i
                class='nav-icon las la-file-image'></i> Banners</a></li>
@endif

@if (backpack_user()->can('list logo'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('default-photo') }}'><i
                class='nav-icon las la-file-image'></i> Default Shop Logo</a></li>
@endif
@if (backpack_user()->can('list enquiry'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('enquiry') }}'><i
                class='nav-icon las la-comment'></i> Enquiries</a></li>
@endif
@if (backpack_user()->can('list return medicine'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('return-medicine') }}'><i
                class='nav-icon las la-pills'></i> Return medicines</a></li>
@endif

@if (backpack_user()->can('list notification'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-comments"></i>Notifications</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('notification') }}'><i
                        class='nav-icon las la-comment'></i> Notifications</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('app-notification') }}'><i
                        class='nav-icon las la-comment'></i> App notifications</a></li>
        </ul>
    </li>
@endif
<!-- Reports -->
{{-- @if (backpack_user()->can('list stock') || backpack_user()->can('list stock in') || backpack_user()->can('list stock out'))
    @php
        $badge_class = '';
        $warning = getWarningProduct();
        if ($warning > 0) {
            $badge_class = 'badge badge-danger ml-2';
        } else {
            $badge_class = 'badge badge-success ml-2';
        }
    @endphp

    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-archive"></i>Inventory<span
                class="{{ $badge_class }}">{{ $warning }}</span></a>
        <ul class="nav-dropdown-items">
            @if (backpack_user()->can('list stock'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stockout') }}'><i
                            class='nav-icon las la-tasks'></i>Stock List <span
                            class="{{ $badge_class }}">{{ $warning }}</span> </a></li>
            @endif
            @if (backpack_user()->can('list stock in'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stockin-log') }}'><i
                            class='nav-icon las la-cart-arrow-down'></i> Stock In</a></li>
            @endif
            @if (backpack_user()->can('list stock out'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stockout-log') }}'><i
                            class='nav-icon las la-share-square'></i> Stock Out</a></li>
            @endif
        </ul>
    </li>
@endif --}}

<!-- Users, Roles, Permissions -->
@if (backpack_user()->can('list user') || backpack_user()->can('list role') || backpack_user()->can('list shop'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i>
            Authentication</a>
        <ul class="nav-dropdown-items">
            @if (backpack_user()->can('list user'))
                <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i
                            class="nav-icon la la-user"></i> <span>Users</span></a></li>
            @endif
            @if (backpack_user()->can('list role'))
                <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i
                            class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
            @endif
            @if (backpack_user()->can('list shop'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shop-not-approve') }}'><i
                            class='nav-icon las la-store-alt'></i> Shops (Not yet approved)</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shop') }}'><i
                            class='nav-icon las la-store-alt'></i> Shops</a></li>
            @endif
            {{-- <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a>
</li> --}}
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('edit-account-info') }}"><i
                        class="nav-icon la la-id-badge"></i> <span>Account Info</span></a></li>
        </ul>
    </li>
@else
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i>
            Authentication</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('edit-account-info') }}"><i
                        class="nav-icon la la-id-badge"></i> <span>Account Info</span></a></li>
        </ul>
    </li>
@endif
@if (backpack_user()->can('list tax'))
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tax') }}'><i
        class="nav-icon las la-hand-holding-usd" style="margin-right: 8px"></i>Tax</a></li>
@endif

<!-- Products Section -->

<li class="nav-title">Product Section</li>
{{-- @if (backpack_user()->can('list category') ||
        backpack_user()->can('list brand') ||
        backpack_user()->can('list unit') ||
        backpack_user()->can('list product'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-shopping-bag"></i> Manage
            Products</a> --}}
        {{-- <ul class="nav-dropdown-items"> --}}
            @if (backpack_user()->can('list product'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i
                            class='nav-icon las la-shopping-bag'></i> Products</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('group-product') }}'><i
                            class='nav-icon las la-object-group'></i> Group Products</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('group-product-balance') }}'><i
                    class='nav-icon la la-balance-scale'></i> Group Products Balance</a></li>
            @endif
            @if (backpack_user()->can('list category'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i
                            class='nav-icon las la-list-ol'></i> Categories</a></li>
            @endif
            @if (backpack_user()->can('list brand'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('brand') }}'><i
                            class='nav-icon la la-font-awesome'></i> Brands</a></li>
            @endif
            @if (backpack_user()->can('list unit'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('unit') }}'><i
                            class='nav-icon las la-cube'></i> Units</a></li>
            @endif
            @if (backpack_user()->can('list unit relationship'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('unit-relationship') }}'><i
                            class='nav-icon las la-cubes'></i> Unit Relationships</a></li>
            @endif
        {{-- </ul> --}}
    {{-- </li>
@endif --}}

<!-- Sale Section -->

<li class="nav-title">Sale Section</li>
@if (backpack_user()->can('list order') || backpack_user()->can('create print voucher'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-chart-bar"></i>Orders<span
                class="badge badge-success">{{ getTodayOrder() }}</span></a>
        <ul class="nav-dropdown-items">
            @if (backpack_user()->can('list order'))
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'><i
                            class='nav-icon las la-truck'></i>All Orders</a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment-pending-order') }}'><i
                            class='nav-icon las la-truck text-warning'></i><span class="text-warning"> Payment
                            Pending</span></a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shipping-pending-order') }}'><i
                            class='nav-icon las la-truck text-info'></i><span class="text-info"> Delivering Order
                        </span></a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('completed-order') }}'><i
                            class='nav-icon las la-truck text-success'></i> <span class="text-success"> Completed
                            Orders</span></a></li>
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('cash-on-delivery-order') }}'><i class="las la-people-carry" style="font-size: 23px;margin-right:3px;color:#813103"></i><span style="color:#813103">Cash On Deli Orders</span></a></li>

                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('rejected-order') }}'><i
                            class='nav-icon las la-trash text-danger'></i><span class="text-danger">Rejected
                            Orders</span></a></li>
            @endif
        </ul>
    </li>
@endif
@if (backpack_user()->can('list coupon'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('coupon') }}'><i
                class='nav-icon las la-tag'></i>
            Coupons</a></li>
@endif

<!-- Purchase Section -->

<li class="nav-title">Purchase Section</li>
@if (backpack_user()->can('list supplier'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('supplier') }}'><i
                class='nav-icon la la-users'></i>
            Suppliers</a></li>
@endif
@if (backpack_user()->can('list purchase order'))
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-chart-bar"></i>Purchase
            Orders</a>
        <ul class="nav-dropdown-items">
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('purchase-order') }}'><i
                        class="nav-icon las la-shopping-cart"></i> All Orders</a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pending-purchase-order') }}'><i
                        class="nav-icon las la-shopping-cart text-warning"></i><span class="text-warning"> Pending
                        Orders
                    </span></a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('complete-purchase-order') }}'><i
                        class="nav-icon las la-shopping-cart text-success"></i><span class="text-success"> Complete
                        Orders
                    </span></a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('credit-complete-purchase-order') }}'><i
                        class="nav-icon las la-shopping-cart text-info"></i><span class="text-info"> Credit Orders
                    </span></a></li>
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('rejected-purchase-order') }}'><i
                        class="nav-icon las la-shopping-cart text-danger"></i><span class="text-danger"> Rejected
                        Orders
                    </span></a></li>
        </ul>
    </li>
@endif


<li class="nav-title">Report Section</li>
@if (backpack_user()->can('list stock report'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stock-daily-report/create') }}'><i
                class='nav-icon las la-calendar-day' style="margin-right:5px"></i>   Daily Report</a></li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stock-report/create') }}'><i
                class='nav-icon las la-code-branch' style="margin-right:5px"></i>   Report By Type</a></li>
@endif
@if (backpack_user()->can('list product balance report'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product-balance-report/create') }}'><i
                class='nav-icon las la-barcode' style="margin-right:5px"></i>   Report By SKU-Code</a></li>
    <li class='nav-item'><a class='nav-link'
            href='{{ backpack_url('product-balance-by-invoice-report/create') }}'><i
                class='nav-icon las la-receipt' style="margin-right:5px"></i>   Report By Invoice</a></li>
@endif
@if (backpack_user()->can('list purchase order report'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('purchase-order-report/create') }}'><i
                class='nav-icon las la-shopping-cart' style="margin-right:5px"></i> Purchase Order Report</a></li>
@endif
@if (backpack_user()->can('list sale report'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('sale-report/create') }}'><i
                class='nav-icon la la-balance-scale'></i> Sale Reports</a></li>
@endif
@if (backpack_user()->can('create product report'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product-report/create') }}'><i
                class='nav-icon las la-capsules' style="margin-right:5px"></i>   Product Report</a></li>
@endif
@if (backpack_user()->can('create order report'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('order-report/create') }}'><i
                class='nav-icon las la-truck'></i> Order Report</a></li>
@endif
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('order-print/create') }}'><i
                class='las la-clipboard-list' style="margin-right:5px"></i>   Print Voucher</a></li> --}}

<!-- Finance Section -->

<li class="nav-title">Finance Section</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-money"></i> Finance</a>
    <ul class="nav-dropdown-items">
        {{-- @if (backpack_user()->can('list gl account'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('gl-account') }}'><i
                        class="nav-icon la la-user"></i> Gl accounts</a></li>
        @endif --}}
        @if (backpack_user()->can('list finance cash account'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('finance-cash-account') }}'><i
                        class="nav-icon la la-credit-card"></i> Finance cash accounts</a></li>
        @endif
        @if (backpack_user()->can('list finance particular'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('finance-particular') }}'><i
                        class='nav-icon la la-credit-card-alt'></i> Finance particulars</a></li>
        @endif
        @if (backpack_user()->can('list fill amount'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('fill-amount') }}'><i
                        class='nav-icon la la-money'></i> Fill amounts</a></li>
        @endif
        @if (backpack_user()->can('list finance cash report'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('finance-cash-report') }}'><i
                        class='nav-icon la la-print'></i> Finance cash report</a></li>
        @endif
    </ul>
</li>
@if (backpack_user()->can('list payment type'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment-type') }}'><i
                class='nav-icon lab la-cc-visa'></i> Payment Types</a></li>
@endif

<!-- Inventory Section -->

<li class="nav-title">Inventory Section</li>
@if (backpack_user()->can('create stock opening'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stock-opening/import-view') }}'><i
                class='nav-icon la la-th-large'></i> Stock Opening</a></li>
@endif
@if (backpack_user()->can('list product balance'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product-balance') }}'><i
                class='nav-icon la la-list-ol'></i> Stock List</a></li>
@endif
@if (backpack_user()->can('create product balance'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product-balance/create') }}'><i
                class='nav-icon la la-balance-scale'></i> Stock Unit Balance</a></li>
@endif
@if (backpack_user()->can('list stock adjustment'))
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stock-adjustment') }}'><i
                class='nav-icon la la-sliders'></i> Stock Adjustments</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('adjustment-type') }}'><i
                class='nav-icon la la-window-restore'></i> Adjustment Types</a></li>
@endif

<!-- Report Section -->

@if (backpack_user()->can('list profit report'))
    <li class="nav-title">Profit/Loss Section</li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('profit-report/create') }}'><i
                class='nav-icon las la-flag'></i> Profit/Loss</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('profit-comparison-report/create') }}'><i
                class='nav-icon las la-clipboard-list' style="margin-right:5px"></i>   Comparison</a></li>
@endif


