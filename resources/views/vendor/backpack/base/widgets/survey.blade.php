<?php

use App\Models\Notification;

$smile_count = Notification::where('type', 'Survey')->where('answer', 'Good')->get()->count();
$cry_count = Notification::where('type', 'Survey')->where('answer', 'Bad')->get()->count();
$total_count = Notification::where('type', 'Survey')->get()->count();
?>
<style>
    .card {
        background-color: #f5f7fa;
        border-radius: 10px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        min-height: 120px;
        max-height: 120px;
        /* padding: 20px;
        width: 300px; */
    }

    .card-icon {
        font-size: 30px;
        margin-right: 10px;
    }

    .good-count {
        color: #2ecc71;
    }

    .bad-count {
        color: #e74c3c;
    }
</style>
<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title font-weight-bold text-info">Total Survey</h5>
                <div class="d-flex align-items-center mt-3">
                    <i class="las la-clipboard-check card-icon text-info"></i>
                    <span class="text-info">{{$total_count}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title font-weight-bold text-success">Smily Survey</h5>
                <div class="d-flex align-items-center">
                    <i class="la la-laugh-beam card-icon good-count"></i>
                    <span class="good-count">{{$smile_count}}</span>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title font-weight-bold text-danger">Bad Results</h5>
                <div class="d-flex align-items-center mt-3">
                    <i class="las la-sad-tear card-icon bad-count"></i>
                    <span class="bad-count">{{$cry_count}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title font-weight-bold text-warning">No Answer</h5>
                <div class="d-flex align-items-center mt-3">
                    <i class="las la-frown card-icon text-warning"></i>
                    <span class="text-warning">{{$total_count - ($smile_count + $cry_count)}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
