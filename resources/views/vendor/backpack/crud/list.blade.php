@extends(backpack_view('blank'))


@php
    $defaultBreadcrumbs = [
        trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
        $crud->entity_name_plural => url($crud->route),
        trans('backpack::crud.list') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
    <div class="container-fluid">
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small id="datatable_info_stack">{!! $crud->getSubheading() ?? '' !!}</small>
        </h2>
    </div>
@endsection

@section('content')
    <!-- Default box -->
    <div class="row">

        <!-- THE ACTUAL CONTENT -->
        <div class="{{ $crud->getListContentClass() }}">

            <div class="row mb-0">
                <div class="col-sm-6">
                    @if ($crud->buttons()->where('stack', 'top')->count() || $crud->exportButtons())
                        <div class="d-print-none {{ $crud->hasAccess('create') ? 'with-border' : '' }}">

                            @include('crud::inc.button_stack', ['stack' => 'top'])

                        </div>
                    @endif
                </div>
                <div class="col-sm-6">
                    <div id="datatable_search_stack" class="mt-sm-0 mt-2 d-print-none"></div>
                </div>
            </div>

            {{-- Backpack List Filters --}}
            @if ($crud->filtersEnabled())
                @include('crud::inc.filters_navbar')
            @endif

            <table id="crudTable" class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2"
                cellspacing="0">
                <thead>
                    <tr>
                        {{-- Table columns --}}
                        @foreach ($crud->columns() as $column)
                            <th data-orderable="{{ var_export($column['orderable'], true) }}"
                                data-priority="{{ $column['priority'] }}" {{--

                        data-visible-in-table => if developer forced field in table with 'visibleInTable => true'
                        data-visible => regular visibility of the field
                        data-can-be-visible-in-table => prevents the column to be loaded into the table (export-only)
                        data-visible-in-modal => if column apears on responsive modal
                        data-visible-in-export => if this field is exportable
                        data-force-export => force export even if field are hidden

                    --}} {{-- If it is an export field only, we are done. --}}
                                @if (isset($column['exportOnlyField']) && $column['exportOnlyField'] === true) data-visible="false"
                      data-visible-in-table="false"
                      data-can-be-visible-in-table="false"
                      data-visible-in-modal="false"
                      data-visible-in-export="true"
                      data-force-export="true"
                    @else
                      data-visible-in-table="{{ var_export($column['visibleInTable'] ?? false) }}"
                      data-visible="{{ var_export($column['visibleInTable'] ?? true) }}"
                      data-can-be-visible-in-table="true"
                      data-visible-in-modal="{{ var_export($column['visibleInModal'] ?? true) }}"
                      @if (isset($column['visibleInExport']))
                         @if ($column['visibleInExport'] === false)
                           data-visible-in-export="false"
                           data-force-export="false"
                         @else
                           data-visible-in-export="true"
                           data-force-export="true" @endif
                            @else data-visible-in-export="true" data-force-export="false" @endif
                        @endif
                        >
                        {!! $column['label'] !!}
                        </th>
                        @endforeach

                        @if ($crud->buttons()->where('stack', 'line')->count())
                            <th data-orderable="false" data-priority="{{ $crud->getActionsColumnPriority() }}"
                                data-visible-in-export="false">{{ trans('backpack::crud.actions') }}</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        {{-- Table columns --}}
                        @foreach ($crud->columns() as $column)
                            <th>{!! $column['label'] !!}</th>
                        @endforeach

                        @if ($crud->buttons()->where('stack', 'line')->count())
                            <th>{{ trans('backpack::crud.actions') }}</th>
                        @endif
                    </tr>
                </tfoot>
            </table>

            @if ($crud->buttons()->where('stack', 'bottom')->count())
                <div id="bottom_buttons" class="d-print-none text-center text-sm-left">
                    @include('crud::inc.button_stack', ['stack' => 'bottom'])

                    <div id="datatable_button_stack" class="float-right text-right hidden-xs"></div>
                </div>
            @endif

        </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
        crossorigin="anonymous"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.shopApprove', function(event) {
            let shopId = $(this).attr('value');

            swal({
                    title: "Approve?",
                    text: "Are you sure, You want to approve?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('shop.approve')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                id: shopId
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Approved!',
                                    text: 'Successfully Approved!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    }
                });
        });

        $(document).on('click', '.to-complete-purchase-from-credit', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');

                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.credit.to.complete-status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Purchase Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        );

        $(document).on('click', '.change-complete-for-purchase', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');

                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.complete.for.purchase')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Purchase Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        );

        $(document).on('click', '.change-credit-for-purchase', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');

                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.credit.for.purchase')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Purchase Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        );
    </script>
@endsection

@section('after_styles')
@php
    $routeName = Route::currentRouteName();
@endphp
@if ($routeName != 'purchase-order.index')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endif

    <!-- DATA TABLES -->
    <link rel="stylesheet" type="text/css"
        href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

    <link rel="stylesheet"
        href="{{ asset('packages/backpack/crud/css/crud.css') . '?v=' . config('backpack.base.cachebusting_string') }}">
    <link rel="stylesheet"
        href="{{ asset('packages/backpack/crud/css/form.css') . '?v=' . config('backpack.base.cachebusting_string') }}">
    <link rel="stylesheet"
        href="{{ asset('packages/backpack/crud/css/list.css') . '?v=' . config('backpack.base.cachebusting_string') }}">

    <!-- CRUD LIST CONTENT - crud_list_styles stack -->
    @stack('crud_list_styles')

    <style>

    </style>
@endsection

@section('after_scripts')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    @include('crud::inc.datatables_logic')
    <script src="{{ asset('packages/backpack/crud/js/crud.js') . '?v=' . config('backpack.base.cachebusting_string') }}">
    </script>
    <script src="{{ asset('packages/backpack/crud/js/form.js') . '?v=' . config('backpack.base.cachebusting_string') }}">
    </script>
    <script src="{{ asset('packages/backpack/crud/js/list.js') . '?v=' . config('backpack.base.cachebusting_string') }}">
    </script>

    <!-- CRUD LIST CONTENT - crud_list_scripts stack -->
    @stack('crud_list_scripts')
@endsection
