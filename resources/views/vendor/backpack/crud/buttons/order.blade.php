@if($entry->shipping_status === 'Pending' || $entry->shipping_status === 'Processing' || $entry->shipping_status === 'Confirmed')
<button title="Complete" class="btn btn-sm btn-success rounded-circle" onclick="redir('order/{{$entry->id}}/edit')"><span class="las la-edit"></span></button>
@endif
<script>
  function redir(url)
  {
    window.location.href= url;
  }
</script>