<a href={{ route('product.import.view') }}>
    <button type='button' class="btn btn-xs btn-info open-modal-button"
        style="padding:0.375rem 0.75rem; line-height:1.5; font-size:1rem;">
        <i class="las la-file-import"></i>
        Import Excel
    </button>
</a>
