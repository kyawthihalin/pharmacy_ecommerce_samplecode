@php
    use Carbon\Carbon;
@endphp

<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        /* padding: 20px; */
        /* -webkit-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        -moz-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48); */
        border-radius: 5px;
        /* overflow: scroll; */
    }

    .stock-table {
        width: 100%;
    }

    .stock-table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
        min-width: 75px;
    }

    .stock-table tbody tr.data {
        text-align: center;
    }

    /* .stock-table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    } */

    .stock-table tbody tr.odd {
        background-color: var(--white);
    }

    .stock-table tbody tr.even {
        background-color: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        /* margin: 0 0 0 15px; */
        background: #1b252b;
        color: var(--white);
        /* text-align: center; */
    }

    td.increase {
        color: green;
    }

    td.increase-stock:before {
        content: "";
        display: inline-block;
        float: right;
        border: 0.3em solid transparent;
        border-bottom: 0.4em solid green;
        position: relative;
        right: 4.5em;
        top: 0.2em;
    }

    td.decrease-stock:before {
        content: "";
        display: inline-block;
        float: right;
        border: 0.3em solid transparent;
        border-top: 0.4em solid red;
        position: relative;
        right: 4.5em;
        top: 0.5em;
    }

    td.decrease {
        color: red;
    }
</style>

@if (isset($product_balances) && count($product_balances) > 0)
    <div class="col-md-12 p-0 table-report">
        <table class="stock-table table-bordered">
            <thead>
                <th>No</th>
                <th>Invoice No</th>
                <th>Product ID</th>
                <th>Name</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Purchase Price</th>
                <th>Purchase Period</th>
            </thead>
            <tbody>
                @php
                    $count = 0;
                    $color = 0;
                @endphp
                @if (count($product_balances) > 0)
                    @foreach ($product_balances as $key => $product_balance)
                        @php
                            if ($product_balance->is_span == 1) {
                                ++$color;
                            }
                        @endphp
                        <tr class="data {{ $color % 2 === 0 ? 'even' : 'odd' }}">
                            @if ($product_balance->is_span === 1)
                                <td rowspan="{{ $product_balance->rowspan }}">{{ ++$count }}</td>
                                <td rowspan="{{ $product_balance->rowspan }}">
                                    {{ $product_balance->purchaseOrder->invoice_no }}</td>
                            @endif
                            <td>{{ $product_balance->product->product_id }}</td>
                            <td>{{ $product_balance->product->name }}</td>
                            <td>{{ $product_balance->product->unit->name }}</td>
                            <td>{{ $product_balance->quantity }}</td>
                            <td>{{ intval($product_balance->price) }}</td>
                            @if ($product_balance->is_span === 1)
                                <td rowspan="{{ $product_balance->rowspan }}">
                                    {{ Carbon::parse($product_balance->created_at)->format('Y-m-d') }}</td>
                            @endif
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

        <table class="stock-table table-bordered" id="excelreport" style="display: none">
            <thead>
                <th>No</th>
                <th>Invoice No</th>
                <th>Product ID</th>
                <th>Name</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Purchase Price</th>
                <th>Purchase Date</th>
            </thead>
            <tbody>
                @php
                    $count = 0;
                    $color = 0;
                @endphp
                @if (count($product_balances) > 0)
                    @foreach ($product_balances as $key => $product_balance)
                        <tr class="data {{ $color % 2 === 0 ? 'even' : 'odd' }}">
                            <td>{{ ++$count }}</td>
                            <td>{{ $product_balance->purchaseOrder->invoice_no }}</td>
                            <td>{{ $product_balance->product->product_id }}</td>
                            <td>{{ $product_balance->product->name }}</td>
                            <td>{{ $product_balance->product->unit->name }}</td>
                            <td>{{ $product_balance->quantity }}</td>
                            <td>{{ intval($product_balance->price) }}</td>
                            <td>{{ Carbon::parse($product_balance->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endif

<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script>
    //
    var product_balances = @json($product_balances);
    if (product_balances.length == 0) {
        $('#excel_download').hide();
        $('.print-data').hide();
    }

    $("#excel_download").on('click', function(event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude: "#DivIdToPrint",
            name: "Worksheet Name",
            filename: "product_balance_by_invoice", //do not include extension
            fileext: ".csv," // file extension
        });
    });

    $('.print-data').on('click', function() {
        printDiv();
    });

    function printDiv() {

        var divToPrint = document.getElementById('report-body');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<head>' +
            '<style>\n' +
            '    th\n' +
            '    {\n' +
            '        color:black !important;\n' +
            '    }\n' +
            '\n' +
            '    table, td, th \n' +
            '    {\n' +
            '        border: 1px solid #ddd;\n' +
            '    }\n' +
            '\n' +
            '    table \n' +
            '    {\n' +
            '        width: 100%;\n' +
            '        border-collapse: collapse;\n' +
            '    }\n' +
            '</style>' +
            '</head>' +
            '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
            '</html>');
        newWin.document.close();

        setTimeout(function() {
            newWin.close();
        }, 10);

    }
</script>
