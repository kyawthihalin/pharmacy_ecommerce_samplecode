<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        /* padding: 20px; */
        /* -webkit-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        -moz-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48); */
        border-radius: 5px;
        margin-bottom: 20px;
        /* overflow: scroll; */
    }

    .stock-table {
        width: 100%;
    }

    .stock-table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
        min-width: 75px;
    }

    .stock-table tbody tr.data {
        text-align: center;
    }

    .stock-table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        /* margin: 0 0 0 15px; */
        background: #1b252b;
        color: var(--white);
        /* text-align: center; */
    }

    td.increase {
        color: green;
    }

    td.increase-stock, td.decrease-stock {
        display: flex;
        justify-content: center;
    }

    td.increase-stock:after {
        content: "";
        display: inline-block;
        float: right;
        border: 0.3em solid transparent;
        border-bottom: 0.4em solid green;
        position: relative;
        /* right: 4.3em; */
        bottom: 0.6em;
        margin-left: 8px;
    }

    td.decrease-stock:after {
        content: "";
        display: inline-block;
        float: right;
        border: 0.3em solid transparent;
        border-top: 0.4em solid red;
        position: relative;
        /* right: 4.3em; */
        top: 0.5em;
        margin-left: 8px;
    }

    td.decrease {
        color: red;
    }
</style>

@if (isset($stock_logs) && count($stock_logs) > 0)
    <div class="col-md-12 p-0 table-report">
        <table class="stock-table"  id="excelreport">
            <thead>
                <th>No</th>
                <th>SKU Code</th>
                <th>Name</th>
                <th>Unit</th>
                <th>Stock In/Out</th>
                <th>Current Stock</th>
                <th>Date</th>
            </thead>
            <tbody>
                @php
                    $count = 1;
                @endphp
                @if (count($sales) > 0)
                    <tr class="title">
                        <td colspan="7">
                            <h3>Sales</h3>
                        </td>
                    </tr>
                    @foreach ($sales as $key => $sale)
                        <tr class="data">
                            <td>{{ $count++ }}</td>
                            <td>{{ $sale->product->skuCode ? $sale->product->skuCode->code : '-' }}</td>
                            <td>{{ $sale->product->name }}</td>
                            <td>{{ $sale->product->unit->name }}</td>
                            <td class="decrease decrease-stock">{{ $sale->quantity }}</td>
                            <td class="decrease">{{ $sale->current_qty }}</td>
                            <td>{{ \Carbon\Carbon::parse($sale->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
                @if (count($purchases) > 0)
                    <tr class="title">
                        <td colspan="7">
                            <h3>Purchase</h3>
                        </td>
                    </tr>
                    @foreach ($purchases as $key => $purchase)
                        <tr class="data">
                            <td>{{ $count++ }}</td>
                            <td>{{ $purchase->product->skuCode ? $purchase->product->skuCode->code : '-' }}</td>
                            <td>{{ $purchase->product->name }}</td>
                            <td>{{ $purchase->product->unit->name }}</td>
                            <td class="increase increase-stock">{{ $purchase->quantity }}</td>
                            <td class="increase">{{ $purchase->current_qty }}
                            </td>
                            <td>{{ \Carbon\Carbon::parse($purchase->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
                @if (count($adjustments) > 0)
                    <tr class="title">
                        <td colspan="7">
                            <h3>Stock Adjustment</h3>
                        </td>
                    </tr>
                    @foreach ($adjustments as $key => $adjustment)
                        <tr class="data">
                            <td>{{ $count++ }}</td>
                            <td>{{ $adjustment->product->skuCode ? $adjustment->product->skuCode->code : '-' }}</td>
                            <td>{{ $adjustment->product->name }}</td>
                            <td>{{ $adjustment->product->unit->name }}</td>
                            <td class="{{ $adjustment->type == 'Adjustment_Out' ? 'decrease decrease-stock' : 'increase increase-stock' }}">{{ $adjustment->quantity }}</td>
                            <td class="{{ $adjustment->type == 'Adjustment_Out' ? 'decrease' : 'increase' }}">{{ $adjustment->current_qty }}</td>
                            <td>{{ \Carbon\Carbon::parse($adjustment->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
                @if (count($balances_in) > 0)
                    <tr class="title">
                        <td colspan="7">
                            <h3>Product Balance In</h3>
                        </td>
                    </tr>
                    @foreach ($balances_in as $key => $balance_in)
                        <tr class="data">
                            <td>{{ $count++ }}</td>
                            <td>{{ $balance_in->product->skuCode ? $balance_in->product->skuCode->code : '-' }}</td>
                            <td>{{ $balance_in->product->name }}</td>
                            <td>{{ $balance_in->product->unit->name }}</td>
                            <td class="increase increase-stock">{{ $balance_in->quantity }}</td>
                            <td class="increase">{{ $balance_in->current_qty }}
                            </td>
                            <td>{{ \Carbon\Carbon::parse($balance_in->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
                @if (count($balances_out) > 0)
                    <tr class="title">
                        <td colspan="7">
                            <h3>Product Balance Out</h3>
                        </td>
                    </tr>
                    @foreach ($balances_out as $balance_out)
                        <tr class="data">
                            <td>{{ $count++ }}</td>
                            <td>{{ $balance_out->product->skuCode ? $balance_out->product->skuCode->code : '-' }}</td>
                            <td>{{ $balance_out->product->name }}</td>
                            <td>{{ $balance_out->product->unit->name }}</td>
                            <td class="decrease decrease-stock">{{ $balance_out->quantity }}</td>
                            <td class="decrease">{{ $balance_out->current_qty }}
                            </td>
                            <td>{{ \Carbon\Carbon::parse($balance_out->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endif
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script>
    $("#excel_download").on('click',function (event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude:"#DivIdToPrint",
            name:"Worksheet Name",
            filename: "stock_report",//do not include extension
            fileext:".csv," // file extension
        });
    });

    $('.print-data').on('click', function() {
        printDiv();
    });

    function printDiv() {

    var divToPrint = document.getElementById('report-body');

    var newWin = window.open('', 'Print-Window');

    newWin.document.open();

    newWin.document.write('<html>' +
        '<head>' +
        '<style>\n' +
        '    th\n' +
        '    {\n' +
        '        color:black !important;\n' +
        '    }\n' +
        '\n' +
        '    table, td, th \n' +
        '    {\n' +
        '        border: 1px solid #ddd;\n' +
        '    }\n' +
        '\n' +
        '    table \n' +
        '    {\n' +
        '        width: 100%;\n' +
        '        border-collapse: collapse;\n' +
        '    }\n' +
        '\n' +
        '    .title h3 \n' +
        '    {\n' +
        '        color:black !important;\n' +
        '    }\n' +
        '</style>' +
        '</head>' +
        '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
        '</html>');
    newWin.document.close();

    setTimeout(function() {
        newWin.close();
    }, 10);

    }

</script>
