<h6 style="font-weight:bold;">Type Order Number</h6>

<div class="row">
    <div class="col-md-5">
        <div class="form-group">
            <input type="textbox" name="order_no" class="form-control" id="exampleInputPassword1" placeholder="Type Order Number">
        </div>
    </div>
    <div class="col-md-7">
        <div style="text-align:left">
            <button type="button" class="btn btn-app btn-success print-data">
                <i class="la la-print"></i> Print Report
            </button>
            <button type="button" class="btn btn-app btn-danger clear-data">
                <i class="las la-sync"></i> Clear Filter
            </button>
            <button type="button" class="btn btn-app btn-info search-data">
                <i class="la la-search"></i> Search Invoice
            </button>
        </div>
    </div>
</div>

<br>
<div class="col-md-12 show-report" id="DivIdToPrint" style=" width: 100%; background-color: #ffffff; padding: 20px; -webkit-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
-moz-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48); border-radius: 5px;overflow: scroll;"></div>

@push('crud_fields_styles')
<style>
     li.active a {
            background-color: #222d32 !important;
        }

        .table-data {
            width: 100%;
        }

        .table-data, .table-data th, .table-data td {
            border-collapse: collapse;
            border: 1px solid #a8a8a8;
        }

        .table-data th {
            text-align: center;
            padding: 5px;
        }

        .table-data td {
            padding: 5px;
        }

        .table-data tbody > tr:nth-child(odd) {
            color: #606060;
        }
</style>
@endpush

@push('after_scripts')
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

@endpush
@push('crud_fields_scripts')
<script>
    var token = '{{csrf_token()}}';
  
    $(document).ready(function() {
        $('#saveActions').hide();
        $('#saveActions button').prop('disabled', true);
        $('small').hide();
        $("div").find(".col-md-8").addClass("col-md-12").remove("col-md-8");
        $('.show-report').hide();
        $('.print-data').hide();
        $('.clear-data').on('click',function(){
            window.location.reload();
        })
    })
    $('.print-data').on('click', function() {
        printDiv();
    });
    $('.search-data').on('click', function(e) {
        var invoice = $('[name="order_no"]').val();
        if(invoice.length != 0 || invoice != ""){
            $.ajax({ 
                url: '{{route('invoice')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    invoice
                },
                success: function(res){
                    $('.show-report').html(res);
                    $('.show-report').show();
                    $('.print-data').show();
                },
                error:function(res)
                {
                    $('.show-report').show();
                    $('.show-report').html("<p style='text-align:center'>There is no report with this order number.</p>");
                    $('.print-data').hide();
                }
            })
        }else
        {
            alert("Please add Order Number");
        }
        
    });
  
    $("#excel").on('click',function (event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude:"#DivIdToPrint",
            name:"Worksheet Name",
            filename: "order",//do not include extension
            fileext:".csv," // file extension
        });
    });
    
    function printDiv() {

        var divToPrint = document.getElementById('DivIdToPrint');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<head>' +
            '<style>\n' +
            '    .table-data\n' +
            '    {\n' +
            '        width:100%;\n' +
            '    }\n' +
            '\n' +
            '    .table-data,.table-data  th, .table-data  td\n' +
            '    {\n' +
            '        border-collapse:collapse;\n' +
            '        border: 1px solid #a8a8a8;\n' +
            '    }\n' +
            '\n' +
            '    .table-data  th\n' +
            '    {\n' +
            '        text-align: center;\n' +
            '        padding: 5px;\n' +
            '    }\n' +
            '\n' +
            '    .table-data  td\n' +
            '    {\n' +
            '        padding: 5px;\n' +
            '    }\n' +
            '\n' +

            '</style>' +
            '</head>' +
            '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
            '</html>');

        newWin.document.close();

        setTimeout(function() {
            newWin.close();
        }, 10);

    }
</script>
@endpush