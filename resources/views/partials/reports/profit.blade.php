<div class="profit-report col-md-12 p-0"></div>

@push('crud_fields_scripts')
    <script>
        $('#saveActions').hide();

        $('.clear-data').on('click', function() {
            window.location.reload();
        });

        $("div").find(".col-md-8").addClass("col-md-12").remove("col-md-8");

        var data = {
            first_compare: $('[name="first_compare"]').val(),
            second_compare: $('[name="second_compare"]').val(),
        };

        const routeName = $('[name="route_name"]').val();
        switch (true) {
            case (routeName == 'profit_report'):
                url = '{{ route('profit_report') }}';
                break;
            case (routeName == 'profit_comparison_report'):
                url = '{{ route('profit_comparison_report') }}';
                $("div").find(".card-body").addClass("justify-content-end");
                $('.filter').hide();
                break;
        }

        $('.compare').on('change', function(e) {
            e.preventDefault();
            var first_compare = $('[name="first_compare"]').val();
            var second_compare = $('[name="second_compare"]').val();
            var data = {
                first_compare,
                second_compare
            };
            report(data);
        });

        $('.search-data').on('click', function(e) {
            e.preventDefault();
            var start_date = $('[name="start_date"]').val();
            var end_date = $('[name="end_date"]').val();
            var data = {
                start_date,
                end_date
            };
            report(data);
        });

        report(data);

        function report(data) {
            $.ajax({
                url,
                type: 'POST',
                async: false,
                data,
                success: function(res) {
                    $('.profit-report').html(res);
                },
                error: function(res) {
                    alert(res.responseJSON.message);
                }
            });
        }
    </script>
@endpush
