@php
    use Carbon\Carbon;
@endphp

<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        border-radius: 5px;
    }

    .order-table {
        width: 100%;
    }

    .order-table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
    }

    .order-table tbody tr.data {
        text-align: center;
    }

    .order-table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        background: #1b252b;
        color: var(--white);
    }

    td {
        text-align: center;
        padding: 10px;
    }

    th {
        text-align: center;
    }
</style>

@if (count($orders) > 0)
    <table id="excelreport" class="order-table table-striped table-bordered">
        <thead>
            <th>No</th>
            <th>OrderNo</th>
            <th>Payment</th>
            <th>Shipping</th>
            <th>Shop Name</th>
            <th>Shipping Address</th>
            <th>City/Tsp</th>
            <th>Phone</th>
            <th>Total Qty</th>
            <th>Paid By</th>
            <th>Total</th>
            <th>Discount</th>
            <th>Coupon Discount</th>
            <th>Grand Total</th>
            <th>Delivery Fee</th>
            <th>Net Amount</th>
        </thead>

        <tbody>
            @php
                $count = 1;
                $total = 0;
                $now = Carbon::now();
                $string = '';
                $profit_amount = 0;
            @endphp
            @foreach ($orders as $order)
                @php
                    $profit_amount += $order->net_amount;
                @endphp
                <tr style='text-align:center'>
                    <td>{{ $count++ }}</td>
                    <td>{{ $order->order_no }}</td>
                    <td>{{ $order->payment_status }}</td>
                    <td>{{ $order->shipping_status }}</td>
                    <td>{{ $order->shop->name }}({{ $order->shipping_address->name }})</td>
                    <td>{{ $order->shipping_address->ship_address }}</td>
                    <td>{{ $order->shipping_address->city->name }}/{{ $order->shipping_address->township->name }}</td>
                    <td>{{ $order->shipping_address->phone_number }}</td>
                    <td>{{ $order->total_quantity }}</td>
                    <td>{{ $order->payment_type ? $order->payment_type->name : '-' }}</td>
                    <td>{{ number_format($order->total_amount, 0) }}</td>
                    <td>{{ number_format($order->discount_amount, 0) }}</td>
                    <td>{{ number_format($order->coupon_discount, 0) }}</td>
                    <td>{{ number_format($order->grand_total, 0) }}</td>
                    <td>{{ number_format($order->shipping_fees) }}</td>
                    <td>{{ number_format($order->net_amount, 0) }}</td>
                </tr>
            @endforeach
            <tr style="text-align: center;">
                <td colspan="15">Total</td>
                <td>{{ number_format($profit_amount, 0) }}</td>
            </tr>
        </tbody>

    </table>
@else
    <p style='text-align:center'>No data founded!!!</p>
@endif
