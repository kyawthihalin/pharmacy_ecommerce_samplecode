<div style="text-align: center">
    <button type="button" class="btn btn-app btn-info search-data btn-sm">
        <i class="la la-search"></i> Search
    </button>
    <button type="button" class="btn btn-app btn-danger clear-data btn-sm">
        <i class="las la-sync"></i> Clear Filter
    </button>
    <button type="button" class="btn btn-app btn-success print-data btn-sm">
        <i class="la la-print"></i> Print Report
    </button>
    <button class="btn export-btn btn-success btn-sm" title="Export Excel"  id="excel_download" style="font-size: 15px">
        <i class="las la-download"></i> Download Excel
    </button>
</div>
<div class="stock-report col-md-12 p-0" id="report-body"></div>

@push('crud_fields_scripts')
    <script>
        $('#saveActions').hide();
        $("div").find(".col-md-8").addClass("col-md-12").remove("col-md-8");
        $("div").find(".card-body").addClass("justify-content-center");
        $("small").hide();

        $('.clear-data').on('click', function() {
            window.location.reload();
        });

        const routeName = $('[name="route_name"]').val();
        switch (true) {
            case (routeName == 'stock_report'):
                url = '{{ route('stock_report') }}';
                break;
            case (routeName == 'stock_daily_report'):
                url = '{{ route('stock_daily_report') }}';
                break;
        }

        $('.search-data').on('click', function(e) {
            e.preventDefault();
            var sku_code = $('[name="sku_code"]').val();
            var product_id = $('[name="product_id"]').val();
            var type = $('[name="type"]').val();
            var start_date = $('[name="start_date"]').val();
            var end_date = $('[name="end_date"]').val();
            $.ajax({
                url,
                type: 'POST',
                async: false,
                data: {
                    sku_code,
                    product_id,
                    type,
                    start_date,
                    end_date
                },
                success: function(res) {
                    $('.stock-report').html(res);
                },
                error: function(res) {
                    alert(res.responseJSON.message);
                }
            })
        });

        $.ajax({
            url,
            type: 'POST',
            async: false,
            data: {},
            success: function(res) {
                $('.stock-report').html(res);
            },
            error: function(res) {
                alert(res.responseJSON.message);
            }
        });
    </script>
@endpush
