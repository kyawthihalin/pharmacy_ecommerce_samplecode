<div style="text-align: center">
    <button type="button" class="btn btn-app btn-danger clear-data">
        <i class="las la-sync"></i> Clear Filter
    </button>
    <button type="button" class="btn btn-app btn-info search-data">
        <i class="la la-search"></i> Search
    </button>
    <button type="button" class="btn btn-app btn-success print-data">
        <i class="la la-print"></i> Print Report
    </button>
    <button class="btn export-btn btn-success" title="Export Excel"  id="excel">
        <i class="las la-download"></i> Download Excel
    </button>
</div>
