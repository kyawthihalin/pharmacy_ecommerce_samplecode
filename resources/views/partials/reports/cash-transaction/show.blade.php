<style>
    /* tr,
    th,
    td {
        border: 1px solid #bdbdbd;
    } */

    table,thead,tbody,tr, td, th {
    border: 1px solid;
    }

    table {
    width: 100%;
    border-collapse: collapse;
    }

    th,
    td {
        text-align: center;
        padding:10px;
    }

    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .profit-table {
        width: 100%;
    }

    .profit-table tr td {
        padding: 10px;
    }

    .profit-table thead tr td {
        background: var(--darkblue);
        color: var(--white);
    }

    .profit-table tfoot tr td {
        background: var(--darkblue);
        color: var(--white);
    }


    .profit-table tbody tr td {
        background: var(--lightblue);
    }

    .profit-table tr.net td {
        background: #1b252b;
        color: var(--white);
    }

    .profit-table tr.child td.indent {
        padding-left: 30px;
    }
</style>
<?php

use App\Models\FinanceCashAccount;

// $current_account = FinanceCashAccount::find($current_cash_account_id);
// $currency = $current_account->currency;
$balance = $opening_balance;
$excel_balance = $opening_balance;
$balance_display = '';
?>
<div class="col-md-12 p-0 mt-3 table-report">
    @if($transactions->count() > 0)
    <table  class="profit-table" id="transaction_report_data">
        <thead style="position: sticky;top: 0">
            <tr>
                <td colspan="3"> Transactions (showing {{$transactions->count()}} transaction ) </td>
                <td colspan="3" > Opening Balance</td>
                <td> {{ $balance ?? '0' }}</td>
            </tr>
            <tr>
                <td>Date</td>
                <td>Fiance Cash Account</td>
                <td>Finace Particular</td>
                <td>Reference Id</td>
                <td>Debit</td>
                <td>Credit</td>
                <td>Balance</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $transaction)
            <?php
                $account = null;
                $debit_amount = 0;
                $credit_amount = 0;
                $credit_account = $transaction->credit_account;
                $debit_account = $transaction->debit_account;

                if ( $credit_account && $credit_account->id === $current_cash_account->id) {
                    $account = $credit_account;
                    if($transaction->finance_particular->credit_account_operation == 'Subtraction')
                    {
                        $credit_amount = $transaction->amount;
                    }
                    else{
                        $debit_amount =  $transaction->amount;
                    }
                } elseif ($debit_account && $debit_account->id === $current_cash_account->id) {
                    $account = $debit_account;
                    if($transaction->finance_particular->debit_account_operation == 'Subtraction')
                    {
                        $credit_amount = $transaction->amount;
                    }
                    else{
                        $debit_amount =  $transaction->amount;
                    }
                }
                if ($account->id === $current_cash_account->id) {
                    $balance += $debit_amount - $credit_amount;
                }
                $balance_display = $balance < 0 ? '(' . str_replace('-', '', $balance) . ')' : $balance;
                ?>
            <tr>
                <td>{{ $transaction->created_at->format('Y-m-d') }}</td>
                <td>{{ $current_cash_account->name }}</td>
                <td>{{ $transaction->finance_particular->name }}</td>
                {{-- <td>{{ $transaction->finance_particular_id }}</td> --}}
                <td>{{ $transaction->transactionable->order_no }}</td>
                {{-- <td>{{ $transaction->debit_account->name }}</td>
                <td>{{ $transaction->credit_account->name}}</td> --}}
                <td>{{ (bool) $debit_amount ? $debit_amount : '' }}</td> <!-- original -->
                <td>{{ (bool) $credit_amount ? $credit_amount : '' }}</td>
                <td>{{ $balance_display }} </td>
            </tr>
            @endforeach

            <tfoot>
                <td colspan="6">Closing Balance</td>
                <td style="font-weight: bold;">{{ $balance < 0 ? '(' . str_replace('-', '', $balance) . ')' : $balance }}
                </td>
            </tfoot>
        </tbody>
    </table>


    {{-- For Excel Download --}}
    <table id="excelreport"  id="transaction_report_data" style="display:none">
        <thead style="position: sticky;top: 0">
            <tr>
                <td>Date</td>
                <td>Fiance Cash Account</td>
                <td>Finace Particular</td>
                <td>Reference Id</td>
                <td>Debit</td>
                <td>Credit</td>
                <td>Balance</td>
            </tr>
        </thead>
        <tbody>
            @foreach ($transactions as $transaction)
            <?php
                $account = null;
                $debit_amount = 0;
                $credit_amount = 0;
                $credit_account = $transaction->credit_account;
                $debit_account = $transaction->debit_account;

                if ( $credit_account && $credit_account->id === $current_cash_account->id) {
                    $account = $credit_account;
                    if($transaction->finance_particular->credit_account_operation == 'Subtraction')
                    {
                        $credit_amount = $transaction->amount;
                    }
                    else{
                        $debit_amount =  $transaction->amount;
                    }
                } elseif ($debit_account && $debit_account->id === $current_cash_account->id) {
                    $account = $debit_account;
                    if($transaction->finance_particular->debit_account_operation == 'Subtraction')
                    {
                        $credit_amount = $transaction->amount;
                    }
                    else{
                        $debit_amount =  $transaction->amount;
                    }
                }
                if ($account->id === $current_cash_account->id) {
                    $excel_balance += $debit_amount - $credit_amount;
                }
                $balance_display = $excel_balance < 0 ? '(' . str_replace('-', '', $excel_balance) . ')' : $excel_balance;
                ?>
            <tr>
                <td>{{ $transaction->created_at->format('Y-m-d') }}</td>
                <td>{{ $current_cash_account->name }}</td>
                <td>{{ $transaction->finance_particular->name }}</td>
                {{-- <td>{{ $transaction->finance_particular_id }}</td> --}}
                <td>{{ $transaction->transactionable->order_no }}</td>
                {{-- <td>{{ $transaction->debit_account->name }}</td>
                <td>{{ $transaction->credit_account->name}}</td> --}}
                <td>{{ (bool) $debit_amount ? $debit_amount : '' }}</td> <!-- original -->
                <td>{{ (bool) $credit_amount ? $credit_amount : '' }}</td>
                <td>{{ $balance_display }} </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @else
    <div style="text-align: center;margin-top:200px;opacity:0.7">
        <i class="las la-exclamation-circle" style="font-size:50px"></i>
        <p style="font-size:20px">Result Not Found !</p>
    </div>
    @endif



</div>


<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script>
    var transactions = @json($transactions);
    if(transactions.length == 0)
    {
        $('#excel_finance').hide();
        $('.print-data').hide();
    }

    $("#excel_finance").on('click',function (event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude:"#DivIdToPrint",
            name:"Worksheet Name",
            filename: "finance_cash_report",//do not include extension
            fileext:".csv," // file extension
        });
    });

    $('.print-data').on('click', function() {
        printDiv();
    });

    function printDiv() {

    var divToPrint = document.getElementById('report-body');

    var newWin = window.open('', 'Print-Window');

    newWin.document.open();

    newWin.document.write('<html>' +
        '<head>' +
        '<style>\n' +
        '    .profit-table thead tr td\n' +
        '    {\n' +
        '        color:black !important;\n' +
        '    }\n' +
        '\n' +
        '    .profit-table tfoot tr td\n' +
        '    {\n' +
        '        color: black !important;\n' +
        '    }\n' +
        '\n' +
        '    table,thead,tbody,tr, td, th\n' +
        '    {\n' +
        '        border: 1px solid !important;\n' +
        '    }\n' +
        '</style>' +
        '</head>' +
        '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
        '</html>');

    newWin.document.close();

    setTimeout(function() {
        newWin.close();
    }, 10);

    }

</script>
