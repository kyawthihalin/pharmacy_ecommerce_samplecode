@extends(backpack_view('blank'))

@push('after_styles')
<meta charset="utf-8">
<title></title>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/loader.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.17.1/extensions/sticky-header/bootstrap-table-sticky-header.min.css">
{{-- <link rel="stylesheet" href="extensions/sticky-header/bootstrap-table-sticky-header.css"> --}}
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .date_btn {
        background-color: #fff !important;
        border: 1px solid #aaa !important;
        border-radius: 4px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        margin-top: 6px !important;
    }

    .select-lang .select2-container .select2-selection--single .select2-selection__rendered {
        margin-top: 0 !important;
    }

    .select2-container--default .select2-selection--multiple {
        min-height: 40px !important;
    }

    .select2-container .select2-selection--single {
        height: 40px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 8px !important;
    }

    .select2-selection--multiple {
        overflow: hidden !important;
        height: auto !important;
    }

    /* .export-btn {
        font-size: 30px;
        color: #7c69ef;
    } */

    ul{
        margin: 0;
        padding: 0;
    }

    li:first-child {
    margin-top: 10px;
    }
    li{
        margin: 5px;
    }

    #transaction_type{
            background-color: white;
            /* width: 100%; */
            /* height: auto; */
            min-height: 37px;
            max-height: 120px;
            overflow: auto;
            border: 1px solid grey;
            border-radius: 5px;
    }

    #cash_account{
        border: 1px solid grey;
        border-radius: 5px;
    }

    .select2-results__option
    {
        padding:0px;
    }

    .select2-container .select2-search--inline .select2-search__field {
        margin-top: 10px;
        font-size: 14px;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
        font-size: 12px;
    }

    .select2-results__option {
        font-size: 15px;
    }

</style>
@endpush

@section('header')
    <div class="container-fluid py-2 px-4">
        <h4>
            Cash Account Report
        </h4>
    </div>
@endsection

@section('content')
    <div id="loader-container">
        <div class="spinner spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row col-md-12" style="padding: 0px !important;margin: 0 !important;">

                <div class="col-md-3 form-group">
                    <label for="">
                        Finance Cash Account
                    </label>
                    <select name="cash_account" id="cash_account" class="form-control js-example-basic-single">
                        <option disabled> - </option>
                        @if (count($cash_accounts) > 0)
                        @foreach ($cash_accounts as $cash_account)
                        <option value="{{ $cash_account->id }}">{{ $cash_account->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>

                {{-- <div class="col-md-3 form-group">
                    <label for="">
                        Particular
                    </label>
                    <select name="transaction_type" id="transaction_type" class="form-control js-example-basic-single" multiple>
                        <option disabled> - </option>
                    </select>
                </div>
                --}}




                <div id="checkboxes" class="col-md-3 form-group">
                    <label>Finance Particular</label>
                    {{-- <ul id="transaction_type"  style="list-style-type:none;" style="width: 100%">
                        @if(count($finance_particulars)>0)
                            @foreach ($finance_particulars as $finance_particular)
                                <li><input type="checkbox" name="transaction_type" id={{$finance_particular->id}} value={{$finance_particular->id}}><label  for={{$finance_particular->id}} style="padding-left:5px" >{{$finance_particular->name}}</label></li>
                            @endforeach
                        @endif
                    </ul> --}}
                    <div>
                        <select class="js-example-basic-multiple" name="transaction_type[]" multiple="multiple"  style="width: 100%" id="transaction_type">
                            @if(count($finance_particulars)>0)
                                @foreach ($finance_particulars as $finance_particular)
                                        <option value={{$finance_particular->id}}>{{$finance_particular->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    </div>

                <div class="col-md-2">
                    <label>
                        Date Range
                    </label>
                    <input  class="form-control date_btn"  type="text" name="datetimes" id="datetimes" >
                </div>

                <div class="col-md-4" style="padding-top:32px">
                    <a href="" id="export-hidden" style="display: none;"></a>
                    <button class="btn btn-success" id="data-button">
                        Get Report
                    </button>

                    <button class="btn export-btn btn-success" title="Export Excel"  id="excel_finance" style="font-size: 15px">
                        Download Excel
                    </button>
                    <button type="button" class="btn btn-app btn-info print-data">
                        <i class="la la-print"></i> Print Report
                    </button>


                </div>
            </div>

            <div class="card" id="card" style="height: 530px; overflow: scroll;">
                <div id="report-body">
                    <div class="data"></div>
                    <div id="no-data"></div>
                </div>
            </div>
            <div class="col-md-12 show-report" id="DivIdToPrint" style="display:none; width: 100%; background-color: #ffffff; padding: 20px; -webkit-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
    -moz-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
    box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48); border-radius: 5px;overflow: scroll;"></div>
        </div>
    </div>
@endsection

@push('after_scripts')
{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
{{-- <script type="text/javascript"     ="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('js/loader.js') }}"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    $(document).ready(function() {
        $('#excel_finance').hide();
        $('.print-data').hide();
        $('#datetimes').daterangepicker();
        $('.js-example-basic-multiple').select2({
            placeholder: "Select finance particulars",
        });
        const cashAccountSelect = $('#cash_account');
        const transactionTypeSelect = $('#transaction_type');

        cashAccountSelect.on('change', function () {
            const cash_account = $(this).val();
            $.ajax({
                url: '{{route('select.transaction.type')}}',// Replace with the URL of your Laravel route to fetch towns
                method: 'POST', // Use 'POST' or 'GET' as per your route definition
                data: {
                    cash_account_id: cash_account,
                },
                success: function (response) {
                    transactionTypeSelect.empty();
                    // $.each(response, function (key, transactionType) {
                    //     transactionTypeSelect.append('<li><input type="checkbox" name="transaction_type" id="' + transactionType.id + '" value="' + transactionType.id + '"><label  for="' + transactionType.id + '" style="padding-left:5px" >' + transactionType.name + '</label></li>');
                    // });
                    $.each(response, function (key, transactionType) {
                        transactionTypeSelect.append($('<option></option>').attr('value', transactionType.id).text(transactionType.name));
                    });
                },
                error: function (error) {
                    console.log(error);
                },
            });
        });
    });




    // const Toast = Swal.mixin({
    //     toast: true,
    //     position: 'top-end',
    //     showConfirmButton: false,
    //     timer: 3000,
    //     timerProgressBar: true,
    //     didOpen: (toast) => {
    //         toast.addEventListener('mouseenter', Swal.stopTimer)
    //         toast.addEventListener('mouseleave', Swal.resumeTimer)
    //     }
    // })
    // let from = '';
    // let to = '';
    // let payuser = '';
    // let currentUser = '';
    // $(function() {
    //     $('.js-example-basic-single').select2();
    //     $('input[name="datetimes"]').daterangepicker({
    //         timePicker: false,
    //         timePickerSeconds: false,
    //         startDate: moment().startOf('month'),
    //         endDate: moment(),
    //         locale: {
    //             format: 'Y/MM/DD'
    //         },
    //     });
    // });

    $('#data-button').on('click', function() {
        $('.data').html("<b>Preparing report...</b>");
        let transaction_type = $('#transaction_type').val();
        let cash_account = $('#cash_account').val();
        let date = $('#datetimes').val();

        // var checkedCheckboxes = $("input[name='transaction_type']:checked");
        // var selectedValues = [];
        // checkedCheckboxes.each(function() {
        // selectedValues.push($(this).val());
        // });

        // showLoader();
        $.ajax({
                url: '{{route('finance.cash.report')}}',// Replace with the URL of your Laravel route to fetch towns
                method: 'POST', // Use 'POST' or 'GET' as per your route definition
                data: {
                    cash_account_id: cash_account,
                    transaction_type_id : transaction_type,
                    datetimes : date,
                },
                success: function (response) {
                    $('#excel_finance').show();
                    $('.print-data').show();
                    $('.data').html(response);
                    Toast.fire({
                        icon: 'success',
                        title: 'Success',
                    });
                    // hideLoader();
                },
                error: function (error) {
                    console.log(error);
                },
            });
        // $.ajax({
        //     url: '{{route('finance.cash.report')}}',
        //     type: 'post',
        //     data: {
        //         transaction_types,
        //         cash_account,
        //         date
        //     },
        //     success: function(response) {
        //         $('.data').html(response);
        //         Toast.fire({
        //             icon: 'success',
        //             title: 'Success',
        //         });
        //         hideLoader();
        //     },
        //     error: function(response) {
        //         Swal.fire({
        //             title: 'Error!',
        //             text: response.responseJSON.message,
        //             icon: 'error',
        //             confirmButtonText: 'Ok'
        //         })
        //         hideLoader();
        //     }
        // });


    });

    // $('#export-button').on('click', function() {
    //     let cash_account = $('#cash_account').val();
    //     let date = $('#datetimes').val();

    //     $("#export-hidden").attr('href', `/YP+VF)W7BYM(/5zAvtMBKzXMJ3y/fincashreport/excel?cash_account=${cash_account}&date=${date}`);
    //     document.getElementById("export-hidden").click();
    // });


</script>
@endpush

