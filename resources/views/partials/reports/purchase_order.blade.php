<div style="text-align: center">
    <button type="button" class="btn btn-app btn-info search-data btn-sm">
        <i class="la la-search"></i> Search
    </button>
    <button type="button" class="btn btn-app btn-danger clear-data btn-sm">
        <i class="las la-sync"></i> Clear Filter
    </button>
    <button type="button" class="btn btn-app btn-success print-data btn-sm">
        <i class="la la-print"></i> Print Report
    </button>
    <button class="btn export-btn btn-success btn-sm" title="Export Excel"  id="excel">
        <i class="las la-download"></i> Download Excel
    </button>
</div>
<br>
<div class="col-md-12 show-report" id="DivIdToPrint" style=" width: 100%; background-color: #ffffff; padding: 20px; -webkit-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
-moz-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48); border-radius: 5px;overflow: scroll;"></div>

@push('crud_fields_styles')
<style>
     li.active a {
            background-color: #222d32 !important;
        }

        .table-data {
            width: 100%;
        }

        .table-data, .table-data th, .table-data td {
            border-collapse: collapse;
            border: 1px solid #a8a8a8;
        }

        .table-data th {
            text-align: center;
            padding: 5px;
        }

        .table-data td {
            padding: 5px;
        }

        .table-data tbody > tr:nth-child(odd) {
            color: #606060;
        }
</style>
@endpush

@push('after_scripts')
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>

@endpush
@push('crud_fields_scripts')
<script>
    var token = '{{csrf_token()}}';

    $(document).ready(function() {
        $('#saveActions').hide();
        $('small').hide();
        $("div").find(".col-md-8").addClass("col-md-12").remove("col-md-8");
        $("div").find(".card-body").addClass("justify-content-center");
        $('.clear-data').on('click',function(){
            window.location.reload();
        })
    })
    $('.print-data').on('click', function() {
        printDiv();
    });
    $('.search-data').on('click', function(e) {
        e.preventDefault();
        var end_date = $('[name="Edate"]').val();
        var start_date = $('[name="Sdate"]').val();
        $.ajax({
            url: '{{route('purchase_order_report')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                end_date,
                start_date,
            },
            success: function(res){
                $('.show-report').html(res);
                $('.show-report').show();
                $('.print-data').show();
                $('.export-btn').show();
            },
            error:function(res)
            {
                $('.print-data').hide();
                $('.export-btn').hide();
                $('.show-report').show();
                alert(res.responseJSON.message);
            }
        })
    });

    $.ajax({
        url: '{{ route('purchase_order_report') }}',
        type: 'POST',
        async: false,
        data: {},
        success: function(res) {
            $('.show-report').html(res);
        },
        error: function(res) {
            alert(res.responseJSON.message);
        }
    });

    $("#excel").on('click',function (event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude:"#DivIdToPrint",
            name:"Worksheet Name",
            filename: "purchase_order_report",//do not include extension
            fileext:".csv," // file extension
        });
    });

    function printDiv() {

        var divToPrint = document.getElementById('DivIdToPrint');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<head>' +
            '<style>\n' +
            '    th\n' +
            '    {\n' +
            '        color:black !important;\n' +
            '    }\n' +
            '\n' +
            '    table, td, th \n' +
            '    {\n' +
            '        border: 1px solid #ddd;\n' +
            '    }\n' +
            '\n' +
            '    table \n' +
            '    {\n' +
            '        width: 100%;\n' +
            '        border-collapse: collapse;\n' +
            '    }\n' +
            '\n' +
            '    .title h3 \n' +
            '    {\n' +
            '        color:black !important;\n' +
            '    }\n' +
            '</style>' +
            '</head>' +
            '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
            '</html>');

        newWin.document.close();

        setTimeout(function() {
            newWin.close();
        }, 10);

    }
</script>
@endpush
