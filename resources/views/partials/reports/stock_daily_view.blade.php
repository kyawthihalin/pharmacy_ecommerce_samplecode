<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        /* padding: 20px; */
        /* -webkit-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        -moz-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48); */
        border-radius: 5px;
        /* overflow: scroll; */
    }

    .stock-table {
        width: 100%;
    }

    .stock-table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
        /* min-width: 75px; */
    }

    .stock-table tbody tr.data {
        text-align: center;
    }

    .stock-table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        /* margin: 0 0 0 15px; */
        background: #1b252b;
        color: var(--white);
        /* text-align: center; */
    }

    td{
        text-align: center;
        padding: 10px;
    }

    th{
        text-align: center;
    }

    td.increase {
        color: green;
    }

    td.increase-stock, td.decrease-stock {
        display: flex;
        justify-content: center;
    }

    td.increase-stock:after {
        content: "";
        display: inline-block;
        float: right;
        border: 0.3em solid transparent;
        border-bottom: 0.4em solid green;
        position: relative;
        /* right: 4.3em; */
        bottom: 0.6em;
        margin-left: 8px;
    }

    td.decrease-stock:after {
        content: "";
        display: inline-block;
        float: right;
        border: 0.3em solid transparent;
        border-top: 0.4em solid red;
        position: relative;
        /* right: 4.3em; */
        top: 0.5em;
        margin-left: 8px;
    }

    td.decrease {
        color: red;
    }
</style>

@if (isset($stock_logs) && count($stock_logs) > 0)
    <div class="col-md-12 p-0 table-report">
        <table class="stock-table" id="excelreport" >
            <thead>
                <th>No</th>
                <th>Type</th>
                <th>SKU Code</th>
                <th>Name</th>
                <th>Unit</th>
                <th>Stock In/Out</th>
                <th>Current Stock</th>
                <th>Date</th>
            </thead>
            <tbody>
                @php
                    $count = 1;
                @endphp
                @if (count($stock_logs) > 0)
                    @foreach ($stock_logs as $key => $stock_log)
                        @php
                            $stock_type = App\Constants\GetStatus::STOCK_TYPE;
                            if ($stock_log->type == $stock_type['Balance_Out'] || $stock_log->type == $stock_type['Adjustment_Out'] || $stock_log->type == $stock_type['Sale']) {
                                $qty_class = "decrease decrease-stock";
                                $cur_qty_class = "decrease";
                            }

                            if ($stock_log->type == $stock_type['Balance_In'] || $stock_log->type == $stock_type['Purchase'] || $stock_log->type == $stock_type['Adjustment_In']) {
                                $qty_class = "increase increase-stock";
                                $cur_qty_class = "increase";
                            }
                        @endphp
                        <tr class="data">
                            <td>{{ $count++ }}</td>
                            <td>{{ $stock_log->type }}</td>
                            <td>{{ $stock_log->product->skuCode ? $stock_log->product->skuCode->code : '-' }}</td>
                            <td>{{ $stock_log->product->name }}</td>
                            <td>{{ $stock_log->product->unit->name }}</td>
                            <td class="{{ $qty_class }}">{{ $stock_log->quantity }}</td>
                            <td class="{{ $cur_qty_class }}">{{ $stock_log->current_qty }}
                            </td>
                            <td>{{ \Carbon\Carbon::parse($stock_log->created_at)->format('Y-m-d') }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>

@endif

<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script>
    $("#excel_download").on('click',function (event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude:"#DivIdToPrint",
            name:"Worksheet Name",
            filename: "stock_daily_report",//do not include extension
            fileext:".csv," // file extension
        });
    });

    $('.print-data').on('click', function() {
        printDiv();
    });

    function printDiv() {

    var divToPrint = document.getElementById('report-body');

    var newWin = window.open('', 'Print-Window');

    newWin.document.open();

    newWin.document.write('<html>' +
        '<head>' +
        '<style>\n' +
        '    th\n' +
        '    {\n' +
        '        color:black !important;\n' +
        '    }\n' +
        '\n' +
        '    table, td, th \n' +
        '    {\n' +
        '        border: 1px solid #ddd;\n' +
        '    }\n' +
        '\n' +
        '    table \n' +
        '    {\n' +
        '        width: 100%;\n' +
        '        border-collapse: collapse;\n' +
        '    }\n' +
        '</style>' +
        '</head>' +
        '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
        '</html>');
    newWin.document.close();

    setTimeout(function() {
        newWin.close();
    }, 10);

    }

</script>

