@php
use Carbon\Carbon;
@endphp
<style>
    table {
        width: 100%;
    }

    table,
    th,
    td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }
   
</style>
@if(isset($order))
<div style="text-align: center;margin-top:20px">
    <a target='blank' href={{$url}}><img src={{$url}} alt='Logo' width='auto' height='70px'/></a>
</div>
<h5 style="text-align: left;margin-top:20px;font-weight:bold">Invoice For Order - {{$order->order_no}}</h5>
<table id="excelreport">
    <thead>
        <th>No</th>
        <th>Item Name</th>
        <th>Item Code</th>
        <th>Item Price</th>
        <th>Item Quantity</th>
        <th>Item Discount</th>
        <th>Total</th>
    </thead>

    <tbody>
        @php
        $count = 1;
        $total = 0;
        $now = Carbon::now(); $string = "";
       
        @endphp
        @foreach($order->details as $detail)
        @php
        $check_qty = $detail->quantity;
        if($check_qty == 0)
        {
            $check_qty = 1;
        }
        @endphp
        <tr style='text-align:center'>
            <td>{{$count++}}</td>
            <td>{{$detail->product->name}}</td>
            <td>{{$detail->product->product_id}}</td>
            <td>{{number_format($detail->amount ? $detail->amount / $check_qty : 0)}}</td>
            <td>{{$detail->quantity ? $detail->quantity : 0}}</td>
            <td style="color:red">{{number_format($detail->discount_amount ? $detail->discount_amount : 0)}} MMK</td>
            @if($detail->quantity == 0)
            <td style="text-align: left;">0 MMK</td>
            @else
            <td style="text-align: left;">{{number_format(bcsub($detail->amount,$detail->discount_amount)) }} MMK</td>
            @endif
        </tr>
        @endforeach
        
        <tr>
            <td rowspan="6" colspan="5" style="text-align: center !important;">{!! $qr_code !!}</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-right" style="text-align: right !important;">Total</td>
            <td class="text-success font-weight-bold" style="color: green !important;">{{number_format(bcsub($order->total_amount,$order->discount_amount))}} MMK</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-right" style="text-align: right !important;">Coupon Discount</td>
            <td class="text-danger font-weight-bold" style="color: red !important;">{{number_format($order->coupon_discount)}} MMK</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-right" style="text-align: right !important;">Grand Total</td>
            <td class="text-success font-weight-bold" style="color: green !important;">{{number_format($order->grand_total)}} MMK</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-right" style="text-align: right !important;">Shipping Fee</td>
            <td class="text-success font-weight-bold" style="color: green !important;">{{number_format($order->shipping_fees)}} MMK</td>
        </tr>
        <tr>
            <td class="font-weight-bold text-right" style="text-align: right !important;">Net Amount</td>
            <td class="text-success font-weight-bold" style="color: green !important;">{{number_format($order->net_amount)}} MMK</td>
        </tr>
    </tbody>

</table>
@else
<p style='text-align:center'>There is no report with this order number.</p>
@endif