@php
use Carbon\Carbon;
@endphp

<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        border-radius: 5px;
    }

    .product-table {
        width: 100%;
    }

    .product-table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
    }

    .product-table tbody tr.data {
        text-align: center;
    }

    .product-table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        background: #1b252b;
        color: var(--white);
    }

    td {
        text-align: center;
        padding: 10px;
    }

    th {
        text-align: center;
    }
</style>

@if(count($products) > 0)
<table id="excelreport" class="product-table table-bordered table-striped">
    <thead>
        <th>No</th>
        <th>Product ID</th>
        <th>Name</th>
        <th>Brand</th>
        <th>Image</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Discount Duration</th>
        <th>Size</th>
        <th>Balance</th>
    </thead>

    <tbody>
    @php
        $count = 1;
        $total = 0;
        $now = Carbon::now();
    @endphp
    @foreach($products as $product)
    @php
        $end_date =  Carbon::parse($product->discount_end_date);
        $url = Storage::temporaryUrl(
                    $product->feature_image,
                    now()->addMinutes(3)
        );
    @endphp
    <tr style='text-align:center'>
        <td>{{$count++}}</td>
        <td>{{$product->product_id}}</td>
        <td>{{$product->name}}</td>
        <td>{{$product->brand->name}}</td>

        <td> <img src="{{$url}}" alt="" width="80px" height="auto"> </td>
        <td>{{$product->price}}</td>
        @if($now > $end_date)
        @if($product->discount_type === "Percent")
            <td style="color:red">{{$product->discount}} %</td>
            <td style="color:red">{{$product->discount_start_date}} ~ {{$product->discount_end_date}}</td>
            @elseif($product->discount_type === "No Discount")
                <td style="color:red">{{$product->discount}} MMK</td>
                <td style="color:red">{{$product->discount_start_date}} ~ {{$product->discount_end_date}}</td>
            @else
                <td>-</td>
                <td>-</td>
            @endif
        @else
            @if($product->discount_type === "Percent")
            <td>{{$product->discount}} %</td>
            <td>{{$product->discount_start_date}} ~ {{$product->discount_end_date}}</td>
            @elseif($product->discount_type === "No Discount")
            <td>{{$product->discount}} MMK</td>
            <td>{{$product->discount_start_date}} ~ {{$product->discount_end_date}}</td>
            @else
                <td>-</td>
                <td>-</td>
            @endif
        @endif
        <td>{{$product->size}}{{$product->unit->name}}</td>
        @if($product->quantity <= $product->warning_count)
        <td style="color:red">{{$product->quantity}}</td>
        @else
        <td>{{$product->quantity}}</td>
        @endif
    </tr>

    @endforeach
    </tbody>

</table>
@else
<p style='text-align:center'>No data founded!!!</p>
@endif
