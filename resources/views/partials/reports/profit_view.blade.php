<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    table {
        width: 100%;
    }

    /* table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    } */


    .profit-table {
        width: 100%;
    }

    .profit-table tr td {
        padding: 10px;
    }

    .profit-table tr.parent td {
        background: var(--darkblue);
        color: var(--white);
    }

    .profit-table tr.child td {
        background: var(--lightblue);
    }

    .profit-table tr.net td {
        background: #1b252b;
        color: var(--white);
    }

    .profit-table tr.child td.indent {
        padding-left: 30px;
    }
</style>


<div class="col-md-12 p-0 mt-3 table-report" id="DivIdToPrint" >
    <table class="profit-table" id="excelreport" >
        <tbody>
            <tr style="display:none" id="hide_row">
                <td>Date Range</td>
                <td>From  {{$start_date}} To {{$end_date}} </td>
            </tr>
            <tr class="parent">
                <td>Income</td>
                <?php
                    $total_income = 0;
                    foreach ($income_accounts as $key => $income_account) {
                        $total_income = $total_income + $income_account["amount"];
                    }
                ?>
                <td>{{$total_income}} MMK</td>
            </tr>

            @foreach ($income_accounts as $income_account)
            <tr class="child">
                <td class="indent">{{$income_account["name"]}}</td>
                <td>{{$income_account["amount"]}} MMK</td>
            </tr>
            @endforeach


            <tr class="parent">
                <td>Expense</td>
                <?php
                    $total_expense = 0;
                    foreach ($expense_accounts as $key => $expense_account) {
                        $total_expense = $total_expense + $expense_account["amount"];
                    }
                ?>
                <td>{{$total_expense}} MMK</td>
            </tr>
            @foreach ($expense_accounts as $expense_account)
            <tr class="child">
                <td class="indent">{{$expense_account["name"]}}</td>
                <td>{{$expense_account["amount"]}} MMK</td>
            </tr>
            @endforeach

            <tr class="net">
                <td>Net Profit</td>
                {{-- <td>{{$total_income - $total_expense}} MMK</td> --}}
                <td>{{$net_profit_amount}} MMK</td>
            </tr>
        </tbody>
    </table>
</div>



<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script>
    var token = '{{csrf_token()}}';

    $('.print-data').on('click', function() {
        $("#hide_row").css("display", "");
        printDiv();
    });

    $("#excel").on('click',function (event) {
        event.preventDefault();
        $("#excelreport").table2excel({
            exclude:"#DivIdToPrint",
            name:"Worksheet Name",
            filename: "profit_report",//do not include extension
            fileext:".csv," // file extension
        });
    });

    function printDiv() {
        var divToPrint = document.getElementById('DivIdToPrint');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html>' +
            '<head>' +
            '<style>\n' +
                'table\n' +
            '    {\n' +
            '        width:100%;\n' +
            '    }\n' +
            '\n' +
            '    tr\n' +
            '    {\n' +
            '        padding:20px;\n' +
            '    }\n' +
            '\n' +
            '    table, th, td\n' +
            '    {\n' +
            '         border-collapse: collapse;\n' +
            '         border: 1px solid #a8a8a8;\n' +
            '         padding:10px;\n' +
            '    }\n' +
            '\n' +
            '</style>' +
            '</head>' +
            '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
            '</html>');

        newWin.document.close();
        $("#hide_row").css("display", "none");
        setTimeout(function() {
            newWin.close();
        }, 10);
    }
</script>

