<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        border-radius: 5px;
    }

    .sale-table {
        width: 100%;
    }

    .sale-table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
    }

    .sale-table tbody tr.data {
        text-align: center;
    }

    .sale-table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        background: #1b252b;
        color: var(--white);
    }

    td {
        text-align: center;
        padding: 10px;
    }

    th {
        text-align: center;
    }
</style>

@if (count($sale_reports) > 0)
<table id="excelreport" class="sale-table table-bordered table-striped">
    <thead>
        <th>No</th>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>SKU Code</th>
        <th>Image</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total Amount</th>
        <th>Date</th>
    </thead>

    <tbody>
        @php
            $count = 1;
        @endphp
        @foreach ($sale_reports as $sale_report)
            <tr style='text-align:center'>
                @php
                    $url = Storage::temporaryUrl($sale_report['image'], now()->addMinutes(3));
                @endphp
                <td>{{ $count++ }}</td>
                <td>{{ $sale_report['product_id'] }}</td>
                <td>{{ $sale_report['product_name'] }}</td>
                <td>{{ $sale_report['sku_code'] }}</td>
                <td> <img src="{{ $url }}" alt="" width="80px" height="auto"> </td>
                <td>{{ $sale_report['quantity'] }}</td>
                <td>{{ $sale_report['price'] }}</td>
                <td>{{ $sale_report['total_amount'] }}</td>
                <td>{{  \Carbon\Carbon::parse($sale_report['updated_at'])->format('Y-m-d') }}</td>
            </tr>
        @endforeach
    </tbody>

</table>
@else
<p style='text-align:center'>No data founded!!!</p>
@endif

{{-- <script>
    $(document).ready(function()
    {
        var sale_reports = @json($sale_reports);
        if (sale_reports.length == 0) {
            $('.print-data').hide();
            $('.export-btn').hide();
        }else{
            $('.print-data').show();
            $('.export-btn').show();
        }
    })
</script> --}}
