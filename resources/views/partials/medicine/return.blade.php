<div class="row" style="width:80vw;text-align: justify">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="col-md-12 p-0 m-0" style="width:75vw">
            <div class="col-md-12 mb-3 text-center">
                <h5 class="font-weight-bold"> Return Detail </h5>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                <td class="font-weight-bold">Product Category :</td>
                    @if($medicine->product->categories)
                    <td>
                        @foreach($medicine->product->categories as $product_category)
                            <span class="badge badge-pill badge-info">{{$product_category->name}}</span>
                        @endforeach
                    </td>
                    @endif
                </tr>
                <tr>
                    <td class="font-weight-bold">Product Size :</td>
                    <td>
                        {{$medicine->product->size}} {{$medicine->product->unit->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Shop Name:</td>
                    <td class="font-weight-bold">
                        {{$medicine->shop->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Contact Number:</td>
                    <td class="font-weight-bold">
                        {{$medicine->shop->phone}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Return Reason:</td>
                    <td class="font-weight-bold">
                        {{$medicine->description}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Remark By:</td>
                    <td class="font-weight-bold">
                        {{$medicine->user->name?? "-"}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Remark Description:</td>
                    <td class="font-weight-bold">
                        {{$medicine->remark}}
                    </td>
                </tr>
            </table>
        </div>
    </div>

</div>