<style>
    .table-detail td {
        text-align: center;
    }
</style>

<div class="row justify-content-center">
    <div class="col-md-10">
        <h6 class="text-success"><i class="las la-info-circle"></i> YOU CAN MODIFY GROUPED PRODUCTS HERE.</h6>
        <table class="table table-bordered table-detail col-md-6">
            <tr>
                <th class="font-weight-bold">Group Product Quantity</th>
                <th class="font-weight-bold" style="width:200px !important">
                    <div class="custom-control custom-checkbox">
                        <input type="number" min="0" step="1" max="10" data-id="{{ $id }}"
                            class="groupProductQty form-control" placeholder="Group Product Quantity"
                            value="{{ $quantity }}" />
                    </div>
                </th>
            </tr>
        </table>
        <table class="table table-bordered table-detail">
            <tr>
                <th class="font-weight-bold">No</th>
                <th class="font-weight-bold">Product ID</th>
                <th class="font-weight-bold">Product Name</th>
                <th class="font-weight-bold">Brand</th>
                <th class="font-weight-bold">Image</th>
                <th class="font-weight-bold">Unit</th>
                <th class="font-weight-bold">Quantity</th>
                <th class="font-weight-bold">Available Quantity</th>
            </tr>
            @if (count($group_products) > 0)
                @foreach ($group_products as $key => $group_product)
                    @php
                        $url = Storage::temporaryUrl($group_product->product->feature_image, now()->addMinutes(3));
                    @endphp
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $group_product->product->product_id }}</td>
                        <td>{{ $group_product->product->name }}</td>
                        <td>{{ $group_product->product->brand->name }}</td>
                        <td> <img src="{{ $url }}" alt="" width="80px" height="auto"> </td>
                        <td>{{ $group_product->product->unit->name }}</td>
                        @php
                            $available_qty = \App\Models\ProductBalance::where('product_id', $group_product->product_id)->sum('quantity');
                        @endphp
                        <td style="width:200px !important">
                            <div class="custom-control custom-checkbox">
                                {{-- <input type="number" min="0" step="1" max="{{ $available_qty / $quantity }}" --}}
                                <input type="number" min="0" step="1" max="{{ $available_qty }}"
                                    data-id="{{ $group_product->product_id }}" class="form-control productQty"
                                    placeholder="Product Quantity" value="{{ $group_product->quantity }}" />
                            </div>
                        </td>
                        <td id="available_qty{{ $group_product->product_id }}"
                            class="font-weight-bolder">
                            {{ $available_qty ? $available_qty : 0 }}</td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
</div>

<script type="text/javascript">
    $('.checkActive').change(function() {
        var is_selling = $(this).prop('checked');
        var pid = $(this).data("product");
        $.ajax({
            type: 'POST',
            url: "{{ route('selling_status') }}",
            data: {
                is_selling,
                pid
            },
        });
    });

    $('.groupProductQty').on('change', function() {
        var g_qty = $(this).val();
        var gpid = $(this).data('id');

        $.ajax({
            url: '{{ route('group.product.qty') }}',
            type: 'POST',
            async: false,
            data: {
                g_qty,
                gpid
            },
            success: function(response) {
                response.data.forEach(element => {
                    $('#available_qty' + element.pid).html(element.available);
                });
            },
            error: function(error) {
                swal({
                    title: "Sorry!",
                    text: error.responseJSON.message,
                    icon: 'error',
                    timer: 1500,
                    buttons: false,
                })
            }
        });
    });

    $('.productQty').on('change', function() {
        var p_qty = $(this).val();
        var pid = $(this).data('id');
        var gpid = $('.groupProductQty').data('id');

        $.ajax({
            url: '{{ route('product.qty') }}',
            type: 'POST',
            async: false,
            data: {
                p_qty,
                pid,
                gpid
            },
            success: function(response) {
                $('#available_qty' + response.data.pid).html(response.data.available);
                console.log(response);
            },
            error: function(error) {
                swal({
                    title: "Sorry!",
                    text: error.responseJSON.message,
                    icon: 'error',
                    timer: 1500,
                    buttons: false,
                })
            }
        });
    });
</script>
