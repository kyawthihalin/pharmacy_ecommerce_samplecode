<?php

use App\Models\OrderDetail;
use App\Models\ShippingAddress;
use App\Models\ProductBalance;

$order_details = OrderDetail::where('order_id', $entry->id)->get();
$strings = str_split($entry->remark, 100);
$final_string = "";
foreach ($strings as $string) {
    $final_string .= $string . " \n";
}
$final_string = nl2br($final_string);

$item_discount_all = 0;
$payment_bg = "";
$order_status_bg = "";
?>
@extends(backpack_view('blank'))

@section('content')
<link href="{{asset('package/select2/dist/css/select2.min.css')}}" rel="stylesheet">
<div class="row">
    <div class="col-md-12">
        <div class="row mt-5">
            <div class="col-md-10">
                <h4 class="text-dark "><i class="las la-info-circle"></i> Order Details Information For Invoice Number - {{$entry->order_no}}</h4>
                <h6 class="text-success"><i class="las la-info-circle"></i> YOU CAN MODIFY ORDER DETAIL HERE.</h6>
            </div>
            <div class="col-md-2 text-right pb-3">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addProductModal">
                    <i class="las la-plus-square"></i> Add New Product
                </button>
            </div>
        </div>

        <table class="table table-bordered bg-white text-center">
            <thead>
                <tr>
                    <th class="font-weight-bold">No</th>
                    <th class="font-weight-bold">Item Name</th>
                    <th class="font-weight-bold">SKU Code</th>
                    <th class="font-weight-bold">Item Price</th>
                    <th class="font-weight-bold">Item Unit</th>
                    <th class="font-weight-bold">Item Quantity</th>
                    <th class="font-weight-bold">Available Stock</th>
                    <th class="font-weight-bold">Remark</th>
                    <th class="font-weight-bold">Item Discount</th>
                    <th class="font-weight-bold">Total</th>
                    <th class="font-weight-bold">Action</th>
                </tr>
            </thead>
            @if(count($order_details) > 0)
            @foreach($order_details as $key => $order_detail)
            <tr>
                @php
                $item_discount_all += $order_detail->discount_amount;
                @endphp
                <td>{{$key+1}}</td>
                <td>{{$order_detail->product->name}}</td>
                <td>{{$order_detail->product->sku_code}}</td>
                @php
                    $price = $order_detail->product->price ? $order_detail->product->price : 0;
                    // info($order_detail->product->productBalance);
                    // $quantity = $order_detail->product->productBalance ? ProductBalance::where('product_id', $order_detail->product_id)->where('price', $order_detail->purchase_price)->sum('quantity') : 0;
                    $quantity = ProductBalance::where('product_id', $order_detail->product_id)->sum('quantity');

                @endphp
                <td>{{number_format($price)}} Kyats</td>
                <td>{{$order_detail->product->unit->name}}</td>
                <td style="width:200px !important">
                    <div class="custom-control custom-checkbox">
                        <input type="number" min="0" step="1" max="{{$quantity}}"  data-id="{{$order_detail->id}}" class="form-control qty quantity" placeholder="Item Quantity" value="{{$order_detail->quantity}}" />
                    </div>
                </td>
                <td class="@if($quantity > $order_detail->product->warning_count) text-success @else text-danger @endif font-weight-bold">{{$quantity}}</td>
                <td>{{$order_detail->remark ?  $order_detail->remark  : '-'}}</td>
                <td class="text-danger font-weight-bold">
                    <p id="order_discount{{$order_detail->id}}"> {{ number_format($order_detail->discount_amount)}} MMK</p>
                </td>
                <td class="text-success font-weight-bold">
                    @if($order_detail->quantity == 0)
                    <p id="order_total{{$order_detail->id}}">0 MMK</p>
                    @else
                    <p id="order_total{{$order_detail->id}}">{{ number_format($order_detail->amount - $order_detail->discount_amount)}} MMK</p>
                    @endif
                </td>
                <td class="text-center">
                    <a href="#" class="remove" data-id="{{$order_detail->id}}"><i class="las la-times text-danger"></i></a>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan='10'>
                    <span class="text-info">There is no products in the order! <br> Please Add New Items.</span>
                </td>
            </tr>
            @endif
            <tfoot>
                <tr>
                    <th class="font-weight-bold">No</th>
                    <th class="font-weight-bold">Item Name</th>
                    <th class="font-weight-bold">Item Code</th>
                    <th class="font-weight-bold">Item Price</th>
                    <th class="font-weight-bold">Item Quantity</th>
                    <th class="font-weight-bold">Available Stock</th>
                    <th class="font-weight-bold">Remark</th>
                    <th class="font-weight-bold">Item Discount</th>
                    <th class="font-weight-bold">Total</th>
                    <th class="font-weight-bold">Action</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

@endsection
<div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-success" id="exampleModalLongTitle"><i class="las la-plus-square"></i>  Add New Products</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <p class="text-info font-weight-bold"> <i class="las la-info-circle"></i> Click in the textbox to select products!</p>
                    <select id="add" class="form-control add-product" name="state" style="width: 100%;height:auto" multiple required data-placeholder="Choose Products">
                    </select>
                </div>
                <input type="hidden" name="eid" id="oid" value="{{$entry->id}}">
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn " data-dismiss="modal"> <i class="las la-times-circle"></i> Cancle</button>
            <button type="submit" id="add_order" class="btn btn-info"> <i class="las la-plus-square"></i>  Add to Order List</button>
        </div>
        </div>
    </div>
</div>
@section('after_scripts')
<script src="{{asset('package/select2/dist/js/select2.min.js')}}"></script>

<script>
    $(document).ready(function() {
        $('#add_order').click(function(){
            var product_id = $('#add').val();
            var oid = $('#oid').val();
            console.log(oid)
            $.ajax({
                url: '{{route('add.products')}}',
                type: 'POST',
                async: false,
                data: {
                    product_id,
                    oid
                },
                success: function(responese) {
                   window.location.reload();
                },
                error: function(response)
                {
                    alert("Fail to add Product!");
                }
            });
        })
        $('.add-product').select2({
            width: 'resolve',
            height: 'resolve',
            dropdownParent: $('#addProductModal'),
            ajax: {
                url: '{{route('get.products')}}',
                dataType: 'json',
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1,
                    }
                },
                cache: true
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.quantity').on('change', function() {

            var qty = $(this).val();
            var odid = $(this).data('id');

            $.ajax({
                url: '{{route('update.order')}}',
                type: 'POST',
                async: false,
                data: {
                    qty,
                    odid
                },
                success: function(responese) {
                    $('#order_discount'+odid).text(responese.data.discount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " MMK");
                    $('#order_total'+odid).text(responese.data.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " MMK");
                },
                error: function(error)
                {
                    swal({
                        title: "Sorry!",
                        text: error.responseJSON.message,
                        icon: 'error',
                        timer: 1500,
                        buttons: false,
                    })
                }
            });
        })

        $('.remove').on('click', function() {
            var odid = $(this).data('id');
            swal({
                title: "Are you sure,You want to cancel this product?",
                text: "You will not be able to recover this record!",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: '{{route('order.remove')}}',
                        type: 'POST',
                        async: false,
                        data: {
                            odid
                        },
                        success: function(responese) {
                            swal({
                                title: 'Removed!',
                                text: 'Product is successfully removed from order!',
                                icon: 'success'
                            }).then(function() {
                                window.location.reload();
                            });
                        },
                    });

                } else {
                    swal("Cancelled", "Your product is safe :)", "error");
                }
            })
        })

    })
</script>
@endsection
