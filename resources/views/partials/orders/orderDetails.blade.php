<?php

use App\Models\ShippingAddress;
use App\Models\OrderDetail;

?>
@if($order != null)
@php
$order_details = OrderDetail::where('order_id',$order->id)->get();
$strings = str_split($order->remark,100);
$final_string = "";
foreach($strings as $string)
{
$final_string .= $string." \n";
}
$final_string = nl2br($final_string);

$item_discount_all= 0;
$payment_bg="";
$order_status_bg="";
$total = 0;
$grand_total = 0;
$net_amount = 0;
@endphp

<style>
    table {
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;
        border: 1px solid #ddd;
    }

    th,
    td {
        text-align: left;
        padding: 16px;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2;
    }
</style>
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<div class="row" style="width:80vw;text-align: justify">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="col-md-12 p-0 m-0" style="width:75vw">
            <div class="col-md-12 mb-3 text-center">
                <h5 class="font-weight-bold"> Order Informations </h5>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td class="font-weight-bold">Order No:</td>
                    <td class="font-weight-bold">
                        {{$order->order_no}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">ဆိုင်အမည် :</td>
                    <td class="font-weight-bold">
                        {{$order->shop->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">လိပ်စာ :</td>
                    <td class="font-weight-bold">
                        {{$order->shipping_address->ship_address}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">မြိုနယ် :</td>
                    <td class="font-weight-bold">
                        {{$order->shipping_address->township->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">မြိုအမည် :</td>
                    <td class="font-weight-bold">
                        {{$order->shipping_address->city->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">ဖုန်းနံပါတ် :</td>
                    <td class="font-weight-bold">
                        {{$order->shipping_address->phone_number}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">ပို့ဆောင်ခ :</td>
                    <td class="font-weight-bold">
                        {{$order->shipping_address->township->shipping_fees ? $order->shipping_address->township->shipping_fees : 0}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">ငွေလက်ခံရရှိမှုအခြေအနေ :</td>
                    <td class="font-weight-bold">
                        <div class="row">
                            <div class="col-md-4">


                                @if($order->payment_type_status ==="cash_down")
                                    @php
                                    if($order->payment_status === "Completed"){
                                    $payment_bg = "bg-success";
                                    }elseif($order->payment_status === "Pending"){
                                    $payment_bg = "bg-warning";
                                    }else{
                                    $payment_bg = "bg-danger";
                                    }
                                    $payemnt_disable_status = "";
                                    if($order->payment_status === 'Rejected' || $order->payment_status === 'Completed')
                                    {
                                    $payemnt_disable_status = "disabled";
                                    }

                                    @endphp
                                    <input type="hidden" name="" id="oid" value="{{$order->id}}">
                                    @if(backpack_user()->can('change payment status'))
                                    <select class="custom-select" id="change_status" {{$payemnt_disable_status}} style="width:300px">
                                        <option value="Completed" @if($order->payment_status==="Completed") selected @endif>Completed</option>
                                        <option value="Pending" @if($order->payment_status==="Pending") selected @endif>Pending</option>
                                        <option value="Rejected" @if($order->payment_status==="Rejected") selected @endif>Rejected</option>
                                    </select>
                                    @else
                                    <select class="custom-select" id="change_status" disabled style="width:300px">
                                        <option value="Completed" @if($order->payment_status==="Completed") selected @endif>Completed</option>
                                        <option value="Pending" @if($order->payment_status==="Pending") selected @endif>Pending</option>
                                        <option value="Rejected" @if($order->payment_status==="Rejected") selected @endif>Rejected</option>
                                    </select>
                                    @endif

                                @else
                                    @php
                                    if($order->payment_status === "Completed"){
                                    $payment_bg = "bg-success";
                                    }elseif($order->payment_status === "Pending"){
                                    $payment_bg = "bg-warning";
                                    }else{
                                    $payment_bg = "bg-danger";
                                    }
                                    $payemnt_disable_status = "";
                                    if($order->payment_status === 'Rejected' || $order->payment_status === 'Completed'  || $order->shipping_status != 'Completed')
                                    {
                                    $payemnt_disable_status = "disabled";
                                    }
                                    @endphp
                                    <input type="hidden" name="" id="oid" value="{{$order->id}}">
                                    @if(backpack_user()->can('change payment status'))
                                    <select class="custom-select" id="change_status_cod" {{$payemnt_disable_status}} style="width:300px">
                                        <option value="Completed" @if($order->payment_status==="Completed") selected @endif>Completed</option>
                                        <option value="Pending" @if($order->payment_status==="Pending") selected @endif>Pending</option>
                                        <option value="Rejected" @if($order->payment_status==="Rejected") selected @endif>Rejected</option>

                                    </select>
                                    @else
                                    <select class="custom-select" id="change_status_cod" disabled style="width:300px">
                                        <option value="Completed" @if($order->payment_status==="Completed") selected @endif>Completed</option>
                                        <option value="Pending" @if($order->payment_status==="Pending") selected @endif>Pending</option>
                                        <option value="Rejected" @if($order->payment_status==="Rejected") selected @endif>Rejected</option>

                                    </select>
                                    @endif
                                @endif


                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Order အခြေအနေ :</td>
                    <td class="font-weight-bold">
                        <div class="row">
                            <div class="col-md-4">
                                @if($order->payment_type_status ==="cash_down")
                                    @php
                                    if($order->shipping_status === "Completed"){
                                    $order_status_bg = "bg-success";
                                    }elseif($order->shipping_status === "Pending" || $order->shipping_status === "Processing"){
                                    $order_status_bg = "bg-warning";
                                    }elseif($order->shipping_status === "Confirmed" || $order->shipping_status === "Delivering" || $order->shipping_status === "Going to Bus Gate"){
                                    $order_status_bg = "bg-primary";
                                    }else{
                                    $order_status_bg = "bg-danger";
                                    }
                                    $disable_status = "";
                                    if($order->payment_status === 'Rejected' || $order->payment_status === 'Pending' || $order->shipping_status === "Completed")
                                    {
                                    $disable_status = "disabled";
                                    }
                                    @endphp
                                    @if(backpack_user()->can('change shipping status'))
                                        @if($order->shipping_status==="Processing")
                                        <select class="custom-select" id="order_status" {{$disable_status}} style="width:300px">
                                            <option value="Processing" @if($order->shipping_status==="Processing") selected @endif>Processing</option>
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                        </select>
                                        @elseif($order->shipping_status==="Confirmed")
                                        <select class="custom-select" id="order_status" {{$disable_status}} style="width:300px">
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                        </select>
                                        @elseif($order->shipping_status==="Delivering" || $order->shipping_status==="Going to Bus Gate")
                                        <select class="custom-select" id="order_status" {{$disable_status}} style="width:300px">
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                            <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option>
                                        </select>
                                        @elseif($order->shipping_status==="Pending" || $order->shipping_status==="Rejected" || $order->shipping_status==="Completed" )
                                        <select class="custom-select" id="order_status" disabled style="width:300px">
                                            <option value="Pending" @if($order->shipping_status==="Pending") selected @endif>Pending</option>
                                            <option value="Processing" @if($order->shipping_status==="Processing") selected @endif>Processing</option>
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                            <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option>
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                        </select>
                                        @endif
                                    @else
                                        <select class="custom-select" id="order_status" disabled style="width:300px">
                                            <option value="Pending" @if($order->shipping_status==="Pending") selected @endif>Pending</option>
                                            <option value="Processing" @if($order->shipping_status==="Processing") selected @endif>Processing</option>
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                            <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option>
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                        </select>
                                    @endif

                                @else
                                    @php
                                    if($order->shipping_status === "Completed"){
                                    $order_status_bg = "bg-success";
                                    }elseif($order->shipping_status === "Pending"){
                                    $order_status_bg = "bg-warning";
                                    }elseif($order->shipping_status === "Confirmed" || $order->shipping_status === "Delivering" || $order->shipping_status === "Going to Bus Gate"){
                                    $order_status_bg = "bg-primary";
                                    }else{
                                    $order_status_bg = "bg-danger";
                                    }
                                    $disable_status = "";
                                    if($order->payment_status === 'Rejected'  || $order->shipping_status === "Completed")
                                    {
                                    $disable_status = "disabled";
                                    }
                                    @endphp
                                    @if(backpack_user()->can('change shipping status'))
                                        @if($order->shipping_status==="Confirmed")
                                        <select class="custom-select" id="order_status_cod" {{$disable_status}} style="width:300px">
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                        </select>
                                        @elseif($order->shipping_status==="Delivering" || $order->shipping_status==="Going to Bus Gate")
                                        <select class="custom-select" id="order_status_cod" {{$disable_status}} style="width:300px">
                                            <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option>
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                        </select>
                                        @elseif($order->shipping_status==="Pending" )
                                        <select class="custom-select" id="order_status_cod" {{$disable_status}} style="width:300px">
                                            <option value="Pending" @if($order->shipping_status==="Pending") selected @endif>Pending</option>
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            {{-- <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option> --}}
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                            {{-- <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option> --}}
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                        </select>
                                        @elseif($order->shipping_status==="Completed" )
                                            <select class="custom-select" id="order_status_cod" disabled style="width:300px">
                                                <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option>
                                            </select>
                                        @elseif($order->shipping_status==="Rejected" )
                                            <select class="custom-select" id="order_status_cod" disabled style="width:300px">
                                                <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                            </select>
                                        @endif
                                    @else
                                        <select class="custom-select" id="order_status_cod" disabled style="width:300px">
                                            <option value="Pending" @if($order->shipping_status==="Pending") selected @endif>Pending</option>
                                            <option value="Processing" @if($order->shipping_status==="Processing") selected @endif>Processing</option>
                                            <option value="Confirmed" @if($order->shipping_status==="Confirmed") selected @endif>Confirmed</option>
                                            <option value="Delivering" @if($order->shipping_status==="Delivering") selected @endif>Delivering</option>
                                            {{--<option value="Going to Bus Gate" @if($order->shipping_status==="Going to Bus Gate") selected @endif>Going to Bus Gate</option>--}}
                                            <option value="Completed" @if($order->shipping_status==="Completed") selected @endif>Completed</option>
                                            <option value="Rejected" @if($order->shipping_status==="Rejected") selected @endif>Rejected</option>
                                        </select>
                                    @endif
                                @endif

                            </div>
                        </div>
                    </td>
                </tr>
                @if($order->shipping_status === "Rejected" && $order->payment_status === "Rejected")
                <tr>
                    <td class="font-weight-bold">Rejected Reason : </td>
                    <td class="font-weight-bold">
                        {{$order->reject_remark ? $order->reject_remark : "-"}}
                    </td>
                </tr>
                @endif
            </table>
            <div id="items">
                <table class="table table-bordered">
                    <tr>
                        <th class="font-weight-bold">No</th>
                        <th class="font-weight-bold">Item Name</th>
                        <th class="font-weight-bold">Item Code</th>
                        <th class="font-weight-bold">Item Price</th>
                        <th class="font-weight-bold">Item Quantity</th>
                        <th class="font-weight-bold">Item Discount</th>
                        <th class="font-weight-bold">Total</th>
                    </tr>
                    @if(count($order_details) > 0)
                    @foreach($order_details as $key => $order_detail)
                    <tr>
                        @php
                        $item_discount_all += $order_detail->discount_amount;
                        $check_qty = $order_detail->quantity;
                        if($check_qty == 0)
                        {
                            $check_qty = 1;
                        }
                        @endphp
                        <td>{{$key+1}}</td>
                        <td>{{$order_detail->product->name}}</td>
                        <td>{{$order_detail->product->product_id}}</td>
                        <td>{{number_format($order_detail->amount / $check_qty) }} MMK</td>
                        <td>{{$order_detail->quantity}}</td>
                        <td class="text-danger font-weight-bold">{{$order_detail->discount_amount}} MMK</td>
                        @if($order_detail->quantity == 0)
                        <td class="text-success font-weight-bold">0 MMK</td>
                        @else
                        <td class="text-success font-weight-bold">{{ number_format($order_detail->amount - $order_detail->discount_amount)}} MMK</td>
                        @endif
                    </tr>
                    @endforeach
                    @endif
                    <tr>
                        <td rowspan="6" colspan="5" class="text-success font-weight-bold text-center">{!! $qr_code !!}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold text-right">Total</td>
                        <td class="text-success font-weight-bold">{{number_format(bcsub($order->total_amount,$order->discount_amount))}} MMK</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold text-right">Coupon Discount</td>
                        <td class="text-danger font-weight-bold">{{number_format($order->coupon_discount)}} MMK</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold text-right">Grand Total</td>
                        <td class="text-success font-weight-bold">{{number_format($order->grand_total)}} MMK</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold text-right">Shipping Fee</td>
                        <td class="text-success font-weight-bold">{{number_format($order->shipping_fees)}} MMK</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold text-right">Net Amount</td>
                        <td class="text-success font-weight-bold">{{number_format($order->net_amount)}} MMK</td>
                    </tr>
                </table>
            </div>
            <div class="text-right mb-2 mr-1">
                <button class="btn btn-success text-right" onclick="printDiv('items')"><i class='las la-print'></i> Print Report</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#change_status").change(function() {
            var status = $(this).val();
            var oid = $('#oid').val();
            if(status === "Rejected")
            {
                swal("Enter Reject Reason:", {
                    content: "input",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then((value) => {
                    if(!value)
                    {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }else
                    {
                        var remark = value;
                        $.ajax({
                            url: '{{route('change.payment.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid,
                                remark
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
            }else
            {
                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.payment.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        });

        $("#change_status_cod").change(function() {

            var status = $(this).val();
            var oid = $('#oid').val();
            if(status === "Rejected")
            {
                swal("Enter Reject Reason:", {
                    content: "input",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then((value) => {
                    if(!value)
                    {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }else
                    {
                        var remark = value;
                        $.ajax({
                            url: '{{route('change.payment.status.cod')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid,
                                remark
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
            }else
            {
                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.payment.status.cod')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        });


        $("#order_status").change(function() {
            var status = $(this).val();
            var oid = $('#oid').val();
            if(status === "Rejected")
            {
                swal("Enter Reject Reason:", {
                    content: "input",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then((value) => {
                    if(!value)
                    {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }else
                    {
                        var remark = value;
                        $.ajax({
                            url: '{{route('change.shipping.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid,
                                remark
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
            }else{
                swal({
                title: "Change Status?",
                text: "Are you sure,You want to Change Status?",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: '{{route('change.shipping.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid
                            },
                            success: function(response) {
                                swal({
                                    title: 'Updated!',
                                    text: response.message,
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 5000,
                                    buttons: true,
                                }).then(function() {
                                    window.location.reload();
                                })
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }
        });

        $("#order_status_cod").change(function() {

            var status = $(this).val();
            var oid = $('#oid').val();
            if(status === "Rejected")
            {
                swal("Enter Reject Reason:", {
                    content: "input",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then((value) => {
                    if(!value)
                    {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }else
                    {
                        var remark = value;
                        $.ajax({
                            url: '{{route('change.shipping.status.cod')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid,
                                remark
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
            }else{
                swal({
                title: "Change Status?",
                text: "Are you sure,You want to Change Status?",
                icon: "warning",
                buttons: [
                    'No, cancel it!',
                    'Yes, I am sure!'
                ],
                dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {

                        $.ajax({
                            url: '{{route('change.shipping.status.cod')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid
                            },
                            success: function(response) {
                                swal({
                                    title: 'Updated!',
                                    text: response.message,
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 5000,
                                    buttons: true,
                                }).then(function() {
                                    window.location.reload();
                                })
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }
        });
    })

    function printDiv(divId) {

        var divToPrint = document.getElementById(divId);

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

        newWin.document.close();

        setTimeout(function() {
            newWin.close();
        }, 10);

    }
</script>
@endif
