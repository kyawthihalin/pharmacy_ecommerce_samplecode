<div class="product-balance"></div>

@push('crud_fields_scripts')
    <script>
        $('#purchase_order_id').on('change', productBalance);
        $('#sku_code_id').on('change', productBalance);

        function productBalance() {
            var purchaseOrderId = $('[name="purchase_order_id"]').val();
            var skuCodeId = $('[name="sku_code"]').val();

            $.ajax({
                url: '{{ route('product_balance_data') }}',
                type: 'POST',
                async: false,
                data: {
                    purchase_order_id: purchaseOrderId,
                    sku_code_id: skuCodeId,
                },
                success: function(res) {
                    $('.product-balance').html(res);
                },
                error: function(res) {
                    alert(res.responseJSON.message);
                }
            })
        }
    </script>
@endpush
