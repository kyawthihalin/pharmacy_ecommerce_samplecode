<style>
    .table-detail td {
        text-align: center;
    }
</style>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<div class="row justify-content-center">
    <div class="col-md-10">
        <table class="table table-bordered table-detail">
            <tr>
                <th class="font-weight-bold">No</th>
                <th class="font-weight-bold">Product ID</th>
                <th class="font-weight-bold">Product Name</th>
                <th class="font-weight-bold">Brand</th>
                <th class="font-weight-bold">Image</th>
                <th class="font-weight-bold">Unit</th>
                <th class="font-weight-bold">Quantity</th>
                <th class="font-weight-bold">Selling Status</th>
            </tr>
            @if (count($products) > 0)
                @foreach ($products as $key => $product)
                    @php
                        $url = Storage::temporaryUrl($product->feature_image, now()->addMinutes(3));
                    @endphp
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $product->product_id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->brand->name }}</td>
                        <td> <img src="{{ $url }}" alt="" width="80px" height="auto"> </td>
                        <td>{{ $product->unit->name }}</td>
                        <td>{{ $product->quantity }}</td>
                        <td>
                            <form>
                                @if ($product->is_selling == 1)
                                    <input type="checkbox" id="inactive-event" data-product="{{ $product->id }}"
                                        class="checkActive" data-size="xs" checked data-toggle="toggle" data-on="Active"
                                        data-off="Inactive" data-onstyle="success" data-offstyle="danger">
                                @else
                                    <input type="checkbox" id="inactive-event" data-product="{{ $product->id }}"
                                        class="checkActive" data-size="xs" data-toggle="toggle" data-on="Active"
                                        data-off="Inactive" data-onstyle="success" data-offstyle="danger">
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
    </div>
</div>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<script type="text/javascript">
        $('.checkActive').change(function() {
            var is_selling = $(this).prop('checked');
            var pid = $(this).data("product");
            $.ajax({
                type: 'POST',
                url: "{{ route('selling_status') }}",
                data: {
                    is_selling,
                    pid
                },
            });
        })
</script>
