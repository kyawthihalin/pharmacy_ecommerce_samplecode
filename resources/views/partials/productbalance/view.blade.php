<style>
    :root {
        --white: #fff;
        --darkblue: #1b4965;
        --lightblue: #edf2f4;
    }

    .table-report {
        margin-top: 20px;
        width: 100%;
        background-color: var(--white);
        /* padding: 20px; */
        /* -webkit-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        -moz-box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48);
        box-shadow: 0px 6px 26px -6px rgba(0, 0, 0, 0.48); */
        border-radius: 5px;
        /* overflow: scroll; */
    }

    table {
        width: 100%;
    }

    table th {
        color: var(--white);
        background: var(--darkblue);
        padding: 10px;
        /* min-width: 75px; */
    }

    table tbody tr.data {
        text-align: center;
    }

    table tbody tr.data:nth-of-type(even) {
        background: var(--lightblue);
    }

    th {
        text-align: center;
        padding: 5px;
    }

    tr:not(.title) td {
        padding: 5px;
    }

    .title h3 {
        font-weight: bold;
        font-size: 16px;
        margin: 0;
        padding: 10px;
        /* margin: 0 0 0 15px; */
        background: #1b252b;
        color: var(--white);
        /* text-align: center; */
    }

    td{
        text-align: center;
        padding: 10px;
    }

    th{
        text-align: center;
    }
</style>

@if (isset($product_balances) && count($product_balances) > 0)
    <div class="col-md-12 table-report">
        @if (isset($unit_relationships) && count($unit_relationships) > 0)
            <h3 class="title">Unit Relationship</h3>
            <table class="table-bordered">
                <thead>
                    <th>No</th>
                    <th>From Unit</th>
                    <th>To Unit</th>
                    <th>Quantity</th>
                </thead>
                <tbody>
                    @php
                        $count = 1;
                    @endphp
                    @foreach ($unit_relationships as $unit_relationship)
                        <tr style='text-align:center'>
                            <td>{{ $count++ }}</td>
                            <td>{{ $unit_relationship->baseUnit->name }}</td>
                            <td>{{ $unit_relationship->conversionUnit->name }}</td>
                            <td>{{ $unit_relationship->conversion_factor }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
        <h3 class="title" style="margin-top: 20px">Product Balances</h3>
        <table class="table-bordered">
            <thead>
                <th>No</th>
                <th>Invoice No</th>
                <th>SKU Code</th>
                <th>Product Name</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Purchase Price</th>
                <th>Selling Price</th>
            </thead>
            <tbody>
                @php
                    $count = 1;
                @endphp
                @foreach ($product_balances as $product_balance)
                    <tr style='text-align:center'>
                        <td>{{ $count++ }}</td>
                        <td>{{ $product_balance->purchaseOrder ? $product_balance->purchaseOrder->invoice_no : '-' }}</td>
                        <td>{{ $product_balance->skuCode->code }}</td>
                        <td>{{ $product_balance->product->name }}</td>
                        <td>{{ $product_balance->product->unit->name }}</td>
                        <td>{{ $product_balance->quantity }}</td>
                        <td>{{ $product_balance->price }}</td>
                        <td>{{ $product_balance->product->price }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endif
