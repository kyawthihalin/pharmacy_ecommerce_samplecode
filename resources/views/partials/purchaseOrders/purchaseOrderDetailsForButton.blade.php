<?php

use App\Models\PurchaseOrderDetail;

?>

@php
$purchase_order_details = PurchaseOrderDetail::where('purchase_order_id',$todayPurchaseOrder->id)->get();

@endphp

<div class="row" style="width:80vw;text-align: justify">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="col-md-12 p-0 m-0" style="width:75vw">
            <div class="col-md-12 mb-3 text-center">
                <h5 class="font-weight-bold"> Purchase Order Informations </h5>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td class="font-weight-bold">Invoice No :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->invoice_no}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">User :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->user->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Supplier :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->supplier->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Order No :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->order_no}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Total Amount :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->total_amount}}
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Date :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->date}}
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Order Status :</td>
                    <td class="font-weight-bold">
                        @php
                            $color = "";
                            if ($todayPurchaseOrder->order_status == "Pending") {
                                $color = "badge-warning";
                            }
                            if ($todayPurchaseOrder->order_status == "Arrived") {
                                $color = "badge-success";
                            }
                            if ($todayPurchaseOrder->order_status == "Cancelled") {
                                $color = "badge-danger";
                            }
                        @endphp
                        <span class="badge  {{$color}} badge-pill"> {{$todayPurchaseOrder->order_status}} </span>
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Payment Status :</td>
                    <td class="font-weight-bold">
                        @php
                            $payment_color = "";
                            if ($todayPurchaseOrder->payment_status == "Pending") {
                                $payment_color = "badge-warning";
                            }
                            if ($todayPurchaseOrder->payment_status == "Completed") {
                                $payment_color = "badge-success";
                            }
                            if ($todayPurchaseOrder->payment_status == "Rejected") {
                                $payment_color = "badge-danger";
                            }
                            if ($todayPurchaseOrder->payment_status == "Credit Completed") {
                                $payment_color = "badge-info";
                            }
                        @endphp
                        <span class="badge {{$payment_color}} badge-pill">{{$todayPurchaseOrder->payment_status}}</span>

                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Remark :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->remark}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

