<?php

use App\Models\PurchaseOrderDetail;

?>

@php
$purchase_order_details = PurchaseOrderDetail::where('purchase_order_id',$todayPurchaseOrder->id)->get();

@endphp

<div class="row" style="width:80vw;text-align: justify">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="col-md-12 p-0 m-0" style="width:75vw">
            <div class="col-md-12 mb-3 text-center">
                <h5 class="font-weight-bold"> Purchase Order Informations </h5>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td class="font-weight-bold">Invoice No :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->invoice_no}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">User :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->user->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Supplier :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->supplier->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Order No :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->order_no}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Total Amount :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->total_amount}}
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Date :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->date}}
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Order Status :</td>
                    <td class="font-weight-bold">
                        <div class="row">
                            <div class="col-md-4">
                                @php
                                if($todayPurchaseOrder->order_status === "Arrived"){
                                $payment_bg = "bg-success";
                                }elseif($todayPurchaseOrder->order_status === "Pending"){
                                $payment_bg = "bg-warning";
                                }else{
                                $payment_bg = "bg-danger";
                                }
                                $order_disable_status = '';
                                if($todayPurchaseOrder->order_status === 'Cancelled' || $todayPurchaseOrder->order_status === 'Arrived' || $todayPurchaseOrder->payment_status === 'Rejected' )
                                {
                                $order_disable_status = 'disabled';
                                }
                                @endphp
                                <input type="hidden" name="" id="oid" value="{{$todayPurchaseOrder->id}}">
                                <select class="custom-select" id="change_order_status" {{$order_disable_status}}  style="width:300px">
                                    <option value="Arrived" @if($todayPurchaseOrder->order_status==="Arrived") selected @endif>Arrived</option>
                                    <option value="Pending" @if($todayPurchaseOrder->order_status==="Pending") selected @endif>Pending</option>
                                    <option value="Cancelled" @if($todayPurchaseOrder->order_status==="Cancelled") selected @endif>Cancelled</option>
                                </select>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Payment Status :</td>
                    {{-- <td class="font-weight-bold">
                        {{$todayPurchaseOrder->payment_status}}
                    </td> --}}
                    <td class="font-weight-bold">
                        <div class="row">
                            <div class="col-md-4">
                                @php
                                if($todayPurchaseOrder->payment_status === "Completed"){
                                $payment_bg = "bg-success";
                                }elseif($todayPurchaseOrder->payment_status === "Pending"){
                                $payment_bg = "bg-warning";
                                }else{
                                $payment_bg = "bg-danger";
                                }
                                $order_disable_status = '';
                                if($todayPurchaseOrder->payment_status === 'Rejected' || $todayPurchaseOrder->payment_status === 'Completed' || $todayPurchaseOrder->order_status === 'Cancelled')
                                {
                                $order_disable_status = 'disabled';
                                }
                                @endphp
                                <input type="hidden" name="" id="oid" value="{{$todayPurchaseOrder->id}}">
                                <select class="custom-select" id="change_payment_status" {{$order_disable_status}}  style="width:300px">
                                    <option value="Completed" @if($todayPurchaseOrder->payment_status==="Completed") selected @endif>Completed</option>
                                    <option value="Pending" @if($todayPurchaseOrder->payment_status==="Pending") selected @endif>Pending</option>
                                    <option value="Rejected" @if($todayPurchaseOrder->payment_status==="Rejected") selected @endif>Rejected</option>
                                    <option value="Credit Completed" @if($todayPurchaseOrder->payment_status==="Credit Completed") selected @endif>Credit Completed</option>
                                </select>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Payment Method :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->payment_method}}
                    </td>
                </tr>

                <tr>
                    <td class="font-weight-bold">Remark :</td>
                    <td class="font-weight-bold">
                        {{$todayPurchaseOrder->remark}}
                    </td>
                </tr>
            </table>


            {{-- <div id="items">
                <div class="col-md-12 mb-3 text-center">
                    <h5 class="font-weight-bold"> Product List </h5>
                </div>
                <table class="table table-bordered">
                    <tr>
                        <th class="font-weight-bold">No</th>
                        <th class="font-weight-bold">Product Name</th>
                        <th class="font-weight-bold">Item Name</th>
                        <th class="font-weight-bold">Item Quantity</th>
                        <th class="font-weight-bold">Item Price</th>
                    </tr>
                    @if(count($purchase_order_details) > 0)
                    @foreach($purchase_order_details as $key => $purchase_order_detail)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$purchase_order_detail->product->name}}</td>
                        <td>{{$purchase_order_detail->name}}</td>
                        <td>{{$purchase_order_detail->quantity}}</td>
                        <td>{{$purchase_order_detail->price}} MMK</td>
                    </tr>
                    @endforeach
                    @endif
                </table>
            </div> --}}


        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#change_order_status").change(function() {
            var status = $(this).val();
            var oid = $('#oid').val();
            if(status === "Cancelled")
            {
                swal("Enter Cancel Reason:", {
                    content: "input",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then((value) => {
                    if(!value)
                    {
                        swal("Cancelled", "Your purchase order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }else
                    {
                        var remark = value;
                        $.ajax({
                            url: '{{route('change.purchase.order.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid,
                                remark
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
            }else
            {
                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.purchase.order.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Purchase Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        });

        $("#change_payment_status").change(function() {
            var status = $(this).val();
            var oid = $('#oid').val();
            if(status === "Rejected")
            {
                swal("Enter rejecte Reason:", {
                    content: "input",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then((value) => {
                    if(!value)
                    {
                        swal("Cancelled", "Your purchase order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }else
                    {
                        var remark = value;
                        $.ajax({
                            url: '{{route('change.purchase.payment.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid,
                                remark
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                });
            }else
            {
                swal({
                    title: "Change Status?",
                    text: "Are you sure,You want to Change Status?",
                    icon: "warning",
                    buttons: [
                        'No, cancel it!',
                        'Yes, I am sure!'
                    ],
                    dangerMode: true,
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '{{route('change.purchase.payment.status')}}',
                            type: 'POST',
                            async: false,
                            data: {
                                status,
                                oid
                            },
                            success: function(responese) {
                                swal({
                                    title: 'Updated!',
                                    text: 'Purchase Order is successfully Updated!',
                                    icon: 'success'
                                }).then(function() {
                                    window.location.reload();
                                });
                            },
                            error: function(response) {
                                swal({
                                    title: "Sorry!",
                                    text: response.responseJSON.message,
                                    icon: 'error',
                                    timer: 1500,
                                    buttons: false,
                                }).then(function() {
                                    window.location.reload();
                                });
                            }
                        });

                    } else {
                        swal("Cancelled", "Your order got no changes :)", "error").then(function() {
                            window.location.reload();
                        });
                    }
                })
            }

        });

    })
</script>
