@extends(backpack_view('blank'))

@php
    $defaultBreadcrumbs = [
        trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
        $crud->entity_name_plural => url($crud->route),
        trans('backpack::crud.add') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp
@section('header')
    <section class="container-fluid">
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add') . ' ' . $crud->entity_name !!}.</small>

            @if ($crud->hasAccess('list'))
                <small><a href="{{ url($crud->route) }}" class="d-print-none font-sm"><i
                            class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i>
                        {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <div class="row" style="width:70%;margin-left:0">
        <form method="POST" action="{{ route('purchase.order.import.store') }}" enctype="multipart/form-data">
            @if ($errors->any())
                <div class="alert alert-danger pb-0">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li><i class="la la-info-circle"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! csrf_field() !!}
            <div class="card">
                <div class="card-body row">
                    <div class="col-md-12 form-group">
                        <label for="">
                            Supplier
                        </label>
                        <select name="supplier" id="supplier" class="form-control js-example-basic-single">
                            <option disabled> - </option>
                            @if (count($suppliers) > 0)
                                @foreach ($suppliers as $supplier)
                                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('supplier')
                            <div class="text-red-500" style="color:#DF4759">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-12 form-group">
                        <label for="">
                            Invoice no
                        </label>
                        <div>
                            <input type="text" class="form-control" id="invoice_no" name="invoice_no"
                                style="width:100%;padding-right:20px" value="{{ old('invoice_no') }}">
                        </div>
                        @error('invoice_no')
                            <div class="text-red-500" style="color:#DF4759">{{ $message }}</div>
                        @enderror

                    </div>

                    <div class="col-md-12 form-group">
                        <label for="remark">Remark</label>
                        <div>
                            <textarea id="remark" class="form-control" name="remark" rows="4" cols="50" style="width:100%;">
                        </textarea>
                        </div>
                    </div>

                    <div class="col-md-6 form-group">
                        <label for="date">Date</label>
                        <div>
                            <input type="date" class="form-control" id="date" name="date" style="width:100%;"
                                value="{{ old('date') }}">
                            @error('date')
                                <div class="text-red-500" style="color:#DF4759">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-6 form-group" style="margin-top:30px">
                        <label class="custom-file-label" for="validatedCustomFile" id="label">Choose excel
                            file...</label>
                        <div>
                            <input type="file" id="excel" name="excel" class="custom-file-input"
                                id="validatedCustomFile" required>
                        </div>
                    </div>
                </div>
            </div>

            <div id="saveActions" class="form-group d-flex justify-content-between">
                <div>
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn btn-success">
                            <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                            <span data-value="save_and_back">Save and back</span>
                        </button>

                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                    class="caret"></span><span class="sr-only">▼</span></button>
                        </div>
                    </div>
                    <a href="/admin/purchase-order" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
                </div>
                <a href="{{ asset('excel/purchase_products.xlsx') }}" class="btn export-btn btn-success btn-lg text-white"
                    title="Export Excel" id="excel_download" style="font-size: 15px;" download>
                    Download Sample Excel
                </a>
            </div>
        </form>
    </div>
@endsection

@push('after_scripts')
    <script>
        $('#excel').change(function() {
            var file = $('#excel')[0].files[0].name;
            $('#label').text(file);
        });
    </script>
@endpush
