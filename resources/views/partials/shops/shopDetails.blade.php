<?php

use App\Models\ShippingAddress;
?>
@if($shop != null)
@php
$strings = str_split($shop->description,100);
$final_string = "";
foreach($strings as $string)
{
$final_string .= $string." \n";
}
$final_string = nl2br($final_string);
@endphp

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

<div class="row" style="width:80vw;text-align: justify">
    <div class="col-md-1"></div>
    <div class="col-md-10">

        <div class="col-md-12 p-0 m-0" style="width:75vw">
            <div class="col-md-12 mb-3 text-center">
                <img src="{{ asset($shop->shop_logo) }}" alt="" class="img-responsive img-circle" width="100vw" height="auto">
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                @if(backpack_user()->can('freeze shop') && backpack_user()->can('unfreeze shop'))
                <tr>
                    <td class="font-weight-bold">Active Status :</td>
                    <td>
                        <form>
                            @if($shop->freeze_status == 1)
                            <input type="checkbox" id="freeze-event" data-shop="{{$shop->id}}" class="checkActive" data-size="xs"  data-toggle="toggle" data-on="Freeze Shop" data-off="Unfreeze Shop" data-onstyle="success" data-offstyle="danger">
                            @else
                            <input type="checkbox" id="freeze-event" data-shop="{{$shop->id}}" class="checkActive" data-size="xs" checked data-toggle="toggle" data-on="Freeze Shop" data-off="Unfreeze Shop" data-onstyle="success" data-offstyle="danger">
                            @endif
                        </form>
                    </td>
                </tr>
                @endif
                <tr>
                    <td class="font-weight-bold">Freeze Reason :</td>
                    <td>
                        <form>
                            <div class="form-group">
                            @if(backpack_user()->can('freeze shop') && backpack_user()->can('unfreeze shop'))
                                <textarea class="form-control" id="freeze_reason" data-shop="{{$shop->id}}" rows="3">{{$shop->freeze_reason ? $shop->freeze_reason : ""}}</textarea>
                            @else
                                <textarea class="form-control" id="freeze_reason" disabled data-shop="{{$shop->id}}" rows="3">{{$shop->freeze_reason ? $shop->freeze_reason : ""}}</textarea>
                            @endif
                            </div>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold">Shop Name :</td>
                    <td>
                        {{$shop->name}}
                    </td>
                </tr>
                <tr>
                    <td class="font-weight-bold ">Shop Description :</td>
                    <td style="text-align:justify">
                        {!! $final_string !!}
                    </td>
                </tr>
                @if($shop->shipping_addresses != null || $shop->shipping_addresses != "")
                @foreach($shop->shipping_addresses as $key => $address)
                <tr>
                    <td class="font-weight-bold">Shipping Address {{$key+1}}:</td>
                    <td>
                        <span class="font-weight-bold"> Full Address : </span> {{$address->ship_address}} </br> <span class="font-weight-bold"> Township : </span> <span> {{$address->township->name}} </span> </br> <span class="font-weight-bold"> City : </span> <span> {{$address->city->name}}</span> </br> <span class="font-weight-bold"> Phone : </span> <span> {{$address->phone_number}} </span> </br> <span class="font-weight-bold"> Shipping Fees : </span> <span> {{$address->township->shipping_fees ? $address->township->shipping_fees : 0 }} MMK </span>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#freeze-event').change(function() {
            var status = $(this).prop('checked');
            var sid = $(this).data("shop");
            $.ajax({
                type: 'POST',
                url: "{{ route('shopstatus') }}",
                data: {
                    status,
                    sid
                },
                success: function(data) {
                    window.location.reload();
                }
            });
        })

        $('#freeze_reason').keyup(function(){
            var value = $(this).val();
            var sid = $(this).data("shop");

            $.ajax({
                type: 'POST',
                url: "{{ route('freezeReason') }}",
                data: {
                    value,
                    sid
                },
            });
        });
    })
</script>
@endif