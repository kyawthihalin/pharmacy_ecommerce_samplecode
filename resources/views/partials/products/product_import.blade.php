@extends(backpack_view('blank'))

@push('after_scripts')
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Nunito:wght@200;300;400;600;700;800;900&display=swap');

        button:focus,
        input:focus {
            outline: none;
            box-shadow: none;
        }

        a,
        a:hover {
            text-decoration: none;
        }

        body {
            font-family: 'Nunito', sans-serif;
        }

        .file-upload-contain {
            position: relative;
            margin-bottom: 30px;
        }

        .file-upload-contain .file-input,
        .file-upload-contain .file-preview {
            position: initial;
        }

        .file-upload-contain .file-drop-zone {
            border: 2px dashed #02029B;
            transition: 0.3s;
            margin: 0;
            padding: 0;
            border-radius: 20px;
            background-color: #f1f8fe;
            min-height: auto;
            cursor: pointer;
        }

        .file-upload-contain .file-drop-zone.clickable:hover,
        .file-upload-contain .file-drop-zone.clickable:focus,
        .file-upload-contain .file-highlighted {
            border: 2px dashed #02029B !important;
            background-color: #dfedfc;
        }

        .upload-area i {
            color: #02029B;
            font-size: 50px;
        }

        .upload-area p {
            margin-bottom: 30px;
            margin-top: 30px;
            font-size: 20px;
            font-weight: 600;
            color: #13139b;
        }

        .upload-area p b {
            color: #02029B;
        }

        .upload-area button {
            display: none;
        }

        .file-preview {
            padding: 0;
            border: none;
            margin-bottom: 30px;
        }

        .file-preview .fileinput-remove {
            display: none;
        }

        .file-drop-zone-title {
            padding: 55px 10px;
            text-align: center;
        }

        .file-drop-zone .file-preview-thumbnails {
            cursor: pointer;
        }

        .file-preview-frame {
            cursor: default;
            display: flex;
            align-items: center;
            border: none;
            background-color: #13139b;
            box-shadow: none;
            border-radius: 8px;
            width: 100%;
            padding: 15px;
            margin: 8px 0px;
        }

        .file-preview-frame:not(.file-preview-error):hover {
            border: none;
            box-shadow: 0 0 10px 0 rgb(0 0 0 / 20%);
        }

        .file-preview-frame .kv-file-content {
            display: none;
        }

        .file-preview-image {
            border-radius: 4px;
        }

        .file-preview-frame .file-footer-caption {
            padding-top: 0;
        }

        .file-preview-frame .file-footer-caption {
            text-align: left;
            margin-bottom: 0;
        }

        .file-detail {
            font-size: 14px;
            height: auto;
            width: 100%;
            line-height: initial;
        }

        .file-detail .file-caption-name {
            color: #fff;
            font-size: 15px;
            font-weight: 600;
            margin-bottom: 6px;
        }

        .file-detail .file-size {
            color: #f1f8fe;
            font-size: 12px;
        }

        .kv-zoom-cache {
            display: none;
        }

        .file-preview-frame .file-thumbnail-footer {
            height: auto;
            display: flex;
            align-items: center;
            justify-content: space-between;
            width: 100%;
        }

        .file-preview-frame .file-drag-handle,
        .file-preview-frame .file-upload-indicator {
            float: none;
        }

        .file-preview-frame .file-footer-buttons {
            float: none;
            display: flex;
            align-items: center;
        }

        .file-preview-status.text-center {
            display: none;
        }

        .kv-file-remove.file-remove {
            border: none;
            background-color: #fd201a;
            color: #fff;
            width: 25px;
            height: 25px;
            font-size: 12px;
            border-radius: 4px;
            margin: 0px 4px;
        }

        .file-drag-handle.file-drag {
            display: none;
        }

        .kv-file-upload.file-upload {
            border: none;
            background-color: #48bd22;
            color: #fff;
            width: 25px;
            height: 25px;
            font-size: 12px;
            border-radius: 4px;
            margin: 0px 4px;
        }

        .file-thumb-loading {
            background: none !important;
        }

        .file-preview-frame.sortable-chosen {
            background-color: #64a5ef;
            border-color: #64a5ef;
            box-shadow: none !important;
        }

        #excel {
            display: none;
        }

        span .fileinput-cancel-button {
            display: none;
        }

        .bg-diffrent {
            width: 700px;
        }
    </style>
@endpush

@php
    $defaultBreadcrumbs = [
        trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
        $crud->entity_name_plural => url($crud->route),
        trans('backpack::crud.add') => false,
    ];

    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp
@section('header')
    <section class="container-fluid">
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add') . ' ' . $crud->entity_name !!}.</small>

            @if ($crud->hasAccess('list'))
                <small><a href="{{ url($crud->route) }}" class="d-print-none font-sm"><i
                            class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i>
                        {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <div class="row justify-content-center mt-5" style="margin-left:0">
        <form method="POST" action="{{ route('product.import.store') }}" enctype="multipart/form-data">
            @if ($errors->any())
                <div class="alert alert-danger pb-0">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li><i class="la la-info-circle"></i> {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! csrf_field() !!}
            <section class="bg-diffrent">
                <div class="container p-0">
                    <div class="row justify-content-center">
                        <div class="col-xl-12">
                            <div class="file-upload-contain">
                                <input id="excel" name="excel" type="file" required />
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div id="saveActions" class="form-group d-flex justify-content-between">
                <a href="{{ asset('excel/products.xlsx') }}" class="btn export-btn btn-success btn-lg" title="Export Excel"
                    id="excel_download" style="font-size: 15px" download>
                    Download Sample Excel
                </a>
                <div>
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn btn-success">
                            <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                            <span data-value="save_and_back">Save and back</span>
                        </button>

                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                    class="caret"></span><span class="sr-only">▼</span></button>
                        </div>
                    </div>
                    <a href="/admin/product" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('after_scripts')
    <script src="{{ asset('js/bootstrap-fileinput.js') }}"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
@endpush
