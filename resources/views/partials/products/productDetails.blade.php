<?php

use App\Models\ProductCategory;
use App\Models\Category;
use Carbon\Carbon;

if ($product->id) {
    $product_categories = ProductCategory::where('product_id', $product->id)->get();
}
?>

@if ($product != null)
    @php
        $strings = str_split($product->description, 100);
        $final_string = '';
        foreach ($strings as $string) {
            $final_string .= $string . " \n";
        }
        $final_string = nl2br($final_string);

        $quantity = \App\Models\ProductBalance::where('product_id', $product->id)->sum('quantity');
    @endphp

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <div class="row" style="width:80vw;text-align: justify">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            <div class="col-md-12 p-0 m-0" style="width:75vw">
                <div class="col-md-12 mb-1 text-center">
                    <h4 class="font-weight-bold pb-5"> {{ $product->name }}'s Gallery</h4>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        @foreach ($product->gallery as $key => $image)
                            @php
                                $url = Storage::temporaryUrl($image, now()->addMinutes(3));
                            @endphp
                            <div class="col-md-3 mb-5 col-sm-10">
                                <a href="{{ asset($url) }}" target="_blank" title="product image">
                                    <img src="{{ asset($url) }}" alt="product image" height="150px">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-1 text-center">
                <h4 class="font-weight-bold pb-3 pt-2"> {{ $product->name }}'s Detail Information</h4>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td class="font-weight-bold">Active Status :</td>
                        <td>
                            <form>
                                <input name="status" type="checkbox" id="inactive-event"
                                    data-product="{{ $product->id }}" class="checkActive" data-size="xs"
                                    data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success"
                                    data-offstyle="danger" @if ($product->status == 1) checked @endif>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Selling Status :</td>
                        <td>
                            <form>
                                <input name="is_selling" type="checkbox" id="inactive-event"
                                    data-product="{{ $product->id }}" class="checkActive" data-size="xs"
                                    data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success"
                                    data-offstyle="danger" @if ($product->is_selling == 1) checked @endif>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Special Product :</td>
                        <td>
                            <form>
                                <input name="is_popular" type="checkbox" id="inactive-event"
                                    data-product="{{ $product->id }}" class="checkActive" data-size="xs"
                                    data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success"
                                    data-offstyle="danger" @if ($product->is_popular == 1) checked @endif>
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Product Name :</td>
                        <td>
                            {{ $product->name }}
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Product Category :</td>
                        @if ($product_categories)
                            <td>
                                @foreach ($product_categories as $product_category)
                                    @php
                                        $category = Category::find($product_category->category_id);
                                    @endphp
                                    <span class="badge badge-pill badge-info">{{ $category->name }}</span>
                                @endforeach
                            </td>
                        @endif
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Product Size :</td>
                        <td>
                            {{ $product->size }} {{ $product->unit->name }}
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Product Quantity :</td>
                        <td>
                            {{ $quantity }}
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Product Limit :</td>
                        <td>
                            {{ $product->limit }}
                        </td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">Product Warning Count :</td>
                        <td>
                            {{ $product->warning_count }}
                        </td>
                    </tr>
                    @if ($product->skuCode)
                        <tr>
                            <td class="font-weight-bold ">Discount :</td>
                            <td style="text-align:justify">
                                {{ $product->skuCode->discount }}
                            </td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold ">Discount Type :</td>
                            <td style="text-align:justify">
                                {{ $product->skuCode->discount_type }}
                            </td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold ">Discount Period :</td>
                            <td style="text-align:justify">
                                {!! $product->skuCode->discount_start_date
                                    ? "From <span class='badge badge-success'>" .
                                        Carbon::parse($product->skuCode->discount_start_date)->format('d M Y') .
                                        '</span>' .
                                        '  to  ' .
                                        "<span class='badge badge-success'>" .
                                        Carbon::parse($product->skuCode->discount_end_date)->format('d M Y') .
                                        '</span>'
                                    : '-' !!}
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td class="font-weight-bold ">Product Description :</td>
                        <td style="text-align:justify">
                            {!! $final_string ? $final_string : '-' !!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.checkActive').change(function() {
                // var status = $(this).prop('checked');
                // var is_selling = $(this).prop('checked');
                var status = $('[name="status"]').prop('checked');
                var is_selling = $('[name="is_selling"]').prop('checked');
                var is_popular = $('[name="is_popular"]').prop('checked');
                var pid = $(this).data("product");
                $.ajax({
                    type: 'POST',
                    url: "{{ route('status') }}",
                    data: {
                        status,
                        is_selling,
                        is_popular,
                        pid
                    },
                });
            })
        })
    </script>
@endif
