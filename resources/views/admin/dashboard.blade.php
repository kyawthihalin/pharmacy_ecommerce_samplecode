@extends(backpack_view('blank'))

@push('after_styles')
<style>
     input[type=date] {
        height: 30px;
        margin-top: 8px;
     }
</style>


@endpush
@section('content')

<script type="text/javascript">
    function display_c() {
        var refresh = 1000; // Refresh rate in milli seconds
        mytime = setTimeout('display_ct()', refresh)
    }

    function display_ct() {
        var x = new Date();
        var h = x.getHours(),
            m = x.getMinutes() + ":" + x.getSeconds();
        var ime = (h > 12) ? (h - 12 + ':' + m + ' PM') : (h + ':' + m + ' AM');
        document.getElementById('ct').innerHTML = ime;
        display_c();
    }
</script>
<section class="content-header" style="padding-top: 5px">
    <strong>
        <h1 class="container-fluid" style="font-family:Times New Roman; font-size:35px; font-weight: bold !important; color: #404e67;">
            Welcome Back, {{backpack_user()->name}} !
        </h1>
    </strong>
    <h3 class="container-fluid mb-5" style="padding-top: 0px;margin-top: 10px;">
        <small style="font-family: Centaur; font-size: 20px; font-weight: bold; color: #60099e;">
            Today is <?php
                        $date = Carbon\Carbon::now();
                        echo date('l, d F Y', strtotime($date)); //June, 2017
                        ?>

            <body onload=display_ct();>
                <span id='ct'></span>
            </body>
        </small>
    </h3>
    <div> {{$from_date_input_value}} </div>

</section>

@php
use Carbon\Carbon;
$date = Carbon::now()->toDateString();
$today = Carbon::now();

@endphp
    @push('after_styles')
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
@endpush
@endsection

@section('content')
    <?php
    $widgets['after_content'][] = [
        'type' => 'div',
        'class' => 'row pt-2 customwidth',
        'content' => [
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard1', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-user-lock" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Admin</span></p><p class="total_amount">' .$admins .' Admins </p>
                        <div class="view_detail"><a href="/admin/user" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard3', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-store" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Shops</span></p><p class="total_amount"> ' .$shops .' Shops </p>
                        <div class="view_detail"><a href="/admin/shop" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard2', // optional
                'content'    => [
                    'body'   =>"<p class='icon'><i class='las la-exchange-alt' style='font-size:30px;padding-top:4px;'></i>&nbsp;&nbsp;&nbsp;<span class='title'>Today Order</span></p class='total_amount'><p>" . $today_order ."</p>
                    <div class='view_detail'><a href='/admin/order?from_to={\"from\":\"".$date." 00:00:00\",\"to\":\"".$date." 23:59:59\"}' style='color:white;font-weight:bold'>View Detail</a> </div>"
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard3', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-wallet" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Today Income</span></p><p class="total_amount"> ' .$today_income .' MMK</p>
                    <div class="view_detail"><a href="/admin/order" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-6'], // optional
                'class'   => 'card text-white customcard4', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-wallet" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Orders</span></p><p class="total_amount"> ' .$orders .' Orders</p>
                    <div class="view_detail"><a href="/admin/order" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-6'], // optional
                'class'   => 'card text-white customcard2', // optional
                'content'    => [
                    'body'   =>"<p class='icon'><i class='las la-wallet' style='font-size:30px;padding-top:4px;'></i>&nbsp;&nbsp;&nbsp;<span class='title'>Monthly Income</span></p class='total_amount'><p>" . $monthly_income ."</p>
                    <div class='view_detail'><a href='/admin/order?from_to={\"from\":\"".$today->year ."-". $today->month."-01"." 00:00:00\",\"to\":\"".$today." 23:59:59\"}' style='color:white;font-weight:bold'>View Detail</a> </div>"
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard1', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-truck" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Completed Orders</span></p><p class="total_amount">' .$completed_orders .' Orders</p>
                    <div class="view_detail"><a href="/admin/completed-order" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard2', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-truck" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Delivering Orders</span></p><p class="total_amount">' .$delivering_orders .' Orders</p>
                    <div class="view_detail"><a href="/admin/shipping-pending-order" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard2', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-truck" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Payment Pendings</span></p><p class="total_amount">' .$payment_pending_orders .' Orders</p>
                    <div class="view_detail"><a href="/admin/payment-pending-order" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
                'class'   => 'card text-white customcard4', // optional
                'content'    => [
                    'body'   => '<p class="icon"><i class="las la-truck" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Rejected Orders</span></p><p class="total_amount">' .$rejected_orders .' Orders</p>
                    <div class="view_detail"><a href="/admin/rejected-order" style="color:white;font-weight:bold">View Detail</a> </div>'
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                // 'class' => 'bg-white text-center',
                'controller' => \App\Http\Controllers\Admin\Charts\SurveyChartController::class,
                'content' => [
                    'header' => "Survey", // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                // 'class' => 'bg-white text-center',
                'controller' => \App\Http\Controllers\Admin\Charts\OrdersByShippingChartController::class,
                'content' => [
                    'header' => "Orders By Shipping Status", // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                // 'class' => 'bg-white text-center',
                'controller' => \App\Http\Controllers\Admin\Charts\OrdersByPaymentChartController::class,
                'content' => [
                    'header' => "Orders By Payment Status", // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                // 'class' => 'bg-white text-center',
                'controller' => \App\Http\Controllers\Admin\Charts\WeeklyCityChartController::class,
                'content' => [
                    'header' => "Monthly Order", // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-12',
                // 'class' => 'bg-white text-center',
                'controller' => \App\Http\Controllers\Admin\Charts\IncomeChartController::class,
                'content' => [
                    'header' => "Monthly Income", // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-12',
                // 'class' => 'bg-white text-center',
                'controller' =>  \App\Http\Controllers\Admin\Charts\WeeklyShopsChartController::class,
                'content' => [
                    'header' => "Monthly Registered Shops/Users", // optional
                ]
            ],
            [
                'type'       => 'card',
                'wrapper' => ['class' => 'col-sm-12 col-md-12'], // optional
                'content'    => [
                    'body'   => '<div>Net Profit</div>
                                <hr style="margin:0px;margin-top:5px;margin-bottom:7px"/>
                                <div style=" display: flex;">
                                    <label style="margin-top:10px;font-size:15px">From Date</label><input type="date" id="from_date" class="form-control" id="date" name="date" style="width:150px;margin-left:10px" value='.$from_date_input_value.' >
                                    <label style="margin-left: 10px;margin-top:10px;font-size:15px">To Date</label><input type="date" id="to_date" class="form-control" id="date" name="date" style="width:150px;margin-left:10px" value='.$to_date_input_value.' >
                                </div>
                                <canvas id="canvas" height="130"></canvas>'
                ]
            ],
        ]
    ]
    ?>
@endsection


@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    var profit_chart_label = @json($profit_chart_label);
    var profit_chart_data = @json($profit_chart_data);
    var barChartData = {
        labels: profit_chart_label,
        datasets: [{
            label: 'Profit (MMK)  ',
            borderColor:"pink",
            backgroundColor: "pink",
            fill: false,
            data:  profit_chart_data,
        }]
    };
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: barChartData,
            options: {
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: '#c1c1c1',
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                title: {
                    display: true,
                    text: ''
                }
            }
        });
    };


    var from_date   = document.getElementById('from_date');
    var to_date     = document.getElementById('to_date');
    var old_from_date_value = from_date.value;
    from_date.addEventListener('change', function(event) {
        const check_from_date = new Date(from_date.value);
        const check_to_date = new Date(to_date.value);
        if (check_from_date > check_to_date) {
            const newDate = new Date(old_from_date_value)
            const formattedDate = newDate.toISOString().split('T')[0];
            from_date.value = formattedDate;

            Swal.fire(
            'Error!',
            'From Date is over than To Date',
            'error'
            )
        }
        else{
        var from_date_value = from_date.value;
        var to_date_value = to_date.value;
        var newUrl = window.location.origin + window.location.pathname + '?&from_date=' + from_date_value+"&to_date="+to_date_value;
        window.location.href = newUrl;
        }
    });

    to_date.addEventListener('change', function(event) {
        const check_from_date = new Date(from_date.value);
        const check_to_date = new Date(to_date.value);
        if (check_from_date > check_to_date) {
            const newDate = new Date(old_from_date_value)
            const formattedDate = newDate.toISOString().split('T')[0];
            from_date.value = formattedDate;

            Swal.fire(
            'Error!',
            'To Date is less than From Date',
            'error'
            )
        }
        else{
        var from_date_value = from_date.value;
        var to_date_value = to_date.value;
        var newUrl = window.location.origin + window.location.pathname + '?&from_date=' + from_date_value+"&to_date="+to_date_value;
        window.location.href = newUrl;
        }
    });


</script>
@endsection
