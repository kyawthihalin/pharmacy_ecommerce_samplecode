<?php

namespace App\Constants;

class Status
{
    const shipping_status = ["Pending","Processing","Confirmed","Delivering","Going to Bus Gate","Completed","Rejected"];
    const payment_status = ["Pending","Completed","Rejected","Credit Completed"];
    const order_status = ["Pending","Arrived","Cancelled"];
    const payment_method = ["Cash","Credit"];
    const adjustment_type = ["Damage","Return"];
    const stock_type = ["Sale", "Purchase", "Adjustment_In", "Adjustment_Out", "Balance_In", "Balance_Out"];
}
