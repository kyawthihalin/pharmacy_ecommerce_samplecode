<?php

namespace App\Constants;

class FinanceParticularConstant
{
    const TYPE = [
        "PURCHASE_ORDER_CASH_COMPLETE" => "Purchase Order Cash Complete",
        "PURCHASE_ORDER_CREDIT_COMPLETE" => "Purchase Order Credit Complete",
        "FILL_TO_CAPITAL_ACCOUNT" => "Fill To Capital Account",
        "SALE_CASH_COMPLETE" => "Sale Cash Complete",
        "PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE" => "Purchase Order From Credit To Cash Complete",
        "FILL_AMOUNT_FROM_PROFIT_TO_CAPITAL" => "Fill Amount From Profit To Capital",
        "GET_PROFIT_FROM_ORDER_CASH_COMPLETE" => "Get Profit From Order Cash Complete",
        "PRODUCT_DAMAGE" => "Product Damage"
    ];
}
