<?php

namespace App\Constants;

class OtpAction
{
    const Register =  'register';
    const Login = 'login';
    const UpdatePasscode = 'update passcode';
}
