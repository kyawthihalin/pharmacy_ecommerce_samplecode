<?php

namespace App\Constants;

class GetStatus
{
    const SHIPPING_STATUS = [
        "Pending" => "Pending",
        "Processing" => "Processing",
        "Confirmed" => "Confirmed",
        "Delivering" => "Delivering",
        "Going to Bus Gate" => "Going to Bus Gate",
        "Completed" => "Completed",
        "Rejected" => "Rejected"
    ];

    const PAYMENT_STATUS = [
        "Pending" => "Pending",
        "Completed" => "Completed",
        "Rejected" => "Rejected",
        "Credit Completed" => "Credit Completed",
    ];

    const ORDER_STATUS = [
        "Pending" => "Pending",
        "Arrived" => "Arrived",
        "Cancelled" => "Cancelled",
    ];

    const PAYMENT_METHOD = [
        "Cash" => "Cash",
        "Credit" => "Credit",
    ];

    const ADJUSTMENT_TYPE = [
        "Damage" => "Damage",
        "Return" => "Return",
    ];

    const FINANCE_ACCOUNT_OPERATION = [
        "Addition" => "Addition",
        "Subtraction" => "Subtraction"
    ];

    const FINANCE_CASH_ACCOUNT_STATUS = [
        "Active" => "Active",
        "Inactive" => "Inactive",
    ];

    const STOCK_TYPE = [
        "Sale" => "Sale",
        "Purchase" => "Purchase",
        "Adjustment_In" => "Adjustment_In",
        "Adjustment_Out" => "Adjustment_Out",
        "Balance_In" => "Balance_In",
        "Balance_Out" => "Balance_Out",
    ];

    const STOCK_IN = 'STOCK_IN';
    const STOCK_OUT = 'STOCK_OUT';
}
