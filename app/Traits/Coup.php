<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait Coup
{
    /**
     * Boot function from Laravel.
     */
    protected static function booted()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->coupon_number)) {
                $model->coupon_number = Str::random(6);
            }
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }
}
