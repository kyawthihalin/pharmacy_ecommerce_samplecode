<?php

namespace App\Traits;

trait ApiResponse
{
    /**
     * Boot function from Laravel.
     */
    public function responseWithSuccess($message = "Success", $data = [], $status = 200)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data'   => $data,
        ], $status);
    }
    public function responseWithError($message = "Success", $data = [], $status = 500)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data'   => $data,
        ], $status);
    }
}
