<?php

namespace App\Exceptions;

use Exception;

class ProductStockOutException extends Exception
{
    protected $productId;

    public function __construct($productId, $message = null)
    {
        $this->productId = $productId;

        if ($message === null) {
            $message = "Product with Item Code ( {$this->productId} ) is out of stock.";
        }

        parent::__construct($message);
    }

    public function getProductId()
    {
        return $this->productId;
    }
}
