<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponse;

class OtpExpiredException extends Exception
{
    use ApiResponse;
    public function render(){
        return ApiResponse::Send("OTP is Expired",422); 
    }
}
