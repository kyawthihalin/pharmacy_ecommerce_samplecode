<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponse;

class OtpInvalidException extends Exception
{
    use ApiResponse;
    public function render(){
        return ApiResponse::Send("OTP is Invalid",422); 
    }
}
