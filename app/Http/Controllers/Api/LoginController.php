<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Models\Shop;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\ShopResource;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $shop = Shop::where('phone', $request->phone)->first();
        if (!$shop) {
            return $this->responseWithError("Login Failed");
        }
        if (Hash::check($request->password, $shop->password)) {
            $token = $shop->createToken($shop->phone)->accessToken;
            return $this->responseWithSuccess("Login Success", ["shop_info" => new ShopResource($shop),"token" => $token]);
        }
        return $this->responseWithError("Login Failed");
    }
}
