<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Requests\GetOtpRequest;
use App\Http\Resources\ShopResource;
use App\Services\Auth\SmsService;
use App\Constants\OtpAction;
use App\Http\Requests\VerifyOtpRequest;
use App\Services\Auth\AccessToken;

class RegisterController extends Controller
{
    private $accessToken;
    public function __construct(AccessToken $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function getOtp(GetOtpRequest $request,OtpAction $otpAction)
    {
        (new SmsService($request->phone_number,$request->device_id))->send($otpAction::Register,500);
        return $this->responseWithSuccess("OTP Successfully Sent",["token" => $this->accessToken->generate($request->phone_number, 'mb_register_verify_otp')]);
    }

    public function verifyOpt(VerifyOtpRequest $request)
    {
        (new SmsService($request->phone_number,$request->device_id))->verify($request->otp);
        $this->accessToken->delete($request->phone_number, 'mb_register_verify_otp');
        return $this->responseWithSuccess("OTP Verified",["token" => $this->accessToken->generate($request->phone_number, 'mb_register')]);
    }

    public function register(RegisterRequest $request)
    {
        Shop::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'address' => $request->address,
        ]);
        $shop = Shop::where('phone', $request->phone)->first();
        $token   = $shop->createToken($request->phone)->accessToken;
        return $this->responseWithSuccess("Successfully Registered",["shop_info" => new ShopResource($shop),"token" => $token],201);
    }
}
