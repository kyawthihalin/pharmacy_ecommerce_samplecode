<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SendNotification;
use App\Actions\SendOrderNotification;
use App\Http\Requests\AppNotificationRequest;
use App\Http\Requests\NotificationRequest;
use App\Models\Notification;
use App\Models\Shop;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AppNotificationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AppNotificationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AppNotification::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/app-notification');
        CRUD::setEntityNameStrings('app notification', 'app notifications');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );
        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->addClause('where','type','App Notification');
        CRUD::column('title');
        CRUD::addColumn([
            'name' => 'description',
            'label' => 'message',
            'type' => 'closure',
            'function' => function($entry)
            {
                return $entry->description ?? "-";
            }
        ]);
        CRUD::column('read_at');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AppNotificationRequest::class);
        CRUD::addField([
            'name' => 'title',
            'type' => 'text',
            'label'=> 'Notification Title'
        ]);
        CRUD::field('description');
        

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(NotificationRequest $request)
    {
        $title = $request->title;
        $desc  = $request->description;
        Notification::create([
            'title' => $title,
            'description' => $desc,
            'type' => 'App Notification'
        ]);
        $data = [
            "title" => $title,
            "description" => $desc,
            'type' => 'App Notification'
        ];
        $all_shops = Shop::all();
        foreach($all_shops as $shop)
        {
            (new SendOrderNotification())->execute([$shop->noti_token],$data);
        }
        \Alert::add('success', 'Successfully Sent Notification')->flash();
        return redirect('admin/app-notification');
    }
}
