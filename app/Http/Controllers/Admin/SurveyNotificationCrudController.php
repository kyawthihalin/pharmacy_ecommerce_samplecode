<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SurveyNotificationRequest;
use App\Models\Notification;
use App\Models\Shop;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;

/**
 * Class SurveyNotificationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SurveyNotificationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\SurveyNotification::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/survey-notification');
        CRUD::setEntityNameStrings('survey notification', 'Survey Analysis');
        CRUD::denyAccess('create');

        $this->crud->addFilter([
            'name'  => 'answer',
            'type'  => 'dropdown',
            'label' => 'Survey Filter'
        ], [
            'Good' => 'Good Review',
            'Bad' => 'Bad Review',
            'No' => 'No Review',
        ], function ($value) { // if the filter is active
            if($value === "No")
            {
                $this->crud->addClause('where', 'type', "Survey");
                $this->crud->addClause('where', 'answer', "");
                $this->crud->addClause('orWhere', 'answer', null);
            }else
            {
                $this->crud->addClause('where', 'type', "Survey");
                $this->crud->addClause('where', 'answer', $value);
            }
        });
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );
    }
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->addClause('where','type','Survey');

        Widget::add([
            'type'     => 'view',
            'view'     => 'vendor.backpack.base.widgets.survey',
            'someAttr' => 'some value',
        ]);

        CRUD::addColumn([
            'name' => 'shop_id',
            'type' => 'relationship',
            'label' => 'Shop Name',
            'attribute' => 'name',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("shop/?name=" . $entry->shop->name);
                },
            ],
        ]);
        CRUD::addColumn([
            'name' => 'order_id',
            'type' => 'relationship',
            'label' => 'Order No',
            'attribute' => 'order_no',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("order?order_no=" . $entry->order->order_no);
                },
            ],
        ]);
        CRUD::column('title');
        CRUD::addColumn([
            'name' => 'description',
            'label' => 'Message',
            'type' => 'closure',
            'function' => function($entry)
            {
                return $entry->description ?? "-";
            }
        ]);
        CRUD::addColumn([
            'name' => 'reason',
            'label' => 'Reason',
            'type' => 'closure',
            'function' => function($entry)
            {
                $result = [];
                $string = $entry->reason;
                if(strlen($string) > 0)
                {
                    $final_string = "";
                    $length = mb_strlen($string, 'UTF-8');
                    for ($i = 0; $i < $length; $i += 60) {
                        $result[] = mb_substr($string, $i, 50, 'UTF-8');
                    }
                    foreach ($result as $res) {
                        $final_string .= $res . "\n";
                    }
                    return nl2br($final_string);
                }
                else
                {
                    return "-";
                }

            }
        ]);

        CRUD::column('read_at');
        CRUD::addColumn([
            'name' => 'answer',
            'type' => 'closure',
            'label' => 'Answer',
            'function' => function($entry)
            {
                if($entry->answer == "Bad")
                {
                    return "<span class='badge badge-danger'>Bad</span>";
                }elseif($entry->answer == "Good")
                {
                    return "<span class='badge badge-success'>Good</span>";
                }else{
                    return "<span class='badge badge-secondary'>No Answer</span>";
                }
            }
        ]);
        CRUD::column('type');
        CRUD::column('created_at');
        CRUD::column('updated_at');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SurveyNotificationRequest::class);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
