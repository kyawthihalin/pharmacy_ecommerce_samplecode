<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\FinanceTransaction;
use Illuminate\Http\Request;
use App\Models\ProfitDailyLog;
use App\Models\FinanceCashAccount;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProfitReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProfitReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StockLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/profit-report');
        CRUD::setEntityNameStrings('profit/loss report', 'profit/loss reports');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        if (!backpack_user()->can('list profit report')) {
            abort(403);
        }

        CRUD::addField([   // date_range
            'name'  => 'start_date',
            'type'  => 'date_picker',
            'label' => 'Start Date',
            'default' => Carbon::yesterday()->toDateString(),
            // optional:
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy',
                'language' => 'mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3 SDate'
            ]
        ]);

        CRUD::addField([   // date_range
            'name'  => 'end_date',
            'type'  => 'date_picker',
            'label' => 'End Date',
            // 'default' => Carbon::tomorrow()->toDateString(),
            'default' => Carbon::today()->toDateString(),
            // optional:
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy',
                'language' => 'mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3 EDate'
            ]
        ]);

        CRUD::addField([
            'name' => 'custom-ajax-buttons',
            'type' => 'view',
            'view' => 'partials/reports/profit_btn',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6 d-flex align-items-end'
            ]
        ]);

        CRUD::addField([
            'name'  => 'route_name',
            'type'  => 'hidden',
            'value' => 'profit_report',
        ]);

        CRUD::addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/profit',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function report(Request $request)
    {
        if($request->start_date && $request->end_date)
        {

            $start_date = Carbon::parse($request->start_date)->format('Y-m-d');
            $end_date = Carbon::parse($request->end_date)->format('Y-m-d');
        }
        else{
            $start_date = Carbon::yesterday()->format('Y-m-d');
            $end_date = Carbon::today()->format('Y-m-d');
        }


        $finance_cash_accounts = FinanceCashAccount::all();

        $income_accounts = [];
        $expense_accounts = [];


        foreach($finance_cash_accounts as $finance_cash_account)
        {
            $finance_cash_income_amount = FinanceTransaction::where('debit_account_id', $finance_cash_account->id)->where('created_at', '>=', $start_date)->where('created_at', '<', $end_date)->sum('amount');
            $finance_cash_expense_amount = FinanceTransaction::where('credit_account_id', $finance_cash_account->id)->where('created_at', '>=', $start_date)->where('created_at', '<', $end_date)->sum('amount');

            array_push($income_accounts, [
                "name" => $finance_cash_account->name,
                "amount" => round($finance_cash_income_amount),
            ]);

            if ($finance_cash_account->name != 'Sale Gl' && $finance_cash_account->name != 'Supplier Gl' && $finance_cash_account->name != 'Product damage') {
                array_push($expense_accounts, [
                    "name" => $finance_cash_account->name,
                    "amount" => round($finance_cash_expense_amount),
                ]);
            }
        }

        $net_profit_amount = ProfitDailyLog::where('created_at', '>=', $start_date)->where('created_at', '<', $end_date)->sum('total_amount');

        return view('partials.reports.profit_view', [
            'income_accounts' => $income_accounts,
            'expense_accounts' => $expense_accounts,
            'net_profit_amount' => round($net_profit_amount),
            'start_date' =>  $start_date,
            'end_date' => $end_date,
        ]);

    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
