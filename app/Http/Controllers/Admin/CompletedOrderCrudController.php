<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CompletedOrderRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\OrderDetail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Actions\SendNotification;
use App\Actions\SendOrderNotification;
use App\Actions\SetCrudPermission;
use App\Constants\Status;
use App\Http\Requests\OrderRequest;
use App\Models\City;
use App\Models\Notification;
use App\Models\Order;
use App\Models\PaymentType;
use App\Models\Product;
use App\Models\ShippingAddress;
use App\Models\Township;
/**
 * Class CompletedOrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CompletedOrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CompletedOrder::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/completed-order');
        CRUD::setEntityNameStrings('completed order', 'completed orders');
        (new SetCrudPermission())->execute('order', $this->crud);
        $this->crud->setEditView('partials.orders.editOrder');
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );
        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'order_no',
                'label' => 'Type Order Number'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('order_no', 'LIKE', "%{$value}%");
                });
            }
        );

        $this->crud->addFilter(
            [
                'name'        => 'city_id',
                'type'        => 'select2_ajax',
                'label'       => 'City',
                'placeholder' => 'Pick a City'
            ],
            url('admin/test/ajax-city-options'), // the ajax route
            function ($value) {
                $address = ShippingAddress::where('city_id', $value)->pluck('id')->toArray();
                $this->crud->addClause('whereIn', 'shipping_address_id', $address);
            }
        );

        $this->crud->addFilter(
            [
                'name'        => 'township_id',
                'type'        => 'select2_ajax',
                'label'       => 'Township',
                'placeholder' => 'Pick a Township'
            ],
            url('admin/test/ajax-township-options'),
            function ($value) {
                $address = ShippingAddress::where('township_id', $value)->pluck('id')->toArray();
                $this->crud->addClause('whereIn', 'shipping_address_id', $address);
            }
        );

        // $this->crud->addFilter([
        //     'name'  => 'payment_status',
        //     'type'  => 'dropdown',
        //     'label' => 'Payment Status'
        // ], [
        //     'Pending' => 'Pending',
        //     'Completed' => 'Completed',
        //     'Rejected' => 'Rejected',
        // ], function ($value) { // if the filter is active
        //     $this->crud->addClause('where', 'payment_status', $value);
        // });

        // $this->crud->addFilter([
        //     'name'  => 'shipping_status',
        //     'type'  => 'dropdown',
        //     'label' => 'Shipping Status'
        // ], [
        //     'Pending' => 'Pending',
        //     'Processing' => 'Processing',
        //     'Confirmed' => 'Confirmed',
        //     'Delivering' => 'Delivering',
        //     'Completed' => 'Completed',
        //     'Rejected' => 'Rejected',
        // ], function ($value) { // if the filter is active
        //     $this->crud->addClause('where', 'shipping_status', $value);
        // });

        $this->crud->addFilter([
            'name'  => 'payment_type_id',
            'type'  => 'select2',
            'label' => 'Payment Type'
        ], function () {
            return PaymentType::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'payment_type_id', $value);
        });
    }
    public function showDetailsRow($id)
    {
        $order = Order::find($id);
        $qr_code = QrCode::generate($order->uuid);

        return view('partials.orders.orderDetails', ['order' => $order, 'qr_code' => $qr_code]);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->addClause('where','payment_status','Completed');
        $this->crud->addClause('where','shipping_status','Completed');
        $this->crud->addClause('orderBy','updated_at','desc');
        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();
        $this->crud->setDefaultPageLength(10);
        $this->crud->removeAllButtonsFromStack('line');
        // if (backpack_user()->can('edit order')) {
        //     $this->crud->setColumnPriority('order', 0);
        //     $this->crud->addButtonFromView('line', 'order', 'order', 'beginning');
        // }
        // CRUD::column('order_no');
        $this->crud->addColumn([
            'label' => 'Order No',
            'type' => 'text',
            'name' => 'order_no',
            // 'wrapper'   => [
            //     'href' => function ($crud, $column, $entry, $related_key) {
            //         if ($entry->payment_status === "Pending" && backpack_user()->can('edit order')) {
            //             return backpack_url("order/$entry->id/edit");
            //         } else {
            //             // Return some other URL or null if you don't want the link to be clickable.
            //             return null;
            //         }
            //     },
            // ],
        ]);
        $this->crud->addColumn([
            'label' => 'Shop',
            'type' => 'select',
            'name' => 'shop_id',
            'entity' => 'shop',
            'attribute' => 'name',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("shop/?name=" . $entry->shop->name);
                },
            ],
        ]);

        CRUD::addColumn([
            'label' => 'Payment Type Status',
            'name' => 'payment_type_status',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($entry->payment_type_status == "cash_down") {
                    return '<span> လက်ငင်းငွေချေ </span>';
                }
                if ($entry->payment_type_status == "cash_on_delivery") {
                    return '<span> ပစ္စည်းရောက်ငွေချေ </span>';
                }
            }
        ]);


        CRUD::column('payment_type_id');

        CRUD::addColumn([
            'name' => 'payment_screenshot',
            'type' => 'closure',
            'label' => 'Payment Screenshot',
            'function' => function ($entry) {
                if ($entry->payment_screenshot != null || $entry->payment_scrrenshot != "") {
                    $url = Storage::temporaryUrl(
                        $entry->payment_screenshot,
                        now()->addMinutes(3)
                    );
                    return "<a target='blank' href='{$url}'><img src='{$url}' alt='Payment Screenshot' width='50' height='50'/></a>";
                } else {
                    return "No Payment Screenshot for this order.";
                }
            },
        ]);

        CRUD::addColumn([
            'label' => ' Net Amount',
            'name' => 'net_amount',
            'type' => 'number',
        ]);
        CRUD::addColumn([
            'label' => 'Payment Status',
            'name' => 'payment_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->payment_status == "Pending") {
                    $color = "badge-warning";
                }
                if ($entry->payment_status == "Completed") {
                    $color = "badge-success";
                }
                if ($entry->payment_status == "Rejected") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->payment_status . '</span>';
            }
        ]);
        CRUD::addColumn([
            'label' => 'Shipping Status',
            'name' => 'shipping_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->shipping_status == "Pending" || $entry->shipping_status == "Processing") {
                    $color = "badge-warning";
                }
                if ($entry->shipping_status == "Confirmed" || $entry->shipping_status == "Delivering" || $entry->shipping_status == "Going to Bus Gate") {
                    $color = "badge-primary";
                }
                if ($entry->shipping_status == "Completed") {
                    $color = "badge-success";
                }
                if ($entry->shipping_status == "Rejected") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->shipping_status . '</span>';
            }
        ]);
        CRUD::addColumn([
            'name' => 'created_at',
            'label' => 'Order Date',
            'type' => 'datetime',
        ]);
        CRUD::column('shipping_address_id');

        CRUD::addColumn([
            'name' => 'full_address',
            'label' => 'Full Address',
            'type' => 'closure',
            'function' => function ($entry) {
                $string = "";
                $stringAry      =   explode("||", wordwrap($entry->shipping_address->ship_address, 120, "||"));
                foreach ($stringAry as $key => $result) {
                    $string .= $result . "<br>";
                }

                return $string . '(' . $entry->shipping_address->township->name . ',' . $entry->shipping_address->city->name . ')';
            }
        ]);

        CRUD::addColumn([
            'label' => 'Phone Model',
            'name' => 'phone_model',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->phone_model ?? '-';
            },
        ]);

        CRUD::addColumn([
            'name' => 'phone_number',
            'label' => 'Phone Number',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->shipping_address->phone_number;
            }
        ]);

        CRUD::addColumn([
            'name' => 'delivery_fee',
            'label' => 'Delivery Fee',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->shipping_address->township->shipping_fees ? $entry->shipping_address->township->shipping_fees : 0;
            }
        ]);

        CRUD::addColumn([
            'label' => 'Total Quantity',
            'name' => 'total_quantity',
            'type' => 'closure',
            'function' => function ($entry) {
                return OrderDetail::where('order_id', $entry->id)->sum('quantity');
            }
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CompletedOrderRequest::class);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
