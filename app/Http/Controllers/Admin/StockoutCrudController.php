<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\StockoutRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class StockoutCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockoutCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Stockout::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stockout');
        CRUD::setEntityNameStrings('Stock List', 'Stock List');
        (new SetCrudPermission())->execute('stock', $this->crud);
        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Type Product Name'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('name', 'LIKE', "%{$value}%");
                });
            }
        );

        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'product_id',
                'label' => 'Type Product ID'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('product_id', 'LIKE', "%{$value}%");
                });
            }
        );

        $this->crud->addFilter([
            'name'  => 'status',
            'type'  => 'dropdown',
            'label' => 'Status'
        ], [
            1 => 'Warning Proucts',
            0 => 'No Warning Proucts',
        ], function ($value) { // if the filter is active
            if($value == 1)
            {
                $this->crud->addClause('whereColumn', 'warning_count','>=','quantity');

            }elseif($value == 0)
            {
                $this->crud->addClause('whereColumn', 'warning_count','<','quantity');

            }
        });

    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        
        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        
        CRUD::column('product_id');
        CRUD::column('name');

        CRUD::addColumn([
            'name' => 'feature_image',
            'type' => 'closure',
            'label' => 'Feature Image',
            'function' => function ($entry) {
                $url = Storage::temporaryUrl(
                    $entry->feature_image,
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}'><img src='{$url}' alt='Feature Image' width='50' height='50'/></a>";
            },
        ]);
        CRUD::addColumn([
            'name' => 'quantity',
            'type' => 'closure',
            'label' => 'Quantity',
            'function' => function($entry)
            {
                if($entry->warning_count >= $entry->quantity)
                {
                    return "<span class='badge badge-danger' style='font-size:15px'>$entry->quantity</span>";
                }else
                {
                    return "<span class='badge badge-success' style='font-size:15px'>$entry->quantity</span>";
                }
            }
        ]);
        CRUD::addColumn([
            'name' => 'warning_count',
            'type' => 'closure',
            'label' => 'Warning Count',
            'function' => function($entry)
            {
                
                return "<span class='badge badge-success' style='font-size:15px'>$entry->warning_count</span>";
               
            }
        ]);
        CRUD::column('quantity');
        $this->crud->removeAllButtonsFromStack('line');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StockoutRequest::class);

        

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
