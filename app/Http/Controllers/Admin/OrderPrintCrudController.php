<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\OrderPrintRequest;
use App\Models\Order;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * Class OrderPrintCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderPrintCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\OrderPrint::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order-print');
        CRUD::setEntityNameStrings('order print', 'order prints');
        (new SetCrudPermission())->execute('print voucher', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderPrintRequest::class);
        // $this->crud->addField([
        //     'type' => 'text',
        //     'name' => 'order_no', // the relationship name in your Model
        //     'label' => 'Type Order Number',
        //     'placeholder' => "Type a Order Number", // placeholder for the select
        //     'wrapperAttributes' => [
        //         'class' => 'form-group col-md-4'
        //     ],
        //     'attributes' => [
        //         'class' => 'form-control'
        //     ],
        // ]);
        
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/invoice',
            'attributes' => [
                'class' => 'enter-check'
            ],
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function report(Request $request)
    {
        $order = Order::where('order_no',$request->invoice)->first();
        $qr_code = QrCode::generate($order->uuid);
        $url = Storage::temporaryUrl(
            'logo.png',
            now()->addMinutes(3)
        );
        return view('partials.reports.invoice_view', compact('order','qr_code','url'))->render();

    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
