<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\DefaultPhotoRequest;
use App\Models\DefaultPhoto;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class DefaultPhotoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DefaultPhotoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\DefaultPhoto::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/default-photo');
        CRUD::setEntityNameStrings('default photo', 'default photos');
        (new SetCrudPermission())->execute('logo', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([   // Browse
            'name'  => 'logo',
            'label' => 'Logo',
            'type'  => 'closure',
            'function' => function () {
                $url = Storage::temporaryUrl(
                    "shops/logo/shop_default.png",
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}' ><img src='{$url}' alt='banner' width='50' height='50'/></a> ";
            }
        ],);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DefaultPhotoRequest::class);
        CRUD::addField([
            'name'      => 'logo',
            'label'     => 'Logo',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'local',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function store(DefaultPhotoRequest $request)
    {
        if(request()->has('logo')){
            $file = request()->file('logo');
            $path = 'shops/logo/shop_default.png';
            if(Storage::exists($path)){
                Storage::delete($path);
            }
            DefaultPhoto::updateOrCreate(
                ['logo' => $path],
            );
            Storage::put($path, file_get_contents($file));

        }
        \Alert::add("success","Successfully Updated Default Shop Logo")->flash();
        return redirect("admin/default-photo");

    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
