<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\StockinLogRequest;
use App\Models\Product;
use App\Models\StockinLog;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StockinLogCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockinLogCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StockinLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stockin-log');
        CRUD::setEntityNameStrings('stock in', 'stock in');
        (new SetCrudPermission())->execute('stock in', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        CRUD::addFilter(
            [
                'name' => 'product_id',
                'type' => 'select2_ajax',
                'label' => "Search Single Product",
                'placeholder' => "Type Proudct Name to Search",
            ],
            url('admin/get-product'),
            function ($value) {
                CRUD::addClause('where', function ($q) use ($value) {
                    return $q->where('product_id', '=', $value);
                });
            }
        );

        $this->crud->addFilter([
            'name'  => 'product_ajax',
            'type'  => 'select2_multiple',
            'label' => 'Select Multiple Products'
        ], function () {
            return $this->getallProduct();
        }, function ($value) { // if the filter is active
            CRUD::addClause('where', function ($q) use ($value) {
                return $q->whereIn('product_id', (array)$value);
            });
        });
        CRUD::column('product_id');
        CRUD::column('user_id');
        CRUD::column('quantity');
        CRUD::column('invoice_no');
        CRUD::column('remark');
        CRUD::column('created_at');
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->enableExportButtons();
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StockinLogRequest::class);
        CRUD::addField(
            [
                'label'       => "Select Product",
                'type'        => "select2_from_ajax",
                'name'        => 'product_id',
                'attribute'   => "name",
                'model'       => '\App\Models\StockinLog',
                'data_source' => url("admin/get-product-name"),
                'placeholder'             => "Select Product",
                'minimum_input_length'    => 0,
            ],
        );
        CRUD::addField([
            'type' => 'hidden',
            'name' => 'user_id',
            'default' => backpack_user()->id,
        ]);
        CRUD::field('quantity');
        CRUD::field('invoice_no');
        CRUD::field('remark');


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    public function store(StockinLogRequest $request)
    {
        $product_id = $request->product_id;
        $user_id = $request->user_id;
        $quantity = $request->quantity;
        $invoice_no = "";
        $remark = "";
        if($request->has('invoice_no'))
        {
            $invoice_no = $request->invoice_no;
        }
        if($request->has('remark'))
        {
            $remark = $request->remark;
        }
        StockinLog::create([
            'user_id' => $user_id,
            'product_id' => $product_id,
            'quantity' => $quantity,
            'invoice_no' => $invoice_no,
            'remark' => $remark
        ]);
        \Alert::add('success', 'Successfully added Stock')->flash();
        return redirect('admin/stockin-log');
    }
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function getallProduct()
    {
        $options = Product::get()->pluck('name', 'id')->toArray();
        return $options;
    }


}
