<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Shop;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\ProfitDailyLog;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

        if(backpack_user()->can('dashboard'))
        {
            $today = Carbon::now();
            $total_days = $today->daysInMonth;
            $currentDate = Carbon::now()->format('Y-m-d');
            $this_year = $today->year;
            $this_month = $today->month;

            if($request->from_date && $request->to_date)
            {
                $from_date_input_value = $request->from_date;
                $to_date_input_value   = $request->to_date;

                $fromDate = $request->input('from_date');
                $fromCarbonDate = Carbon::createFromFormat('Y-m-d', $fromDate);

                $toDate = $request->input('to_date');
                $toCarbonDate = Carbon::createFromFormat('Y-m-d', $toDate);

                $from_year  = $fromCarbonDate->year;
                $from_month = $fromCarbonDate->month;
                $from_day   = $fromCarbonDate->day;

                $to_year  = $toCarbonDate->year;
                $to_month = $toCarbonDate->month;
                $to_day   = $toCarbonDate->day;

                if($from_year == $to_year &&  $from_month == $to_month ){
                    $start_loop_count  = $from_day;
                    $start_loop_end = $to_day;
                }
                else if($from_year == $to_year){
                    $start_loop_count  = $from_month;
                    $start_loop_end = $to_month;


                }
                else if($from_year != $to_year){
                    $start_loop_count  = $from_year;
                    $start_loop_end = $to_year;
                }

            }
            else
            {
                $from_date_input_value = $this_month < 10 ? $this_year."-0".$this_month."-01" : $this_year."-".$this_month."-01";
                $to_date_input_value   = $this_month < 10 ? $this_year."-0".$this_month."-".$total_days : $this_year."-".$this_month."-".$total_days;
                $start_loop_count  = 1;
                $start_loop_end = $total_days;
                $from_year = 0;
                $from_month = 0;
                $to_year = 0;
                $to_month = 0;
            }
// label
// from date over to ddate

            for ($start_loop_count; $start_loop_count <= $start_loop_end; $start_loop_count++) {

                if($request->from_date && $request->to_date)
                {
                    if($from_year == $to_year &&  $from_month == $to_month ){
                        $profit_chart_label[] = $start_loop_count . " day of ".Carbon::create(null, $from_month)->format('F');
                    }
                    else if($from_year == $to_year){
                        $profit_chart_label[] = Carbon::create(null, $start_loop_count)->format('F')." of ".$to_year;
                    }
                    else if($from_year != $to_year){
                        $profit_chart_label[] = $start_loop_count;
                    }
                }
                else{
                    $profit_chart_label[] = $start_loop_count . " day of ".$today->format('F');;
                }


                $profit_chart_data[] = ProfitDailyLog::where(function ($query) use ($start_loop_count,$this_year,$this_month,$request,$from_year,$from_month,$to_year,$to_month) {
                                                    if( !$request->from_date || !$request->to_date)
                                                    {
                                                        $query->whereYear('created_at', $this_year)
                                                            ->whereMonth('created_at',$this_month)
                                                            ->whereDay('created_at', $start_loop_count);
                                                    }
                                                    else if($from_year == $to_year &&  $from_month == $to_month ){
                                                        $query->whereYear('created_at', $from_year)
                                                            ->whereMonth('created_at',$from_month)
                                                            ->whereDay('created_at', $start_loop_count);
                                                    }
                                                    else if($from_year == $to_year){
                                                        $query->whereYear('created_at', $from_year)
                                                            ->whereMonth('created_at',$start_loop_count);
                                                    }
                                                    else if($from_year != $to_year){
                                                        $query->whereYear('created_at', $start_loop_count);
                                                    }
                                                })->sum('total_amount');
            }

            $admins = User::get()->count();
            $shops  = Shop::get()->count();
            $orders  = Order::get()->count();
            $completed_orders  = Order::where('shipping_status',"Completed")->get()->count();
            $payment_pending_orders  = Order::where('payment_status',"Pending")->get()->count();
            $delivering_orders  = Order::where('payment_status',"Completed")->where('shipping_status',"!=","Completed")->get()->count();
            $rejected_orders  = Order::where('shipping_status',"Rejected")->get()->count();
            $today_order = Order::whereDate('created_at',$currentDate)->get()->count();
            $today_income = Order::whereDate('created_at',$currentDate)->where('payment_status','Completed')->get()->sum('net_amount');
            $monthly_income = Order::whereYear('created_at',$this_year)->whereMonth('created_at',$this_month)->where('payment_status','Completed')->get()->sum('net_amount');
            return view("admin.dashboard",compact('rejected_orders','payment_pending_orders','delivering_orders','admins','shops','today_order','today_income','completed_orders','orders','monthly_income','profit_chart_label','profit_chart_data','from_date_input_value','to_date_input_value'));
        }else
        {
            abort(403);
        }

    }
}
