<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Product;
use App\Models\StockLog;
use App\FinanceTransaction;
use Illuminate\Support\Str;
use App\Constants\GetStatus;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\ProductBalance;
use App\Models\FinanceCashAccount;
use Illuminate\Support\Facades\DB;
use App\Models\PurchaseOrderDetail;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PurchaseOrderRequest;
use App\Http\Requests\CompletePurchaseOrderRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CompletePurchaseOrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CompletePurchaseOrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CompletePurchaseOrder::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/complete-purchase-order');
        CRUD::setEntityNameStrings('complete purchase order', 'complete purchase orders');
    }

    protected function setupShowOperation()
    {
        $this->crud->addClause('with', 'purchaseOrderDetails');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addClause('where', 'payment_status', 'Completed');
        $this->crud->orderBy('created_at', 'desc');
        $this->crud->disableResponsiveTable();
        $this->crud->enableDetailsRow();
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        $this->crud->addFilter([
            'name'  => 'order_status',
            'type'  => 'dropdown',
            'label' => 'Order Status'
        ],
            GetStatus::ORDER_STATUS,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', 'order_status', $value);
        });

        $this->crud->addFilter([
            'name'  => 'payment_status',
            'type'  => 'dropdown',
            'label' => 'Payment Status'
        ],
            GetStatus::PAYMENT_STATUS,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', 'payment_status', $value);
        });

        $this->crud->addFilter([
            'name' => 'supplier_id',
            'type' => 'select2',
            'label' => 'Supplier',
        ], function() {
            return \App\Models\Supplier::pluck('name', 'id')->toArray();
        });

        CRUD::column('invoice_no');
        CRUD::column('order_no');
        CRUD::column('reference_id');
        CRUD::addColumn([
            'name' => 'supplier_id',
            'type' => 'relationship',
            'label' => 'Supplier Name',
        ]);
        CRUD::column('total_amount');
        CRUD::addColumn([
            'label' => 'Order status',
            'name' => 'order_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->order_status == "Pending") {
                    $color = "badge-warning";
                }
                if ($entry->order_status == "Arrived") {
                    $color = "badge-success";
                }
                if ($entry->order_status == "Cancelled") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->order_status . '</span>';
            }
        ]);

        CRUD::addColumn([
            'label' => 'Payment Status',
            'name' => 'payment_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->payment_status == "Pending") {
                    $color = "badge-warning";
                }
                if ($entry->payment_status == "Completed") {
                    $color = "badge-success";
                }
                if ($entry->payment_status == "Rejected") {
                    $color = "badge-danger";
                }
                if ($entry->payment_status == "Credit Completed") {
                    $color = "badge-info";
                }

                return '<span class="badge ' . $color . ' badge-pill">' . $entry->payment_status . '</span>';
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    public function showDetailsRow($id)
    {
        $todayPurchaseOrder = PurchaseOrder::find($id);

        return view('partials.purchaseOrders.purchaseOrderDetailsForButton', ['todayPurchaseOrder' => $todayPurchaseOrder]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PurchaseOrderRequest::class);
        CRUD::field('supplier_id');
        CRUD::field('invoice_no');
        CRUD::field('remark');
        CRUD::field('date');

        CRUD::addField([   // repeatable
            'name'  => 'products',
            'label' => 'Product',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'type' => 'select2_from_ajax',
                    'name' => 'product_id', // the relationship name in your Model
                    'label' => 'Select product',
                    'entity' => 'products', // the relationship name in your Model
                    'attribute' => 'name', // attribute on Article that is shown to admin
                    'placeholder' => "Select Product", // placeholder for the select
                    'minimum_input_length' => 0, // minimum characters to type before querying results
                    'data_source' => url("admin/api/product"), // url to controller search function (with /{id} should return model)
                    'wrapperAttributes' => [
                        'class' => 'form-group col-md-6'
                    ],
                    'attributes' => [
                        'class' => 'form-control'
                    ],
                ],
                [
                    'name'    => 'quantity',
                    'type'    => 'number',
                    'label'   => 'Quantity',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'price',
                    'type'    => 'number',
                    'label'   => 'Price',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
            ],

            // optional
            'new_item_label'  => 'Add Product', // customize the text of the button
            'allows_null' => false,
            'init_rows' => 1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
            'max_rows' => 200, // maximum rows allowed, when reached the "new item" button will be hidden
        ],);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store(PurchaseOrderRequest $request)
    {
        $total_product_price = 0;
        foreach ($request->products as $product) {
            $total_product_price += $product['quantity']*$product['price'];
        }

        try {
            DB::beginTransaction();
            $purchase_order =  PurchaseOrder::create([
                'user_id' => Auth::id(),
                'supplier_id' => $request->supplier_id,
                'invoice_no' => $request->invoice_no,
                'order_no' => 'ORD'.random_int(100000000,999999999),
                'reference_id' => Str::random(15),
                'remark' => $request->remark,
                'total_amount' => $total_product_price,
                'date' => $request->date,
            ]);

            foreach ($request->products as $product) {
                PurchaseOrderDetail::create([
                    'purchase_order_id' => $purchase_order->id,
                    'product_id' => $product['product_id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                ]);
            }
            DB::commit();
            return redirect('admin/purchase-order');
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */

}
