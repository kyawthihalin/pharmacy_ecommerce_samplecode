<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Product;
use App\Models\SkuCode;
use App\Models\OrderDetail;
use App\Models\GroupProduct;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Actions\SetCrudPermission;
use Illuminate\Support\Facades\DB;
use App\Imports\ProductStockImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\GroupProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Requests\GroupProductUpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class GroupProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GroupProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/group-product');
        CRUD::setEntityNameStrings('group product', 'group products');
        CRUD::addClause('where', 'is_group', 1);
        (new SetCrudPermission())->execute('product', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();

        // CRUD::column('product_id');
        CRUD::addColumn([
            'name' => 'product_id',
            'type' => 'text',
            'label' => 'Group Product Id',
        ]);
        CRUD::column('name');
        CRUD::addColumn([
            'name' => 'sku_code_id',
            'type' => 'select',
            'attribute' => 'code',
            'entity' => 'skuCode',
            'model' => SkuCode::class,
        ]);
        CRUD::addColumn([
            'name' => 'feature_image',
            'type' => 'closure',
            'label' => 'Feature Image',
            'function' => function ($entry) {
                $url = Storage::temporaryUrl(
                    $entry->feature_image,
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}'><img src='{$url}' alt='Feature Image' width='50' height='50'/></a>";
            },
        ]);
        CRUD::column('brand_id');
        CRUD::column('unit_id');
        CRUD::addColumn([
            'name' => 'price',
            'type' => 'number',
        ]);
        CRUD::addColumn([
            'name' => 'quantity',
            'type' => 'closure',
            'function' => function ($entry) {
                $quantity = ProductBalance::where('product_id', $entry->id)->sum('quantity');
                return $quantity;
            },
        ]);
        CRUD::addColumn([
            'name' => 'created_at',
            'type' => 'closure',
            'function' => function ($entry) {
                return Carbon::parse($entry->created_at)->format('Y-m-d');
            },
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
        CRUD::addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Type Product Name'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('name', 'LIKE', "%{$value}%");
                });
            }
        );

        CRUD::addFilter(
            [
                'name' => 'product_id',
                'type' => 'select2',
                'label' => 'Product ID',
            ],
            function () {
                return \App\Models\Product::where('is_group', 1)->pluck('product_id', 'product_id')->toArray();
            },
            function ($value) {
                $this->crud->addClause('where', 'product_id', $value);
            }
        );

        CRUD::addFilter(
            [
                'name' => 'sku_code_id',
                'type' => 'select2',
                'label' => 'SKU Code',
            ],
            function () {
                return \App\Models\SkuCode::pluck('code', 'id')->toArray();
            },
            function ($value) {
                $this->crud->addClause('where', 'sku_code_id', $value);
            }
        );

        CRUD::addFilter([
            'name'  => 'status',
            'type'  => 'select2',
            'label' => 'Status'
        ], [
            1 => 'PRODUCT ACTIVE',
            0 => 'PRODUCT INACTIVE',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });
    }

    public function importView()
    {
        return view('partials.products.product_import', ['crud' => $this->crud]);
    }

    public function showDetailsRow($id)
    {
        $product = Product::find($id);
        return view('partials.products.productDetails', ['product' => $product]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductRequest::class);

        CRUD::addField([
            'label' => 'Group Products',
            'type' => 'select2_multiple',
            'name' => 'group_products',
            'attribute' => 'name',
            'model' => Product::class,
            'select_all' => true,
            'options' => function ($query) {
                $product_ids = GroupProduct::pluck('product_id')->toArray();
                return $query->where('is_group', 0)->whereNotIn('id', $product_ids)->get();
            },
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'type' => 'hidden',
            'name' => 'hidden',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product ID',
            'name' => 'product_id',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        CRUD::addField([
            'label' => 'Product Name',
            'name' => 'name',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Category',
            'type' => 'relationship',
            'name' => 'categories',
            'entity' => 'category',
            'attribute' => 'name',
            'inline_create' => true,
            'ajax' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Choose Brand',
            'type' => 'relationship',
            'name' => 'brand_id',
            'entity' => 'brand',
            'attribute' => 'name',
            'inline_create' => true,
            'ajax' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Choose Unit',
            'type' => 'relationship',
            'name' => 'unit_id',
            'entity' => 'unit',
            'attribute' => 'name',
            'inline_create' => true,
            'ajax' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'SKU Code',
            'name' => 'sku_code',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Price',
            'name' => 'price',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product Limit Per Order',
            'name' => 'limit',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product Warning Count',
            'name' => 'warning_count',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Price',
            'name' => 'discount',
            'type' => 'number',
            'default' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Type',
            'name' => 'discount_type',
            'type' => 'select2_from_array',
            'options' => [
                'No Discount' => 'No Discount',
                'Percent' => 'Percent',
                'Fixed' => 'Fixed',
            ],
            'default' => 'No Discount',
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Start Date',
            'name' => 'discount_start_date',
            'type' => 'datetime_picker',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Start Date',
            'name' => 'discount_end_date',
            'type' => 'datetime_picker',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::field('description')->type('wysiwyg');

        CRUD::addField([
            'name'      => 'feature_image',
            'label'     => 'Feature Image',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'local',
            //'temporary' => 10
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        CRUD::addField([
            'name'      => 'gallery',
            'label'     => 'Product Gallery',
            'type'      => 'upload_multiple',
            'upload'    => true,
            'disk'      => 'local',
            //'temporary' => 10
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
        ]);

        CRUD::addField([
            'label' => 'Active Status',
            'type' => 'checkbox',
            'name' => 'status',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
        ]);

        CRUD::addField([
            'label' => 'Selling Status',
            'type' => 'checkbox',
            'name' => 'is_selling',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
        ]);

        CRUD::addField([
            'label' => 'Special Product',
            'type' => 'checkbox',
            'name' => 'is_popular',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $id = CRUD::getCurrentEntryId();
        $ids = GroupProduct::where('group_id', $id)->pluck('product_id')->toArray();

        CRUD::addField([
            'label' => 'Group Products',
            'type' => 'select2_multiple_manual',
            'name' => 'group_products',
            'attribute' => 'name',
            // 'entity' => 'groupProducts',
            'model' => Product::class,
            'default_value' => $ids,
            // 'pivot' => true,
            'select_all' => true,
            'options' => function ($query) use ($ids) {
                $product_ids = GroupProduct::whereNotIn('product_id', $ids)->pluck('product_id')->toArray();
                return $query->where('is_group', 0)->whereNotIn('id', $product_ids)->get();
            },
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'type' => 'hidden',
            'name' => 'hidden',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product ID',
            'name' => 'product_id',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        CRUD::addField([
            'label' => 'Product Name',
            'name' => 'name',
            'type' => 'text',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Category',
            'type' => 'relationship',
            'name' => 'categories',
            'entity' => 'category',
            'attribute' => 'name',
            'inline_create' => true,
            'ajax' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Choose Brand',
            'type' => 'relationship',
            'name' => 'brand_id',
            'entity' => 'brand',
            'attribute' => 'name',
            'inline_create' => true,
            'ajax' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Choose Unit',
            'type' => 'relationship',
            'name' => 'unit_id',
            'entity' => 'unit',
            'attribute' => 'name',
            'inline_create' => true,
            'ajax' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        $sku_code = $this->crud->getCurrentEntry()->skuCode;

        CRUD::addField([
            'label' => 'SKU Code',
            'name' => 'sku_code',
            'type' => 'text',
            'default' => $sku_code ? $sku_code->code : '',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Price',
            'name' => 'price',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product Limit Per Order',
            'name' => 'limit',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product Warning Count',
            'name' => 'warning_count',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Price',
            'name' => 'discount',
            'type' => 'number',
            'default' => $sku_code ? $sku_code->discount : 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Type',
            'name' => 'discount_type',
            'type' => 'select2_from_array',
            'options' => [
                'No Discount' => 'No Discount',
                'Percent' => 'Percent',
                'Fixed' => 'Fixed',
            ],
            'default' => $sku_code ? $sku_code->discount_type : 'No Discount',
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Start Date',
            'name' => 'discount_start_date',
            'type' => 'datetime_picker',
            'default' => $sku_code ? $sku_code->discount_start_date : null,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Discount Start Date',
            'name' => 'discount_end_date',
            'type' => 'datetime_picker',
            'default' => $sku_code ? $sku_code->discount_end_date : null,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::field('description')->type('wysiwyg');

        CRUD::addField([
            'name'      => 'feature_image',
            'label'     => 'Feature Image',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'local',
            //'temporary' => 10
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        CRUD::addField([
            'name'      => 'gallery',
            'label'     => 'Product Gallery',
            'type'      => 'upload_multiple',
            'upload'    => true,
            'disk'      => 'local',
            //'temporary' => 10
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        CRUD::addField([
            'label' => 'Active Status',
            'type' => 'checkbox',
            'name' => 'status',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
        ]);

        CRUD::addField([
            'label' => 'Selling Status',
            'type' => 'checkbox',
            'name' => 'is_selling',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
        ]);

        CRUD::addField([
            'label' => 'Special Product',
            'type' => 'checkbox',
            'name' => 'is_popular',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
        ]);
    }

    public function store(GroupProductRequest $request)
    {
        DB::transaction(function () use ($request) {
            $sku_code = SkuCode::where('code', $request->sku_code)->first();

            if (!$sku_code) {
                $sku_code = SkuCode::create([
                    'code' => $request->sku_code,
                    'discount' => $request->discount,
                    'discount_type' => $request->discount_type,
                    'discount_start_date' => $request->discount_start_date,
                    'discount_end_date' => $request->discount_end_date,
                ]);
            }

            if ($request->hasFile('feature_image')) {
                $featureImage = $request->file('feature_image');
                $path = $featureImage->store('products/feature_image');
            }

            if ($request->hasFile('gallery')) {
                $gallery = $request->file('gallery');
                $gallery_paths = [];

                foreach ($gallery as $image) {
                    $gallery_path = $image->store('products/gallery');
                    $gallery_paths[] = $gallery_path;
                }
            }

            $new_array = array_merge($request->except(['feature_image', 'gallery', 'sku_code', 'discount', 'discount_type', 'discount_start_date', 'discount_end_date']), [
                'feature_image' => $path,
                'gallery' => $gallery_paths,
                'sku_code_id' => $sku_code->id,
                'is_group' => 1,
            ]);

            $group_product = Product::create($new_array);

            $group_product->categories()->sync($request->categories);

            $product_ids = $request->group_products;
            foreach ($product_ids as $product_id) {
                GroupProduct::create([
                    'group_id' => $group_product->id,
                    'product_id' => $product_id,
                ]);
            }
        });

        return redirect('admin/group-product');
    }

    public function update(GroupProductUpdateRequest $request)
    {
        DB::transaction(function () use ($request) {
            $group_product = Product::find($request->id);

            $sku_code = SkuCode::where('code', $request->sku_code)->first();

            $data = [
                'code' => $request->sku_code,
                'discount' => $request->discount,
                'discount_type' => $request->discount_type,
                'discount_start_date' => $request->discount_start_date,
                'discount_end_date' => $request->discount_end_date,
            ];

            if (!$sku_code) {
                $sku_code = SkuCode::create($data);
            } else {
                $sku_code->update($data);
            }

            if ($request->hasFile('feature_image')) {
                $featureImage = $request->file('feature_image');
                $path = $featureImage->store('products/feature_image');
            }

            if ($request->hasFile('gallery')) {
                $gallery = $request->file('gallery');
                $gallery_paths = [];

                foreach ($gallery as $image) {
                    $gallery_path = $image->store('products/gallery');
                    $gallery_paths[] = $gallery_path;
                }
            }

            $new_array = array_merge($request->except(['feature_image', 'gallery', 'sku_code_id']), [
                'feature_image' => $request->hasFile('feature_image') ? $path : $group_product->feature_image,
                'gallery' => $request->hasFile('gallery') ? $gallery_paths : $group_product->gallery,
                'sku_code_id' => $sku_code->id,
            ]);

            $group_product->update($new_array);

            $group_product->categories()->sync($request->categories);

            $gps = GroupProduct::where('group_id', $request->id)->get();
            foreach ($gps as $gp) {
                if (!in_array($gp->product_id, $request->group_products)) {
                    $p_qty = $gp->quantity;
                    if ($p_qty > 0) {
                        $gp_qty = ProductBalance::where('product_id', $request->id)->value('quantity');

                        if ($gp_qty > 0) {
                            $net_qty = $gp_qty * $p_qty;
                            $p_balance = ProductBalance::lockForUpdate()
                            ->where('product_id', $gp->product_id)
                            ->orderBy('created_at', 'desc')
                            ->first();
                            $p_balance->increment('quantity', $net_qty);
                        }
                    }
                    $gp->delete();
                }
            }

            $p_id_array = GroupProduct::where('group_id', $request->id)->pluck('product_id')->toArray();

            $product_ids = $request->group_products;
            foreach ($product_ids as $product_id) {
                if (!in_array($product_id, $p_id_array)) {
                    GroupProduct::create([
                        'group_id' => $request->id,
                        'product_id' => $product_id,
                    ]);
                }
            }
        });

        return redirect('admin/group-product');
    }

    public function fetchCategory()
    {
        return $this->fetch(\App\Models\Category::class);
    }

    public function fetchBrand()
    {
        return $this->fetch(\App\Models\Brand::class);
    }

    public function fetchUnit()
    {
        return $this->fetch(\App\Models\Unit::class);
    }

    public function statusChange(Request $request)
    {
        $status = $request->status;
        $is_selling = $request->is_selling;
        $pid = $request->pid;
        $product = Product::find($pid);
        if ($product) {
            if ($status == "false") {
                $active = 0;
            } else {
                $active = 1;
            }

            if ($is_selling == "false") {
                $selling = 0;
            } else {
                $selling = 1;
            }
            $product->status = $active;
            $product->is_selling = $selling;
            $product->save();
            return response()->json([
                "status" => 200,
                "message" => "Success"
            ], 200);
        }
    }

    public function getProduct(Request $request)
    {
        $uri  = parse_url(url()->previous());
        $oid  = trim($uri['path'], "/admin/order//edit");
        $product_id = OrderDetail::where('order_id', $oid)->pluck('product_id')->toArray();
        $page = $request->page;
        $resultCount = 25;

        $offset = ($page - 1) * $resultCount;
        $products = Product::where('name', 'LIKE',  '%' . $request->term . '%')->whereNotIn('id', $product_id)->orderBy('name')->skip($offset)->take($resultCount)->get(['id', DB::raw('name as text')]);

        $count = Count(Product::where('name', 'LIKE',  '%' . $request->term . '%')->whereNotIn('id', $product_id)->orderBy('name')->get(['id', DB::raw('name as text')]));
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $products,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

    public function addProduct(Request $request)
    {
        $products = $request->product_id;
        $product_array = [];

        foreach ($products as $product) {
            $product_price = Product::find($product);

            $todayDate = Carbon::today()->format('Y-m-d');

            $product_sku = SkuCode::where('id',$product_price->sku_code_id)->where('discount_start_date', '<=', $todayDate)->where('discount_end_date', '>=', $todayDate)->first();

            if($product_sku)
            {
                $discount_amount = $product_sku->discount;
            }
            else{
                $discount_amount = 0;
            }

            $product_array[] = ['product_id' => $product, 'amount' => $product_price->price,'discount_amount' => $discount_amount];
        }
        $order_id = $request->oid;



        $order = Order::find($order_id);
        if ($order) {
            $order->details()->createMany($product_array);
        }
    }
}
