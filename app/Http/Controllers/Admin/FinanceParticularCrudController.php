<?php

namespace App\Http\Controllers\Admin;

use App\FinanceParticular;
use Illuminate\Support\Str;
use App\Constants\GetStatus;
use App\Actions\SetCrudPermission;
use App\Http\Requests\FinanceParticularRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FinanceParticularCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FinanceParticularCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\FinanceParticular::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/finance-particular');
        CRUD::setEntityNameStrings('finance particular', 'finance particulars');
        // (new SetCrudPermission())->execute('finance particular', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        $this->crud->disableResponsiveTable();

        CRUD::column('name');
        CRUD::column('reference_id');
        CRUD::addColumn([
            'name' => 'debit_account_id',
            'type' => 'relationship',
            'label' => 'Debit account',
        ]);

        CRUD::addColumn([
            'label' => 'Debit Account Operation',
            'name' => 'debit_account_operation',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->debit_account_operation == "Addition") {
                    $color = "badge-success";
                }
                if ($entry->debit_account_operation == "Subtraction") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->debit_account_operation . '</span>';
            }
        ]);

        CRUD::addColumn([
            'name' => 'credit_account_id',
            'type' => 'relationship',
            'label' => 'Credit account',
        ]);

        CRUD::addColumn([
            'label' => 'Credit Account Operation',
            'name' => 'credit_account_operation',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->credit_account_operation == "Addition") {
                    $color = "badge-success";
                }
                if ($entry->credit_account_operation == "Subtraction") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->credit_account_operation . '</span>';
            }
        ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FinanceParticularRequest::class);

        CRUD::field('name');

        CRUD::field('debit_account_id');

        CRUD::addField([
            'name'        => 'debit_account_operation',
            'label'       => "Choose Debit Account Operation Type",
            'type'        => 'select2_from_array',
            'options'     => GetStatus::FINANCE_ACCOUNT_OPERATION,
            'allows_null' => true,
            'allows_multiple' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'name'        => 'reference_id',
            'type'        => 'hidden',
            'value'     => Str::random(10),
        ]);

        CRUD::field('credit_account_id');

        CRUD::addField([
            'name'        => 'credit_account_operation',
            'label'       => "Choose Credit Account Operation Type",
            'type'        => 'select2_from_array',
            'options'     => GetStatus::FINANCE_ACCOUNT_OPERATION,
            'allows_null' => true,
            'allows_multiple' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        // CRUD::addField(['name' => 'amount', 'type' => 'number']);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store(FinanceParticularRequest $request)
    {
        $data = [
            'name' => $request->name,
            'reference_id' => $request->reference_id,
            'debit_account_id' => $request->debit_account_id,
            'credit_account_id' => $request->credit_account_id,
        ];

        if ($request->debit_account_id == null) {
            $data['debit_account_operation'] = null;
        } else {
            $data['debit_account_operation'] = $request->debit_account_operation;
        }

        if ($request->credit_account_id == null) {
            $data['credit_account_operation'] = null;
        } else {
            $data['credit_account_operation'] = $request->credit_account_operation;
        }

        FinanceParticular::create($data);
        return redirect('admin/finance-particular');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
