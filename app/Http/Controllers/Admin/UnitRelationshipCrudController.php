<?php

namespace App\Http\Controllers\Admin;

use App\Models\Unit;
use App\Models\SkuCode;
use Illuminate\Http\Request;
use App\Models\UnitRelationship;
use App\Actions\SetCrudPermission;
use App\Http\Requests\UnitRelationshipRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class UnitRelationshipCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UnitRelationshipCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\UnitRelationship::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/unit-relationship');
        CRUD::setEntityNameStrings('unit relationship', 'unit relationships');
        (new SetCrudPermission())->execute('unit relationship', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => 'SKU Code',
            'type' => 'select',
            'name' => 'sku_code_id',
            'entity' => 'skuCode',
            'attribute' => 'code',
            'model' => SkuCode::class,
        ]);
        CRUD::addColumn([
            'label' => 'Base Unit',
            'type' => 'select',
            'name' => 'base_unit_id',
            'entity' => 'baseUnit',
            'attribute' => 'name',
            'model' => Unit::class,
        ]);
        CRUD::addColumn([
            'label' => 'Conversion Unit',
            'type' => 'select',
            'name' => 'conversion_unit_id',
            'entity' => 'conversionUnit',
            'attribute' => 'name',
            'model' => Unit::class,
        ]);
        CRUD::column('conversion_factor');
        // CRUD::column('created_at');
        // CRUD::column('updated_at');
        $this->crud->enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UnitRelationshipRequest::class);

        CRUD::addField([
            'label' => 'Base Unit',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'base_unit_id',
            'attribute' => 'name',
            'model' => Unit::class,
            'data_source' => url("admin/api/unit-by-sku-code"),
            'placeholder' => 'Select Base Unit',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Conversion Unit',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'conversion_unit_id',
            'attribute' => 'name',
            'model' => Unit::class,
            'data_source' => url("admin/api/conversion-unit"),
            'placeholder' => 'Select Conversion Unit',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Conversion Factor (Quantity)',
            'name' => 'conversion_factor',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'SKU Code',
            'type' => 'select2',
            'name' => 'sku_code_id',
            'attribute' => 'code',
            'model' => SkuCode::class,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(UnitRelationshipRequest::class);

        CRUD::removeFields(['base_unit_id', 'conversion_unit_id', 'conversion_factor', 'sku_code']);
        $sku_code = UnitRelationship::find(CRUD::getCurrentEntry()->id)->sku_code;

        CRUD::addField([
            'label' => 'Base Unit',
            'type' => 'select2',
            'name' => 'base_unit_id',
            'attribute' => 'name',
            'model' => Unit::class,
            'default' => CRUD::getCurrentEntry()->base_unit_id,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control',
                'disabled'    => 'disabled',
            ],
        ]);

        CRUD::addField([
            'label' => 'Conversion Unit',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'conversion_unit_id',
            'attribute' => 'name',
            'model' => Unit::class,
            'data_source' => url("admin/api/conversion-unit"),
            'placeholder' => 'Select Conversion Unit',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control',
                'readonly'    => 'readonly',
                'disabled'    => 'disabled',
            ],
        ]);

        CRUD::addField([
            'label' => 'Conversion Factor (Quantity)',
            'name' => 'conversion_factor',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        if ($sku_code != null) {
            CRUD::addField([
                'label' => 'SKU Code',
                'type' => 'text',
                'name' => 'sku_code',
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ],
            ]);
        }
    }

    public function store(UnitRelationshipRequest $request)
    {
        ['base_unit_id' => $base_unit_id, 'conversion_unit_id' => $conversion_unit_id, 'conversion_factor' => $conversion_factor] = $request->all();

        $existed = UnitRelationship::where('sku_code_id', null)->where('base_unit_id', $base_unit_id)->where('conversion_unit_id', $conversion_unit_id)->where('conversion_factor', $conversion_factor)->first();

        if (!$existed) {
            UnitRelationship::create([
                'sku_code_id' => $request->sku_code_id ? $request->sku_code_id : null,
                'base_unit_id' => $base_unit_id,
                'conversion_unit_id' => $conversion_unit_id,
                'conversion_factor' => $conversion_factor,
            ]);
        }

        return redirect('admin/unit-relationship');
    }

    public function update(Request $request)
    {
        $request->validate([
            'sku_code_id' => 'nullable|exists:products,sku_code_id|string',
            'conversion_factor' => 'required|numeric',
        ]);

        $unit_relationship = UnitRelationship::find($request->id);
        $unit_relationship->update([
            'conversion_factor' => $request->conversion_factor,
            'sku_code_id' => $request->sku_code_id ? $request->sku_code_id : null,
        ]);

        return redirect('admin/unit-relationship');
    }
}
