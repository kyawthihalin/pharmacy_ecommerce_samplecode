<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use Carbon\Carbon;
use App\Models\StockLog;
use App\Constants\Status;
use App\Constants\GetStatus;
use Illuminate\Http\Request;
use App\Actions\SetCrudPermission;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StockReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StockLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stock-report');
        CRUD::setEntityNameStrings('stock report', 'stock reports');
        // (new SetCrudPermission())->execute('stock report', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::addField([
            'label' => 'SKU Code',
            'type' => 'select2',
            'name' => 'sku_code',
            'attribute' => 'code',
            'model' => \App\Models\SkuCode::class,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 skuCode'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product ID',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'product_id',
            'attribute' => 'product_id',
            'model' => \App\Models\Product::class,
            'data_source' => url("admin/api/product-by-sku-code"),
            'placeholder' => 'Select Product ID',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 productId'
            ],
        ]);

        CRUD::addField([
            'label'     => "Type",
            'type'      => 'select2_from_array',
            'name'      => 'type', // the db column for the foreign key
            'options'     => GetStatus::STOCK_TYPE,
            'allows_null' => true,
            // 'default'     => 'one',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 type'
            ]
        ]);
        CRUD::addField([   // date_range
            'name'  => 'start_date',
            'type'  => 'date_picker',
            'label' => 'Start Date',
            // 'default' => Carbon::today()->toDateString(),
            // optional:
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy',
                'language' => 'mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 SDate'
            ]
        ]);
        CRUD::addField([   // date_range
            'name'  => 'end_date',
            'type'  => 'date_picker',
            'label' => 'End Date',
            // 'default' => Carbon::tomorrow()->toDateString(),
            // optional:
            'date_picker_options' => [
                'todayBtn' => 'linked',
                'format'   => 'dd-mm-yyyy',
                'language' => 'mm'
            ],
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 EDate'
            ]
        ]);

        CRUD::addField([
            'name'  => 'route_name',
            'type'  => 'hidden',
            'value' => 'stock_report',
        ]);

        CRUD::addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/stock',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function report(Request $request)
    {
        $sku_code = $request->sku_code;
        $product_id = $request->product_id;
        $type = $request->type;
        $start_date = $request->start_date;
        $end_date   = $request->end_date;

        if ($start_date != null && $end_date != null) {
            if ($start_date > $end_date) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }

            $start_date = new DateTime($start_date);
            $start_date = $start_date->format('Y-m-d 00:00:00');

            $end_date   = new DateTime($end_date);
            $end_date   = $end_date->format('Y-m-d 23:59:59');
        }

        if (($start_date != null && $end_date == null) || ($start_date == null && $end_date != null)) {
            return response()->json([
                'message' => 'Start Date and End Date Must be choose'
            ], 422);
        }

        $stock_logs = StockLog::where(function ($query) use ($product_id, $type, $start_date, $end_date) {
            if ($product_id != null) {
                $query->where('product_id', $product_id);
            }
            if ($type !== null) {
                $query->where('type', $type);
            }
            if ($start_date !== null && $end_date !== null) {
                $query->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            }
        })->whereHas('product', function ($query) use ($sku_code) {
            if ($sku_code != null) {
                $query->where('sku_code_id', $sku_code);
            }
        })->orderBy('updated_at', 'desc');

        if (!$sku_code && !$product_id && !$type && !$start_date && !$end_date) {
            $today = Carbon::today();
            // $stock_logs = $stock_logs->where('created_at', $today)->get();
            $stock_logs = $stock_logs->get();
        } else {
            $stock_logs = $stock_logs->get();
        }

        $sales = $stock_logs->where('type', GetStatus::STOCK_TYPE['Sale']);
        $purchases = $stock_logs->where('type', GetStatus::STOCK_TYPE['Purchase']);
        $adjustments = $stock_logs->whereIn('type', [GetStatus::STOCK_TYPE['Adjustment_In'], GetStatus::STOCK_TYPE['Adjustment_Out']]);
        $balances_in = $stock_logs->where('type', GetStatus::STOCK_TYPE['Balance_In']);
        $balances_out = $stock_logs->where('type', GetStatus::STOCK_TYPE['Balance_Out']);

        return view('partials.reports.stock_view', compact('stock_logs', 'sales', 'purchases', 'adjustments', 'balances_in', 'balances_out'))->render();
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
