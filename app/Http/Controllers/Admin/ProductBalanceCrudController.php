<?php

namespace App\Http\Controllers\Admin;

use App\Models\Unit;
use App\Models\Product;
use App\Models\StockLog;
use App\Constants\GetStatus;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Models\UnitRelationship;
use App\Actions\SetCrudPermission;
use App\Http\Requests\ProductBalanceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductBalanceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductBalanceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ProductBalance::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product-balance');
        CRUD::setEntityNameStrings('stock list', 'stock lists');
        (new SetCrudPermission())->execute('product balance', $this->crud);
        CRUD::denyAccess(['delete', 'show', 'update']);

        // Setters
        CRUD::setTitle('Stock Unit Balance', 'create'); // set the Title for the create action
        CRUD::setHeading('stock unit balance', 'create'); // set the Heading for the create action
        CRUD::setSubheading('stock unit balance', 'create'); // set the Subheading for the create action
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::removeButton('create');

        CRUD::setModel(\App\Models\SkuCode::class);
        CRUD::enableDetailsRow();
        CRUD::addFilter([
            'name' => 'id',
            'type' => 'select2',
            'label' => 'SKU Code',
        ], function () {
            return \App\Models\SkuCode::pluck('code', 'id')->toArray();
        });

        CRUD::addColumn([
            'type' => 'text',
            'name' => 'code',
            'label' => 'SKU Code'
        ]);

        CRUD::addColumn([
            'name' => 'total_quantity',
            'label' => 'Total Quantity',
            'type' => 'closure',
            'function' => function ($entry) {
                $products = Product::where('sku_code_id', $entry->id)->get();
                $string = "";
                foreach ($products as $key => $product) {
                    $quantity = ProductBalance::where('product_id', $product->id)->sum('quantity');
                    $unit = Unit::find($product->unit_id)->name;
                    $string .= '<span class="badge badge-success">' . $quantity .  ' ' . $unit . '</span> ';
                }
                return $string;
            }
        ]);

        $this->crud->enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    public function showDetailsRow($id)
    {
        $all_products = Product::where('sku_code_id', $id)->get();

        foreach ($all_products as $key => $product) {
            $product->quantity = ProductBalance::where('product_id', $product->id)->sum('quantity');
            $products[] = $product;
        }

        return view('partials.productbalance.detail', compact('products'))->render();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        // CRUD::setValidation(ProductBalanceRequest::class);

        CRUD::addField([
            'label' => 'Invoice No',
            'type' => 'select2',
            'name' => 'purchase_order_id',
            'attribute' => 'invoice_no',
            'entity' => 'purchaseOrder',
            'model' => \App\Models\PurchaseOrder::class,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'id' => 'purchase_order_id',
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'SKU Code',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'sku_code',
            'attribute' => 'code',
            'model' => \App\Models\SkuCode::class,
            'data_source' => url("admin/api/sku-code-by-invoice"),
            'placeholder' => 'Select SKU-Code',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'id' => 'sku_code_id',
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'From Unit',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'from_unit_id',
            'attribute' => 'name',
            'model' => \App\Models\Unit::class,
            'data_source' => url("admin/api/balance-from-unit"),
            'placeholder' => 'Select From Unit',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'To Unit',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'to_unit_id',
            'attribute' => 'name',
            'model' => \App\Models\Unit::class,
            'data_source' => url("admin/api/balance-to-unit"),
            'placeholder' => 'Select To Unit',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'id' => 'to_unit_id',
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Quantity',
            'name' => 'quantity',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control',
                // 'min' => 0,
                // 'max' => 10,
            ],
        ]);

        CRUD::addField([
            'label' => 'Purchase Price (optional)',
            'name' => 'purchase_price',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/productbalance/data',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(ProductBalanceRequest::class);
        $this->setupCreateOperation();
    }

    public function store(ProductBalanceRequest $request)
    {
        $sku_code_id = $request->sku_code ? $request->sku_code : ProductBalance::where('purchase_order_id', $request->purchase_order_id)->value('sku_code_id');

        $from_product = Product::where('sku_code_id', $sku_code_id)->where('unit_id', $request->from_unit_id)->first();
        $to_product = Product::where('sku_code_id', $sku_code_id)->where('unit_id', $request->to_unit_id)->first();

        $unit_relationship = UnitRelationship::where(function ($query) use ($request, $sku_code_id) {
            $query->where('base_unit_id', $request->from_unit_id)
                ->where('conversion_unit_id', $request->to_unit_id)
                ->where('sku_code_id', $sku_code_id);
        })->orWhere(function ($query) use ($request, $sku_code_id) {
            $query->where('conversion_unit_id', $request->from_unit_id)
                ->where('base_unit_id', $request->to_unit_id)
                ->where('sku_code_id', $sku_code_id);
        })->first();

        if (!$unit_relationship) {
            $unit_relationship = UnitRelationship::where(function ($query) use ($request) {
                $query->where('base_unit_id', $request->from_unit_id)
                    ->where('conversion_unit_id', $request->to_unit_id)
                    ->whereNull('sku_code_id');
            })->orWhere(function ($query) use ($request) {
                $query->where('conversion_unit_id', $request->from_unit_id)
                    ->where('base_unit_id', $request->to_unit_id)
                    ->whereNull('sku_code_id');
            })->first();
        }

        $from_unit_order = $from_product->unit->lft;
        $to_unit_order = $to_product->unit->lft;

        $result = ($from_unit_order < $to_unit_order)
            ? ($request->quantity * $unit_relationship->conversion_factor)
            : ($request->quantity / $unit_relationship->conversion_factor);

        $from_balance = ProductBalance::lockForUpdate()->where([
            ['product_id', $from_product->id],
            ['purchase_order_id', $request->purchase_order_id],
        ])->first();
        $from_balance->decrement('quantity', $request->quantity);
        $to_balance = ProductBalance::lockForUpdate()->where([
            ['product_id', $to_product->id],
            ['purchase_order_id', $request->purchase_order_id],
        ])->first();
        if (!$to_balance) {
            $to_balance = ProductBalance::create([
                'purchase_order_id' => $request->purchase_order_id,
                'sku_code_id' => $sku_code_id,
                'product_id' => $to_product->id,
                'quantity' => 0,
                'price' => $request->purchase_price ? $request->purchase_price : 0,
            ]);
        }
        $to_balance->increment('quantity', $result);

        $from_balance->refresh();
        $to_balance->refresh();

        $from_current_qty = ProductBalance::where('product_id', $from_balance->product_id)->sum('quantity');
        $to_current_qty = ProductBalance::where('product_id', $to_balance->product_id)->sum('quantity');

        StockLog::create([
            'product_id' => $from_balance->product_id,
            'type' => GetStatus::STOCK_TYPE['Balance_Out'],
            'quantity' => $request->quantity,
            'current_qty' => $from_current_qty,
        ]);

        StockLog::create([
            'product_id' => $to_balance->product_id,
            'type' => GetStatus::STOCK_TYPE['Balance_In'],
            'quantity' => $result,
            'current_qty' => $to_current_qty,
        ]);

        return redirect('admin/product-balance/create')->with('success', 'Sccessfully stock balancing!');
    }

    public function data(Request $request)
    {
        $invoice_no = $request->purchase_order_id;
        $sku_code_id = $request->sku_code_id;

        $product_balances = null;
        $unit_relationships = null;

        if ($invoice_no || $sku_code_id) {
            $product_balances_query = ProductBalance::query();
            $product_query = Product::query();

            if ($invoice_no && $sku_code_id) {
                $product_balances_query->where([
                    ['sku_code_id', $sku_code_id],
                    ['purchase_order_id', $invoice_no],
                ]);
            } else {
                $product_balances_query->where(function ($query) use ($sku_code_id, $invoice_no) {
                    $query->where('sku_code_id', $sku_code_id);
                    if ($invoice_no) {
                        $query->orWhere('purchase_order_id', $invoice_no);
                    } else {
                        $query->whereNull('purchase_order_id');
                    }
                });
            }

            // $sku_code_array = array_unique($product_balances_query->pluck('sku_code_id')->toArray());
            $product_query->where('sku_code_id', $sku_code_id);

            $product_balances = $product_balances_query->get();
            $product_unit_id = $product_query->pluck('unit_id')->toArray();

            $sku_code_relationships = UnitRelationship::whereIn('base_unit_id', $product_unit_id)
                ->whereIn('conversion_unit_id', $product_unit_id)
                ->where('sku_code_id', $sku_code_id)
                ->get();

            $unit_relationships = UnitRelationship::whereIn('base_unit_id', $product_unit_id)
                ->whereIn('conversion_unit_id', $product_unit_id)
                ->whereNull('sku_code_id')
                ->get();

            foreach ($unit_relationships as $key => $unit_relationship) {
                foreach ($sku_code_relationships as $sku_code_relationship) {
                    if ($sku_code_relationship->base_unit_id == $unit_relationship->base_unit_id && $sku_code_relationship->conversion_unit_id == $unit_relationship->conversion_unit_id) {
                        $unit_relationships->forget($key);
                        $unit_relationships->push($sku_code_relationship);
                    }
                }
            }
        }

        return view('partials.productbalance.view', compact('product_balances', 'unit_relationships'))->render();
    }

    public function changeSellingStatus(Request $request)
    {
        $is_selling = $request->is_selling;
        $pid = $request->pid;
        $product = Product::find($pid);
        if ($product) {
            if ($is_selling == "false") {
                $selling = 0;
            } else {
                $selling = 1;
            }

            $product->is_selling = $selling;
            $product->save();
            return response()->json([
                "status" => 200,
                "message" => "Success"
            ], 200);
        }
    }
}
