<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\ProductReport;
use App\Models\ProductBalance;
use App\Actions\SetCrudPermission;
use App\Http\Requests\ProductReportRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ProductReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product-report');
        CRUD::setEntityNameStrings('product report', 'product reports');
        (new SetCrudPermission())->execute('product report', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductReportRequest::class);
        $this->crud->addField(
            [   // date_range
                'name'  => 'Sdate',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                // 'default' => Carbon::today()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 SDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'Edate',
                'type'  => 'date_picker',
                'label' => 'End Date',
                // 'default' => Carbon::tomorrow()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 EDate'
                ]
            ],
        );

        $this->crud->addField(
            [   // date_range
                'name'  => 'DSdate',
                'type'  => 'date_picker',
                'label' => 'Discount Start Date',
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 DSDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'DEdate',
                'type'  => 'date_picker',
                'label' => 'Discount End Date',
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 DEDate'
                ]
            ],
        );
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/product'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function report(Request $request)
    {
        $startDate = $request->start_date;
        $endDate   = $request->end_date;
        $dstartDate = $request->dstart_date;
        $dendDate   = $request->dend_date;

        if ($startDate != null) {
            $startDate = new DateTime($startDate);
            $startDate = $startDate->format('Y-m-d 00:00:00');
        }

        if ($endDate != null) {
            $endDate   = new DateTime($endDate);
            $endDate   = $endDate->format('Y-m-d 23:59:59');
        }

        if ($dstartDate != null) {
            $dstartDate = new DateTime($dstartDate);
            $dstartDate = $dstartDate->format('Y-m-d 00:00:00');
        }

        if ($dendDate != null) {
            $dendDate   = new DateTime($dendDate);
            $dendDate   = $dendDate->format('Y-m-d 23:59:59');
        }

        if ($startDate != null && $endDate != null && $dstartDate != null && $dendDate != null) {
            if ($startDate > $endDate || $dstartDate > $dendDate) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }
            $products = ProductReport::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->where('discount_start_date', '>=', $dstartDate)->where('discount_end_date', '<=', $dendDate)->get();
            $products = $this->getQty($products);
            return view('partials.reports.product_view', compact('products'))->render();
        } elseif ($startDate != null && $endDate != null && $dstartDate == null && $dendDate == null) {
            if ($startDate > $endDate) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }
            $products = ProductReport::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
            $products = $this->getQty($products);
            return view('partials.reports.product_view', compact('products'))->render();
        } elseif ($startDate == null && $endDate == null && $dstartDate != null && $dendDate != null) {
            if ($dstartDate > $dendDate) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }
            $products = ProductReport::where('discount_start_date', '>=', $dstartDate)->where('discount_end_date', '<=', $dendDate)->get();
            $products = $this->getQty($products);
            return view('partials.reports.product_view', compact('products'))->render();
        } elseif ($startDate == null && $endDate == null && $dstartDate == null && $dendDate == null) {
            $products = ProductReport::get();
            $products = $this->getQty($products);
            return view('partials.reports.product_view', compact('products'))->render();
        } elseif ($startDate != null || $endDate == null) {
            return response()->json([
                'message' => 'Start Date and End Date Must be choose'
            ], 422);
        } elseif ($startDate == null || $endDate != null) {
            return response()->json([
                'message' => 'Start Date and End Date Must be choose'
            ], 422);
        } elseif ($dstartDate != null || $dendDate == null) {
            return response()->json([
                'message' => 'Discount Start Date and End Date Must be choose'
            ], 422);
        } elseif ($dstartDate == null || $dendDate != null) {
            return response()->json([
                'message' => 'Discount Start Date and End Date Must be choose'
            ], 422);
        }
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function getQty($all_products)
    {
        foreach ($all_products as $key => $product) {
            $product->quantity = ProductBalance::where('product_id', $product->id)->sum('quantity');
            $products[] = $product;
        }

        return $products;
    }
}
