<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\SaleReportRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SaleReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SaleReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Order::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sale-report');
        CRUD::setEntityNameStrings('sale report', 'sale reports');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {

        CRUD::setValidation(SaleReportRequest::class);

        $this->crud->addField(
            [   // date_range
                'name'  => 'Sdate',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                // 'default' => Carbon::today()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 SDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'Edate',
                'type'  => 'date_picker',
                'label' => 'End Date',
                // 'default' => Carbon::tomorrow()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 EDate'
                ]
            ],
        );

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/sale_report',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function detail(Request $request)
    {
    }

    public function report(Request $request)
    {
        $start_date = $request->start_date;
        $end_date   = $request->end_date;

        if ($start_date != null && $end_date != null) {
            if ($start_date > $end_date) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }

            $start_date = new DateTime($start_date);
            $start_date = $start_date->format('Y-m-d 00:00:00');

            $end_date   = new DateTime($end_date);
            $end_date   = $end_date->format('Y-m-d 23:59:59');
        }

        if (($start_date != null && $end_date == null) || ($start_date == null && $end_date != null)) {
            return response()->json([
                'message' => 'Start Date and End Date Must be choose'
            ], 422);
        }

        $sales = Order::where(function ($query) use ($start_date, $end_date) {
            if ($start_date !== null && $end_date !== null) {
                $query->where('updated_at', '>=', $start_date)->where('updated_at', '<=', $end_date);
            }
        })->where('payment_status', 'Completed')
            ->orderBy('updated_at', 'desc')
            ->get();

        $rowspan = 0;
        $sale_reports = [];
        $array = [];
        foreach ($sales as $sale) {
            foreach ($sale->details as $key => $detail) {
                if ($detail->quantity > 0) {
                    $array['product_id'] = $detail->product->product_id;
                    $array['product_name'] = $detail->product->name;
                    $array['sku_code'] = $detail->product->skuCode ? $detail->product->skuCode->code : '-';
                    $array['image'] = $detail->product->feature_image;
                    $array['quantity'] = $detail->quantity;
                    $array['price'] = $detail->product->price;
                    $array['total_amount'] = $detail->amount;
                    $array['updated_at'] = $detail->updated_at;
                    $sale_reports[] = $array;
                }
            }
        }

        return view('partials.reports.sale_report_view', compact('sale_reports'))->render();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
