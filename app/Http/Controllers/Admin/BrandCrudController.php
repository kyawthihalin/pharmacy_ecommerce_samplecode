<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\BrandRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class BrandCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BrandCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Brand::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/brand');
        CRUD::setEntityNameStrings('brand', 'brands');
        (new SetCrudPermission())->execute('brand', $this->crud);

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Type Brand Name'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('name', 'LIKE', "%{$value}%");
                });
            }
        );
        CRUD::column('name');
        CRUD::addColumn([
            'name' => 'feature_status',
            'type' => 'closure',
            'function' => function($entry){
                if($entry->feature_status)
                {
                    return "<span class='badge badge-success badge-pill'>Active</span>";
                }
                return "<span class='badge badge-danger badge-pill'>Inactive</span>";
            }
        ]);
        CRUD::addColumn([
            'name' => 'photo',
            'type' => 'closure',
            'label' => 'Photo',
            'function' => function ($entry) {
                $url = Storage::temporaryUrl(
                    $entry->photo,
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}'><img src='{$url}' alt='Brand Logo' width='50' height='50'/></a>";
            },
        ]);
        CRUD::column('created_at');
        CRUD::column('updated_at');
        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BrandRequest::class);

        CRUD::field('name');
        CRUD::field('feature_status');
        CRUD::addField([
            'label' => "Image",
            'name'      => 'photo',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'local',
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
