<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Models\FinanceCashAccount;
use App\Http\Requests\FinanceCashAccountRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FinanceCashAccountCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FinanceCashAccountCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\FinanceCashAccount::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/finance-cash-account');
        CRUD::setEntityNameStrings('finance cash account', 'finance cash accounts');
        (new SetCrudPermission())->execute('finance cash account', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::denyAccess(["update", "delete", "show"]);
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->enableExportButtons();

        CRUD::column('name');
        CRUD::addColumn([
            'label' => 'Amount',
            'name' => 'amount',
            'type' => 'closure',
            'function' => function ($entry) {
                return abs($entry->amount);
            }
        ]);
        // CRUD::addColumn([
        //     'label' => 'status',
        //     'name' => 'status',
        //     'type' => 'closure',
        //     'function' => function ($entry) {
        //         $color = "";
        //         if ($entry->status == "inactive") {
        //             $color = "badge-warning";
        //         }
        //         if ($entry->status == "active") {
        //             $color = "badge-success";
        //         }
        //         return '<span class="badge ' . $color . ' badge-pill">' . $entry->status . '</span>';
        //     }
        // ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FinanceCashAccountRequest::class);
        CRUD::field('name');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store(FinanceCashAccountRequest $request)
    {
        $account_code = str_replace(' ', '_', strtolower($request->name));



        $same_name = FinanceCashAccount::where('account_code',$account_code)->first();

        if($same_name || $account_code = 'supplier_debit_gl')
        {
            return redirect()->back()->withErrors(['name' =>'This name is not available ' ]);
        }

        FinanceCashAccount::create([
            'name' => $request->name,
            'account_code' => $account_code,
        ]);

        return redirect('admin/finance-cash-account');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
