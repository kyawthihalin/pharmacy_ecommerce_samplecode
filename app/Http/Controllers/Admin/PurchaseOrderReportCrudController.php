<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PurchaseOrderReportRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\ProductReportRequest;
use App\Models\PurchaseOrderReport;
use Illuminate\Http\Request;
use DateTime;

/**
 * Class PurchaseOrderReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PurchaseOrderReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PurchaseOrderReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/purchase-order-report');
        CRUD::setEntityNameStrings('purchase order report', 'purchase order reports');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PurchaseOrderReportRequest::class);
        $this->crud->addField(
            [   // date_range
                'name'  => 'Sdate',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                // 'default' => Carbon::today()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 SDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'Edate',
                'type'  => 'date_picker',
                'label' => 'End Date',
                // 'default' => Carbon::tomorrow()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 EDate'
                ]
            ],
        );

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/purchase_order',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-12'
            ]
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }


    public function report(Request $request)
    {
        $start_date = $request->start_date;
        $end_date   = $request->end_date;

        if ($start_date != null && $end_date != null) {
            if ($start_date > $end_date) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }

            $start_date = new DateTime($start_date);
            $start_date = $start_date->format('Y-m-d 00:00:00');

            $end_date   = new DateTime($end_date);
            $end_date   = $end_date->format('Y-m-d 23:59:59');
        }

        if (($start_date != null && $end_date == null) || ($start_date == null && $end_date != null)) {
            return response()->json([
                'message' => 'Start Date and End Date Must be choose'
            ], 422);
        }

        $purchase_order_reports = PurchaseOrderReport::where(function ($query) use ($start_date, $end_date) {
            if ($start_date !== null && $end_date !== null) {
                $query->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            }
        })->orderBy('created_at', 'desc')->get();

        return view('partials.reports.purchase_order_view', compact('purchase_order_reports'))->render();
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
