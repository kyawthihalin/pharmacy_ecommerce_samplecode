<?php

namespace App\Http\Controllers\Admin;

use App\Models\Unit;
use App\Models\Product;
use App\Models\StockLog;
use App\Constants\GetStatus;
use App\Models\GroupProduct;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Models\UnitRelationship;
use App\Actions\SetCrudPermission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ProductBalanceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class GroupProductBalanceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GroupProductBalanceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/group-product-balance');
        CRUD::setEntityNameStrings('group product balance', 'group product balances');
        CRUD::addClause('where', 'is_group', 1);
        (new SetCrudPermission())->execute('product', $this->crud);
        CRUD::denyAccess(['delete', 'show', 'update']);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::removeButton('create');
        // CRUD::setModel(\App\Models\SkuCode::class);
        CRUD::enableDetailsRow();

        CRUD::addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Type Product Name'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('name', 'LIKE', "%{$value}%");
                });
            }
        );

        CRUD::addFilter(
            [
                'name' => 'product_id',
                'type' => 'select2',
                'label' => 'Product ID',
            ],
            function () {
                return \App\Models\Product::where('is_group', 1)->pluck('product_id', 'product_id')->toArray();
            },
            function ($value) {
                $this->crud->addClause('where', 'product_id', $value);
            }
        );

        CRUD::addFilter(
            [
                'name' => 'sku_code_id',
                'type' => 'select2',
                'label' => 'SKU Code',
            ],
            function () {
                return \App\Models\SkuCode::pluck('code', 'id')->toArray();
            },
            function ($value) {
                $this->crud->addClause('where', 'sku_code_id', $value);
            }
        );

        CRUD::addFilter([
            'name'  => 'status',
            'type'  => 'select2',
            'label' => 'Status'
        ], [
            1 => 'PRODUCT ACTIVE',
            0 => 'PRODUCT INACTIVE',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });

        CRUD::addColumn([
            'name' => 'product_id',
            'type' => 'text',
            'label' => 'Group Product Id',
        ]);
        CRUD::column('name');
        CRUD::addColumn([
            'name' => 'sku_code_id',
            'type' => 'select',
            'attribute' => 'code',
            'entity' => 'skuCode',
            'model' => SkuCode::class,
        ]);
        CRUD::addColumn([
            'name' => 'feature_image',
            'type' => 'closure',
            'label' => 'Feature Image',
            'function' => function ($entry) {
                $url = Storage::temporaryUrl(
                    $entry->feature_image,
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}'><img src='{$url}' alt='Feature Image' width='50' height='50'/></a>";
            },
        ]);

        CRUD::addColumn([
            'name' => 'group_products',
            'label' => 'Products',
            'type' => 'closure',
            'function' => function ($entry) {
                $products = GroupProduct::where('group_id', $entry->id)->get();
                $string = "";
                foreach ($products as $key => $product) {
                    $string .= '<span class="badge badge-success">' . $product->product->name . '</span> ';
                }
                return $string;
            }
        ]);

        $this->crud->enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    public function showDetailsRow($id)
    {
        $group_products = GroupProduct::where('group_id', $id)->get();
        $quantity = ProductBalance::where('product_id', $id)->sum('quantity');

        return view('partials.groupProductBalance.detail', compact('group_products', 'id', 'quantity'))->render();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(ProductBalanceRequest::class);
        $this->setupCreateOperation();
    }

    public function updateGroupProductQty(Request $request)
    {
        $gpid = $request->gpid;
        $req_qty = $request->g_qty;

        $response = DB::transaction(function () use ($gpid, $req_qty) {
            $gp_balance = ProductBalance::lockForUpdate()->where('product_id', $gpid)->first();
            if (!$gp_balance) {
                $gp = Product::find($gpid);
                $gp_balance = ProductBalance::create([
                    'product_id' => $gpid,
                    'sku_code_id' => $gp->sku_code_id,
                    'quantity' => 0,
                ]);
            }

            $g_qty = $gp_balance->quantity;

            ($req_qty > $g_qty) ? $diff_qty = ($req_qty - $g_qty) : $diff_qty = ($g_qty - $req_qty);

            $gps = GroupProduct::where('group_id', $gpid)->get();

            foreach ($gps as $gp) {
                ['product_id' => $pid, 'quantity' => $p_qty] = $gp;
                $net_qty = $diff_qty * $p_qty;

                if ($req_qty > $g_qty) {
                    $p_available = ProductBalance::where('product_id', $pid)->sum('quantity');

                    if ($p_available < $net_qty || !$p_available) {
                        return false;
                    }

                    do {
                        $p_balance = ProductBalance::lockForUpdate()
                            ->where('product_id', $pid)
                            ->where('quantity', '>', 0)
                            ->orderBy('created_at', 'asc')
                            ->first();

                        if ($net_qty > $p_balance->quantity) {
                            $net_qty -=  $p_balance->quantity;
                            $p_balance->update([
                                'quantity' => 0,
                            ]);
                        } else {
                            $left_stock = $p_balance->quantity - $net_qty;
                            $net_qty = 0;
                            $p_balance->update([
                                'quantity' => $left_stock,
                            ]);
                        }
                    } while ($net_qty > 0);

                    $type = "Out";
                } else {
                    $p_balance = ProductBalance::lockForUpdate()
                        ->where('product_id', $pid)
                        ->orderBy('created_at', 'desc')
                        ->first();
                    $p_balance->increment('quantity', $net_qty);

                    $type = "In";
                }

                $p_available = ProductBalance::where('product_id', $pid)->sum('quantity');
                $p_array[] = ['pid' => $pid, 'available' => $p_available];

                if (($diff_qty * $p_qty) != 0) {
                    StockLog::create([
                        'product_id' => $pid,
                        'type' => $type == "Out" ? GetStatus::STOCK_TYPE['Balance_Out'] : GetStatus::STOCK_TYPE['Balance_In'],
                        'quantity' => $diff_qty * $p_qty,
                        'current_qty' => $p_available,
                    ]);
                }
            }

            if ($req_qty > $g_qty) {
                $gp_balance->increment('quantity', $diff_qty);
                $type = "In";
            } else {
                $gp_balance->decrement('quantity', $diff_qty);
                $type = "Out";
            }

            $gp_balance->refresh();

            StockLog::create([
                'product_id' => $gpid,
                'type' => $type == "Out" ? GetStatus::STOCK_TYPE['Balance_Out'] : GetStatus::STOCK_TYPE['Balance_In'],
                'quantity' => $diff_qty,
                'current_qty' => $gp_balance->quantity,
            ]);

            return $p_array;
        });

        if (!$response) {
            return response()->json([
                'message' => 'Product quantity is not enough!',
                'status' => 422
            ], 422);
        }

        return response()->json([
            'data' => $response,
            'status' => 'success',
            'message' => 'Product Qty Updated'
        ]);
    }

    public function updateProductQty(Request $request)
    {
        $pid = $request->pid;
        $gpid = $request->gpid;
        $req_qty = $request->p_qty;
        $g_qty = ProductBalance::where('product_id', $gpid)->value('quantity');

        $response = DB::transaction(function () use ($pid, $gpid, $req_qty, $g_qty) {
            $gp = GroupProduct::where('product_id', $pid)->first();
            $p_qty = $gp->quantity;

            $diff_qty = ($req_qty > $p_qty) ? ($req_qty - $p_qty) : ($p_qty - $req_qty);
            $net_qty = $diff_qty * $g_qty;

            if ($req_qty > $p_qty) {
                $p_available = ProductBalance::where('product_id', $pid)->sum('quantity');

                if ($p_available < $net_qty) {
                    return false;
                }

                do {
                    $p_balance = ProductBalance::lockForUpdate()
                        ->where('product_id', $pid)
                        ->where('quantity', '>', 0)
                        ->orderBy('created_at', 'asc')
                        ->first();

                    if ($net_qty > $p_balance->quantity) {
                        $net_qty -=  $p_balance->quantity;
                        $p_balance->update([
                            'quantity' => 0,
                        ]);
                    } else {
                        $left_stock = $p_balance->quantity - $net_qty;
                        $net_qty = 0;
                        $p_balance->update([
                            'quantity' => $left_stock,
                        ]);
                    }
                } while ($net_qty > 0);

                $gp->increment('quantity', $diff_qty);

                $type = "Out";
            } else {
                $p_balance = ProductBalance::lockForUpdate()
                    ->where('product_id', $pid)
                    ->orderBy('created_at', 'desc')
                    ->first();
                $p_balance->increment('quantity', $net_qty);
                $gp->decrement('quantity', $diff_qty);

                $type = "In";
            }

            $p_available = ProductBalance::where('product_id', $pid)->sum('quantity');

            if (($diff_qty * $g_qty) != 0) {
                StockLog::create([
                    'product_id' => $pid,
                    'type' => $type == "Out" ? GetStatus::STOCK_TYPE['Balance_Out'] : GetStatus::STOCK_TYPE['Balance_In'],
                    'quantity' => $diff_qty * $g_qty,
                    'current_qty' => $p_available,
                ]);
            }

            return $p_available;
        });

        if (!$response) {
            return response()->json([
                'message' => 'Product quantity is not enough!',
                'status' => 422
            ], 422);
        }

        return response()->json([
            'data' => [
                'available' => $response,
                'pid' => $pid,
            ],
            'status' => 'success',
            'message' => 'Product Qty Updated'
        ]);
    }
}
