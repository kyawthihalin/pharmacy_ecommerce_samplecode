<?php

namespace App\Http\Controllers\Admin;

use App\Models\StockLog;
use App\FinanceParticular;
use App\FinanceTransaction;
use App\Constants\GetStatus;
use App\Models\AdjustmentType;
use App\Models\ProductBalance;
use App\Models\StockAdjustment;
use App\Actions\SetCrudPermission;
use App\Models\FinanceCashAccount;
use Illuminate\Support\Facades\DB;
use App\Constants\FinanceParticularConstant;
use App\Http\Requests\StockAdjustmentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StockAdjustmentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockAdjustmentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StockAdjustment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stock-adjustment');
        CRUD::setEntityNameStrings('stock adjustment', 'stock adjustments');
        (new SetCrudPermission())->execute('stock adjustment', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'purchase_order_id',
            'type' => 'select',
            'entity' => 'purchaseOrder',
            'attribute' => 'invoice_no',
            'model' => \App\Models\PurchaseOrder::class,
        ]);
        CRUD::column('product_id');
        CRUD::addColumn([
            'name' => 'adjustment_type_id',
            'type' => 'select',
            'entity' => 'adjustmentType',
            'model' => \App\Models\AdjustmentType::class,
        ]);
        CRUD::column('quantity');
        CRUD::enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StockAdjustmentRequest::class);

        CRUD::addField([
            'label' => 'Invoice No',
            'type' => 'select2',
            'name' => 'purchase_order_id',
            'attribute' => 'invoice_no',
            'entity' => 'purchaseOrder',
            'model' => \App\Models\PurchaseOrder::class,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
                'id' => 'purchase_order_id',
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'product_id',
            'attribute' => 'name',
            'model' => \App\Models\Product::class,
            'data_source' => url("admin/api/product-by-invoice"),
            'placeholder' => 'Select Product',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::addField([
            'label' => 'Choose Adjustment Type',
            'type' => 'select2',
            'name' => 'adjustment_type_id',
            'entity' => 'adjustmentType',
            'model' => \App\Models\AdjustmentType::class,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6',
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);


        CRUD::addField([
            'label' => 'Choose Finance Particular Type',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'finance_particular_id',
            'attribute' => 'name',
            'model' => FinanceParticular::class,
            'data_source' => url("admin/api/finance-particular"),
            'placeholder' => 'Choose Finance Particular Type',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);


        CRUD::addField([
            'label' => 'Quantity',
            'name' => 'quantity',
            'type' => 'number',
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        CRUD::field('remark')->type('wysiwyg');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(StockAdjustmentRequest::class);
        $this->setupCreateOperation();
    }

    public function store(StockAdjustmentRequest $request)
    {

        DB::transaction(function () use ($request){
            $stock_adjustment = StockAdjustment::create([
                'purchase_order_id' => $request->purchase_order_id,
                'product_id' => $request->product_id,
                'adjustment_type_id' => $request->adjustment_type_id,
                'quantity' => $request->quantity,
                'remark' => $request->remark,
            ]);

            $product_balance = ProductBalance::lockForUpdate()->where([
                ['product_id', $request->product_id],
                ['purchase_order_id', $request->purchase_order_id],
            ])->first();

            $adjustment_type = AdjustmentType::find($request->adjustment_type_id);
            switch ($adjustment_type->type) {
                case GetStatus::STOCK_IN:
                    if (!$product_balance) {
                        $product_balance = ProductBalance::create([
                            'product_id' => $request->product_id,
                            'sku_code_id' => $stock_adjustment->product->sku_code_id,
                            'quantity' => $request->quantity,
                        ]);
                    } else {
                        $product_balance->increment('quantity', $request->quantity);
                    }

                    $adjustment_type = GetStatus::STOCK_TYPE['Adjustment_In'];
                    break;
                case GetStatus::STOCK_OUT:
                    $product_balance->decrement('quantity', $request->quantity);
                    $adjustment_type = GetStatus::STOCK_TYPE['Adjustment_Out'];
                    break;
            }

            if($request->finance_particular_id)
            {
                $amount = $product_balance->price * $request->quantity;
                $finance_particular = FinanceParticular::where('id',$request->finance_particular_id)->first();

                $credit_account_data = [];
                $debit_account_data = [];

                $finance_transaction_data= [
                    'finance_particular_id' => $finance_particular->id,
                    'transactionable_id' => $stock_adjustment->id,
                    'transactionable_type' => get_class($stock_adjustment),
                ];

                if($finance_particular->credit_account_id)
                {
                    $credit_account_gl = FinanceCashAccount::where('id',$finance_particular->credit_account_id)->lockForUpdate()->first();
                    $credit_account_gl_amount_before = $credit_account_gl->amount;

                    if($finance_particular->credit_account_operation == "Addition")
                    {
                        $credit_account_gl_amount_after = bcadd($credit_account_gl_amount_before, $amount, 4);
                    }

                    if($finance_particular->credit_account_operation == "Subtraction")
                    {
                        $credit_account_gl_amount_after = bcsub($credit_account_gl_amount_before, $amount, 4);
                    }

                    $credit_account_gl->update([
                        'amount' => $credit_account_gl_amount_after,
                    ]);

                    $credit_account_data= [
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'credit_account_before_amount' => $credit_account_gl_amount_before,
                        'credit_account_after_amount' =>  $credit_account_gl_amount_after,
                    ];
                }

                if($finance_particular->debit_account_id)
                {
                    $debit_account_gl = FinanceCashAccount::where('id',$finance_particular->debit_account_id)->lockForUpdate()->first();
                    $debit_account_gl_amount_before = $debit_account_gl->amount;

                    if($finance_particular->debit_account_operation == "Addition")
                    {
                        $debit_account_gl_amount_after = bcadd($debit_account_gl_amount_before, $amount, 4);
                    }

                    if($finance_particular->debit_account_operation == "Subtraction")
                    {
                        $debit_account_gl_amount_after = bcsub($debit_account_gl_amount_before, $amount, 4);
                    }

                    $debit_account_gl->update([
                        'amount' => $debit_account_gl_amount_after,
                    ]);

                    $debit_account_data  = [
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'debit_account_before_amount' => $debit_account_gl_amount_before,
                        'debit_account_after_amount' =>  $debit_account_gl_amount_after,
                    ];
                }

                $combined_data = array_merge($credit_account_data, $debit_account_data,$finance_transaction_data);

                FinanceTransaction::create($combined_data);
            }


            $current_quantity = ProductBalance::where('product_id', $request->product_id)->sum('quantity');

            StockLog::create([
                'product_id' => $request->product_id,
                'type' => $adjustment_type,
                'quantity' => $request->quantity,
                'current_qty' => $current_quantity,
            ]);
        });

        return redirect('admin/stock-adjustment');
    }
}
