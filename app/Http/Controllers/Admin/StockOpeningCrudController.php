<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Imports\StockOpeningImport;
use Maatwebsite\Excel\Facades\Excel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StockOpeningCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockOpeningCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stock-opening/import-view');
        CRUD::setEntityNameStrings('stock', 'stocks');
    }

    public function importView()
    {
        if (!backpack_user()->can('create stock opening')) {
            abort(403);
        }

        return view('partials.products.stock_import', ['crud' => $this->crud]);
    }

    public function importStore(Request $request)
    {
        $request->validate([
            'excel' => 'required|mimes:xls,xlsx'
        ]);

        if ($request->hasFile('excel')) {
            $import = new StockOpeningImport();
            Excel::import($import, $request->file('excel'));
        }

        return redirect('admin/stock-opening/import-view')->with('success', 'Stock imported successfully!');
    }
}
