<?php

// namespace App\Http\Controllers\Admin;

// use App\Constants\GlAccountType;
// use App\Actions\SetCrudPermission;
// use App\Http\Requests\GlAccountRequest;
// use Backpack\CRUD\app\Http\Controllers\CrudController;
// use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

// class GlAccountCrudController extends CrudController
// {
//     use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
//     // use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
//     // use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
//     // use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//     use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;


//     public function setup()
//     {
//         CRUD::setModel(\App\Models\GlAccount::class);
//         CRUD::setRoute(config('backpack.base.route_prefix') . '/gl-account');
//         CRUD::setEntityNameStrings('gl account', 'gl accounts');
//         (new SetCrudPermission())->execute('gl account', $this->crud);
//     }


//     protected function setupListOperation()
//     {
//         CRUD::column('name');
//         CRUD::column('type');
//         CRUD::column('reference_id');


//     }


//     protected function setupCreateOperation()
//     {
//         CRUD::setValidation(GlAccountRequest::class);

//         CRUD::field('name');
//         $this->crud->addField(
//             [
//                 'name'        => 'type',
//                 'label'       => "Choose Type",
//                 'type'        => 'select2_from_array',
//                 'options'     => GlAccountType::TYPE,
//                 'allows_null' => false,
//                 'allows_multiple' => false,
//                 'wrapperAttributes' => [
//                     'class' => 'form-group col-md-12'
//                 ],
//                 'attributes' => [
//                     'class' => 'form-control'
//                 ],
//             ],
//         );
//         CRUD::field('reference_id');


//     }

//     protected function setupUpdateOperation()
//     {
//         $this->setupCreateOperation();
//     }
// }
