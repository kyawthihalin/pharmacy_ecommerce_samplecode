<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Carbon\Carbon;
use App\Models\City;
use App\Models\Order;
use App\Models\Product;
use App\Models\StockLog;
use App\Models\Township;
use App\Constants\Status;
use App\FinanceParticular;
use App\FinanceTransaction;
use App\Models\OrderDetail;
use App\Models\PaymentType;
use App\Constants\GetStatus;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Models\ProfitDailyLog;
use App\Models\ShippingAddress;
use App\Actions\SendNotification;
use App\Actions\SetCrudPermission;
use App\Models\FinanceCashAccount;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrderRequest;
use App\Actions\SendOrderNotification;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Constants\FinanceParticularConstant;
use App\Exceptions\ProductStockOutException;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Order::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order');
        CRUD::setEntityNameStrings('order', 'orders');
        (new SetCrudPermission())->execute('order', $this->crud);
        $this->crud->setEditView('partials.orders.editOrder');
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );
        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'order_no',
                'label' => 'Type Order Number'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('order_no', 'LIKE', "%{$value}%");
                });
            }
        );

        $this->crud->addFilter(
            [
                'name'        => 'city_id',
                'type'        => 'select2_ajax',
                'label'       => 'City',
                'placeholder' => 'Pick a City'
            ],
            url('admin/test/ajax-city-options'), // the ajax route
            function ($value) {
                $address = ShippingAddress::where('city_id', $value)->pluck('id')->toArray();
                $this->crud->addClause('whereIn', 'shipping_address_id', $address);
            }
        );

        $this->crud->addFilter(
            [
                'name'        => 'township_id',
                'type'        => 'select2_ajax',
                'label'       => 'Township',
                'placeholder' => 'Pick a Township'
            ],
            url('admin/test/ajax-township-options'),
            function ($value) {
                $address = ShippingAddress::where('township_id', $value)->pluck('id')->toArray();
                $this->crud->addClause('whereIn', 'shipping_address_id', $address);
            }
        );

        $this->crud->addFilter([
            'name'  => 'payment_status',
            'type'  => 'dropdown',
            'label' => 'Payment Status'
        ], [
            'Pending' => 'Pending',
            'Completed' => 'Completed',
            'Rejected' => 'Rejected',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'payment_status', $value);
        });

        $this->crud->addFilter([
            'name'  => 'shipping_status',
            'type'  => 'dropdown',
            'label' => 'Shipping Status'
        ], [
            'Pending' => 'Pending',
            'Processing' => 'Processing',
            'Confirmed' => 'Confirmed',
            'Delivering' => 'Delivering',
            'Completed' => 'Completed',
            'Rejected' => 'Rejected',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'shipping_status', $value);
        });

        $this->crud->addFilter([
            'name'  => 'payment_type_id',
            'type'  => 'select2',
            'label' => 'Payment Type'
        ], function () {
            return PaymentType::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'payment_type_id', $value);
        });
    }
    public function showDetailsRow($id)
    {
        $order = Order::find($id);
        $qr_code = QrCode::generate($order->uuid);

        return view('partials.orders.orderDetails', ['order' => $order, 'qr_code' => $qr_code]);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Pending') DESC, FIELD(shipping_status , 'Pending','Processing') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Pending') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Processing') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Confirmed') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Delivering', 'Going to Bus Gate') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Completed') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Rejected') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Completed') DESC, FIELD(shipping_status , 'Completed') DESC");

        // $this->crud->addClause('orderByRaw',"FIELD(payment_status , 'Rejected') DESC, FIELD(shipping_status ,  'Pending','Rejected') DESC");


        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();
        $this->crud->setDefaultPageLength(10);
        $this->crud->removeAllButtonsFromStack('line');
        // if (backpack_user()->can('edit order')) {
        //     $this->crud->setColumnPriority('order', 0);
        //     $this->crud->addButtonFromView('line', 'order', 'order', 'beginning');
        // }
        // CRUD::column('order_no');
        $this->crud->addColumn([
            'label' => 'Order No',
            'type' => 'text',
            'name' => 'order_no',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    if ( ($entry->payment_status === "Pending" && backpack_user()->can('edit order') && $entry->payment_type_status == 'cash_down') || ($entry->shipping_status != "Completed" && backpack_user()->can('edit order') && $entry->payment_type_status != 'cash_down') ) {
                        return backpack_url("order/$entry->id/edit");
                    } else {
                        // Return some other URL or null if you don't want the link to be clickable.
                        return null;
                    }
                },
            ],

        ]);
        $this->crud->addColumn([
            'label' => 'Shop',
            'type' => 'select',
            'name' => 'shop_id',
            'entity' => 'shop',
            'attribute' => 'name',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("shop/?name=" . $entry->shop->name);
                },
            ],
        ]);


        CRUD::addColumn([
            'label' => 'Payment Type Status',
            'name' => 'payment_type_status',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($entry->payment_type_status == "cash_down") {
                    return '<span> လက်ငင်းငွေချေ </span>';
                }
                if ($entry->payment_type_status == "cash_on_delivery") {
                    return '<span> ပစ္စည်းရောက်ငွေချေ </span>';
                }
            }
        ]);

        CRUD::column('payment_type_id');

        CRUD::addColumn([
            'name' => 'payment_screenshot',
            'type' => 'closure',
            'label' => 'Payment Screenshot',
            'function' => function ($entry) {
                if ($entry->payment_screenshot != null || $entry->payment_scrrenshot != "") {
                    $url = Storage::temporaryUrl(
                        $entry->payment_screenshot,
                        now()->addMinutes(3)
                    );
                    return "<a target='blank' href='{$url}'><img src='{$url}' alt='Payment Screenshot' width='50' height='50'/></a>";
                } else {
                    return "No Payment Screenshot for this order.";
                }
            },
        ]);

        CRUD::addColumn([
            'label' => ' Net Amount',
            'name' => 'net_amount',
            'type' => 'number',
        ]);
        CRUD::addColumn([
            'label' => 'Payment Status',
            'name' => 'payment_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->payment_status == "Pending") {
                    $color = "badge-warning";
                }
                if ($entry->payment_status == "Completed") {
                    $color = "badge-success";
                }
                if ($entry->payment_status == "Rejected") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->payment_status . '</span>';
            }
        ]);
        CRUD::addColumn([
            'label' => 'Shipping Status',
            'name' => 'shipping_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->shipping_status == "Pending" || $entry->shipping_status == "Processing") {
                    $color = "badge-warning";
                }
                if ($entry->shipping_status == "Confirmed" || $entry->shipping_status == "Delivering" || $entry->shipping_status == "Going to Bus Gate") {
                    $color = "badge-primary";
                }
                if ($entry->shipping_status == "Completed") {
                    $color = "badge-success";
                }
                if ($entry->shipping_status == "Rejected") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->shipping_status . '</span>';
            }
        ]);
        CRUD::addColumn([
            'name' => 'created_at',
            'label' => 'Order Date',
            'type' => 'datetime',
        ]);
        CRUD::column('shipping_address_id');

        CRUD::addColumn([
            'name' => 'full_address',
            'label' => 'Full Address',
            'type' => 'closure',
            'function' => function ($entry) {
                $string = "";
                $stringAry      =   explode("||", wordwrap($entry->shipping_address->ship_address, 120, "||"));
                foreach ($stringAry as $key => $result) {
                    $string .= $result . "<br>";
                }

                return $string . '(' . $entry->shipping_address->township->name . ',' . $entry->shipping_address->city->name . ')';
            }
        ]);

        CRUD::addColumn([
            'label' => 'Phone Model',
            'name' => 'phone_model',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->phone_model ?? '-';
            },
        ]);


        CRUD::addColumn([
            'name' => 'phone_number',
            'label' => 'Phone Number',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->shipping_address->phone_number;
            }
        ]);

        CRUD::addColumn([
            'name' => 'delivery_fee',
            'label' => 'Delivery Fee',
            'type' => 'closure',
            'function' => function ($entry) {
                return $entry->shipping_address->township->shipping_fees ? $entry->shipping_address->township->shipping_fees : 0;
            }
        ]);

        CRUD::addColumn([
            'label' => 'Total Quantity',
            'name' => 'total_quantity',
            'type' => 'closure',
            'function' => function ($entry) {
                return OrderDetail::where('order_id', $entry->id)->sum('quantity');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderRequest::class);

        CRUD::field('id');
        CRUD::field('order_no');
        CRUD::field('shipping_address_id');
        CRUD::field('payment_type_id');
        CRUD::field('total_quantity');
        CRUD::field('total_amount');
        CRUD::field('discount_amount');
        CRUD::field('coupon_id');
        CRUD::field('coupon_discount');
        CRUD::field('grand_total');
        CRUD::field('sub_total');
        CRUD::field('net_amount');
        CRUD::field('shipping_fees');
        CRUD::field('remark');
        CRUD::field('shipping_status');
        CRUD::field('payment_status');
        CRUD::field('created_at');
        CRUD::field('updated_at');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function changePaymentStatus(Request $request)
    {
        $order = Order::find($request->oid);
        $status = $request->status;
        $current_status = $order->payment_status;
        $shipping_status = $order->shipping_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        } elseif ($current_status === "Rejected" || $current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change Rejected or Completed Status."
            ], 422);
        } elseif ($status === "Completed" && $shipping_status === "Pending") {

            try {
                DB::beginTransaction();
                $sale_total_amount = $order->total_amount;
                $profit = 0;
                $total_purchase_price = 0;

                foreach ($order->details as $detail)
                {
                    if ($detail->quantity > 0) {
                        // get purchase price from supplier sale
                        // $product_purchase_price = $detail->purchase_price;

                        // $purchase_product_total_quantity = ProductBalance::where('product_id', $detail->product_id)->where('price', $product_purchase_price)->sum('quantity');
                        $purchase_product_total_quantity = ProductBalance::where('product_id', $detail->product_id)->sum('quantity');


                        // to show has product but diff sale price
                        if ($purchase_product_total_quantity  == 0) {
                            // $purchase_product_diff_price = ProductBalance::where('product_id', $detail->product_id)->where('price', '!=', $product_purchase_price)->latest('created_at')->first();

                            // if ($purchase_product_diff_price) {
                            //     throw new ProductStockOutException($detail->product_id, "This product ( Item Code : " . $detail->product->product_id .") is not available with this sale price");
                            // }

                            throw new ProductStockOutException($detail->product->product_id);
                        } else if ($detail->quantity > $purchase_product_total_quantity) {
                            throw new ProductStockOutException($detail->product_id, "Not enough item quantity at Item Code " . $detail->product->product_id);
                        } else {
                            $total_purchase_price += $detail->discount_amount + ($detail->quantity *  $detail->purchase_price);

                            $total_sale_quantity = $detail->quantity;

                            do {
                                // $purchase_product = ProductBalance::where('product_id', $detail->product_id)->where('price', $product_purchase_price)->where('quantity', '>', 0)->first();
                                $purchase_product = ProductBalance::where('product_id', $detail->product_id)->where('quantity', '>', 0)->first();

                                if($total_sale_quantity > $purchase_product->quantity)
                                {
                                    $total_sale_quantity -=  $purchase_product->quantity;
                                    $purchase_product->update([
                                        'quantity' => 0,
                                    ]);
                                }
                                else{
                                    $left_stock = $purchase_product->quantity - $total_sale_quantity;
                                    $total_sale_quantity = 0;
                                    $purchase_product->update([
                                        'quantity' => $left_stock,
                                    ]);
                                }

                                $current_quantity = ProductBalance::where('product_id', $detail->product_id)->sum('quantity');

                                StockLog::create([
                                    'product_id' => $detail->product_id,
                                    'type' => GetStatus::STOCK_TYPE['Sale'],
                                    'quantity' => $detail->quantity,
                                    'current_qty' => $current_quantity,
                                ]);
                            } while ($total_sale_quantity > 0);

                        }
                    }
                }

                $profit = $order->total_amount - $total_purchase_price;

                // Sale Gl
                $sale_gl = FinanceCashAccount::where('account_code', 'sale_gl')->first();
                $sale_gl_amount_before = $sale_gl->amount;
                $sale_gl_amount_after = bcadd($sale_gl_amount_before, $sale_total_amount, 4);

                $sale_gl->update([
                    'amount' => $sale_gl_amount_after,
                ]);

                $sale_finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['SALE_CASH_COMPLETE'])->first();

                FinanceTransaction::create([
                    'finance_particular_id' => $sale_finance_particular->id,
                    'transactionable_id' => $order->id,
                    'transactionable_type' => get_class($order),
                    'debit_account_id' => $sale_finance_particular->debit_account_id,
                    'credit_account_id' => $sale_finance_particular->credit_account_id,
                    'amount' => $sale_total_amount,
                    'debit_account_before_amount' => $sale_gl_amount_before,
                    'debit_account_after_amount' => $sale_gl_amount_after,
                ]);

                // Profit
                $profit_gl = FinanceCashAccount::where('account_code', 'profit_gl')->first();
                $profit_gl_amount_before = $profit_gl->amount;
                $profit_gl_amount_after = bcadd($profit_gl_amount_before, $profit, 4);


                $profit_gl->update([
                    'amount' => $profit_gl_amount_after,
                ]);

                $profit_finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['GET_PROFIT_FROM_ORDER_CASH_COMPLETE'])->first();

                FinanceTransaction::create([
                    'finance_particular_id' => $profit_finance_particular->id,
                    'transactionable_id' => $order->id,
                    'transactionable_type' => get_class($order),
                    'debit_account_id' => $profit_finance_particular->debit_account_id,
                    'credit_account_id' => $profit_finance_particular->credit_account_id,
                    'amount' => $profit,
                    'debit_account_before_amount' => $profit_gl_amount_before,
                    'debit_account_after_amount' => $profit_gl_amount_after,
                ]);


                $has_today_profit_record = ProfitDailyLog::whereDate('created_at', Carbon::today())->first();
                if ($has_today_profit_record) {
                    $update_total_amount = $has_today_profit_record->total_amount + $profit;
                    $has_today_profit_record->update([
                        'total_amount' => $update_total_amount,
                    ]);
                } else {
                    ProfitDailyLog::create([
                        'total_amount' => $profit,
                    ]);
                }

                $order->update([
                    'payment_status' => $status,
                    'shipping_status' =>  $order->payment_type_status == "cash_on_delivery" ? "Completed" : 'Processing',
                ]);

                DB::commit();
            } catch (ProductStockOutException $e) {
                DB::rollback();
                return response()->json([
                    "status" => "422",
                    "message" => $e->getMessage(),
                ], 422);
            } catch (Exception $e) {
                DB::rollback();
                info($e);
                return response()->json([
                    "status" => "422",
                    "message" => 'Error Occur.Please try agin',
                ], 422);
            }

            return response()->json([
                "status" => "200",
                "message" => "Successfully Changed Status."
            ], 200);
        } elseif ($status === "Rejected" && $shipping_status === "Pending") {
            $order->payment_status = $status;
            $order->reject_remark = $request->remark;
            $order->rejected_by = backpack_user()->id;
            $order->shipping_status = "Rejected";
            $order->save();
            return response()->json([
                "status" => "200",
                "message" => "Successfully Changed Status."
            ], 200);
        } else {
            return response()->json([
                "status" => "500",
                "message" => "Something Went Wrong!."
            ], 500);
        }
    }

    public function changePaymentCodStatus(Request $request)
    {
        $order = Order::find($request->oid);
        $status = $request->status;
        $current_status = $order->payment_status;
        $shipping_status = $order->shipping_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        } elseif ($current_status === "Rejected" || $current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change Rejected or Completed Status."
            ], 422);
        } elseif ($status === "Completed" && $shipping_status === "Rejected") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change Rejected or Completed Status."
            ], 422);

        } elseif ($status === "Rejected" && $shipping_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change Rejected  Status."
            ], 422);
        }
        elseif ($status === "Completed" && $shipping_status === "Completed") {

            $order->payment_status = $status;
            $order->save();
            return response()->json([
                "status" => "200",
                "message" => "Successfully Changed Status."
            ], 200);
        } elseif ($status === "Rejected" && $shipping_status != "Completed") {
            $order->payment_status = $status;
            $order->reject_remark = $request->remark;
            $order->rejected_by = backpack_user()->id;
            $order->shipping_status = "Rejected";
            $order->save();
            return response()->json([
                "status" => "200",
                "message" => "Successfully Changed Status."
            ], 200);
        } else {
            return response()->json([
                "status" => "500",
                "message" => "Something Went Wrong!."
            ], 500);
        }
    }

    public function changeShippingStatus(Request $request)
    {
        $order = Order::find($request->oid);
        $order_details = OrderDetail::where('order_id', $order->id)->get();
        $status = $request->status;
        $current_status = $order->shipping_status;
        $payment_status = $order->payment_status;
        $higher_status = getHigherStatus($current_status, Status::shipping_status);
        $lower_status = getLowerStatus($current_status, Status::shipping_status);

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        } elseif ($current_status === "Processing" && $status === "Pending") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to or Pending Status."
            ], 422);
        } elseif ($payment_status === "Rejected") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to or Pending Status."
            ], 422);
        } elseif ($current_status === "Processing" || $current_status === "Pending") {
            if ($status === "Delivering" || $status === "Going to Bus Gate" || $status === "Completed") {
                return response()->json([
                    "status" => "422",
                    "message" => "You order have to Confirm before change your status."
                ], 422);
            }
        }

        if ($current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change any status to Completed Order."
            ], 422);
        }
        if ($current_status === "Processing" && $status === "Confirmed") {
            // foreach ($order_details as $od) {
            //     $product = Product::find($od->product_id);
            //     $product->quantity -= $od->quantity;
            //     $product->save();
            // }
            $order->shipping_status = $status;
            $order->save();

            $this->sendNotification($status, $order);

            return response()->json([
                "status" => "200",
                "message" => "Order is confirmed and cannot cancel."
            ], 200);
        } elseif ($current_status === "Confirmed") {
            if (in_array($status, $lower_status)) {
                return response()->json([
                    "status" => "422",
                    "message" => "You can't change to $status Status."
                ], 422);
            } elseif (in_array($status, $higher_status)) {
                $order->shipping_status = $status;
                $order->save();
                $this->sendNotification($status, $order);
                return response()->json([
                    "status" => "200",
                    "message" => "Your Order Cannot Cancel and Status updated."
                ], 200);
            } else {
                return response()->json([
                    "status" => "422",
                    "message" => "You can't change to $status Status."
                ], 422);
            }
        } elseif ($current_status === "Delivering" && $status === "Going to Bus Gate" || $status === "Completed") {
            $order->shipping_status = $status;
            $order->save();
            $this->sendNotification($status, $order);
            return response()->json([
                "status" => "200",
                "message" => "Your Order Cannot Cancel and Status updated."
            ], 200);
        } elseif ($current_status === "Going to Bus Gate" && $status === "Delivering"  || $status === "Completed") {
            $order->shipping_status = $status;
            $order->save();
            $this->sendNotification($status, $order);
            return response()->json([
                "status" => "200",
                "message" => "Your Order Cannot Cancel and Status updated."
            ], 200);
        } elseif ($current_status === "Processing" && $status == "Rejected") {
            $this->sendNotification($status, $order);
            $order->shipping_status = $status;
            $order->payment_status = "Rejected";
            $order->reject_remark = $request->remark;
            $order->rejected_by = backpack_user()->id;
            $order->save();
            return response()->json([
                "status" => "200",
                "message" => "Your Order is Rejected."
            ], 200);
        } else {
            return response()->json([
                "status" => "500",
                "message" => "Something Went Wrong!."
            ], 500);
        }
    }

    public function changeShippingCodStatus(Request $request)
    {

        $order = Order::find($request->oid);
        $order_details = OrderDetail::where('order_id', $order->id)->get();
        $status = $request->status;
        $current_status = $order->shipping_status;
        $payment_status = $order->payment_status;
        $higher_status = getHigherStatus($current_status, Status::shipping_status);
        $lower_status = getLowerStatus($current_status, Status::shipping_status);

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        } elseif ($current_status === "Delivering" && $status === "Confirmed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to or Confirmed Status."
            ], 422);
        }

        if ($current_status !== 'Rejected' ) {

            if($status == 'Completed')
            {
                try {
                    DB::beginTransaction();
                    $sale_total_amount = $order->total_amount;
                    $profit = 0;
                    $total_purchase_price = 0;

                    foreach ($order->details as $detail)
                    {
                        if ($detail->quantity > 0) {
                            // get purchase price from supplier sale
                            $product_purchase_price = $detail->purchase_price;

                            // $purchase_product_total_quantity = ProductBalance::where('product_id', $detail->product_id)->where('price', $product_purchase_price)->sum('quantity');
                            $purchase_product_total_quantity = ProductBalance::where('product_id', $detail->product_id)->sum('quantity');


                            // to show has product but diff sale price
                            if ($purchase_product_total_quantity  == 0) {
                                // $purchase_product_diff_price = ProductBalance::where('product_id', $detail->product_id)->where('price', '!=', $product_purchase_price)->latest('created_at')->first();

                                // if ($purchase_product_diff_price) {
                                //     throw new ProductStockOutException($detail->product_id, "This product ( Item Code : " . $detail->product->product_id .") is not available with this sale price");
                                // }

                                throw new ProductStockOutException($detail->product->product_id);
                            } else if ($detail->quantity > $purchase_product_total_quantity) {
                                throw new ProductStockOutException($detail->product_id, "Not enough item quantity at Item Code " . $detail->product->product_id);
                            } else {
                                $total_purchase_price += $detail->discount_amount + ($detail->quantity *  $detail->purchase_price);

                                $total_sale_quantity = $detail->quantity;

                                do {
                                    // $purchase_product = ProductBalance::where('product_id', $detail->product_id)->where('price', $product_purchase_price)->where('quantity', '>', 0)->first();
                                    $purchase_product = ProductBalance::where('product_id', $detail->product_id)->where('quantity', '>', 0)->first();

                                    if($total_sale_quantity > $purchase_product->quantity)
                                    {
                                        $total_sale_quantity -=  $purchase_product->quantity;
                                        $purchase_product->update([
                                            'quantity' => 0,
                                        ]);
                                    }
                                    else{
                                        $left_stock = $purchase_product->quantity - $total_sale_quantity;
                                        $total_sale_quantity = 0;
                                        $purchase_product->update([
                                            'quantity' => $left_stock,
                                        ]);
                                    }

                                    $current_quantity = ProductBalance::where('product_id', $detail->product_id)->sum('quantity');

                                    StockLog::create([
                                        'product_id' => $detail->product_id,
                                        'type' => GetStatus::STOCK_TYPE['Sale'],
                                        'quantity' => $detail->quantity,
                                        'current_qty' => $current_quantity,
                                    ]);
                                } while ($total_sale_quantity > 0);

                            }
                        }
                    }

                    $profit = $order->total_amount - $total_purchase_price;
info($profit);
                    // Sale Gl
                    $sale_gl = FinanceCashAccount::where('account_code', 'sale_gl')->first();
                    $sale_gl_amount_before = $sale_gl->amount;
                    $sale_gl_amount_after = bcadd($sale_gl_amount_before, $sale_total_amount, 4);

                    $sale_gl->update([
                        'amount' => $sale_gl_amount_after,
                    ]);

                    $sale_finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['SALE_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $sale_finance_particular->id,
                        'transactionable_id' => $order->id,
                        'transactionable_type' => get_class($order),
                        'debit_account_id' => $sale_finance_particular->debit_account_id,
                        'credit_account_id' => $sale_finance_particular->credit_account_id,
                        'amount' => $sale_total_amount,
                        'debit_account_before_amount' => $sale_gl_amount_before,
                        'debit_account_after_amount' => $sale_gl_amount_after,
                    ]);

                    // Profit
                    $profit_gl = FinanceCashAccount::where('account_code', 'profit_gl')->first();
                    $profit_gl_amount_before = $profit_gl->amount;
                    $profit_gl_amount_after = bcadd($profit_gl_amount_before, $profit, 4);

                    $profit_gl->update([
                        'amount' => $profit_gl_amount_after,
                    ]);

                    $profit_finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['GET_PROFIT_FROM_ORDER_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $profit_finance_particular->id,
                        'transactionable_id' => $order->id,
                        'transactionable_type' => get_class($order),
                        'debit_account_id' => $profit_finance_particular->debit_account_id,
                        'credit_account_id' => $profit_finance_particular->credit_account_id,
                        'amount' => $profit,
                        'debit_account_before_amount' => $profit_gl_amount_before,
                        'debit_account_after_amount' => $profit_gl_amount_after,
                    ]);



                    $has_today_profit_record = ProfitDailyLog::whereDate('created_at', Carbon::today())->first();
                    if ($has_today_profit_record) {
                        $update_total_amount = $has_today_profit_record->total_amount + $profit;
                        $has_today_profit_record->update([
                            'total_amount' => $update_total_amount,
                        ]);
                    } else {
                        ProfitDailyLog::create([
                            'total_amount' => $profit,
                        ]);
                    }

                    $order->update([
                        'shipping_status' =>  $status,
                    ]);

                    DB::commit();
                } catch (ProductStockOutException $e) {
                    DB::rollback();
                    return response()->json([
                        "status" => "422",
                        "message" => $e->getMessage(),
                    ], 422);
                } catch (Exception $e) {
                    DB::rollback();
                    info($e);
                    return response()->json([
                        "status" => "422",
                        "message" => 'Error Occur.Please try agin',
                    ], 422);
                }
            }
            $order->payment_status = $status == "Rejected" ? "Rejected" : $order->payment_status ;
            $order->reject_remark = $status == "Rejected" ? $request->remark : $order->reject_remark;
            $order->shipping_status = $status;
            $order->save();
            $this->sendNotification($status, $order);

            return response()->json([
                "status" => "200",
                "message" => "Order is ". $status ." ",
            ], 200);
        }
        else
        {
            return response()->json([
                "status" => "500",
                "message" => "Something Went Wrong!."
            ], 500);
        }
    }

    public function townshipOptions(Request $request)
    {
        $term = $request->input('term');
        $options = Township::where('name', 'like', '%' . $term . '%')->get()->pluck('name', 'id');
        return $options;
    }
    public function cityOptions(Request $request)
    {
        $term = $request->input('term');
        $options = City::where('name', 'like', '%' . $term . '%')->get()->pluck('name', 'id');
        return $options;
    }

    public function sendNotification($status, $order)
    {
        if ($status === "Completed") {
            $noti_message = "$order->order_no အတွက်မှာယူထားသောပစ္စည်းများပိုဆောင်ပြီးဖြစ်ပါသည်။";
            $this->createNotification("Notification", $noti_message, $order->id, $order->shop->id);

            $data = [
                "title" => "Moe Hein Gabar",
                "message" => $noti_message,
                'order_no' => $order->order_no,
                'order_id' => $order->uuid,
                'Type' => "Notification"
            ];
            (new SendOrderNotification())->execute([$order->shop->token], $data);

            $message = "$order->order_no အတွက်ပို့ဆောင်မှူ အဆင်ပြေပါသလား။";
            $this->createNotification("Survey", $message, $order->id, $order->shop->id);

            $survey_data = [
                "title" => "Moe Hein Gabar",
                "message" => $message,
                'type' => "Survey",
                'order_no' => $order->order_no,
                'order_id' => $order->uuid,
            ];
            (new SendOrderNotification())->execute([$order->shop->token], $survey_data);
        } elseif ($status === "Delivering") {

            $message = "$order->order_no အတွက်မှာယူထားသော ပစ္စည်းများ Deliveryသို့အပ်ပြီးဖြစ်ပါသည်။";
            $this->createNotification("Notification", $message, $order->id, $order->shop->id);

            $data = [
                "title" => "Moe Hein Gabar",
                "message" => $message,
                'order_no' => $order->order_no,
                'order_id' => $order->uuid,
                'Type' => "Notification"
            ];
            (new SendOrderNotification())->execute([$order->shop->token], $data);
        } elseif ($status === "Rejected") {

            $message = "Order အမှတ်စဉ် $order->order_no သည် ငြင်းပယ်လိုက်ပြီးဖြစ်ပါသည်။";
            $this->createNotification("Notification", $message, $order->id, $order->shop->id);

            $data = [
                "title" => "Moe Hein Gabar",
                "message" => $message,
                'order_no' => $order->order_no,
                'order_id' => $order->uuid,
                'Type' => "Notification"
            ];
            (new SendOrderNotification())->execute([$order->shop->token], $data);
        } elseif ($status === "Going to Bus Gate") {
            $message = "$order->order_no အတွက်မှာယူထားသော ပစ္စည်းများကားဂိတ်သို့ပိုဆောင်အပ်နှံပြီးဖြစ်ပါသည်။";
            $this->createNotification("Notification", $message, $order->id, $order->shop->id);

            $data = [
                "title" => "Moe Hein Gabar",
                "message" => $message,
                'order_no' => $order->order_no,
                'order_id' => $order->uuid,
                'Type' => "Notification"
            ];
            (new SendOrderNotification())->execute([$order->shop->token], $data);
        } elseif ($status === "Confirmed") {
            $message = "$order->order_no အားအတည်ပြုလက်ခံပြီးဖြစ်ပါသည်။";
            $this->createNotification("Notification", $message, $order->id, $order->shop->id);

            $data = [
                "title" => "Moe Hein Gabar",
                "message" => $message,
                'order_no' => $order->order_no,
                'order_id' => $order->uuid,
                'Type' => "Notification"
            ];
            (new SendOrderNotification())->execute([$order->shop->token], $data);
        }
    }

    public function createNotification($type, $message, $order_id, $shop_id, $title = "Moe Hein Gabar")
    {
        Notification::create([
            "title" => $title,
            "description" => $message,
            "order_id" => $order_id,
            "shop_id" => $shop_id,
            "type" => $type
        ]);
    }
}
