<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\FinanceParticular;
use App\Models\FillAmount;
use App\FinanceTransaction;
use App\Constants\GetStatus;
use App\Actions\SetCrudPermission;
use App\Models\FinanceCashAccount;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\FillAmountRequest;
use App\Constants\FinanceParticularConstant;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FillAmountCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FillAmountCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\FillAmount::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/fill-amount');
        CRUD::setEntityNameStrings('fill amount', 'fill amounts');
        (new SetCrudPermission())->execute('fill amount', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->orderBy('created_at', 'desc');
        CRUD::addColumn([
            'name' => 'from_account_id',
            'type' => 'relationship',
            'label' => 'From account',
        ]);
        CRUD::addColumn([
            'name' => 'to_account_id',
            'type' => 'relationship',
            'label' => 'To account',
        ]);
        CRUD::column('amount');
        CRUD::column('created_at');


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FillAmountRequest::class);

        $this->crud->addField([
            'name' => 'finance_particular_id',
            'label' => 'Finance Particular',
            'type' => 'select2', // Assuming you want to use a select2 field.
            'entity' => 'finance_particular', // Replace 'debitAccount' with the name of your related model if necessary.
            'attribute' => 'name', // Replace 'name' with the attribute you want to display in the select box.
            'model' => "App\FinanceParticular", // Replace with the actual namespace of your DebitAccount model.
            'options' => (function ($query) {
                return $query->whereNotIn('name',[FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'],FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'],FinanceParticularConstant::TYPE['SALE_CASH_COMPLETE'],FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'],FinanceParticularConstant::TYPE['GET_PROFIT_FROM_ORDER_CASH_COMPLETE']])->get(); // Replace with your query to retrieve the specific attributes.

            }),
        ]);

        CRUD::addField(['name' => 'amount', 'type' => 'number']);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store(FillAmountRequest $request)
    {

        try {
            DB::beginTransaction();
            $finance_particular = FinanceParticular::find($request->finance_particular_id);

            $credit_account_data = [];
            $debit_account_data = [];

            if($finance_particular->credit_account)
            {

                $finance_credit_account_before = $finance_particular->credit_account->amount;

                if($finance_particular->credit_account_operation ==  GetStatus::FINANCE_ACCOUNT_OPERATION['Subtraction'] )
                {
                    if ($request->amount > $finance_particular->credit_account->amount) {
                        $errors = ['amount' => 'Not enought amount balance'];
                        return redirect()->back()->withErrors($errors);
                    }

                    $finance_credit_account_after = bcsub($finance_credit_account_before, $request->amount, 4);

                    $finance_particular->credit_account->update([
                        'amount' => $finance_credit_account_after,
                    ]);

                    $credit_account_data= [
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'credit_account_before_amount' => $finance_credit_account_before,
                        'credit_account_after_amount' =>  $finance_credit_account_after,
                    ];
                }
                else
                {
                    $finance_credit_account_after = bcadd($finance_credit_account_before, $request->amount, 4);

                    $finance_particular->credit_account->update([
                        'amount' => $finance_credit_account_after,
                    ]);

                    $credit_account_data = [
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'credit_account_before_amount' => $finance_credit_account_before,
                        'credit_account_after_amount' =>  $finance_credit_account_after,
                    ];
                }
            }

            if($finance_particular->debit_account)
            {

                $finance_debit_account_before = $finance_particular->debit_account->amount;

                if($finance_particular->debit_account_operation ==  GetStatus::FINANCE_ACCOUNT_OPERATION['Subtraction'] )
                {
                    if ($request->amount > $finance_particular->debit_account->amount) {
                        $errors = ['amount' => 'Not enought amount balance'];
                        return redirect()->back()->withErrors($errors);
                    }

                    $finance_debit_account_after = bcsub($finance_debit_account_before, $request->amount, 4);

                    $finance_particular->debit_account->update([
                        'amount' => $finance_debit_account_after,
                    ]);

                    $debit_account_data  = [
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'debit_account_before_amount' => $finance_debit_account_before,
                        'debit_account_after_amount' =>  $finance_debit_account_after,
                    ];
                }
                else
                {
                    $finance_debit_account_after = bcadd($finance_debit_account_before, $request->amount, 4);

                    $finance_particular->debit_account->update([
                        'amount' => $finance_debit_account_after,
                    ]);

                    $debit_account_data  = [
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'debit_account_before_amount' => $finance_debit_account_before,
                        'debit_account_after_amount' =>  $finance_debit_account_after,
                    ];
                }
            }

            $fill_amount_data =[
                'from_account_id' => $finance_particular->credit_account_id,
                'to_account_id' => $finance_particular->debit_account_id,
                'amount' => $request->amount,
            ];

            $fill_amount = FillAmount::create($fill_amount_data);

            $finance_transaction_data = [
                "finance_particular_id" =>  $finance_particular->id,
                "transactionable_id" =>  $fill_amount->id,
                "transactionable_type" => get_class($fill_amount),
                "amount" =>  $request->amount,
            ];

            $combined_data = array_merge($credit_account_data, $debit_account_data,$finance_transaction_data);

            FinanceTransaction::create($combined_data);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            info($e);
            $errors = ['Check and try again'];
            return redirect()->back()->withErrors($errors);
        }

        return redirect('admin/fill-amount');

    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
