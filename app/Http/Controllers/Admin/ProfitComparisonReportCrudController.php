<?php

namespace App\Http\Controllers\Admin;

use App\FinanceTransaction;
use Illuminate\Http\Request;
use App\Models\ProfitDailyLog;
use App\Models\FinanceCashAccount;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProfitComparisonReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProfitComparisonReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StockLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/profit-comparison-report');
        CRUD::setEntityNameStrings('profit/loss comparison report', 'profit/loss comparison reports');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        if (!backpack_user()->can('list profit report')) {
            abort(403);
        }

        $years = FinanceTransaction::orderBy('created_at', 'desc')->pluck('created_at')->mapWithKeys(function ($item) {
            $year = $item->format('Y');
            return [$year => $year];
        })->unique()->toArray();

        CRUD::addField([
            'label' => '',
            'type' => 'select2_from_array',
            'name' => 'first_compare',
            'options'     => $years,
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 compare',
                'style' => 'margin-right: 5.5rem;',
            ],
        ]);

        CRUD::addField([
            'label'     => '',
            'type'      => 'select2_from_array',
            'name'      => 'second_compare',
            'options'     => $years,
            'allows_null' => false,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 compare',
                'style' => 'margin-right: 8.5rem;',
            ]
        ]);


        CRUD::addField([
            'name'  => 'route_name',
            'type'  => 'hidden',
            'value' => 'profit_comparison_report',
        ]);

        CRUD::addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/profit',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function report(Request $request)
    {

        $first_year = $request->first_compare;
        $second_year = $request->second_compare;

        $cash_accounts = FinanceCashAccount::all();

        $income_accs = [];
        $expense_accs = [];


        foreach ($cash_accounts as $cash_acc) {
            $first_income_amt = FinanceTransaction::where('debit_account_id', $cash_acc->id)->whereYear('created_at', $first_year)->sum('amount');
            $first_expense_amt = FinanceTransaction::where('credit_account_id', $cash_acc->id)->whereYear('created_at', $first_year)->sum('amount');
            $second_income_amt = FinanceTransaction::where('debit_account_id', $cash_acc->id)->whereYear('created_at', $second_year)->sum('amount');
            $second_expense_amt = FinanceTransaction::where('credit_account_id', $cash_acc->id)->whereYear('created_at', $second_year)->sum('amount');

            array_push($income_accs, [
                "name" => $cash_acc->name,
                "first_amount" => round($first_income_amt),
                "second_amount" => round($second_income_amt),
            ]);


            if ($cash_acc->name != 'Sale Gl' && $cash_acc->name != 'Supplier Gl' && $cash_acc->name != 'Product damage') {
                array_push($expense_accs, [
                    "name" => $cash_acc->name,
                    "first_amount" => round($first_expense_amt),
                    "second_amount" => round($second_expense_amt),
                ]);
            }
        }

        $first_net_profit_amount = ProfitDailyLog::whereYear('created_at', $first_year)->sum('total_amount');
        $second_net_profit_amount = ProfitDailyLog::whereYear('created_at', $second_year)->sum('total_amount');


        return view('partials.reports.profit_comparison_view', [
            'income_accounts' => $income_accs,
            'expense_accounts' => $expense_accs,
            'first_net_profit_amount' => $first_net_profit_amount,
            'second_net_profit_amount' => $second_net_profit_amount,
            'first_year' => $first_year,
            'second_year' => $second_year
        ]);
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
