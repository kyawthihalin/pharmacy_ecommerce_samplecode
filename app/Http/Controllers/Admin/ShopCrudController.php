<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\ShopRequest;
use App\Models\Shop;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class ShopCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShopCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Shop::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/shop');
        CRUD::setEntityNameStrings('shop', 'shops');
        CRUD::addClause('where', 'is_approve', true);
        (new SetCrudPermission())->execute('shop', $this->crud);
    }
    public function showDetailsRow($id)
    {
        $shop = Shop::find($id);
        return view('partials.shops.shopDetails', ['shop' => $shop]);
    }
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();
        $this->crud->removeAllButtonsFromStack('line');

        CRUD::column('name');
        CRUD::column('address');
        CRUD::column('phone');
        CRUD::column('frozen_at');
        CRUD::addColumn([
            'name'         => 'user', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Frozen By', // Table column heading
            'entity'    => 'user', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\User::class, // foreign key model
        ]);
        CRUD::addColumn([
            'label' => 'Freeze Status',
            'name' => 'freeze_status',
            'type' => 'closure',
            'function' => function($entry)
            {
                if($entry->freeze_status == 0)
                {
                    return "<span class='badge badge-success badge-pill'>Active</span>";
                }
                else
                {
                    return "<span class='badge badge-danger badge-pill'>Freezed</span>";
                }
            }
        ]);
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Type Shop Name'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('name', 'LIKE', "%{$value}%");
                });
            }
        );

        $this->crud->addFilter([
            'name'  => 'status',
            'type'  => 'dropdown',
            'label' => 'Status'
        ], [
            1 => 'Shop INACTIVE',
            0 => 'Shop ACTIVE',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'freeze_status', $value);
        });
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ShopRequest::class);

        CRUD::field('address');
        CRUD::field('created_at');
        CRUD::field('description');
        CRUD::field('freeze_status');
        CRUD::field('frozen_at');
        CRUD::field('id');
        CRUD::field('name');
        CRUD::field('password');
        CRUD::field('phone');
        CRUD::field('updated_at');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function statusChange(Request $request)
    {
        $status = $request->status;
        $sid = $request->sid;
        $shop = Shop::find($sid);
        if($shop)
        {
            if($status == "false")
            {
                $active = 1;
                $shop->freeze_status = $active;
                $shop->tokens()->delete();
                $shop->frozen_at = Carbon::now();
                $shop->user_id = backpack_user()->id;
                $shop->save();
            }else{
                $active = 0;
                $shop->freeze_status = $active;
                $shop->frozen_at = null;
                $shop->user_id = null;
                $shop->freeze_reason = "";
                $shop->save();
            }

            return response()->json([
                "status" => 200,
                "message" => "Success"
            ],200);
        }
    }
    public function freezeReason(Request $request)
    {
        $value = $request->value;
        $sid = $request->sid;
        $shop = Shop::find($sid);
        if($shop)
        {

            $shop->freeze_reason = $value;
            $shop->save();

            return response()->json([
                "status" => 200,
                "message" => "Success"
            ],200);
        }
    }
}
