<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\EnquiryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class EnquiryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EnquiryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Enquiry::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/enquiry');
        CRUD::setEntityNameStrings('enquiry', 'enquiries');
        (new SetCrudPermission())->execute('enquiry', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );
        CRUD::column('medicine_name');
        CRUD::column('brand_name');
        CRUD::addColumn([
            'name' => 'photo',
            'type' => 'closure',
            'label' => 'Image',
            'function' => function ($entry) {
                $url = Storage::temporaryUrl(
                    $entry->photo,
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}'><img src='{$url}' alt='Medicine' width='50' height='50'/></a>";
            },
        ]);
        CRUD::column('type');
        CRUD::column('size');
        CRUD::addColumn([
            'name'         => 'shop', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Shop',
            'entity'    => 'shop',
            'attribute' => 'name',
        ]);
        CRUD::addColumn([
            'name'         => 'shop_contact', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Contact Phone Number',
            'entity'    => 'shop',
            'attribute' => 'phone',
        ]);
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->enableExportButtons();
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(EnquiryRequest::class);

        

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
