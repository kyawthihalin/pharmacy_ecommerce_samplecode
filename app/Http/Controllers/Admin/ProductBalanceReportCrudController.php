<?php

namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Actions\SetCrudPermission;
use Illuminate\Support\Facades\DB;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductBalanceReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductBalanceReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ProductBalance::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product-balance-report');
        CRUD::setEntityNameStrings('product balance report', 'product balance reports');
        (new SetCrudPermission())->execute('product balance report', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::addField([
            'label' => 'SKU Code',
            'type' => 'select2',
            'name' => 'sku_code',
            'attribute' => 'code',
            // 'entity' => 'purchaseOrder',
            'model' => \App\Models\SkuCode::class,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 skuCode'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product ID',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'product_id',
            'attribute' => 'product_id',
            'model' => \App\Models\Product::class,
            'data_source' => url("admin/api/product-by-sku-code"),
            'placeholder' => 'Select Product ID',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 productId'
            ],
        ]);

        CRUD::addField([
            'label' => 'Product Name',
            'type' => 'select2_from_ajax_by_other_field',
            'name' => 'product_name',
            'attribute' => 'name',
            'model' => \App\Models\Product::class,
            'data_source' => url("admin/api/product-by-sku-code"),
            'placeholder' => 'Select Product Name',
            'minimum_input_length' => 0,
            'wrapperAttributes' => [
                'class' => 'form-group col-md-2 productName'
            ],
        ]);

        CRUD::addField(
            [   // date_range
                'name'  => 'start_date',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                // 'default' => Carbon::today()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-2 SDate'
                ]
            ],
        );

        CRUD::addField(
            [   // date_range
                'name'  => 'end_date',
                'type'  => 'date_picker',
                'label' => 'End Date',
                // 'default' => Carbon::tomorrow()->toDateString(),
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-2 EDate'
                ]
            ],
        );

        CRUD::addField([
            'name'  => 'route_name',
            'type'  => 'hidden',
            'value' => 'product_balance_report',
        ]);

        CRUD::addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/product_balance',
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function report(Request $request)
    {
        $sku_code_id = $request->sku_code_id;
        $product_id = $request->product_id;
        $product_name = $request->product_name;
        $start_date = $request->start_date;
        $end_date   = $request->end_date;

        if ($start_date != null && $end_date != null) {
            if ($start_date > $end_date) {
                return response()->json([
                    'message' => 'Start Date must be greater than End Date'
                ], 422);
            }

            $start_date = new DateTime($start_date);
            $start_date = $start_date->format('Y-m-d 00:00:00');

            $end_date   = new DateTime($end_date);
            $end_date   = $end_date->format('Y-m-d 23:59:59');
        }

        if (($start_date != null && $end_date == null) || ($start_date == null && $end_date != null)) {
            return response()->json([
                'message' => 'Start Date and End Date Must be choose'
            ], 422);
        }

        $balances = ProductBalance::where(function ($query) use ($sku_code_id, $start_date, $end_date) {
            if ($sku_code_id != null) {
                $query->where('sku_code_id', $sku_code_id);
            }
            if ($start_date !== null && $end_date !== null) {
                $query->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            }
        })->whereHas('product', function ($query) use ($product_id, $product_name) {
            if ($product_id != null) {
                $query->where('id', $product_id);
            }

            if ($product_name != null) {
                $query->where('id', $product_name);
            }
        })->select('product_id', 'sku_code_id', DB::raw('SUM(quantity) as quantity'))
            ->groupBy('product_id', 'sku_code_id')->orderBy(DB::raw('MAX(created_at)'), 'asc')->get();

        $product_balances = [];
        $row_span_count = 0;
        $total_quantity = [];

        $final_index = count($balances) - 1;

        foreach ($balances as $key => $balance) {
            if ($key > 0 && $balance->sku_code_id != $balances[$key - 1]->sku_code_id) {
                $row_span_count = 1;
                $total_quantity = [];
                array_push($total_quantity, [
                    'quantity' => $balance->quantity,
                    'unit' => $balance->product->unit->name,
                ]);
            } else {
                array_push($total_quantity, [
                    'quantity' => $balance->quantity,
                    'unit' => $balance->product->unit->name,
                ]);
                $row_span_count++;
            }

            $balance->rowspan = $row_span_count;
            $balance->is_span = 0;
            $balance->total_quantity = $total_quantity;

            if ($key == $final_index || $balance->sku_code_id != $balances[$key + 1]->sku_code_id) {
                $balance->is_span = 1;
            }

            $product_balances[] = $balance;
        }

        $product_balances = array_reverse($product_balances);

        return view('partials.reports.product_balance_view', compact('product_balances'))->render();
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
