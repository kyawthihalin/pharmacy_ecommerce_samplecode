<?php

namespace App\Http\Controllers\Admin;

use App\Actions\GetCity;
use App\Actions\GetTownship;
use App\Actions\SetCrudPermission;
use App\Constants\GetStatus;
use App\Constants\Status;
use App\Http\Requests\OrderReportRequest;
use App\Models\Order;
use App\Models\PaymentType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class OrderReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\OrderReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order-report');
        CRUD::setEntityNameStrings('order report', 'order reports');
        (new SetCrudPermission())->execute('order report', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderReportRequest::class);
        $this->crud->addField([
            'name' => 'start_date', // a unique name for this field
            'label' => 'Select Start Date',
            'type' => 'date_picker',
            'default' => Carbon::today()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]

        ]);
        $this->crud->addField([
            'name' => 'end_date', // a unique name for this field
            'label' => 'Select End Date',
            'type' => 'date_picker',
            'default' => Carbon::tomorrow()->toDateString(),
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ]
        ]);

        $this->crud->addField([
            'type' => 'select2_from_ajax_multiple',
            'name' => 'shop', // the relationship name in your Model
            'label' => 'Select Shops',
            'entity' => 'shop', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin
            'placeholder' => "Select a Shop", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'data_source' => url("admin/api/shop"), // url to controller search function (with /{id} should return model)
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);
        $this->crud->addField([
            'type' => 'select2_from_ajax_multiple',
            'name' => 'payment_type', // the relationship name in your Model
            'label' => 'Select Payment Type',
            'entity' => 'payment_type', // the relationship name in your Model
            'attribute' => 'name', // attribute on Article that is shown to admin
            'placeholder' => "Select a Payment Type", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'data_source' => url("admin/api/payment_type"), // url to controller search function (with /{id} should return model)
            'wrapperAttributes' => [
                'class' => 'form-group col-md-3'
            ],
            'attributes' => [
                'class' => 'form-control'
            ],
        ]);

        $this->crud->addField(
            [   // select_from_array
                'name'        => 'city',
                'label'       => "Choose City",
                'type'        => 'select2_from_array',
                'options'     => (new GetCity())->execute(),
                'allows_null' => false,
                'allows_multiple' => true,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ],
            ],
        );

        $this->crud->addField(
            [   // select_from_array
                'name'        => 'township',
                'label'       => "Choose Township",
                'type'        => 'select2_from_array',
                'options'     => (new GetTownship())->execute(),
                'allows_null' => false,
                'allows_multiple' => true,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ],
            ],
        );
        $this->crud->addField(
            [   // select_from_array
                'name'        => 'payment_status',
                'label'       => "Choose Payment Status",
                'type'        => 'select2_from_array',
                'options'     => GetStatus::PAYMENT_STATUS,
                'allows_null' => false,
                'allows_multiple' => true,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ],
            ],
        );
        $this->crud->addField(
            [   // select_from_array
                'name'        => 'shipping_status',
                'label'       => "Choose Shipping Status",
                'type'        => 'select2_from_array',
                'options'     => GetStatus::SHIPPING_STATUS,
                'allows_null' => false,
                'allows_multiple' => true,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3'
                ],
                'attributes' => [
                    'class' => 'form-control'
                ],
            ],
        );

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/order'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function report(Request $request)
    {
        $start_date = $request->start_date; //
        $end_date = $request->end_date; //
        $shops[] = $request->shops; //
        $payment_types[] = $request->payment_types; //
        $cities[] = $request->cities;
        $townships[] = $request->townships;
        $shipping_status[] = $request->shipping_status; //
        $payment_status[] = $request->payment_status; //
        $ids = [];
        $orders = Order::where(function ($query) use ($start_date, $end_date) {
            return $query->whereDate('created_at', '>=', $start_date)
                ->whereDate('created_at', '<=', $end_date);
        })->where(function ($query) use ($cities, $ids) {
            if ($cities[0] != null) {
                $city_addresses = DB::table('shipping_addresses')->whereIn('city_id', $cities[0])->get();
                if (count($city_addresses) > 0) {
                    foreach ($city_addresses as $caddress) {
                        $ids[] = $caddress->id;
                    }
                }
                return $query->whereIn('shipping_address_id', $ids);
            }
        })->where(function ($query) use ($townships, $ids) {
            if ($townships[0] != null) {
                $township_addresses = DB::table('shipping_addresses')->whereIn('township_id', $townships[0])->get();
                if (count($township_addresses) > 0) {
                    foreach ($township_addresses as $taddress) {
                        $ids[] = $taddress->id;
                    }
                }
                return $query->whereIn('shipping_address_id', $ids);
            }
        })
            ->where(function ($query) use ($shops) {
                if ($shops[0] != null) {
                    return $query->whereIn('shop_id', $shops[0]);
                }
            })->where(function ($query) use ($payment_types) {
                if ($payment_types[0] != null) {
                    return $query->whereIn('payment_type_id', $payment_types[0]);
                }
            })->where(function ($query) use ($payment_status) {
                if ($payment_status[0] != null) {
                    return $query->whereIn('payment_status', $payment_status[0]);
                }
            })->where(function ($query) use ($shipping_status) {
                if ($shipping_status[0] != null) {
                    return $query->whereIn('shipping_status', $shipping_status[0]);
                }
            })->get();
        return view('partials.reports.order_view', compact('orders'))->render();
    }
}
