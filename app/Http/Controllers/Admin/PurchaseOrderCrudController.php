<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Product;
use App\Models\StockLog;
use App\Models\Supplier;
use App\Constants\Status;
use App\Models\GlAccount;
use App\FinanceParticular;
use App\FinanceTransaction;
use Illuminate\Support\Str;
use App\Constants\GetStatus;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Imports\ProductImport;
use App\Models\ProductBalance;
use App\Models\StockAdjustment;
use App\Actions\SetCrudPermission;
use App\Models\FinanceCashAccount;
use Illuminate\Support\Facades\DB;
use App\Models\PurchaseOrderDetail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\PurchaseOrderRequest;
use App\Constants\FinanceParticularConstant;
use App\Http\Requests\PurchaseOrderUpdateRequest;
use App\Http\Requests\PurchaseOrderImportExcelRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Validation\ValidationException; // Import the ValidationException

/**
 * Class PurchaseOrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PurchaseOrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PurchaseOrder::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/purchase-order');
        CRUD::setEntityNameStrings('purchase order', 'purchase orders');
        // (new SetCrudPermission())->execute('purchase order', $this->crud);
    }

    protected function setupShowOperation()
    {
        // $this->setupListOperation();
        // $this->crud->addClause('with', 'purchaseOrderDetails');

         // Add a relationship clause to eager load the related purchase order details
        $this->crud->addClause('with', 'purchaseOrderDetails');

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addButtonFromView('top', 'importView', 'excel_import', 'end');


        $this->crud->orderBy('created_at', 'desc');
        $this->crud->disableResponsiveTable();
        $this->crud->enableDetailsRow();
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        $this->crud->addFilter([
            'name'  => 'order_status',
            'type'  => 'dropdown',
            'label' => 'Order Status'
        ],
            GetStatus::ORDER_STATUS,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', 'order_status', $value);
        });

        $this->crud->addFilter([
            'name'  => 'payment_status',
            'type'  => 'dropdown',
            'label' => 'Payment Status'
        ],
            GetStatus::PAYMENT_STATUS,
        function ($value) { // if the filter is active
            $this->crud->addClause('where', 'payment_status', $value);
        });

        $this->crud->addFilter([
            'name' => 'supplier_id',
            'type' => 'select2',
            'label' => 'Supplier',
        ], function() {
            return \App\Models\Supplier::pluck('name', 'id')->toArray();
        });

        CRUD::column('invoice_no');
        CRUD::column('order_no');
        CRUD::column('reference_id');
        CRUD::addColumn([
            'name' => 'supplier_id',
            'type' => 'relationship',
            'label' => 'Supplier Name',
        ]);
        CRUD::column('total_amount');
        CRUD::addColumn([
            'label' => 'Order status',
            'name' => 'order_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->order_status == "Pending") {
                    $color = "badge-warning";
                }
                if ($entry->order_status == "Arrived") {
                    $color = "badge-success";
                }
                if ($entry->order_status == "Cancelled") {
                    $color = "badge-danger";
                }
                return '<span class="badge ' . $color . ' badge-pill">' . $entry->order_status . '</span>';
            }
        ]);

        CRUD::addColumn([
            'label' => 'Payment Status',
            'name' => 'payment_status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = "";
                if ($entry->payment_status == "Pending") {
                    $color = "badge-warning";
                }
                if ($entry->payment_status == "Completed") {
                    $color = "badge-success";
                }
                if ($entry->payment_status == "Rejected") {
                    $color = "badge-danger";
                }
                if ($entry->payment_status == "Credit Completed") {
                    $color = "badge-info";
                }

                return '<span class="badge ' . $color . ' badge-pill">' . $entry->payment_status . '</span>';
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    public function importView()
    {
        $suppliers = Supplier::all();

        return view('partials.purchaseOrders.purchaseOrderImport', [
            'suppliers' => $suppliers,
            'crud' => $this->crud,
        ]);
    }

    public function importStore(PurchaseOrderImportExcelRequest $request)
    {
        try {
            DB::beginTransaction();
            $purchase_order =  PurchaseOrder::create([
                'user_id' => Auth::id(),
                'supplier_id' => $request->supplier,
                'invoice_no' => $request->invoice_no,
                'order_no' => 'ORD'.random_int(100000000,999999999),
                'reference_id' => Str::random(15),
                'remark' => $request->remark,
                'total_amount' => 0,
                'date' => $request->date,
            ]);

            if ($request->hasFile('excel')) {
                $import = new ProductImport($purchase_order->id);
                Excel::import($import, $request->file('excel'));
                $totalPrice = $import->totalPrice; // Access the calculated total price

                $purchase_order->update(
                    ['total_amount' => $totalPrice]
                );
            }

            DB::commit();
            return redirect('admin/purchase-order');
        } catch (ValidationException  $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->errors());
        }
    }

    public function showDetailsRow($id)
    {
        $todayPurchaseOrder = PurchaseOrder::find($id);

        return view('partials.purchaseOrders.purchaseOrderDetails', ['todayPurchaseOrder' => $todayPurchaseOrder]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PurchaseOrderRequest::class);
        CRUD::field('supplier_id');
        CRUD::field('invoice_no');

        // $this->crud->addField(
        //     [   // select_from_array
        //         'name'        => 'payment_method',
        //         'label'       => "Choose payment method",
        //         'type'        => 'select2_from_array',
        //         'options'     => GetStatus::PAYMENT_METHOD,
        //         'allows_null' => false,
        //         'allows_multiple' => false,
        //         'wrapperAttributes' => [
        //             'class' => 'form-group col-md-12'
        //         ],
        //         'attributes' => [
        //             'class' => 'form-control'
        //         ],
        //     ],
        // );

        CRUD::field('remark');
        CRUD::field('date');

        // $this->crud->addField([
        //     'name' => 'import_file',
        //     'type' => 'upload',
        //     'label' => 'Product List Excel File',
        //     'upload'    => true,
        // ]);

        CRUD::addField([   // repeatable
            'name'  => 'products',
            'label' => 'Product',
            'type'  => 'repeatable',
            'fields' => [
                [
                    'type' => 'select2_from_ajax',
                    'name' => 'product_id', // the relationship name in your Model
                    'label' => 'Select product',
                    'entity' => 'products', // the relationship name in your Model
                    'attribute' => 'name', // attribute on Article that is shown to admin
                    'placeholder' => "Select Product", // placeholder for the select
                    'minimum_input_length' => 0, // minimum characters to type before querying results
                    'data_source' => url("admin/api/product"), // url to controller search function (with /{id} should return model)
                    'wrapperAttributes' => [
                        'class' => 'form-group col-md-6'
                    ],
                    'attributes' => [
                        'class' => 'form-control'
                    ],
                ],
                [
                    'name'    => 'quantity',
                    'type'    => 'number',
                    'label'   => 'Quantity',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
                [
                    'name'    => 'price',
                    'type'    => 'number',
                    'label'   => 'Price',
                    'wrapper' => ['class' => 'form-group col-md-6'],
                ],
            ],

            // optional
            'new_item_label'  => 'Add Product', // customize the text of the button
            'allows_null' => false,
            'init_rows' => 1, // number of empty rows to be initialized, by default 1
            'min_rows' => 1, // minimum rows allowed, when reached the "delete" buttons will be hidden
            'max_rows' => 200, // maximum rows allowed, when reached the "new item" button will be hidden
        ],);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store(PurchaseOrderRequest $request)
    {

        try {
            DB::beginTransaction();
            $purchase_order =  PurchaseOrder::create([
                'user_id' => Auth::id(),
                'supplier_id' => $request->supplier_id,
                'invoice_no' => $request->invoice_no,
                'order_no' => 'ORD'.random_int(100000000,999999999),
                'reference_id' => Str::random(15),
                'remark' => $request->remark,
                'total_amount' => 0,
                'date' => $request->date,
            ]);

            $total_product_price = 0;
            foreach ($request->products as $product) {
                PurchaseOrderDetail::create([
                    'purchase_order_id' => $purchase_order->id,
                    'product_id' => $product['product_id'],
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                ]);

                $total_product_price += $product['quantity']*$product['price'];
            }
            $purchase_order->update(
                ['total_amount' => $total_product_price]
            );

            DB::commit();
            return redirect('admin/purchase-order');
        } catch (ValidationException  $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->errors());
        }
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */

    public function changePurchaseOrderStatus(Request $request)
    {
        $purchaseOrder = PurchaseOrder::find($request->oid);
        $status = $request->status;
        $current_status = $purchaseOrder->order_status;
        $payment_status = $purchaseOrder->payment_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        }

        if ($current_status === "Arrived") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Pending or Cancelled Status."
            ], 422);
        }

        if ( ($payment_status === "Completed" && $status === 'Cancelled') || ($payment_status === "Credit Completed" && $status === 'Cancelled') ) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Cancelled Status. There is payment is completed"
            ], 422);
        }

        if ($payment_status === "Rejected") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There is payment is rejected"
            ], 422);
        }

        try {
            DB::beginTransaction();

            $purchaseOrder->update([
                'order_status' => $status,
                'remark' => $request->remark,
            ]);

            $purchaseOrderDetails = PurchaseOrderDetail::where('purchase_order_id',$purchaseOrder->id)->get();

            if($status == "Arrived" )
            {
                foreach ($purchaseOrderDetails as $purchaseOrderDetail) {
                    $product = Product::find($purchaseOrderDetail['product_id']);
                    ProductBalance::create([
                        'product_id' => $purchaseOrderDetail['product_id'],
                        'sku_code_id' => $product->skuCode->id,
                        'purchase_order_id' => $purchaseOrder->id,
                        'quantity' => $purchaseOrderDetail['quantity'],
                        'price' => $purchaseOrderDetail['price'],
                    ]);

                    $current_quantity = ProductBalance::where('product_id', $purchaseOrderDetail['product_id'])->sum('quantity');

                    StockLog::create([
                        'product_id' => $purchaseOrderDetail['product_id'],
                        'type' => GetStatus::STOCK_TYPE['Purchase'],
                        'quantity' => $purchaseOrderDetail['quantity'],
                        'current_qty' => $current_quantity,
                    ]);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Purchase Order status is changed successfully."
            ], 200);

            } catch (Exception $e) {
                DB::rollback();
                info($e);
                return response()->json([
                    "status" => "422",
                    "message" => "Error Occur in process"
                ], 422);
            }
    }


    public function changePurchasePaymentStatus(Request $request)
    {

        $purchaseOrder = PurchaseOrder::find($request->oid);
        $status = $request->status;
        $current_status = $purchaseOrder->payment_status;
        $order_status = $purchaseOrder->order_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        }

        if ($current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Pending or Rejected Status."
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Rejected') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Rejected Status. There is order is arrived"
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Pending') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to pending Status. There is order is arrived"
            ], 422);
        }

        if (($order_status === "Cancelled" && $status === 'Completed') || ($order_status === "Cancelled" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There is order status is cancelled"
            ], 422);
        }

        if (($order_status != "Arrived" && $status === 'Completed') || ($order_status != "Arrived" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There are order status is not arrived"
            ], 422);
        }



        try {
            DB::beginTransaction();

            $amount = $purchaseOrder->total_amount;

            // directly credit
            if($status == 'Credit Completed' && $current_status != 'Completed')
            {
                $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                $supplier_debit_gl_amount_after = bcadd($supplier_debit_gl_amount_before, $amount, 4);

                $supplier_debit_gl->update([
                    'amount' => $supplier_debit_gl_amount_after,
                ]);

                $finance_particular =  FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'])->first();

                FinanceTransaction::create([
                    'finance_particular_id' => $finance_particular->id,
                    'transactionable_id' => $purchaseOrder->id,
                    'transactionable_type' => get_class($purchaseOrder),
                    'debit_account_id' => $finance_particular->debit_account_id,
                    'credit_account_id' => $finance_particular->credit_account_id,
                    'amount' => $amount,
                    'debit_account_before_amount' => $supplier_debit_gl_amount_before,
                    'debit_account_after_amount' => $supplier_debit_gl_amount_after,
                ]);

                $purchaseOrder->update([
                    'payment_status' => $status,
                    'remark' =>  $request->remark,
                ]);
            }

            if($status == 'Completed')
            {   // from credit to Complete
                if($current_status == 'Credit Completed')
                {
                    $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                    $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                    $supplier_debit_gl_amount_after = bcsub($supplier_debit_gl_amount_before, $amount, 4);

                    $supplier_debit_gl->update([
                        'amount' => $supplier_debit_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'amount' => $amount,
                        'credit_account_before_amount' => $supplier_debit_gl_amount_before,
                        'credit_account_after_amount' => $supplier_debit_gl_amount_after,
                    ]);
                }
                    // Complete
                    $supplier_gl = FinanceCashAccount::where('account_code','supplier_gl')->first();
                    $supplier_gl_amount_before = $supplier_gl->amount;
                    $supplier_gl_amount_after = bcadd($supplier_gl_amount_before, $amount, 4);

                    $capital_gl = FinanceCashAccount::where('account_code','capital_gl')->first();
                    $capital_gl_amount_before = $capital_gl->amount;
                    $capital_gl_amount_after = bcsub($capital_gl_amount_before, $amount, 4);

                    $supplier_gl->update([
                        'amount' => $supplier_gl_amount_after,
                    ]);

                    $capital_gl->update([
                        'amount' => $capital_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'amount' => $amount,
                        'debit_account_before_amount' => $supplier_gl_amount_before,
                        'debit_account_after_amount' => $supplier_gl_amount_after,
                        'credit_account_before_amount' => $capital_gl_amount_before,
                        'credit_account_after_amount' => $capital_gl_amount_after,
                    ]);
            }

            $purchaseOrder->update([
                'payment_status' => $status,
                'remark' =>  $request->remark,
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            info($e);
            return response()->json([
                "status" => "422",
                "message" => 'Error Occur.Please try agin',
            ], 422);
        }

        return response()->json([
            "status" => "200",
            "message" => "Purchase Order status is changed successfully."
        ], 200);
    }


    // For Button
    public function changeCreditToCompleteStatus(Request $request)
    {
        $purchaseOrder = PurchaseOrder::find($request->id);
        $status = 'Completed';
        $current_status = $purchaseOrder->payment_status;
        $order_status = $purchaseOrder->order_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        }

        if ($current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Pending or Rejected Status."
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Rejected') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Rejected Status. There is order is arrived"
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Pending') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to pending Status. There is order is arrived"
            ], 422);
        }

        if (($order_status === "Cancelled" && $status === 'Completed') || ($order_status === "Cancelled" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There is order status is cancelled"
            ], 422);
        }

        if (($order_status != "Arrived" && $status === 'Completed') || ($order_status != "Arrived" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There are order status is not arrived"
            ], 422);
        }


        try {
            DB::beginTransaction();

            $amount = $purchaseOrder->total_amount;

            if($status == 'Credit Completed' && $current_status != 'Completed')
            {
                $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                $supplier_debit_gl_amount_after = bcadd($supplier_debit_gl_amount_before, $amount, 4);

                $supplier_debit_gl->update([
                    'amount' => $supplier_debit_gl_amount_after,
                ]);

                $finance_particular =  FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'])->first();

                FinanceTransaction::create([
                    'finance_particular_id' => $finance_particular->id,
                    'transactionable_id' => $purchaseOrder->id,
                    'transactionable_type' => get_class($purchaseOrder),
                    'debit_account_id' => $finance_particular->debit_account_id,
                    'credit_account_id' => $finance_particular->credit_account_id,
                    'amount' => $amount,
                    'debit_account_before_amount' => $supplier_debit_gl_amount_before,
                    'debit_account_after_amount' => $supplier_debit_gl_amount_after,
                ]);

                $purchaseOrder->update([
                    'payment_status' => $status,
                ]);
            }

            if($status == 'Completed')
            {
                if($current_status == 'Credit Completed')
                {
                    $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                    $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                    $supplier_debit_gl_amount_after = bcsub($supplier_debit_gl_amount_before, $amount, 4);

                    $supplier_debit_gl->update([
                        'amount' => $supplier_debit_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'amount' => $amount,
                        'credit_account_before_amount' => $supplier_debit_gl_amount_before,
                        'credit_account_after_amount' => $supplier_debit_gl_amount_after,
                    ]);
                }

                    $supplier_gl = FinanceCashAccount::where('account_code','supplier_gl')->first();
                    $supplier_gl_amount_before = $supplier_gl->amount;
                    $supplier_gl_amount_after = bcadd($supplier_gl_amount_before, $amount, 4);

                    $capital_gl = FinanceCashAccount::where('account_code','capital_gl')->first();
                    $capital_gl_amount_before = $capital_gl->amount;
                    $capital_gl_amount_after = bcsub($capital_gl_amount_before, $amount, 4);

                    $supplier_gl->update([
                        'amount' => $supplier_gl_amount_after,
                    ]);

                    $capital_gl->update([
                        'amount' => $capital_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'amount' => $amount,
                        'debit_account_before_amount' => $supplier_gl_amount_before,
                        'debit_account_after_amount' => $supplier_gl_amount_after,
                        'credit_account_before_amount' => $capital_gl_amount_before,
                        'credit_account_after_amount' => $capital_gl_amount_after,
                    ]);


                $purchaseOrder->update([
                    'payment_status' => $status,
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            info($e);
            return response()->json([
                "status" => "422",
                "message" => 'Error Occur.Please try agin',
            ], 422);
        }

        return response()->json([
            "status" => "200",
            "message" => "Purchase Order status is changed successfully."
        ], 200);
    }

    public function changeCreditForPurchase(Request $request)
    {
        $purchaseOrder = PurchaseOrder::find($request->id);
        $status = 'Credit Completed';
        $current_status = $purchaseOrder->payment_status;
        $order_status = $purchaseOrder->order_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        }

        if ($current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Pending or Rejected Status."
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Rejected') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Rejected Status. There is order is arrived"
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Pending') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to pending Status. There is order is arrived"
            ], 422);
        }

        if (($order_status === "Cancelled" && $status === 'Completed') || ($order_status === "Cancelled" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There is order status is cancelled"
            ], 422);
        }

        if (($order_status != "Arrived" && $status === 'Completed') || ($order_status != "Arrived" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There are order status is not arrived"
            ], 422);
        }


        try {
            DB::beginTransaction();

            $amount = $purchaseOrder->total_amount;

            if($status == 'Credit Completed' && $current_status != 'Completed')
            {
                $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                $supplier_debit_gl_amount_after = bcadd($supplier_debit_gl_amount_before, $amount, 4);

                $supplier_debit_gl->update([
                    'amount' => $supplier_debit_gl_amount_after,
                ]);

                $finance_particular =  FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'])->first();

                FinanceTransaction::create([
                    'finance_particular_id' => $finance_particular->id,
                    'transactionable_id' => $purchaseOrder->id,
                    'transactionable_type' => get_class($purchaseOrder),
                    'debit_account_id' => $finance_particular->debit_account_id,
                    'credit_account_id' => $finance_particular->credit_account_id,
                    'amount' => $amount,
                    'debit_account_before_amount' => $supplier_debit_gl_amount_before,
                    'debit_account_after_amount' => $supplier_debit_gl_amount_after,
                ]);

                $purchaseOrder->update([
                    'payment_status' => $status,
                ]);
            }

            if($status == 'Completed')
            {
                if($current_status == 'Credit Completed')
                {
                    $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                    $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                    $supplier_debit_gl_amount_after = bcsub($supplier_debit_gl_amount_before, $amount, 4);

                    $supplier_debit_gl->update([
                        'amount' => $supplier_debit_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'amount' => $amount,
                        'credit_account_before_amount' => $supplier_debit_gl_amount_before,
                        'credit_account_after_amount' => $supplier_debit_gl_amount_after,
                    ]);
                }

                    $supplier_gl = FinanceCashAccount::where('account_code','supplier_gl')->first();
                    $supplier_gl_amount_before = $supplier_gl->amount;
                    $supplier_gl_amount_after = bcadd($supplier_gl_amount_before, $amount, 4);

                    $capital_gl = FinanceCashAccount::where('account_code','capital_gl')->first();
                    $capital_gl_amount_before = $capital_gl->amount;
                    $capital_gl_amount_after = bcsub($capital_gl_amount_before, $amount, 4);

                    $supplier_gl->update([
                        'amount' => $supplier_gl_amount_after,
                    ]);

                    $capital_gl->update([
                        'amount' => $capital_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'amount' => $amount,
                        'debit_account_before_amount' => $supplier_gl_amount_before,
                        'debit_account_after_amount' => $supplier_gl_amount_after,
                        'credit_account_before_amount' => $capital_gl_amount_before,
                        'credit_account_after_amount' => $capital_gl_amount_after,
                    ]);


                $purchaseOrder->update([
                    'payment_status' => $status,
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            info($e);
            return response()->json([
                "status" => "422",
                "message" => 'Error Occur.Please try agin',
            ], 422);
        }

        return response()->json([
            "status" => "200",
            "message" => "Purchase Order status is changed successfully."
        ], 200);
    }

    public function changeCompleteForPurchase(Request $request)
    {
        $purchaseOrder = PurchaseOrder::find($request->id);
        $status = 'Completed';
        $current_status = $purchaseOrder->payment_status;
        $order_status = $purchaseOrder->order_status;

        if ($status === $current_status) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to same status."
            ], 422);
        }

        if ($current_status === "Completed") {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Pending or Rejected Status."
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Rejected') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Rejected Status. There is order is arrived"
            ], 422);
        }

        if ($order_status === "Arrived" && $status === 'Pending') {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to pending Status. There is order is arrived"
            ], 422);
        }

        if (($order_status === "Cancelled" && $status === 'Completed') || ($order_status === "Cancelled" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There is order status is cancelled"
            ], 422);
        }

        if (($order_status != "Arrived" && $status === 'Completed') || ($order_status != "Arrived" && $status === 'Credit Completed')) {
            return response()->json([
                "status" => "422",
                "message" => "You can't change to Status. There are order status is not arrived"
            ], 422);
        }


        try {
            DB::beginTransaction();

            $amount = $purchaseOrder->total_amount;

            if($status == 'Credit Completed' && $current_status != 'Completed')
            {
                $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                $supplier_debit_gl_amount_after = bcadd($supplier_debit_gl_amount_before, $amount, 4);

                $supplier_debit_gl->update([
                    'amount' => $supplier_debit_gl_amount_after,
                ]);

                $finance_particular =  FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'])->first();

                FinanceTransaction::create([
                    'finance_particular_id' => $finance_particular->id,
                    'transactionable_id' => $purchaseOrder->id,
                    'transactionable_type' => get_class($purchaseOrder),
                    'debit_account_id' => $finance_particular->debit_account_id,
                    'credit_account_id' => $finance_particular->credit_account_id,
                    'amount' => $amount,
                    'debit_account_before_amount' => $supplier_debit_gl_amount_before,
                    'debit_account_after_amount' => $supplier_debit_gl_amount_after,
                ]);

                $purchaseOrder->update([
                    'payment_status' => $status,
                ]);
            }

            if($status == 'Completed')
            {
                if($current_status == 'Credit Completed')
                {
                    $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
                    $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
                    $supplier_debit_gl_amount_after = bcsub($supplier_debit_gl_amount_before, $amount, 4);

                    $supplier_debit_gl->update([
                        'amount' => $supplier_debit_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'amount' => $amount,
                        'credit_account_before_amount' => $supplier_debit_gl_amount_before,
                        'credit_account_after_amount' => $supplier_debit_gl_amount_after,
                    ]);
                }

                    $supplier_gl = FinanceCashAccount::where('account_code','supplier_gl')->first();
                    $supplier_gl_amount_before = $supplier_gl->amount;
                    $supplier_gl_amount_after = bcadd($supplier_gl_amount_before, $amount, 4);

                    $capital_gl = FinanceCashAccount::where('account_code','capital_gl')->first();
                    $capital_gl_amount_before = $capital_gl->amount;
                    $capital_gl_amount_after = bcsub($capital_gl_amount_before, $amount, 4);

                    $supplier_gl->update([
                        'amount' => $supplier_gl_amount_after,
                    ]);

                    $capital_gl->update([
                        'amount' => $capital_gl_amount_after,
                    ]);

                    $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'])->first();

                    FinanceTransaction::create([
                        'finance_particular_id' => $finance_particular->id,
                        'transactionable_id' => $purchaseOrder->id,
                        'transactionable_type' => get_class($purchaseOrder),
                        'debit_account_id' => $finance_particular->debit_account_id,
                        'credit_account_id' => $finance_particular->credit_account_id,
                        'amount' => $amount,
                        'debit_account_before_amount' => $supplier_gl_amount_before,
                        'debit_account_after_amount' => $supplier_gl_amount_after,
                        'credit_account_before_amount' => $capital_gl_amount_before,
                        'credit_account_after_amount' => $capital_gl_amount_after,
                    ]);


                $purchaseOrder->update([
                    'payment_status' => $status,
                ]);
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            info($e);
            return response()->json([
                "status" => "422",
                "message" => 'Error Occur.Please try agin',
            ], 422);
        }

        return response()->json([
            "status" => "200",
            "message" => "Purchase Order status is changed successfully."
        ], 200);
    }
}
