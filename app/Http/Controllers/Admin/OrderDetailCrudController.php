<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\SkuCode;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\ProductBalance;
use App\Http\Requests\OrderDetailRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OrderDetailCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderDetailCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\OrderDetail::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/order-detail');
        CRUD::setEntityNameStrings('order detail', 'order details');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('id');
        CRUD::column('order_id');
        CRUD::column('product_id');
        CRUD::column('quantity');
        CRUD::column('amount');
        CRUD::column('remark');
        CRUD::column('discount_amount');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderDetailRequest::class);

        CRUD::field('id');
        CRUD::field('order_id');
        CRUD::field('product_id');
        CRUD::field('quantity');
        CRUD::field('amount');
        CRUD::field('remark');
        CRUD::field('discount_amount');
        CRUD::field('created_at');
        CRUD::field('updated_at');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function updateOrder(Request $request)
    {
        $order_detail_id = $request->odid;
        $quantity = $request->qty;
        $order_detail = OrderDetail::find($order_detail_id);

        $old_price = 0;

        if($order_detail->quantity > 0)
        {
            $old_price = $order_detail->amount / $order_detail->quantity;

        }
        $order = Order::find($order_detail->order_id);

        if ($order_detail) {
            $product = Product::find($order_detail->product_id);
            // $available_total_product = ProductBalance::where('product_id', $product->id)->where('price',$order_detail->purchase_price)->sum('quantity');
            $available_total_product = ProductBalance::where('product_id', $product->id)->sum('quantity');

            $discount = 0;

            $todayDate = Carbon::today()->format('Y-m-d');
            $product_sku = SkuCode::where('id',$product->sku_code_id)->where('discount_start_date', '<=', $todayDate)->where('discount_end_date', '>=', $todayDate)->first();

            if($product_sku)
            {
                $discount_amount = $product_sku->discount;
            }
            else{
                $discount_amount = 0;
            }


            if($order_detail->quantity == 0 || $order_detail->quantity == null)
            {
                $old_price = $product->price;
                // $old_price = $product->price;
            }
            // if($product->quantity < $quantity)
            if($available_total_product < $quantity)
            {
                return response()->json([
                    'message' => 'Quantity must under max stock!',
                    'status' => 422
                ], 422);
            }
            if($quantity > 0)
            {
                if ($product && $discount_amount > 0) {

                    if ($product_sku->discount_type == "Fixed") {
                        $discount = bcmul($discount_amount,$quantity);
                    }
                    if ($product_sku->discount_type == "Percent") {
                        $product_discount = bcdiv((float)bcmul($discount_amount, $old_price), 100);
                        $discount = (float)bcmul($product_discount, $quantity);
                    }
                }
            }else
            {
                $discount = 0;
            }


            $order_detail->quantity = $quantity;
            $order_detail->amount = bcmul($old_price,$quantity);
            $order_detail->discount_amount = $discount;
            $order_detail->save();

            $order_details_to_update = OrderDetail::where('order_id',$order->id)->where('quantity','!=',0)->get();

            $total = 0;
            $discount_amount = 0;
            foreach($order_details_to_update as $order_to_update)
            {
                $total += $order_to_update->amount;
                $discount_amount += $order_to_update->discount_amount;
            }

            $coupon = Coupon::find($order->coupon_id);
            $coupon_discount = 0;
            if($coupon)
            {
                $coupon_amount = $coupon->amount;
                if($coupon->coupon_type == "Percent")
                {
                    $coupon_discount = bcdiv(bcmul($coupon_amount,$total),100);
                }else{
                    $coupon_discount = $coupon_amount;
                }
            }



            $order->total_amount = $total;
            $order->discount_amount = $discount_amount;
            $order->grand_total = bcsub(bcsub($total,$coupon_discount),$discount_amount);

            $order->coupon_discount = $coupon_discount;
            $order->net_amount = bcadd(bcsub(bcsub($total,$discount_amount),$coupon_discount),$order->shipping_fees);
            $order->save();



            return response()->json([
                'message' => 'success',
                'status' => 200,
                'data' => [
                    'discount' => $discount,
                    'total' => (float)bcsub($order_detail->amount,$discount)
                ],
            ], 200);
        }



        return response()->json([
            'message' => 'Failed to change quantity!',
            'status' => 500
        ], 500);
    }

    public function removeProduct(Request $request)
    {
        $odid = $request->odid;
        $order_detail = OrderDetail::find($odid);
        if($order_detail->quantity > 0)
        {
            $order = Order::find($order_detail->order_id);
            $order->total_amount -= $order_detail->amount;
            $order->discount_amount -= $order_detail->discount_amount;
            $order->grand_total = bcsub(bcadd($order->grand_total,$order_detail->discount_amount),$order_detail->amount);
            $order->net_amount = bcsub(bcadd($order->net_amount,$order_detail->discount_amount),$order_detail->amount);
            $order->save();
        }
        $order_detail->delete();
        return response()->json([
            'message' => 'success',
            'status' => 200
        ], 200);
    }
}
