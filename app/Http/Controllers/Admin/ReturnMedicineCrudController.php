<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\ReturnMedicineRequest;
use App\Models\Product;
use App\Models\ReturnMedicine;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class ReturnMedicineCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ReturnMedicineCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ReturnMedicine::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/return-medicine');
        CRUD::setEntityNameStrings('return medicine', 'return medicines');
        (new SetCrudPermission())->execute('return medicine', $this->crud);
    }
    public function showDetailsRow($id)
    {
        $medicine = ReturnMedicine::find($id);

        return view('partials.medicine.return', ['medicine' => $medicine]);
    }
    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );
        CRUD::addFilter(
            [
                'name' => 'product_id',
                'type' => 'select2_ajax',
                'label' => "Search Single Product",
                'placeholder' => "Type Proudct Name to Search",
            ],
            url('admin/get-product'),
            function ($value) {
                CRUD::addClause('where', function ($q) use ($value) {
                    return $q->where('product_id', '=', $value);
                });
            }
        );
        $this->crud->addFilter([
            'name'  => 'product_ajax',
            'type'  => 'select2_multiple',
            'label' => 'Select Multiple Products'
        ], function () {
            return $this->getallProduct();
        }, function ($value) { // if the filter is active
            CRUD::addClause('where', function ($q) use ($value) {
                return $q->whereIn('product_id', (array)$value);
            });
        });

        CRUD::addColumn([
            'name'         => 'shop', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Shop',
            'entity'    => 'shop',
            'attribute' => 'name',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("shop/?name=" . $entry->shop->name);
                },
            ],
        ]);
        CRUD::addColumn([
            'name'         => 'product_id', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Product Name',
            'entity'       => 'product',
            'attribute'    => 'name',
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("product?name=" . $entry->product->name);
                },
            ],
        ]);
        CRUD::addColumn([
            'name'         => 'brand_id', // name of relationship method in the model
            'type'         => 'closure',
            'label'        => 'Brand Name',
            'function' => function($entry)
            {
                return $entry->product->brand->name;
            },
            'wrapper'   => [
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url("brand?name=" . $entry->product->brand->name);
                },
            ],
        ]);
      
        // CRUD::column('type');
        CRUD::addColumn([
            'name' => 'size',
            'type' => 'text',
            'label' => 'Return Product Size'
        ]);
        // CRUD::addColumn([
        //     'name' => 'quantity',
        //     'type' => 'number',
        //     'label' => 'Return Quantity'
        // ]);

        CRUD::addColumn([
            'name' => 'price',
            'type' => 'number',
            'label' => 'Return Price'
        ]);
       
        CRUD::addColumn([
            'name'         => 'shop_contact', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Contact Phone Number',
            'entity'    => 'shop',
            'attribute' => 'phone',
        ]);
        CRUD::addColumn([
            'name'         => 'remark_by', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'Remark By',
            'entity'    => 'user',
            'attribute' => 'name',
        ]);
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ReturnMedicineRequest::class);
       

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
        CRUD::addField([
            'name' => 'remark',
            'type' => 'textarea',
            'label' => 'Return Remark'
        ]);
        CRUD::addField([
            'name' => 'user_id',
            'type' => 'hidden',
            'default' => backpack_user()->id,
        ]);
    }
    public function getallProduct()
    {
        $options = Product::get()->pluck('name', 'id')->toArray();
        return $options;
    }
}
