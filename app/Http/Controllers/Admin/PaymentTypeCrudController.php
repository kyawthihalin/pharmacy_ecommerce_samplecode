<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\PaymentTypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Storage;

/**
 * Class PaymentTypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaymentTypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PaymentType::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/payment-type');
        CRUD::setEntityNameStrings('payment type', 'payment types');
        (new SetCrudPermission())->execute('payment type', $this->crud);

    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name');
        CRUD::column('account_name');
        CRUD::column('account_number');
        
        CRUD::addColumn([
            'name' => 'logo',
            'type' => 'closure',
            'label' => 'Logo',
            'function' => function ($entry) {
                $url = Storage::temporaryUrl(
                    $entry->logo,
                    now()->addMinutes(3)
                );
                return "<a target='blank' href='{$url}'><img src='{$url}' alt='Payment Type Logo' width='50' height='50'/></a>";
            },
        ]);

        CRUD::addColumn([
            'name' => 'status',
            'label' => 'Status',
            'type' => 'closure',
            'function' => function($entry){
                if($entry->status == 1)
                {
                    return "<span class='badge badge-pill badge-success'>Active</span>";
                }
                else
                {
                    return "<span class='badge badge-pill badge-danger'>Inactive</span>";
                }
            }
        ]);
        CRUD::column('created_at');
        CRUD::column('updated_at');
        $this->crud->enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PaymentTypeRequest::class);
        CRUD::field('name');
        CRUD::field('account_name');
        CRUD::field('account_number');
        CRUD::addField([
            'label' => "Logo",
            'name'      => 'logo',
            'label'     => 'Logo',
            'type'      => 'upload',
            'upload'    => true,
            'disk'      => 'local',
            //'temporary' => 10 
        ]);
        CRUD::addField([
            'label' => 'Active Status',
            'type' => 'checkbox',
            'name' => 'status',
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
