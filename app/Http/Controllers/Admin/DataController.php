<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Shop;
use App\Models\Unit;
use App\Models\Product;
use App\Models\SkuCode;
use App\Models\Township;
use App\FinanceParticular;
use App\Models\PaymentType;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\ProductBalance;
use App\Models\UnitRelationship;
use App\Http\Controllers\Controller;

class DataController extends Controller
{
    public function getallShops(Request $request){

        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Shop::where('name', 'LIKE', '%'.$search_term.'%')->paginate(100);
        }
        else
        {
            $results = Shop::paginate(100);
        }

        return $results;
    }
    public function getallCities(Request $request){

        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = City::where('name', 'LIKE', '%'.$search_term.'%')->paginate(100);
        }
        else
        {
            $results = City::paginate(100);
        }

        return $results;
    }

    public function getallTownships(Request $request){

        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Township::where('name', 'LIKE', '%'.$search_term.'%')->paginate(100);
        }
        else
        {
            $results = Township::paginate(100);
        }

        return $results;
    }
    public function getallPaymentTypes(Request $request){

        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = PaymentType::where('name', 'LIKE', '%'.$search_term.'%')->paginate(100);
        }
        else
        {
            $results = PaymentType::paginate(100);
        }

        return $results;
    }

    public function getallproducts(Request $request){

        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = Product::where('is_group', 0)->where('name', 'LIKE', '%'.$search_term.'%')->paginate(100);
        }
        else
        {
            $results = Product::where('is_group', 0)->paginate(100);
        }

        return $results;
    }

    public function townshipByCity(Request $request)
    {
        $search_term = $request->input('q');
        $city_id = $request->city;

        if ($search_term) {
            if ($city_id) {
                $results = Township::where('name', 'LIKE', '%' . $search_term . '%')
                    ->where('city_id', $city_id)
                    ->paginate(100);
            } else {
                $results = Township::where('name', 'LIKE', '%' . $search_term . '%')
                    ->paginate(100);
            }
        } else {
            if ($city_id) {
                $results = Township::where('city_id', $city_id)->paginate(100);
            } else {
                $results = Township::paginate(100);
            }
        }

        return $results;
    }

    public function productByInvoice(Request $request)
    {
        $search_term = $request->input('q');
        $invoice_no = $request->purchase_order_id;

        $query = Product::query();

        if ($invoice_no) {
            $purchase_order = PurchaseOrder::find($invoice_no);
            $product_id_array = $purchase_order->purchaseOrderDetails->pluck('product_id')->toArray();
            $query->whereIn('id', $product_id_array);
        }

        if ($search_term) {
            $query->where('code', 'LIKE', '%' . $search_term . '%');
        }

        $results = $query->paginate(100);

        return $results;
    }

    public function skuCodeByInvoice(Request $request)
    {
        $search_term = $request->input('q');
        $purchase_order_id = $request->purchase_order_id;

        $query = SkuCode::query();
        $sku_code_id_array = array_unique(ProductBalance::where('purchase_order_id', $purchase_order_id)->pluck('sku_code_id')->toArray());

        if ($purchase_order_id) {
            $query->whereIn('id', $sku_code_id_array);
        }

        if ($search_term) {
            $query->where('code', 'LIKE', '%' . $search_term . '%');
        }

        $results = $query->paginate(100);

        return $results;
    }

    public function conversionUnit(Request $request)
    {
        $search_term = $request->input('q');
        $sku_code_id = $request->sku_code_id;
        $base_unit_id = $request->base_unit_id;

        $results = null;
        $query = Unit::query();

        if ($base_unit_id) {
            $base_unit_order = Unit::find($base_unit_id)->lft;
            $conversion_unit_id_array = UnitRelationship::where('base_unit_id', $base_unit_id)->where('sku_code_id', $sku_code_id)->pluck('conversion_unit_id')->toArray();
            $query->where('lft', '>', $base_unit_order);

            if ($sku_code_id) {
                $product_unit_id_array = Product::where('sku_code_id', $sku_code_id)->whereNotIn('unit_id', $conversion_unit_id_array)->pluck('unit_id')->toArray();
                $query->whereIn('id', $product_unit_id_array);
            } else {
                $query->whereNotIn('id', $conversion_unit_id_array);
            }

            if ($search_term) {
                $query->where('name', 'LIKE', '%' . $search_term . '%');
            }

            $results = $query->paginate(100);
        }

        return $results;
    }

    public function balanceFromUnit(Request $request)
    {
        $search_term = $request->input('q');
        $sku_code_id = $request->sku_code_id;
        $invoice_no = $request->purchase_order_id;

        if ($sku_code_id) {
            $product_id_array = ProductBalance::where([
                ['sku_code_id', $sku_code_id],
                ['purchase_order_id', $invoice_no],
            ])->pluck('product_id')->toArray();

            $product_unit_id_array = Product::whereIn('id', $product_id_array)
                ->pluck('unit_id')
                ->toArray();

            $query = Unit::query();
            $query->whereIn('id', $product_unit_id_array);

            if ($search_term) {
                $query->where('name', 'LIKE', '%' . $search_term . '%');
            }

            $results = $query->paginate(100);
        } else {
            $results = null;
        }

        return $results;
    }

    public function balanceToUnit(Request $request)
    {
        $search_term = $request->input('q');
        $sku_code_id = $request->sku_code_id;
        $from_unit = $request->from_unit_id;

        if ($sku_code_id && $from_unit) {
            $product_unit_id_array = Product::where('sku_code_id', $sku_code_id)->pluck('unit_id')->toArray();

            $product_unit_id_array = array_diff($product_unit_id_array, [$from_unit]);

            $query = Unit::query();
            $query->whereIn('id', $product_unit_id_array);

            if ($search_term) {
                $query->where('name', 'LIKE', '%' . $search_term . '%');
            }

            $results = $query->paginate(100);
        } else {
            $results = null;
        }

        return $results;
    }

    public function productBySkuCode(Request $request)
    {
        $search_term = $request->input('q');
        $sku_code_id = $request->sku_code_id;

        if ($sku_code_id) {
            $results = Product::where(function ($query) use ($sku_code_id) {
                $query->where('sku_code_id', $sku_code_id);
            })->paginate(100);
        } else {
            $results = Product::paginate(100);
        }

        return $results;
    }

    public function unitBySkuCode(Request $request)
    {
        $search_term = $request->input('q');
        $sku_code_id = $request->sku_code_id;

        $query = Unit::query();

        if ($sku_code_id) {
            $units_id = Product::where('sku_code_id', $sku_code_id)->pluck('unit_id')->toArray();
            $query->whereIn('id', $units_id);
        }

        if ($search_term) {
            $query->where('name', 'LIKE', '%' . $search_term . '%');
        }

        $results = $query->paginate(100);

        return $results;
    }

    public function financeParticular(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = FinanceParticular::where('name', 'LIKE', '%'.$search_term.'%')->paginate(100);
        }
        else
        {
            $results = FinanceParticular::paginate(100);
        }

        return $results;
    }
}

