<?php

namespace App\Http\Controllers\Admin;

use App\FinanceCashAccountsBalance;
use Carbon\Carbon;
use App\FinanceParticular;
use App\FinanceTransaction;
use Illuminate\Http\Request;
use App\Models\FinanceCashAccount;
use App\Http\Controllers\Controller;

class FinanceCashReportController extends Controller
{
    protected function index()
    {
        $transaction_types = FinanceParticular::all();
        $cash_accounts = FinanceCashAccount::all();
        $finance_particulars = FinanceParticular::where('credit_account_id',$cash_accounts[0]->id)->orWhere('debit_account_id',$cash_accounts[0]->id)->get();

        return view('partials.reports.cash-transaction.index', compact('transaction_types', 'cash_accounts','finance_particulars'));
    }

    public function selectTransactionType(Request $request)
    {
        $cash_account = $request->input('cash_account_id');

        $transaction_types = FinanceParticular::where('credit_account_id',$cash_account)->orWhere('debit_account_id',$cash_account)->get();

        return response()->json($transaction_types);
    }

    public function financeCashReport(Request $request)
    {

        $date_ranges = explode(' - ', $request->datetimes);

        $start_date = Carbon::parse($date_ranges[0])->format('Y-m-d');
        $end_date = Carbon::parse($date_ranges[1])->format('Y-m-d');

        $transactions = FinanceTransaction::where(function($query) use($request) {
            $query->where('credit_account_id', $request->cash_account_id)->orWhere('debit_account_id', $request->cash_account_id);
        });

        if ($request->has('transaction_type_id')) {
            $transactions->whereIn('finance_particular_id', $request->transaction_type_id);
        }

        $transactions = $transactions->where('created_at', '>=', $start_date)->where('created_at', '<', $end_date)->orderBy('created_at', 'asc')->get();

        $opening_balance = FinanceCashAccountsBalance::where('finance_cash_account_id', $request->cash_account_id)->whereDate('date', Carbon::parse($start_date)->subDay())->pluck('amount')->first();
        // $opening_balance = 10000;

        $current_cash_account = FinanceCashAccount::where('id',$request->cash_account_id)->first();



        return view('partials.reports.cash-transaction.show', [
            'transactions' => $transactions,
            'opening_balance' => $opening_balance,
            'current_cash_account' =>$current_cash_account,
        ]);

    }

}
