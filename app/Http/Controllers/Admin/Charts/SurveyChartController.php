<?php

namespace App\Http\Controllers\Admin\Charts;

use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\DB;

/**
 * Class SurveyChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SurveyChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        $labels = [];
        $statuses = ['Good','Bad','No Answer'];
        foreach ($statuses as $status) {
            $labels[] = $status;
        }
        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/survey'));

        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $statuses = ['Good','Bad'];
        $datas=[];
        foreach ($statuses as $status) {
            $datas[] = DB::table('notifications')->where('type',"Survey")->where('answer',$status)->get()->count();
        }
        $good = DB::table('notifications')->where('type',"Survey")->where('answer',"Good")->get()->count();
        $bad = DB::table('notifications')->where('type',"Survey")->where('answer',"Bad")->get()->count();
        $total = DB::table('notifications')->where('type',"Survey")->get()->count();
        
        $datas[]= $total - ($good+$bad);
        $this->chart->dataset('Survey', 'pie', $datas)
        ->color('rgba(255, 255, 255, 1)')
        ->backgroundColor([
            'rgb(77, 189, 116)',
            'rgb(209, 6, 57)',
            'rgb(96, 92, 168)',
        ]);
    }
}