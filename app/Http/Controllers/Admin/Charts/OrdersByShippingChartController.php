<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Constants\Status;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\DB;

/**
 * Class OrdersByShippingChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrdersByShippingChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
        $labels = [];
        $statuses = Status::shipping_status;
        foreach ($statuses as $status) {
            $labels[] = $status;
        }
        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/orders-by-shipping'));

        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $statuses = Status::shipping_status;
        $datas=[];
        foreach ($statuses as $status) {
            $datas[] = DB::table('orders')->where('shipping_status',$status)->get()->count();
        }

        $this->chart->dataset('Orders By Payment Status', 'pie', $datas)
        ->color('rgba(255, 255, 255, 1)')
        ->backgroundColor([
            'rgb(61, 107, 245)',
            'rgb(64, 184, 150)',
            'rgb(118, 65, 242)',
            'rgb(84, 235, 170)',
            'rgb(84, 235, 170)',
            'rgb(48, 150, 55)',
            'rgb(209, 6, 57)',
        ]);
    }
}