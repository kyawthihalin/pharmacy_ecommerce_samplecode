<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\City;
use App\Models\Order;
use App\Models\Township;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class WeeklyCityChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WeeklyCityChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $labels = [];
        for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
            if ($days_backwards == 1) {
            }
            $labels[] = $days_backwards.' days ago';
        }
        $this->chart->labels($labels);
        $this->chart->load(backpack_url('charts/weekly-city'));
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);

    }
    public function data()
    {
        for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
            $orders[] = Order::whereDate('created_at', today()
                ->subDays($days_backwards))
                ->count();
           
        }
        $this->chart->dataset('Order', 'line', $orders)
            ->color('rgb(77, 189, 116)')
            ->backgroundColor('rgba(77, 189, 116, 0.4)');

 
    }
}