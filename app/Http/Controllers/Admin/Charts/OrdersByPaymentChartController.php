<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Constants\GetStatus;
use App\Constants\Status;
use App\Models\Order;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\DB;

/**
 * Class OrdersByPaymentChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrdersByPaymentChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
        $labels = [];
        $statuses = Status::payment_status;
        foreach ($statuses as $status) {
            $labels[] = $status;
        }
        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);


        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/orders-by-payment'));

        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $statuses = Status::payment_status;
        $datas=[];
        foreach ($statuses as $status) {
            $datas[] = DB::table('orders')->where('payment_status',$status)->get()->count();
        }

        $this->chart->dataset('Orders By Payment Status', 'pie', $datas)
        ->color('rgba(255, 255, 255, 1)')
        ->backgroundColor([
            'rgb(70, 127, 208)',
            'rgb(77, 189, 116)',
            'rgb(209, 6, 57)',
            'rgb(74, 255, 228)',
        ]);
    }
}