<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_products' => 'required|array|min:2',
            'name' => 'required|min:5|max:255',
            // 'product_id' => 'required|unique:products',
            'categories' => 'required',
            'brand_id' => 'required',
            'unit_id' => 'required',
            // 'size' => 'required',
            'price' => 'required',
            'warning_count' => 'required',
            // 'feature_image' => 'required|image',
            'gallery' => 'required',
            // 'gallery.*' => 'mimes:jpeg,jpg,png,gif|max:2048',
            // 'discount' => 'required',
            'sku_code' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
