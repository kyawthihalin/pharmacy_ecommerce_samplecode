<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supplier_id' => 'required',
            'invoice_no' => 'required|string',
            'payment_method' => 'required|string',
            'date' => 'required',
            'purchaseOrderDetails.*.product_id' =>'required' ,
            'purchaseOrderDetails.*.quantity' => 'required|numeric|min:1',
            'purchaseOrderDetails.*.price' => 'required|numeric|min:1',
        ];
    }

    public function prepareForValidation()
    {
        $purchaseOrderDetails = $this->input('purchaseOrderDetails');

        $this->merge([
            'purchaseOrderDetails' => json_decode($purchaseOrderDetails, true)
        ]);
    }
}
