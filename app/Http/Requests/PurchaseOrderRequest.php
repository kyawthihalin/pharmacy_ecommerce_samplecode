<?php

namespace App\Http\Requests;

use App\Rules\ValidProductDataRule;
use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'supplier_id' => 'required',
            'invoice_no' => 'required|string|unique:purchase_orders,invoice_no',
            'date' => 'required',
            'products' => 'required',
            'products.*.product_id' => 'required_with:products' ,
            'products.*.quantity' => 'required_with:products|numeric|min:1',
            'products.*.price' => 'required_with:products|numeric|min:1',
        ];
    }

    public function prepareForValidation()
    {
        $products = $this->input('products');

        $this->merge([
            'products' => json_decode($products, true)
        ]);
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
