<?php

namespace App\Http\Requests;

use App\Constants\GetStatus;
use App\Models\AdjustmentType;
use App\Models\ProductBalance;
use Illuminate\Foundation\Http\FormRequest;

class StockAdjustmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'purchase_order_id' => 'nullable',
            'product_id' => 'required|exists:products,id|numeric',
            'adjustment_type_id' => 'required|exists:adjustment_types,id|numeric',
            'quantity' => 'required|numeric',
            'remark' => 'nullable|string',
        ];

        $product_balance = ProductBalance::where([
            ['product_id', $this->input('product_id')],
            ['purchase_order_id', $this->input('purchase_order_id')],
        ])->first();

        $adjustment_type = AdjustmentType::find($this->input('adjustment_type_id'));

        if ($adjustment_type) {
            if (!$product_balance && $adjustment_type->type == GetStatus::STOCK_OUT) {
                $rules['product_balance'] = 'required';
            }
        }

        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_balance.required' => 'Product quantity is not enough',
        ];
    }
}
