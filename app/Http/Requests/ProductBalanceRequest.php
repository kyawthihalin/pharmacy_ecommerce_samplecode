<?php

namespace App\Http\Requests;

use App\Models\Product;
use App\Models\ProductBalance;
use App\Models\UnitRelationship;
use Illuminate\Foundation\Http\FormRequest;

class ProductBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'purchase_order_id' => 'nullable|required_without:sku_code|exists:purchase_orders,id|uuid',
            'sku_code' => 'nullable|required_without:purchase_order_id|string|max:225',
            'from_unit_id' => 'required|exists:units,id|numeric',
            'to_unit_id' => 'required|exists:units,id|numeric',
        ];

        $from_unit_id = $this->input('from_unit_id');
        $to_unit_id = $this->input('to_unit_id');

        if (!$from_unit_id || !$to_unit_id) return $rules;

        $sku_code_id = $this->input('sku_code') ? $this->input('sku_code') : ProductBalance::where('purchase_order_id', $this->input('purchase_order_id'))->value('sku_code_id');
        $from_product = Product::where('sku_code_id', $sku_code_id)->where('unit_id', $from_unit_id)->first();
        $to_product = Product::where('sku_code_id', $sku_code_id)->where('unit_id', $to_unit_id)->first();

        $available_quantity = ProductBalance::where([
            ['product_id', $from_product->id],
            ['purchase_order_id', $this->input('purchase_order_id')]
        ])->value('quantity');

        $unit_relationship = UnitRelationship::where(function ($query) use ($from_unit_id, $to_unit_id, $sku_code_id) {
            $query->where('base_unit_id', $from_unit_id)
                ->where('conversion_unit_id', $to_unit_id)
                ->where('sku_code_id', $sku_code_id);
        })->orWhere(function ($query) use ($from_unit_id, $to_unit_id, $sku_code_id) {
            $query->where('conversion_unit_id', $from_unit_id)
                ->where('base_unit_id', $to_unit_id)
                ->where('sku_code_id', $sku_code_id);
        })->first();

        if (!$unit_relationship) {
            $unit_relationship = UnitRelationship::where(function ($query) use ($from_unit_id, $to_unit_id) {
                $query->where('base_unit_id', $from_unit_id)
                    ->where('conversion_unit_id', $to_unit_id)
                    ->whereNull('sku_code_id');
            })->orWhere(function ($query) use ($from_unit_id, $to_unit_id) {
                $query->where('conversion_unit_id', $from_unit_id)
                    ->where('base_unit_id', $to_unit_id)
                    ->whereNull('sku_code_id');
            })->first();
        }

        if ($from_unit_id == $unit_relationship->base_unit_id) {
            $min = 1;
            $max = $available_quantity;
            $rules['quantity'] = [
                'required', 'numeric', 'min:' . $min, 'max:' . $max,
                function ($attribute, $value, $fail) use ($available_quantity) {
                    if ($available_quantity < $value) {
                        $fail('Stocks are not enough.');
                    }
                }
            ];
        } else {
            $min = $unit_relationship->conversion_factor;
            $max = $available_quantity;
            $unit_quantity = $unit_relationship->conversion_factor;

            $rules['quantity'] = [
                'required', 'numeric', 'min:' . $min, 'max:' . $max,
                function ($attribute, $value, $fail) use ($unit_quantity, $available_quantity) {
                    $multiple = $value % $unit_quantity;

                    if ($multiple != 0) {
                        $fail('The ' . $attribute . ' must be valid quantity.');
                    }

                    if ($available_quantity < $value) {
                        $fail('Stocks are not enough.');
                    }
                }
            ];
        }

        $to_balance = ProductBalance::where([
            ['product_id', $to_product->id],
            ['purchase_order_id', $this->input('purchase_order_id')]
        ])->first();

        if (!$to_balance) {
            $rules['purchase_price'] = 'required|numeric';
        }

        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
