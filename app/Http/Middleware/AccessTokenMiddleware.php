<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\ApiResponse;
use App\Models\AccessToken;
use Illuminate\Support\Facades\Hash;

class AccessTokenMiddleware
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,string $action)
    {
        if (!$request->has('token') || !$request->has('phone_number')) {
            return $this->responseWithError("Unauthorized");
        }
        $token = AccessToken::whereIdentifier($request->phone_number)
            ->whereAction($action)->first();
        if (!$token) {
            return $this->responseWithError("Unauthorized");
        }
        if (!Hash::check($request->token, $token->token)) {
            return $this->responseWithError("Unauthorized");
        }
        if ($token->expired_at <= now()) {
            $token->delete();
            return $this->responseWithError("Unauthorized");
        }
        return $next($request);
    }
}
