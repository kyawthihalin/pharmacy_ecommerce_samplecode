<?php

namespace App\Imports;

use App\Models\Unit;
use App\Models\Brand;
use App\Models\Product;
use App\Models\SkuCode;
use App\Models\Category;
use App\Models\ProductBalance;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class StockOpeningImport implements ToModel, WithValidation, WithHeadingRow
{
    public function rules(): array
    {
        return [
            'product_id' => ['required', 'string', 'max:255'],
            'sku_code' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'unit' => ['required', 'string', 'max:255'],
            'selling_price' => ['required', 'integer'],
            'limit' => ['required', 'integer'],
            'warning_count' => ['required', 'integer'],
            'purchase_price' => ['required', 'integer'],
            'quantity' => ['required', 'integer'],
        ];
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        [$is_product, $product] = $this->checkProduct($row);

        if (!$is_product && !$product) {
            return;
        }

        if ($is_product) {
            $product_balance = ProductBalance::where('product_id', $product->id)->whereNull('purchase_order_id')->first();

            if ($product_balance) {
                $product_balance->update([
                    'price' => $row['purchase_price'],
                    'quantity' => $row['quantity'] + $product_balance->quantity,
                ]);

                return;
            }
        }

        return new ProductBalance([
            'product_id' => $product->id,
            'sku_code_id' => $product->sku_code_id,
            'price' => $row['purchase_price'],
            'quantity' => $row['quantity'],
        ]);
    }

    protected function checkProduct($row)
    {
        $product = Product::where('product_id', $row['product_id'])->first();

        if ($product) {
            return [true, $product];
        }

        $sku_code_id = SkuCode::where('code', $row['sku_code'])->value('id');
        $unit_names = Product::where('sku_code_id', $sku_code_id)
            ->join('units', 'products.unit_id', '=', 'units.id')
            ->pluck('units.name')
            ->toArray();

        if (in_array($row['unit'], $unit_names)) {
            return [false, null];
        }

        $sku_code_id = $this->checkSkuCode($row);
        $unit_id = $this->checkUnit($row);

        $brand = Brand::where('name', 'Default')->first();
        if (!$brand) {
            $brand = Brand::first();
        }

        $data = [
            'product_id' => $row['product_id'],
            'sku_code_id' => $sku_code_id,
            'unit_id' => $unit_id,
            'brand_id' => $brand->id,
            'name' => $row['name'],
            'price' => $row['selling_price'],
            'limit' => $row['limit'],
            'warning_count' => $row['warning_count'],
            'feature_image' => 'products/feature_image/UOmT6q5xUUAhP4g7vcIzr17epLddEdqFPhKuLItI.webp',
            'gallery' => ['products/gallery/c1nXiJGnDlbi5z6CPv3fsCLDfm1b1YpMtrjXlwQg.webp'],
        ];

        $product = Product::create($data);

        $category = Category::where('name', 'Default')->first();
        if (!$category) {
            $category = Category::first();
        }

        $product->categories()->sync([$category->id]);

        return [false, $product];
    }

    protected function checkSkuCode($row)
    {
        $sku_code = SkuCode::where('code', $row['sku_code'])->first();

        if (!$sku_code) {
            $sku_code = SkuCode::create([
                'code' => $row['sku_code'],
                'discount' => 0,
                'discount_type' => 'No Discount',
            ]);
        }

        return $sku_code->id;
    }

    protected function checkUnit($row)
    {
        $unit = Unit::where('name', $row['unit'])->first();

        if (!$unit) {
            $unit = Unit::create([
                'name' => $row['unit'],
            ]);
        }

        return $unit->id;
    }
}
