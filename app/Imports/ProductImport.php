<?php

namespace App\Imports;

use App\Models\Product;
use App\Models\PurchaseOrderDetail;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Validation\ValidationException; // Import the ValidationException

class ProductImport implements  ToModel, WithHeadingRow
{
    public $importedData = []; // Initialize the property
    public $purchaseOrderId;
    public $totalPrice = 0;

    public function __construct($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;
    }

    public function model(array $row)
    {
        if (!isset($row['quantity']) || empty($row['quantity'])) {
            throw ValidationException::withMessages(['product_id' => 'Quantity is missing or empty in Excel File.']);
        }

        if (!isset($row['price']) || empty($row['price'])) {
            throw ValidationException::withMessages(['product_id' => 'Price is missing or empty in Excel File.']);
        }

        if (!isset($row['product_id']) || empty($row['product_id'])) {
            throw ValidationException::withMessages(['product_id' => 'Product Id is missing or empty in Excel File.']);
        }

        if (!$row['quantity']) {
            throw ValidationException::withMessages(['product_id' => 'Quantity not found for product_id: ' . $row['product_id'].' in Excel file']);
        }
        else if(!is_int($row['quantity'])){
            throw ValidationException::withMessages(['product_id' => 'Quantity is not integer in  product_id: ' . $row['product_id'].' in Excel file']);
        }

        if (!$row['price']) {
            throw ValidationException::withMessages(['product_id' => 'Price not found for product_id: ' . $row['product_id'].'']);
        }
        else if(!is_int($row['price'])){
            throw ValidationException::withMessages(['product_id' => 'Price is not integer in  product_id: ' . $row['product_id'].' in Excel file']);
        }

        $productData = [
            'purchase_order_id' => $this->purchaseOrderId,
            'product_id' => $this->getProductId($row),
            'quantity' => $row['quantity'],
            'price' => $row['price'],
        ];

        $this->getTotalPrice($productData); // Update the total price

        $this->importedData[] = $productData; // Store the data

        return new PurchaseOrderDetail($productData);
    }

    protected function getProductId($row)
    {
        $product = Product::where('product_id', $row['product_id'])->first();

        if (!$product) {
            throw ValidationException::withMessages(['product_id' => 'Product not found for product_id: ' . $row['product_id']]);
        }
        return $product->id;
    }

    protected function getTotalPrice($productData)
    {
        $this->totalPrice += ($productData['quantity'] * $productData['price']);
    }
}
