<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ProfitDailyLog extends Model
{
    use CrudTrait,Uuid;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'profit_daily_log';
    protected $primaryKey = 'id';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
}
