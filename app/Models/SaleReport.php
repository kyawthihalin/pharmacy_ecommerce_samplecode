<?php

namespace App\Models;

use App\Traits\UniqueUuid;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class SaleReport extends Model
{
    use CrudTrait,UniqueUuid;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'orders';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
    public function shipping_address()
    {
        return $this->belongsTo(ShippingAddress::class);
    }
    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class);
    }
    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }
    public function details()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'rejected_by');
    }
}
