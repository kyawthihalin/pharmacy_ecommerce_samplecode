<?php

namespace App\Models;

use App\User;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class CompletePurchaseOrder extends Model
{
    use CrudTrait,Uuid;

    protected $table = 'purchase_orders';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function transactions()
    {
        return $this->morphMany(FinanceTransaction::class, 'transactionable');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'purchase_order_details')
            ->withPivot('name', 'price', 'quantity');
    }


    public function purchaseOrderDetails()
    {
        return $this->hasMany(PurchaseOrderDetail::class,'purchase_order_id');
    }
}
