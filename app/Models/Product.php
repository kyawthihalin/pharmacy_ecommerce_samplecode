<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use PDO;

class Product extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = [
        'product_id',
        'category_id',
        'name',
        'brand_id',
        'price',
        'description',
        'limit',
        'warning_count',
        'feature_image',
        'gallery',
        'unit_id',
        'status',
        'sku_code_id',
        'size',
        'is_selling',
        'is_group',
        'is_popular',
    ];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'gallery' => 'array',
        'category_id' => 'array'
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function deleteCategories($row)
    {
        $product_categories = ProductCategory::where('product_id', $row->id)->get();
        if ($product_categories) {
            foreach ($product_categories as $product_category) {
                $product_category->delete();
            }
        }
    }

    public static function saveCategories($request, $row, $status)
    {
        if (isset($request->categories)) {
            $categories = $request->categories;
            foreach ($categories as $category) {
                $create_object_product_category = new ProductCategory();
                $create_object_product_category->product_id  = $row->id;
                $create_object_product_category->category_id = $category;
                if ($status == "created") {
                    $create_object_product_category->created_at = Carbon::now();
                }
                $create_object_product_category->updated_at = Carbon::now();
                $create_object_product_category->save();
            }
        }
    }

    public static function boot()
    {
        parent::boot();
        static::created(function ($row) {
            //get form data from request
            $request = request();
            self::saveCategories($request, $row, "created");
            //creating product sizes
        });

        static::updating(function ($row) {
            //get form data from request
            $request = request();
            if ($request->has("categories")) {
                self::deleteCategories($row);
            }
            //creating product varies
            self::saveCategories($request, $row, "updating");
        });

        static::deleting(function ($obj) {
            if (count((array)$obj->photos)) {
                foreach ($obj->photos as $file_path) {
                    \Storage::disk('s3')->delete($file_path);
                }
            } else {
                \Storage::disk('s3')->delete($obj->image);
            }
        });
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function brand()
    {
        return $this->belongsTo(Brand::class, "brand_id");
    }
    public function unit()
    {
        return $this->belongsTo(Unit::class, "unit_id");
    }
    public function categories()
    {
        return  $this->belongsToMany(Category::class, "product_categories")->withTimestamps();
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, "product_categories");
    }

    public function purchaseOrders()
    {
        return $this->belongsToMany(PurchaseOrder::class, 'purchase_order_details')
            ->withPivot('name', 'price', 'quantity');
    }

    public function productBalance()
    {
        return $this->hasOne(ProductBalance::class);
    }

    public function skuCode()
    {
        return $this->belongsTo(SkuCode::class, "sku_code_id");
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    // public function setFeatureImageAttribute($value)
    // {
    //     $attribute_name = "feature_image";
    //     $disk = "s3";

    //     $destination_path = "products/feature_image";
    //     $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    // }

    // public function setGalleryAttribute($value)
    // {
    //     $attribute_name = "gallery";
    //     $disk = "s3";

    //     $destination_path = "products/gallery";
    //     $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    // }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
