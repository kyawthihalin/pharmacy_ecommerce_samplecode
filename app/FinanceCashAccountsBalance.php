<?php

namespace App;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class FinanceCashAccountsBalance extends Model
{
    use CrudTrait,Uuid;

    protected $table = 'finance_cash_accounts_balances';

    protected $guarded = ['id'];


}
