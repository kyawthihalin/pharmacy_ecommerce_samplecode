<?php
/**
 *
 * get random digits
 */

use App\Models\Order;
use App\Models\Product;
use App\Models\ProductBalance;
use App\Models\Unit;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

if (!function_exists('get_random_digit')) {
    function get_random_digit(int $length): string
    {
        $i = 0;
        $pin = "";

        while ($i < $length) {
            $pin .= mt_rand(0, 9);
            $i++;
        }

        return $pin;
    }
}

if (!function_exists('getHigherStatus')) {
    function getHigherStatus(string $status,array $status_array): array
    {
        $current_index = array_search($status, $status_array);
        return array_slice($status_array,$current_index+1,count($status_array));
    }
}

if (!function_exists('getLowerStatus')) {
    function getLowerStatus(string $status,array $status_array): array
    {
        $current_index = array_search($status, $status_array);
        return array_slice($status_array,0,$current_index);
    }
}

if (!function_exists('getImageTempUrl')) {
    function getImageTempUrl($destination_path, $image)
    {
        return Storage::temporaryUrl(
            $destination_path . '/' . $image,
            now()->addMinutes(3)
        );
    }
}

if (!function_exists('getWarningProduct')) {
    function getWarningProduct()
    {
        $count = 0;

        $products = Product::get();
        foreach ($products as $product) {
            $quantity = ProductBalance::where('product_id', $product->id)->sum('quantity');
            if ($quantity <= $product->warning_count) {
                $count++;
            }
        }

        return $count;
    }
}

if (!function_exists('getTodayOrder')) {
    function getTodayOrder()
    {
        $currentDate = Carbon::now()->format('Y-m-d');
        $today_order = Order::whereDate('created_at',$currentDate)->get()->count();
        return $today_order;
    }
}

if (!function_exists('getCompletedOrder')) {
    function getCompletedOrder()
    {
        return Order::where('shipping_status',"Completed")->get()->count();
    }
}

if (!function_exists('getPaymentPendingOrder')) {
    function getPaymentPendingOrder()
    {
        return Order::where('payment_status',"Pending")->get()->count();
    }
}

if (!function_exists('getRejectedOrder')) {
    function getRejectedOrder()
    {
        return Order::where('payment_status',"Rejected")->get()->count();
    }
}

if (!function_exists('getDeliveringOrder')) {
    function getDeliveringOrder()
    {
        return Order::where('payment_status',"Completed")->where('shipping_status','!=','Completed')->get()->count();
    }
}

if (!function_exists('getMaxLft')) {
    function getMaxLft($model)
    {
        return $model->max('lft');
    }
}

if (!function_exists('getMaxRgt')) {
    function getMaxRgt($model)
    {
        return $model->max('rgt');
    }
}
