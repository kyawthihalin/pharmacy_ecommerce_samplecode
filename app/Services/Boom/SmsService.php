<?php

namespace App\Services\Auth;

use App\Models\Shop;
use Illuminate\Support\Facades\Hash;
use App\Models\Otp;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\OtpExpiredException;
use App\Exceptions\OtpInvalidException;
use Illuminate\Support\Facades\Http;
use App\Constants\OtpAction;

class SmsService
{
    private $phone_number;
    private $device_id;

    public function __construct(string $phone_number, string $device_id)
    {
        $this->phone_number = $phone_number;
        $this->device_id = $device_id;
    }
    /**
     * @param string $life_time
     * @return mixed
     */
    public function generate(string $life_time, $action): int
    {
        Otp::where('identifier', $this->phone_number)->delete();
        //generate token
        $token = get_random_digit(6);

        //create otp for a given expired time
        Otp::create([
            'identifier' => $this->phone_number,
            'device_id' => $this->device_id,
            'token' => Hash::make($token),
            'expired_at' => now()->addMinutes($life_time)
        ]);

        return $token;
    }

    public function send($action, int $life_time)
    {
        $otp = $this->generate($action,$life_time);
        $phone_number = str_replace('09', '959', $this->phone_number);
        $response = Http::withHeaders([
            'Accept'        => 'application/json',
            'Authorization' => 'Bearer ' . config('sms.token')
        ])->withOptions([
            "verify" => config('app.env') === 'production' ?? false
        ])->post(config('sms.end_point'), [
            'from' => 'BPSMS MM',
            'text' => $otp . "is your OTP Code",
            'to'   => $phone_number
        ]);
        if ($response['status'] != '0') {
            throw new AuthenticationException();
        }
    }

    public function verify(string $otp): void
    {
        $requested_otp = $this->checkRequested();
        $this->checkOtpValid($requested_otp, $otp);
        $this->checkOtpExpire($requested_otp);
        $this->otpIsValid($requested_otp);
    }
    // check user requested an otp
    private function checkRequested()
    {
        $requested = Otp::where('identifier', $this->phone_number)->where('device_id', $this->device_id)->first();
        if (!$requested) {
            throw new AuthenticationException();
        }

        return $requested;
    }

    private function checkOtpValid(Otp $requested_otp, string $confirm_otp)
    {
        if (!Hash::check($confirm_otp, $requested_otp->token)) {
            throw new OtpInvalidException();
        }
    }

    private function checkOtpExpire(Otp $otp)
    {
        if ($otp->expired_at <= now()) {
            throw new OtpExpiredException();
        }
    }

    public function otpIsValid(Otp $otp)
    {
        $otp->delete();
    }
}
