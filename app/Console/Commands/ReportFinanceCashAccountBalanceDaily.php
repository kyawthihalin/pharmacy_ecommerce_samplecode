<?php

namespace App\Console\Commands;

use App\FinanceCashAccountsBalance;
use App\Models\FinanceCashAccount;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ReportFinanceCashAccountBalanceDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report_finance_cash_account_balance_daily:cron';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $finance_cash_accounts = FinanceCashAccount::all();

        foreach($finance_cash_accounts as $finance_cash_account)
        {
            FinanceCashAccountsBalance::create([
                "finance_cash_account_id" => $finance_cash_account->id,
                "amount" => $finance_cash_account->amount,
                "date" => Carbon::now(),
            ]);
        }
    }
}
