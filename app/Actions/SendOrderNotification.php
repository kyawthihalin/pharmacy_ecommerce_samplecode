<?php

namespace App\Actions;

class SendOrderNotification
{
    public function execute(array $tokens = [], $data = [])
    {
        $curl = curl_init();
        $tokens = json_encode($tokens);
        $data = json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
        "registration_ids":' . $tokens . ',
        "data":' . $data . ',
        }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . config("serverkey.key"),
                'Content-Type: application/json'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        // echo $response;
    }
}
