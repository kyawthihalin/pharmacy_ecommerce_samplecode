<?php

namespace App\Actions;

class GenerateProductId
{
    public function execute(): string
    {
        return "PO-"
            . rand(1000, 99999999);
    }
}
