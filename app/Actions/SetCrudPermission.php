<?php

namespace App\Actions;

class SetCrudPermission
{
    public function execute(string $permission_name, $crud)
    {
        $crud->denyAccess(['list', 'update', 'create', 'delete','show']);

        foreach (['list', 'update', 'create', 'delete'] as $operation) {
            if (backpack_user()->can($operation . ' ' . $permission_name)) {
                $crud->allowAccess($operation);
            }
        }
    }
}
