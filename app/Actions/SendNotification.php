<?php

namespace App\Actions;

class SendNotification
{
    public function execute(string $topic = '', $data = [])
    {
        $curl = curl_init();
        $topic  = json_encode($topic);
        $data = json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
        "to":' . $topic . ',
        "data":' . $data . ',
        }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: key=' . config("serverkey.key"),
                'Content-Type: application/json'
            ),
        ));
        curl_exec($curl);
        curl_close($curl);
    }
}
