<?php

namespace App\Actions;

use App\Models\City;

class GetCity
{
    public function execute(): array
    {
        $cities = City::all();
        $cities_arr = [];
        foreach ($cities as $city) {
            $cities_arr[$city->id] = $city->name;
        }
        return $cities_arr;
    }
}
