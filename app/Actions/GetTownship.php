<?php

namespace App\Actions;

use App\Models\Township;

class GetTownship
{
    public function execute(): array
    {
        $townships = Township::all();
        $townships_arr = [];
        foreach ($townships as $township) {
            $townships_arr[$township->id] = $township->name;
        }
        return $townships_arr;
    }
}
