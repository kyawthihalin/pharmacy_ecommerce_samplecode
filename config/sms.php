<?php

return [
    'end_point' => 'https://boomsms.net/api/sms/json',
    'token' => env('BOOM_SMS_TOKEN'),
];
