<?php

use App\Constants\Status;
use App\Constants\GetStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('supplier_id');
            $table->unsignedBigInteger('user_id');
            $table->string('order_no');
            $table->string('reference_id');
            $table->string('invoice_no')->nullable();
            $table->unsignedDecimal('total_amount', 15, 4)->default(0);
            $table->date('date');
            $table->enum('order_status', Status::order_status)->default(GetStatus::ORDER_STATUS['Pending']);
            $table->enum('payment_status', Status::payment_status)->default(GetStatus::PAYMENT_STATUS['Pending']);
            $table->text('remark')->nullable();
            $table->timestamps();

            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
