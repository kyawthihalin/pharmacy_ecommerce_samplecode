<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFillAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fill_amounts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('from_account_id')->nullable();
            $table->uuid('to_account_id')->nullable();
            $table->unsignedDecimal('amount', 15, 4)->default(0);
            $table->foreign('from_account_id')->references('id')->on('finance_cash_accounts')->onDelete('cascade');
            $table->foreign('to_account_id')->references('id')->on('finance_cash_accounts')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fill_amounts');
    }
}
