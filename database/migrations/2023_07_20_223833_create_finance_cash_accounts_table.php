<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceCashAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_cash_accounts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name')->index();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->string('account_code')->index();
            $table->unsignedDecimal('amount', 15, 4)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_cash_accounts');
    }
}
