<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_transactions', function (Blueprint $table) {
            $table->id();
            // $table->bigIncrements('sequence')->unique();
            $table->uuid('finance_particular_id');
            $table->uuid('transactionable_id');
            $table->string('transactionable_type');
            $table->uuid('debit_account_id')->nullable();
            $table->uuid('credit_account_id')->nullable();
            $table->unsignedDecimal('amount', 15, 4)->default(0);
            $table->unsignedDecimal('debit_account_before_amount', 15, 4)->nullable();
            $table->unsignedDecimal('debit_account_after_amount', 15, 4)->nullable();
            $table->unsignedDecimal('credit_account_before_amount', 15, 4)->nullable();
            $table->unsignedDecimal('credit_account_after_amount', 15, 4)->nullable();
            $table->timestamps();

            $table->index(['transactionable_type', 'transactionable_id'], 'transactionable_index');
            $table->foreign('finance_particular_id')->references('id')->on('finance_particulars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_transactions');
    }
}
