<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string("product_id");
            $table->string("name");
            $table->unsignedBigInteger("brand_id");
            $table->double("price");
            // $table->double("discount")->default(0);
            // $table->enum("discount_type",["Percent","Fixed","No Discount"]);
            // $table->dateTime("discount_start_date")->nullable();
            // $table->dateTime("discount_end_date")->nullable();

            $table->int("size")->default(1);
            $table->boolean("is_selling")->default(0);
            $table->boolean("is_group")->default(0);
            $table->boolean("is_popular")->default(0);
            $table->text("description")->nullable();
            $table->double("limit")->nullable();
            // $table->double("quantity")->nullable();
            $table->double("warning_count")->nullable();
            $table->text("feature_image")->nullable();
            $table->longText("gallery")->nullable();
            $table->uuid("sku_code_id");
            $table->timestamps();

            $table->foreign('sku_code_id')->references('id')->on('sku_codes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
