<?php

use App\Constants\Status;
use App\Constants\GetStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('type', Status::stock_type)->default(GetStatus::STOCK_TYPE['Sale']);
            $table->unsignedBigInteger('product_id');
            $table->integer('quantity')->default(0);
            $table->integer('current_qty')->default(0);
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_logs');
    }
}
