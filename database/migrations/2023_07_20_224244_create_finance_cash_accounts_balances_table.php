<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceCashAccountsBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_cash_accounts_balances', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('finance_cash_account_id');
            $table->unsignedDecimal('amount', 15, 4)->default(0);
            $table->date('date');
            $table->timestamps();
            $table->foreign('finance_cash_account_id')->references('id')->on('finance_cash_accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_cash_accounts_balances');
    }
}
