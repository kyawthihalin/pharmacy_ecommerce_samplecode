<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string("name");
            $table->text("shop_logo");
            $table->text("address");
            $table->string("phone");
            $table->string("password");
            $table->text("description")->nullable();
            $table->text("freeze_reason")->nullable();
            $table->tinyInteger("freeze_status")->default(0);
            $table->dateTime("frozen_at")->nullable();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->string('token')->nullable();
            $table->boolean('is_approve')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
