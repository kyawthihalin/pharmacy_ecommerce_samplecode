<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->string("order_no")->nullable();
            $table->unsignedBigInteger("shipping_address_id")->nullable();
            $table->unsignedBigInteger("shop_id")->nullable();
            $table->unsignedBigInteger("payment_type_id")->nullable();
            $table->double("total_quantity")->default(0);
            $table->double("total_amount")->default(0);
            $table->double("discount_amount")->default(0);
            $table->unsignedBigInteger("coupon_id")->nullable();
            $table->double("coupon_discount")->default(0);
            $table->double("grand_total")->default(0);
            $table->double("sub_total")->default(0);
            $table->double("net_amount")->default(0);
            $table->double("shipping_fees")->default(0);
            $table->text("remark")->nullable();
            $table->text("payment_screenshot")->nullable();
            $table->string('reject_remark')->nullable();
            $table->unsignedBigInteger('rejected_by')->nullable();
            $table->enum("shipping_status", ["Pending", "Processing", "Confirmed", "Delivering", "Going to Bus Gate", "Completed", "Rejected"]);
            $table->enum("payment_status", ["Pending", "Completed", "Rejected"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
