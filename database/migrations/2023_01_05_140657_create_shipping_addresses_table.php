<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_addresses', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->unsignedBigInteger("shop_id");
            $table->string("name")->nullable();
            $table->string("phone_number")->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->unsignedBigInteger('township_id')->nullable();
            $table->text("ship_address")->nullable();
            $table->tinyInteger("default_shipping_address")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_addresses');
    }
}
