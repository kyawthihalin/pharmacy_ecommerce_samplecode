<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->enum("coupon_type",["Percent","Fixed"]);
            $table->double("amount")->default(0);
            $table->string("coupon_number");
            $table->tinyInteger("valid_times")->default(0);
            $table->dateTime('coupon_start_date')->nullable();
            $table->dateTime('coupon_end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
