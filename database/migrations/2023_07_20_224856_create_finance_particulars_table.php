<?php

use App\Constants\Status;
use App\Constants\GetStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Constants\FinanceAccountOperation;
use Illuminate\Database\Migrations\Migration;

class CreateFinanceParticularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance_particulars', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name')->index();
            $table->string('reference_id')->nullable()->index();
            $table->uuid('credit_account_id')->nullable();
            $table->string('credit_account_operation')->nullable();
            $table->uuid('debit_account_id')->nullable();
            $table->string('debit_account_operation')->nullable();
            $table->timestamps();
            $table->foreign('credit_account_id')->references('id')->on('finance_cash_accounts')->onDelete('cascade');
            $table->foreign('debit_account_id')->references('id')->on('finance_cash_accounts')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance_particulars');
    }
}
