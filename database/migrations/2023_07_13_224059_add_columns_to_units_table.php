<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units', function (Blueprint $table) {
            // $table->unsignedBigInteger('sequence')->unique()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('lft')->unsigned()->nullable();
            $table->integer('rgt')->unsigned()->nullable();
            $table->integer('depth')->unsigned()->nullable();
        });

        // $units = DB::table('units')->get();

        // foreach ($units as $index => $unit) {
        //     DB::table('units')
        //         ->where('id', $unit->id)
        //         ->update(['sequence' => $index + 1]);
    // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units', function (Blueprint $table) {
            // if (Schema::hasColumn('units', 'sequence')) {
                // $table->dropColumn('sequence');
                // $table->dropColumn('parent_id');
                // $table->dropColumn('lft');
                // $table->dropColumn('rgt');
                // $table->dropColumn('depth');

                Schema::table('units', function (Blueprint $table) {
                    if (Schema::hasColumn('units', 'parent_id')) {
                        $table->dropColumn('parent_id');
                    }
                    if (Schema::hasColumn('units', 'lft')) {
                        $table->dropColumn('lft');
                    }
                    if (Schema::hasColumn('units', 'rgt')) {
                        $table->dropColumn('rgt');
                    }
                    if (Schema::hasColumn('units', 'depth')) {
                        $table->dropColumn('depth');
                    }
                });
            // }
        });
    }
}
