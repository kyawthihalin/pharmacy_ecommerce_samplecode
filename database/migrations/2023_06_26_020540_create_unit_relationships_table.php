<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_relationships', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('sku_code_id')->nullable()->index();
            $table->unsignedBigInteger('base_unit_id');
            $table->unsignedBigInteger('conversion_unit_id');
            $table->double('conversion_factor');
            $table->timestamps();

            $table->foreign('base_unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->foreign('conversion_unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->foreign('sku_code_id')->references('id')->on('sku_codes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_relationships');
    }
}
