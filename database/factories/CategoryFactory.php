<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Category;
use Illuminate\Support\Str;


$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'photo' =>  $faker->name,
        'uuid' => Str::uuid(), // password
    ];
});
