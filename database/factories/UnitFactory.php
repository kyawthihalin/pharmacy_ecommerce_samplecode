<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Unit;
use Faker\Generator as Faker;

$factory->define(Unit::class, function (Faker $faker) {
    $units = ["Pills","Boxes","Cards","Pcs","Tablet"];
    return [
        'name' => $units[array_rand($units,1)],
    ];
});
