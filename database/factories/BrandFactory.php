<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Brand;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'photo' => $faker->name,
        'feature_status' => 1,
        'uuid' => Str::uuid(), // password
    ];
});
