<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Actions\GenerateProductId;
use App\Model;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Unit;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $product_id = (new GenerateProductId())->execute();
    return [
        'product_id' => $product_id,
        'name' => $faker->name,
        'brand_id' => Brand::all()->random()->id,
        'price' => rand(3000,30000),
        'size' => rand(3,200),
        'limit' => rand(3,100),
        'quantity' => rand(3,100),
        'warning_count' => rand(3,100),
        'feature_image' =>  $faker->text(10),
        'gallery' => $faker->text(10),
        'description' => $faker->text(200),
        'discount_type' => "No Discount",
        "status" => 1,
        "unit_id" => Unit::all()->random()->id,
    ];
});
