<?php

use App\Models\Unit;
use App\Models\UnitRelationship;
use Illuminate\Database\Seeder;

class UnitRelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unit_relationships = [];

        $units = Unit::all();
        for ($i = 0; $i < count($units); $i++) {
            $base_unit = $units[$i];
            foreach ($units as $unit) {
                if ($base_unit->lft < $unit->lft) {
                    array_push($unit_relationships, [
                        'base_unit_id' => $base_unit->id,
                        'conversion_unit_id' => $unit->id,
                        'conversion_factor' => 12,
                    ]);
                }
            }
        }

        foreach ($unit_relationships as $unit_relationship) {
            ['base_unit_id' => $base_unit_id, 'conversion_unit_id' => $conversion_unit_id, 'conversion_factor' => $conversion_factor] = $unit_relationship;

            $existed = UnitRelationship::where('base_unit_id', $base_unit_id)->where('conversion_unit_id', $conversion_unit_id)->first();

            if (!$existed) {
                UnitRelationship::create([
                    'base_unit_id' => $base_unit_id,
                    'conversion_unit_id' => $conversion_unit_id,
                    'conversion_factor' => $conversion_factor,
                ]);
            }
        }
    }
}
