<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(env("APP_ENV") == "production")
        {
            User::create([
                'name' => "Naung Ye Htet",
                'email' => 'pharmacy@moeheingabar.com',
                'password' => bcrypt('pmc@mhgb@123'),
            ]);
        }else{
            User::create([
                'name' => "Naung Ye Htet",
                'email' => 'naungyehtet717@gmail.com',
                'password' => bcrypt('password'),
            ]);
            User::create([
                'name' => "Kyawthihalin",
                'email' => 'Kyawthihalin@gmail.com',
                'password' => bcrypt('password'),
            ]);
            User::create([
                'name' => "Ko Aung Khant",
                'email' => 'aungkhant@gmail.com',
                'password' => bcrypt('password'),
            ]);
        }


        $admin = User::where('name', 'Naung Ye Htet')->first();

        $admin->syncRoles(['Super Admin']);
    }
}
