<?php
namespace Database\Seeders;

use App\Models\PermissionGroup;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grouped_permissions = [
            'General' => [
                'dashboard',
                'list city',
                'create city',

                'list township',
                'create township',
                'update township',

                'list payment type',
                'create payment type',
                'update payment type',

                'list notification',
                'create notification',

                'list banner',
                'create banner',
                'update banner',
                'delete banner',

                'list logo',
                'create logo',
                'update logo',
                'delete logo',

                'list coupon',
                'create coupon',
                'update coupon',

                'list enquiry',

                'list return medicine',
                'update return medicine',

                'list fill amount',
                'create fill amount',
                'show fill amount',

                'list gl account',

                'list finance cash account',
                'create finance cash account',

                'list finance particular',
                'create finance particular',

                'list finance cash report',

                'list tax',
                'update tax',
            ],
            'Inventory Management' => [
                // 'list stock',

                // 'list stock in',
                // 'create stock in',

                // 'list stock out',
                // 'create stock out',

                'list product balance',
                'create product balance',

                'list stock adjustment',
                'create stock adjustment',
                'update stock adjustment',
                'delete stock adjustment',

                'create stock opening',
            ],
            'Product Management' => [
                'list category',
                'create category',
                'update category',

                'list brand',
                'create brand',
                'update brand',

                'list unit',
                'create unit',
                'update unit',

                'list unit relationship',
                'create unit relationship',
                'update unit relationship',
                'delete unit relationship',

                'list product',
                'create product',
                'update product',
            ],
            'Order Management' => [
                'list order',
                'change payment status',
                'change shipping status',
                'edit order',
                'update order',

                'create print voucher',

                'list purchase order',
                'show purchase order',
                'create purchase order',
                'update purchase order',
            ],
            'Report Section' => [
                'create product report',
                'create order report',
                'list profit report',
                'create profit report',
                'list stock report',
                'create stock report',
                'list product balance report',
                'create product balance report',
                'list purchase order report',
                'create purchase order report',
                'list sale report',
                'create sale report',
            ],
            'User Management' => [
                'list user',
                'create user',
                'update user',
                'delete user',

                'list role',
                'create role',
                'update role',
                'delete role',

                'list supplier',
                'create supplier',
                'update supplier',
                'delete supplier',
            ],
            'Shop Management' => [
                'list shop',
                'freeze shop',
                'unfreeze shop',
            ],
        ];

        $admin_permissions = [];
        foreach ($grouped_permissions as $group => $permissions) {
            $group = PermissionGroup::where('name', $group)->first();
            array_merge($admin_permissions, $permissions);

            foreach ($permissions as $permission) {
                $existed = Permission::whereName($permission)->first();

                if (!$existed) {
                    $group->permissions()->create([
                        'name' => $permission
                    ]);
                }
            }
        }

        if (count($admin_permissions)) {
            Permission::whereNotIn($admin_permissions)->delete();
        }
    }
}
