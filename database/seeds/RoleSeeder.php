<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_roles = [
            'Super Admin',
            'Admin',
        ];

        foreach ($admin_roles as $admin_role) {
            $exist = Role::where('name', $admin_role)->first();

            if (!$exist) {
                Role::create([
                    'name' => $admin_role,
                ]);
            }
        }

        $role = Role::where('name', 'Super Admin')->first();

        $permissions =  Permission::all()->pluck('name')->toArray();
        info($permissions);

        $role->syncPermissions($permissions);
    }
}
