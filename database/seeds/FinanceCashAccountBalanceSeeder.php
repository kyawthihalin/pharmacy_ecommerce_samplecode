<?php

use Carbon\Carbon;
use App\Models\Order;
use App\FinanceParticular;
use App\Models\FillAmount;
use App\FinanceTransaction;
use Illuminate\Support\Arr;
use App\Models\PurchaseOrder;
use App\Models\ProfitDailyLog;
use Illuminate\Database\Seeder;
use App\Models\FinanceCashAccount;
use Illuminate\Support\Facades\DB;
use App\FinanceCashAccountsBalance;
use App\Constants\FinanceParticularConstant;

class FinanceCashAccountBalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $startDate = Carbon::now()->subYears(4);
        for ($date = $startDate; $date <= Carbon::now(); $date->addDay()) {
            $this->purchaseCashComplete($date);
            $this->purchaseCreditToCashComplete($date);
            $this->fillAmountToCapitalOrFromProfit($date);
            $this->purchaseCreditComplete($date);
            $this->saleCashComplete($date);
            $this->netProfitSeeder($date);
        }
    }

    private function purchaseCashComplete($date)
    {
        $purchaseOrder = PurchaseOrder::first();
        $amount = mt_rand(10000, 99999);
        $supplier_gl = FinanceCashAccount::where('account_code','supplier_gl')->first();
        $supplier_gl_amount_before = $supplier_gl->amount;
        $supplier_gl_amount_after = bcadd($supplier_gl_amount_before, $amount, 4);
        $capital_gl = FinanceCashAccount::where('account_code','capital_gl')->first();
        $capital_gl_amount_before = $capital_gl->amount;
        $capital_gl_amount_after = bcsub($capital_gl_amount_before, $amount, 4);
        $supplier_gl->update([
            'amount' => $supplier_gl_amount_after,
        ]);
        $capital_gl->update([
            'amount' => $capital_gl_amount_after,
        ]);
        $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'])->first();
        FinanceTransaction::create([
            'finance_particular_id' => $finance_particular->id,
            'transactionable_id' => $purchaseOrder->id,
            'transactionable_type' => get_class($purchaseOrder),
            'debit_account_id' => $finance_particular->debit_account_id,
            'credit_account_id' => $finance_particular->credit_account_id,
            'amount' => $amount,
            'debit_account_before_amount' => $supplier_gl_amount_before,
            'debit_account_after_amount' => $supplier_gl_amount_after,
            'credit_account_before_amount' => $capital_gl_amount_before,
            'credit_account_after_amount' => $capital_gl_amount_after,
            'created_at' => $date,
            'updated_at' => $date,
        ]);
    }

    private function purchaseCreditToCashComplete($date)
    {
        $purchaseOrder = PurchaseOrder::first();
        $amount = mt_rand(100000, 999999);

        $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
        $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
        $supplier_debit_gl_amount_after = bcsub($supplier_debit_gl_amount_before, $amount, 4);

        $supplier_debit_gl->update([
            'amount' => $supplier_debit_gl_amount_after,
        ]);

        $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'])->first();

        FinanceTransaction::create([
            'finance_particular_id' => $finance_particular->id,
            'transactionable_id' => $purchaseOrder->id,
            'debit_account_id' => $finance_particular->debit_account_id,
            'credit_account_id' => $finance_particular->credit_account_id,
            'transactionable_type' => get_class($purchaseOrder),
            'amount' => $amount,
            'credit_account_before_amount' => $supplier_debit_gl_amount_before,
            'credit_account_after_amount' => $supplier_debit_gl_amount_after,
            'created_at' => $date,
            'updated_at' => $date,
        ]);


        $supplier_gl = FinanceCashAccount::where('account_code','supplier_gl')->first();
        $supplier_gl_amount_before = $supplier_gl->amount;
        $supplier_gl_amount_after = bcadd($supplier_gl_amount_before, $amount, 4);
        $capital_gl = FinanceCashAccount::where('account_code','capital_gl')->first();
        $capital_gl_amount_before = $capital_gl->amount;
        $capital_gl_amount_after = bcsub($capital_gl_amount_before, $amount, 4);
        $supplier_gl->update([
            'amount' => $supplier_gl_amount_after,
        ]);
        $capital_gl->update([
            'amount' => $capital_gl_amount_after,
        ]);
        $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'])->first();
        FinanceTransaction::create([
            'finance_particular_id' => $finance_particular->id,
            'transactionable_id' => $purchaseOrder->id,
            'transactionable_type' => get_class($purchaseOrder),
            'debit_account_id' => $finance_particular->debit_account_id,
            'credit_account_id' => $finance_particular->credit_account_id,
            'amount' => $amount,
            'debit_account_before_amount' => $supplier_gl_amount_before,
            'debit_account_after_amount' => $supplier_gl_amount_after,
            'credit_account_before_amount' => $capital_gl_amount_before,
            'credit_account_after_amount' => $capital_gl_amount_after,
            'created_at' => $date,
            'updated_at' => $date,
        ]);
    }

    private function fillAmountToCapitalOrFromProfit($date)
    {
        DB::beginTransaction();
            $array = [FinanceParticular::whereName(FinanceParticularConstant::TYPE['FILL_TO_CAPITAL_ACCOUNT'])->pluck("id")->first(),FinanceParticular::whereName(FinanceParticularConstant::TYPE['FILL_AMOUNT_FROM_PROFIT_TO_CAPITAL'])->pluck("id")->first()];
            $finance_particular_id = Arr::random($array);
            $amount = mt_rand(100000, 999999);
            $finance_particular = FinanceParticular::find($finance_particular_id);

            if($finance_particular->credit_account)
            {

                $fill_amount_data =[
                    'from_account_id' => $finance_particular->credit_account_id,
                ];

                $finance_credit_account_before = $finance_particular->credit_account->amount;
                $finance_credit_account_after = bcsub($finance_credit_account_before, $amount, 4);

                $finance_particular->credit_account->update([
                    'amount' => $finance_credit_account_after,
                ]);

                $finance_transaction_data = [
                    'credit_account_id' => $finance_particular->credit_account_id,
                    'credit_account_before_amount' => $finance_credit_account_before,
                    'credit_account_after_amount' =>  $finance_credit_account_after,
                ];
            }

            $finance_debit_account_before = $finance_particular->debit_account->amount;
            $finance_debit_account_after = bcadd($finance_debit_account_before, $amount, 4);

            $finance_particular->debit_account->update([
                'amount' => $finance_debit_account_after,
            ]);

            $fill_amount_data["to_account_id"] = $finance_particular->debit_account_id;
            $fill_amount_data["amount"] = $amount;
            $fill_amount = FillAmount::create($fill_amount_data);


            $finance_transaction_data["finance_particular_id"] = $finance_particular->id;
            $finance_transaction_data["transactionable_id"] = $fill_amount->id;
            $finance_transaction_data["transactionable_type"] =get_class($fill_amount);
            $finance_transaction_data["amount"] = $amount;
            $finance_transaction_data["debit_account_id"] = $finance_particular->debit_account_id;
            $finance_transaction_data["debit_account_before_amount"] = $finance_debit_account_before;
            $finance_transaction_data["debit_account_after_amount"] =  $finance_debit_account_after;
            $finance_transaction_data["created_at"]  = $date;
            $finance_transaction_data["updated_at"]  = $date;
            FinanceTransaction::create($finance_transaction_data);

            DB::commit();
    }

    private function purchaseCreditComplete($date)
    {
        $amount = mt_rand(10000, 99999);
        $purchaseOrder = PurchaseOrder::first();
        $supplier_debit_gl = FinanceCashAccount::where('account_code','supplier_debit_gl')->first();
        $supplier_debit_gl_amount_before = $supplier_debit_gl->amount;
        $supplier_debit_gl_amount_after = bcadd($supplier_debit_gl_amount_before, $amount, 4);

        $supplier_debit_gl->update([
            'amount' => $supplier_debit_gl_amount_after,
        ]);

        $finance_particular =  FinanceParticular::whereName(FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'])->first();

        FinanceTransaction::create([
            'finance_particular_id' => $finance_particular->id,
            'transactionable_id' => $purchaseOrder->id,
            'transactionable_type' => get_class($purchaseOrder),
            'debit_account_id' => $finance_particular->debit_account_id,
            'credit_account_id' => $finance_particular->credit_account_id,
            'amount' => $amount,
            'debit_account_before_amount' => $supplier_debit_gl_amount_before,
            'debit_account_after_amount' => $supplier_debit_gl_amount_after,
            'created_at' => $date,
            'updated_at' => $date,
        ]);
    }

    private function saleCashComplete($date)
    {
        DB::beginTransaction();
        $amount = mt_rand(10000, 99999);
        $order = Order::first();

        // Sale Gl
        $sale_gl = FinanceCashAccount::where('account_code','sale_gl')->first();
        $sale_gl_amount_before = $sale_gl->amount;
        $sale_gl_amount_after = bcadd($sale_gl_amount_before, $amount, 4);

        $sale_gl->update([
            'amount' => $sale_gl_amount_after,
        ]);

        $finance_particular = FinanceParticular::whereName(FinanceParticularConstant::TYPE['SALE_CASH_COMPLETE'])->first();

        FinanceTransaction::create([
            'finance_particular_id' => $finance_particular->id,
            'transactionable_id' => $order->id,
            'transactionable_type' => get_class($order),
            'debit_account_id' => $finance_particular->debit_account_id,
            'credit_account_id' => $finance_particular->credit_account_id,
            'amount' => $amount,
            'debit_account_before_amount' => $sale_gl_amount_before,
            'debit_account_after_amount' => $sale_gl_amount_after,
            'created_at' => $date,
            'updated_at' => $date,
        ]);
        DB::commit();
    }

    private function netProfitSeeder($date)
    {
        $amount = mt_rand(1000, 9999);

        ProfitDailyLog::create([
            'total_amount' => $amount,
            'created_at' => $date,
            'updated_at' => $date,
        ]);
    }
}
