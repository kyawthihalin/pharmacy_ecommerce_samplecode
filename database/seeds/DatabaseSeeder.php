<?php


use App\Models\Unit;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\PermissionGroupSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call([PermissionGroupSeeder::class]);
        // $this->call([PermissionSeeder::class]);
        // $this->call([RoleSeeder::class]);
        // $this->call([AdminSeeder::class]);//
        // $this->call([CitiesTableSeeder::class]);//
        // $this->call([TownshipsTableSeeder::class]);//

        // factory(Brand::class,10)->create();
        // factory(Category::class,10)->create();
        // $this->call([UnitSeeder::class]);//
        // factory(Product::class,100)->create();//
        // $this->call([PasswordGrantClientSeeder::class]);//
        // $this->call([FinanceCashAccountSeeder::class]);//
        $this->call([FinanceParticularSeeder::class]);
        // $this->call([FinanceCashAccountBalanceSeeder::class]);

        // $this->call([UnitRelationshipSeeder::class]);
        // $this->call([SkucodeSeeder::class]);
        // $this->call([AdjustmentTypeSeeder::class]);
        // $this->call([UnitRelationshipSeeder::class]);
        // $this->call([SkucodeSeeder::class]);
        // $this->call([TaxSeeder::class]);

    }
}
