<?php

use Illuminate\Database\Seeder;
use App\Models\Unit;
class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
            'Pcs',
            'Boxes',
            'Pills',
            'Tablet',
            'Cards',
        ];

        foreach ($units as $unit) {
            Unit::create([
                'name' => $unit
            ]);
        }
        Unit::whereNotIn('name', $units)->delete();
    }
}
