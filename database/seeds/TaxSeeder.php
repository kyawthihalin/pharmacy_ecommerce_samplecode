<?php

use App\Models\Tax;
use Illuminate\Database\Seeder;

class TaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taxes = [
            [
                'name' => 'Sample Tax Percentage',
                'rate' => '0',
            ],
        ];

        foreach ($taxes as $tax) {
            $existed = Tax::whereName($tax['name'])->first();
            if (!$existed) {
                Tax::create([
                    'name' => $tax['name'],
                    'rate' => $tax['rate'],
                ]);
            }
        }
    }
}
