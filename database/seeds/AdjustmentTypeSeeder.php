<?php

use App\Constants\Status;
use App\Models\AdjustmentType;
use Illuminate\Database\Seeder;

class AdjustmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adjustment_types = Status::adjustment_type;

        foreach ($adjustment_types as $adjustment_type) {
            $existed = AdjustmentType::where('name', $adjustment_type)->first();

            if (!$existed) {
                AdjustmentType::create([
                    'name' => $adjustment_type,
                ]);
            }
        }
    }
}
