<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\PermissionGroup;

class PermissionGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            'General',
            'Inventory Management',
            'Product Management',
            'Order Management',
            'Report Section',
            'User Management',
            'Shop Management',
        ];

        foreach ($groups as $group) {
            PermissionGroup::create([
                'name' => $group
            ]);
        }
        PermissionGroup::whereNotIn('name', $groups)->delete();
    }
}
