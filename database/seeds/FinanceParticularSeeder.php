<?php

use App\Constants\FinanceParticularConstant;
use App\Constants\GetStatus;
use App\FinanceParticular;
use App\Models\FinanceCashAccount;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class FinanceParticularSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $finance_particulars = [
            [
                'name' => FinanceParticularConstant::TYPE['PURCHASE_ORDER_CASH_COMPLETE'],
                'reference_id' => Str::random(10),
                'credit_account_id' => FinanceCashAccount::where('account_code','capital_gl')->pluck('id')->first(),
                'credit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Subtraction'],
                'debit_account_id' => FinanceCashAccount::where('account_code','supplier_gl')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],

            ],
            [
                'name' => FinanceParticularConstant::TYPE['PURCHASE_ORDER_CREDIT_COMPLETE'],
                'reference_id' => Str::random(10),
                'credit_account_id' => null,
                'credit_account_operation' => null,
                'debit_account_id' => FinanceCashAccount::where('account_code','supplier_debit_gl')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],
            ],
            [
                'name' => FinanceParticularConstant::TYPE['PURCHASE_ORDER_FROM_CREDIT_TO_CASH_COMPLETE'],
                'reference_id' => Str::random(10),
                'credit_account_id' => FinanceCashAccount::where('account_code','supplier_debit_gl')->pluck('id')->first(),
                'credit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Subtraction'],
                'debit_account_id' => null,
                'debit_account_operation' => null,
            ],
            [
                'name' => FinanceParticularConstant::TYPE['FILL_TO_CAPITAL_ACCOUNT'],
                'reference_id' => Str::random(10),
                'credit_account_id' => null,
                'credit_account_operation' => null,
                'debit_account_id' => FinanceCashAccount::where('account_code','capital_gl')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],
            ],
            [
                'name' => FinanceParticularConstant::TYPE['SALE_CASH_COMPLETE'],
                'reference_id' => Str::random(10),
                'credit_account_id' => null,
                'credit_account_operation' => null,
                'debit_account_id' => FinanceCashAccount::where('account_code','sale_gl')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],
            ],
            [
                'name' => FinanceParticularConstant::TYPE['FILL_AMOUNT_FROM_PROFIT_TO_CAPITAL'],
                'reference_id' => Str::random(10),
                'credit_account_id' => FinanceCashAccount::where('account_code','profit_gl')->pluck('id')->first(),
                'credit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Subtraction'],
                'debit_account_id' => FinanceCashAccount::where('account_code','capital_gl')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],
            ],
            [
                'name' => FinanceParticularConstant::TYPE['GET_PROFIT_FROM_ORDER_CASH_COMPLETE'],
                'reference_id' => Str::random(10),
                'credit_account_id' => null,
                'credit_account_operation' => null,
                'debit_account_id' => FinanceCashAccount::where('account_code','profit_gl')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],
            ],
            [
                'name' => FinanceParticularConstant::TYPE['PRODUCT_DAMAGE'],
                'reference_id' => Str::random(10),
                'credit_account_id' => null,
                'credit_account_operation' => null,
                'debit_account_id' => FinanceCashAccount::where('account_code','product_damage')->pluck('id')->first(),
                'debit_account_operation' => GetStatus::FINANCE_ACCOUNT_OPERATION['Addition'],
            ],
        ];

        foreach ($finance_particulars as $finance_particular) {
            $existed = FinanceParticular::whereName($finance_particular['name'])->first();
            if (!$existed) {
                FinanceParticular::create([
                    'name' => $finance_particular['name'],
                    'reference_id' => $finance_particular['reference_id'],
                    'credit_account_id' =>$finance_particular['credit_account_id'],
                    'credit_account_operation' => $finance_particular['credit_account_operation'],
                    'debit_account_id' =>$finance_particular['debit_account_id'],
                    'debit_account_operation' => $finance_particular['debit_account_operation'],
                ]);
            }
        }
    }
}
