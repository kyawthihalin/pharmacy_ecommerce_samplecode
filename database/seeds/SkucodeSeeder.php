<?php

use App\Models\SkuCode;
use Illuminate\Database\Seeder;

class SkucodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sku_codes = ['001-skucode', '002-skucode', '003-skucode'];

        foreach ($sku_codes as $sku_code) {
            $existed = SkuCode::where('code', $sku_code)->first();

            if (!$existed) {
                SkuCode::create([
                    'code' => $sku_code,
                ]);
            }
        }
    }
}
