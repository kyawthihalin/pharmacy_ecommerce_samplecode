<?php

use App\Constants\GetStatus;
use Illuminate\Database\Seeder;
use App\Models\FinanceCashAccount;

class FinanceCashAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $finance_cash_accounts = [
            [
                'name' => 'Capital',
                'status' => GetStatus::FINANCE_CASH_ACCOUNT_STATUS["Active"],
                'account_code' => 'capital_gl',
                'amount' => 10000000000,
            ],
            [
                'name' => 'Sale Gl',
                'status' => GetStatus::FINANCE_CASH_ACCOUNT_STATUS["Active"],
                'account_code' => 'sale_gl',
                'amount' => 10000000000,
            ],
            [
                'name' => 'Profit Gl',
                'status' => GetStatus::FINANCE_CASH_ACCOUNT_STATUS["Active"],
                'account_code' => 'profit_gl',
                'amount' => 10000000000,
            ],
            [
                'name' => 'Supplier Credit Gl',
                'status' => GetStatus::FINANCE_CASH_ACCOUNT_STATUS["Active"],
                'account_code' => 'supplier_debit_gl',
                'amount' => 10000000000,
            ],
            [
                'name' => 'Supplier Gl',
                'status' => GetStatus::FINANCE_CASH_ACCOUNT_STATUS["Active"],
                'account_code' => 'supplier_gl',
                'amount' => 10000000000,
            ],
            [
                'name' => 'Product damage',
                'status' => GetStatus::FINANCE_CASH_ACCOUNT_STATUS["Active"],
                'account_code' => 'product_damage',
                'amount' => 10000000000,
            ],
        ];

        foreach ($finance_cash_accounts as $finance_cash_account) {
            $existed = FinanceCashAccount::whereName($finance_cash_account['name'])->first();
            if (!$existed) {
                FinanceCashAccount::create([
                    'name' => $finance_cash_account['name'],
                    'status' => $finance_cash_account['status'],
                    'account_code' =>$finance_cash_account['account_code'],
                    'amount' =>$finance_cash_account['amount'],
                ]);
            }
        }
    }
}
