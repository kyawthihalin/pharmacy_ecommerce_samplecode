<?php

use Illuminate\Database\Seeder;
use App\Models\City;
use Illuminate\Support\Str;
class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::truncate();
        
        $objs = array(
            ['id'=>1,'name'=>'Kachin','uuid'=>Str::uuid()],
            ['id'=>2,'name'=>'Kayah','uuid'=>Str::uuid()],
            ['id'=>3,'name'=>'Kayin','uuid'=>Str::uuid()],
            ['id'=>4,'name'=>'Chin','uuid'=>Str::uuid()],
            ['id'=>5,'name'=>'Sagaing','uuid'=>Str::uuid()],
            ['id'=>6,'name'=>'Tanintharyi','uuid'=>Str::uuid()],
            ['id'=>7,'name'=>'Bago','uuid'=>Str::uuid()],
            ['id'=>8,'name'=>'Magway','uuid'=>Str::uuid()],
            ['id'=>9,'name'=>'Mandalay','uuid'=>Str::uuid()],
            ['id'=>10,'name'=>'Mon','uuid'=>Str::uuid()],
            ['id'=>11,'name'=>'Rakhine','uuid'=>Str::uuid()],
            ['id'=>12,'name'=>'Yangon','uuid'=>Str::uuid()],
            ['id'=>13,'name'=>'Shan','uuid'=>Str::uuid()],
            ['id'=>14,'name'=>'Ayeyarwady','uuid'=>Str::uuid()],
            ['id'=>15,'name'=>'Naypyidaw','uuid'=>Str::uuid()],
        );
        
        foreach ($objs as $value) {
            City::create($value);
        }

    }
}
