<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('user', 'UserCrudController');
    Route::crud('city', 'CityCrudController');
    Route::crud('township', 'TownshipCrudController');
    Route::crud('shop', 'ShopCrudController');
    Route::crud('shop-not-approve', 'ShopNotApproveCrudController');
    Route::post('shop-not-approve/approve', 'ShopNotApproveCrudController@approve')->name("shop.approve");
    Route::get('dashboard', 'DashboardController@index');
    Route::get('charts/weekly-shops', 'Charts\WeeklyShopsChartController@response')->name('charts.weekly-shops.index');
    Route::get('charts/weekly-city', 'Charts\WeeklyCityChartController@response')->name('charts.weekly-city.index');
    Route::get('charts/profit', 'Charts\ProfitChartController@response')->name('charts.profit.index');
    Route::get('charts/pie', 'Charts\PieChartController@response')->name('charts.pie.index');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('brand', 'BrandCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('group-product', 'GroupProductCrudController');
    Route::crud('group-product-balance', 'GroupProductBalanceCrudController');
    Route::post('group-product-balance/group-product-qty', 'GroupProductBalanceCrudController@updateGroupProductQty')->name('group.product.qty');
    Route::post('group-product-balance/product-qty', 'GroupProductBalanceCrudController@updateProductQty')->name('product.qty');
    Route::post('product/change/status','ProductCrudController@statusChange')->name("status");
    Route::post('shop/change/status','ShopCrudController@statusChange')->name("shopstatus");
    Route::post('shop/freeze-reason','ShopCrudController@freezeReason')->name("freezeReason");
    Route::get('shop/get-city', 'ShopCrudController@getCity');
    Route::get('shop/get-township', 'ShopCrudController@getCity');
    Route::crud('product-category', 'ProductCategoryCrudController');
    Route::crud('unit', 'UnitCrudController');
    Route::crud('unit-relationship', 'UnitRelationshipCrudController');
    Route::crud('shipping-address', 'ShippingAddressCrudController');
    Route::crud('payment-type', 'PaymentTypeCrudController');
    Route::crud('coupon', 'CouponCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('order-detail', 'OrderDetailCrudController');
    Route::post('update/order-detail','OrderDetailCrudController@updateOrder')->name('update.order');
    Route::post('order/remove/product','OrderDetailCrudController@removeProduct')->name('order.remove');
    Route::get('get/products','ProductCrudController@getProduct')->name('get.products');
    Route::post('add/products','ProductCrudController@addProduct')->name('add.products');
    Route::get('product/import-view', 'ProductCrudController@importView')->name('product.import.view');
    Route::post('product/import-view/store', 'ProductCrudController@importStore')->name('product.import.store');
    Route::get('stock-opening/import-view', 'StockOpeningCrudController@importView')->name('stock.import.view');
    Route::post('stock-opening/import-view/store', 'StockOpeningCrudController@importStore')->name('stock.import.store');
    Route::post('change/status/payment','OrderCrudController@changePaymentStatus')->name('change.payment.status');
    Route::post('change/status/payment/cod','OrderCrudController@changePaymentCodStatus')->name('change.payment.status.cod');
    Route::post('change/status/shipping','OrderCrudController@changeShippingStatus')->name('change.shipping.status');
    Route::post('change/status/shipping/cod','OrderCrudController@changeShippingCodStatus')->name('change.shipping.status.cod');


    Route::crud('notification', 'NotificationCrudController');
    Route::crud('banner', 'BannerCrudController');
    Route::crud('enquiry', 'EnquiryCrudController');
    Route::crud('return-medicine', 'ReturnMedicineCrudController');
    Route::crud('product-report', 'ProductReportCrudController');
    Route::crud('order-report', 'OrderReportCrudController');
    Route::crud('stock-report', 'StockReportCrudController');
    Route::crud('stock-daily-report', 'StockDailyReportCrudController');
    Route::crud('product-balance-report', 'ProductBalanceReportCrudController');
    Route::crud('product-balance-by-invoice-report', 'ProductBalanceByInvoiceReportCrudController');
    Route::crud('stockout', 'StockoutCrudController');
    Route::get('charts/orders-by-payment', 'Charts\OrdersByPaymentChartController@response')->name('charts.orders-by-payment.index');
    Route::get('charts/orders-by-shipping', 'Charts\OrdersByShippingChartController@response')->name('charts.orders-by-shipping.index');
    Route::get('charts/income', 'Charts\IncomeChartController@response')->name('charts.income.index');
    Route::crud('order-print', 'OrderPrintCrudController');
    Route::get('get-product','StockoutLogCrudController@getProduct');
    Route::get('get-product-name','StockoutLogCrudController@getProductName');
    Route::crud('sale-report', 'SaleReportCrudController');
    Route::crud('profit-report', 'ProfitReportCrudController');
    Route::crud('profit-comparison-report', 'ProfitComparisonReportCrudController');

    //reports
    Route::post('product/report','ProductReportCrudController@report')->name('product_report');
    Route::post('purchase-order/report','PurchaseOrderReportCrudController@report')->name('purchase_order_report');
    Route::post('order/report','OrderReportCrudController@report')->name('order_report');
    Route::post('order/invoice','OrderPrintCrudController@report')->name('invoice');
    Route::post('stock/report','StockReportCrudController@report')->name('stock_report');
    Route::post('sale-report/report', 'SaleReportCrudController@report')->name('sale_order_report');
    Route::post('stock-daily/report','StockDailyReportCrudController@report')->name('stock_daily_report');
    Route::post('product-balance/report','ProductBalanceReportCrudController@report')->name('product_balance_report');
    Route::post('product-balance-by-invoice/report','ProductBalanceByInvoiceReportCrudController@report')->name('product_balance_by_invoice_report');
    Route::post('profit/report','ProfitReportCrudController@report')->name('profit_report');
    Route::post('profit-comparison/report','ProfitComparisonReportCrudController@report')->name('profit_comparison_report');

    //ajax routes
    Route::get('test/ajax-township-options', 'OrderCrudController@townshipOptions');
    Route::get('test/ajax-city-options', 'OrderCrudController@cityOptions');

    //ajax data routes
    Route::get('/api/city', 'DataController@getallCities');
    Route::get('/api/township', 'DataController@getallTwonships');
    Route::get('/api/shop', 'DataController@getallShops');
    Route::get('/api/payment_type', 'DataController@getallPaymentTypes');
    Route::get('/api/product', 'DataController@getallproducts');
    Route::get('/api/township-by-city', 'DataController@townshipByCity');
    Route::get('/api/unit-by-sku-code', 'DataController@unitBySkuCode');
    Route::get('/api/conversion-unit', 'DataController@conversionUnit');
    Route::get('/api/balance-from-unit', 'DataController@balanceFromUnit');
    Route::get('/api/balance-to-unit', 'DataController@balanceToUnit');
    Route::get('/api/product-by-invoice', 'DataController@productByInvoice');
    Route::get('/api/product-by-sku-code', 'DataController@productBySkuCode');
    Route::get('/api/sku-code-by-invoice', 'DataController@skuCodeByInvoice');
    Route::get('/api/finance-particular', 'DataController@financeParticular');

    Route::crud('stockin-log', 'StockinLogCrudController');
    Route::crud('stockout-log', 'StockoutLogCrudController');
    Route::get('charts/survey', 'Charts\SurveyChartController@response')->name('charts.survey.index');
    Route::crud('app-notification', 'AppNotificationCrudController');
    Route::crud('survey-notification', 'SurveyNotificationCrudController');
    Route::crud('completed-order', 'CompletedOrderCrudController');
    Route::crud('payment-pending-order', 'PaymentPendingOrderCrudController');
    Route::crud('rejected-order', 'RejectedOrderCrudController');
    Route::crud('shipping-pending-order', 'ShippingPendingOrderCrudController');
    Route::crud('cash-on-delivery-order', 'CashOnDeliveryOrderCrudController');
    Route::crud('default-photo', 'DefaultPhotoCrudController');
    // Route::crud('gl-account', 'GlAccountCrudController');
    Route::crud('supplier', 'SupplierCrudController');

    Route::crud('purchase-order', 'PurchaseOrderCrudController');
    Route::post('change/status/purchase/order','PurchaseOrderCrudController@changePurchaseOrderStatus')->name('change.purchase.order.status');
    Route::post('change/status/purchase/payment','PurchaseOrderCrudController@changePurchasePaymentStatus')->name('change.purchase.payment.status');
    Route::post('change-credit-to-complete-status','PurchaseOrderCrudController@changeCreditToCompleteStatus')->name('change.credit.to.complete-status');
    Route::post('change-credit-for-purchase','PurchaseOrderCrudController@changeCreditForPurchase')->name('change.credit.for.purchase');
    Route::post('change-complete-for-purchase','PurchaseOrderCrudController@changeCompleteForPurchase')->name('change.complete.for.purchase');
    Route::get('purchase-order/import-view','PurchaseOrderCrudController@importView')->name('purchase.order.import.view');
    Route::post('purchase-order/import-view/store','PurchaseOrderCrudController@importStore')->name('purchase.order.import.store');



    Route::crud('inventory', 'InventoryCrudController');
    Route::crud('stock-adjustment', 'StockAdjustmentCrudController');
    Route::crud('adjustment-type', 'AdjustmentTypeCrudController');
    Route::crud('product-balance', 'ProductBalanceCrudController');
    Route::post('product-balance/data','ProductBalanceCrudController@data')->name('product_balance_data');
    Route::post('product-balance/change/selling_status', 'ProductBalanceCrudController@changeSellingStatus')->name('selling_status');
    Route::crud('unit-conversion-log', 'UnitConversionLogCrudController');
    Route::crud('purchase-order-report', 'PurchaseOrderReportCrudController');



    Route::crud('finance-cash-account', 'FinanceCashAccountCrudController');
    Route::crud('finance-particular', 'FinanceParticularCrudController');
    Route::crud('fill-amount', 'FillAmountCrudController');
    Route::crud('pending-purchase-order', 'PendingPurchaseOrderCrudController');
    Route::crud('complete-purchase-order', 'CompletePurchaseOrderCrudController');
    Route::crud('credit-complete-purchase-order', 'CreditCompletePurchaseOrderCrudController');
    Route::crud('rejected-purchase-order', 'RejectedPurchaseOrderCrudController');
    Route::get('finance-cash-report', 'FinanceCashReportController@index');

    // Route::post('add/products','ProductCrudController@addProduct')->name('add.products');
    Route::post('select/transaction-types','FinanceCashReportController@selectTransactionType')->name('select.transaction.type');
    Route::post('finance-cash/report','FinanceCashReportController@financeCashReport')->name('finance.cash.report');

    Route::crud('tax', 'TaxCrudController');
}); // this should be the absolute last line of this file
