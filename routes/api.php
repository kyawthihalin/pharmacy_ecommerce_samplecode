<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register', 'Api\RegisterController@register');
Route::post('login', 'Api\LoginController@login');
Route::post('get/otp','Api\RegisterController@getOtp');
Route::post('verify/otp', 'Api\RegisterController@verifyOpt')->middleware('token:mb_register_verify_otp');
